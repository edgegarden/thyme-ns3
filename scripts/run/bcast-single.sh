#!/bin/sh
app="broadcast-run"
runs=$1
nodes=$2
simTime=720
stopTime=719
speed=$3
pause=$4
trace="euro2016"
intChurn=$5
crashChurn=$6
rp=$7 #0-AODV, 1-OLSR, 2-DSDV, 3-DSR, 4-BATMAN
tagsPerPub=1

if [ $# -ne 7 ]
then
  echo "Usage: $0 <runs> <nodes> <speed> <pause> <intChurn> <crashChurn> <rp>"
  exit
fi

for run in `seq $runs`
do
  tracefile="../scripts/traces/files/n${nodes}-${trace}-t${tagsPerPub}-p${pause}-s${speed}-c${crashChurn}-i${intChurn}.txt"
  echo "##############################"
  echo "PL;SG run ${run} of ${runs}"
  echo "Nodes: ${nodes}, Simulation time: ${simTime}"
  echo "Max speed: ${speed}, Pause: ${pause}"
  echo "Int churn: ${intChurn}"
  echo "Crash churn: ${crashChurn}"
  echo "Routing: ${rp} (0-AODV, 1-OLSR, 2-DSDV, 3-DSR, 4-BATMAN)"
  echo "Trace file: ${tracefile}"
  echo "Time: `date +'%F %H:%M:%S'`"
  echo "##############################"
  start=`date +%s`
  out="../logs/${app}-n${nodes}-${trace}-t${tagsPerPub}-p${pause}-s${speed}-c${crashChurn}-i${intChurn}-rp${rp}-r$run.log"
  
  ./waf --run "${app} --RngRun=${run} --numNodes=${nodes} \
    --simTime=${simTime} --stopTime=${stopTime} --file=${tracefile} \
    --rp=${rp}" > $out 2>&1
  
  end=`date +%s`
  res=`echo "scale=2;($end-$start)/60" | bc`
  res2=`echo "scale=2;$res/60" | bc`
  echo ">>> Time spent: ${res}min (${res2}h)"
done

