#!/bin/sh
app="thyme-run"
runs=$1
nodes=$2
simTime=720
stopTime=719
speed=$3
pause=$4
trace="euro2016"
intChurn=$5
crashChurn=$6
maxX=$7
maxY=$8
tagsPerPub=1

if [ $# -ne 6 ]
then
  echo "Usage: $0 <runs> <nodes> <speed> <pause> <intChurn> <crashChurn> <maxX> <maxY>"
  exit
fi

for run in `seq $runs`
do
  tracefile="../scripts/traces/files/n${nodes}-${trace}-t${tagsPerPub}-p${pause}-s${speed}-c${crashChurn}-i${intChurn}-v${}.txt"
  echo "##############################"
  echo "DCS run ${run} of ${runs}"
  echo "Nodes: ${nodes}, Simulation time: ${simTime} s"
  echo "Max speed: ${speed} m/s, Pause: ${pause} s"
  echo "Int churn: ${intChurn}%"
  echo "Crash churn: ${crashChurn}%"
  echo "Area: ${maxX} m x ${maxY} m"
  echo "Trace file: ${tracefile}"
  echo "Time: `date +'%F %H:%M:%S'`"
  echo "##############################"
  start=`date +%s`
  out="../logs/${app}-n${nodes}-${trace}-t${tagsPerPub}-p${pause}-s${speed}-c${crashChurn}-i${intChurn}-r$run.log"
  
  ./waf --run "${app} --RngRun=${run} --numNodes=${nodes} \
    --simTime=${simTime} --stopTime=${stopTime} --file=${tracefile} \
    --maxX=${maxX} --maxY=${maxY}" > $out 2>&1
  
  end=`date +%s`
  res=`echo "scale=2;(${end}-${start})/60" | bc`
  res2=`echo "scale=2;${res}/60" | bc`
  echo ">>> Time spent: ${res}min (${res2}h)"
done

