#!/bin/sh
runs=10
nodes=$1 # 16 36 64 100 144 196 256 324 400
sizeX=$2
sizeY=$3
maxSpeed=0
minPause=720
maxPause=720
moveProb=0
churnDecision=100
minChurnFreqUp=120
maxChurnFreqUp=120
minChurnFreqDown=60
maxChurnFreqDown=60
churnDownProb=$4
nodeChurnProb=100
minChurnInt=0
maxChurnInt=0
churnOutProb=0

start=`date +%s`

../scripts/run/thyme-single.sh $runs $nodes $maxSpeed $minPause $maxPause \
  $moveProb $sizeX $sizeY $minChurnFreqUp $maxChurnFreqUp \
  $minChurnFreqDown $maxChurnFreqDown $churnDownProb $nodeChurnProb \
  $churnDecision $minChurnInt $maxChurnInt $churnOutProb

end=`date +%s`
res=`echo "scale=2;($end-$start)/60" | bc`
res2=`echo "scale=2;$res/60" | bc`
echo ">>> Total time spent: ${res}min (${res2}h)"

