#!/usr/bin/python
import sys
import math
import numpy as np
import matplotlib.pyplot as plt

SEP = '#$%&-----#$%&'
IDX = 0
Z_VAL = {80: 1.28, 90: 1.645, 95: 1.96, 98: 2.33, 99: 2.58}


def ns_to_ms(val):
  return val / 1000000.0


def b_to_kb(val):
  return val / 1000.0


def get_conf_int(vals, conf_int=95):
  # sample size should be large enough (>= 30 samples)
  return Z_VAL[conf_int] * (np.std(vals) / math.sqrt(len(vals)))


def get_next_line(content):
  global IDX
  line = content[IDX].split(' ')
  line = [x for x in line if x]  # removes blank strings
  IDX += 1
  return line


def get_next_int_line(content):
  global IDX
  line = content[IDX].split(' ')
  # removes blank strings and converts strings to ints
  line = [int(x) for x in line if x]
  IDX += 1
  return line


def process_content(content):
  global IDX
  IDX = 0
  
  # ---------------------------------------------------- PHY
  # {PHY-Rx-bytes	PHY-Rx-pckts} * {...}
  phy = get_next_int_line(content)
  phy_rx_bytes = []
  phy_rx_pckts = []
  index = 0
  while index < len(phy):
    phy_rx_bytes.append(phy[index])
    phy_rx_pckts.append(phy[index + 1])
    index += 2
  phy_rx = (phy_rx_bytes, phy_rx_pckts)
  
  # {PHY-Rx-drop-bytes	PHY-Rx-drop-pckts} * {...}
  phy = get_next_int_line(content)
  phy_rx_drop_bytes = []
  phy_rx_drop_pckts = []
  index = 0
  while index < len(phy):
    phy_rx_drop_bytes.append(phy[index])
    phy_rx_drop_pckts.append(phy[index + 1])
    index += 2
  phy_rx_drop = (phy_rx_drop_bytes, phy_rx_drop_pckts)
  
  # {PHY-Tx-bytes	PHY-Tx-pckts} * {...}
  phy = get_next_int_line(content)
  phy_tx_bytes = []
  phy_tx_pckts = []
  index = 0
  while index < len(phy):
    phy_tx_bytes.append(phy[index])
    phy_tx_pckts.append(phy[index + 1])
    index += 2
  phy_tx = (phy_tx_bytes, phy_tx_pckts)
  
  # {PHY-Tx-drop-bytes	PHY-Tx-drop-pckts} * {...}
  phy = get_next_int_line(content)
  phy_tx_drop_bytes = []
  phy_tx_drop_pckts = []
  index = 0
  while index < len(phy):
    phy_tx_drop_bytes.append(phy[index])
    phy_tx_drop_pckts.append(phy[index + 1])
    index += 2
  phy_tx_drop = (phy_tx_drop_bytes, phy_tx_drop_pckts)
  
  _phy = (phy_rx, phy_rx_drop, phy_tx, phy_tx_drop)
  
  # ---------------------------------------------------- MAC
  assert (IDX == 4), "%d != 4" % IDX
  
  # {MAC-Rx-bytes	MAC-Rx-pckts} * {...}
  mac = get_next_int_line(content)
  mac_rx_bytes = []
  mac_rx_pckts = []
  index = 0
  while index < len(mac):
    mac_rx_bytes.append(mac[index])
    mac_rx_pckts.append(mac[index + 1])
    index += 2
  mac_rx = (mac_rx_bytes, mac_rx_pckts)
  
  # {MAC-Rx-drop-bytes	MAC-Rx-drop-pckts} * {...}
  mac = get_next_int_line(content)
  mac_rx_drop_bytes = []
  mac_rx_drop_pckts = []
  index = 0
  while index < len(mac):
    mac_rx_drop_bytes.append(mac[index])
    mac_rx_drop_pckts.append(mac[index + 1])
    index += 2
  mac_rx_drop = (mac_rx_drop_bytes, mac_rx_drop_pckts)
  
  # {MAC-Tx-bytes	MAC-Tx-pckts} * {...}
  mac = get_next_int_line(content)
  mac_tx_bytes = []
  mac_tx_pckts = []
  index = 0
  while index < len(mac):
    mac_tx_bytes.append(mac[index])
    mac_tx_pckts.append(mac[index + 1])
    index += 2
  mac_tx = (mac_tx_bytes, mac_tx_pckts)
  
  # {MAC-Tx-drop-bytes	MAC-Tx-drop-pckts} * {...}
  mac = get_next_int_line(content)
  mac_tx_drop_bytes = []
  mac_tx_drop_pckts = []
  index = 0
  while index < len(mac):
    mac_tx_drop_bytes.append(mac[index])
    mac_tx_drop_pckts.append(mac[index + 1])
    index += 2
  mac_tx_drop = (mac_tx_drop_bytes, mac_tx_drop_pckts)
  
  # {MAC-Tx-failed-pckts	MAC-Tx-maxfailed-pckts} * {...}
  mac = get_next_int_line(content)
  mac_tx_failed_pckts = []
  mac_tx_maxfailed_pckts = []
  index = 0
  while index < len(mac):
    mac_tx_failed_pckts.append(mac[index])
    mac_tx_maxfailed_pckts.append(mac[index + 1])
    index += 2
  mac_tx_failed = (mac_tx_failed_pckts, mac_tx_maxfailed_pckts)
  
  _mac = (mac_rx, mac_rx_drop, mac_tx, mac_tx_drop, mac_tx_failed)
  
  # ---------------------------------------------------- IP
  assert (IDX == 9), "%d != 9" % IDX
  
  # {IP-Rx-bytes	IP-Rx-pckts} * {...}
  ip = get_next_int_line(content)
  ip_rx_bytes = []
  ip_rx_pckts = []
  index = 0
  while index < len(ip):
    ip_rx_bytes.append(ip[index])
    ip_rx_pckts.append(ip[index + 1])
    index += 2
  ip_rx = (ip_rx_bytes, ip_rx_pckts)
  
  # {IP-Tx-bytes	IP-Tx-pckts} * {...}
  ip = get_next_int_line(content)
  ip_tx_bytes = []
  ip_tx_pckts = []
  index = 0
  while index < len(ip):
    ip_tx_bytes.append(ip[index])
    ip_tx_pckts.append(ip[index + 1])
    index += 2
  ip_tx = (ip_tx_bytes, ip_tx_pckts)
  
  # {IP-Fwd-bytes IP-Fwd-pckts} * {...}
  ip = get_next_int_line(content)
  ip_fwd_bytes = []
  ip_fwd_pckts = []
  index = 0
  while index < len(ip):
    ip_fwd_bytes.append(ip[index])
    ip_fwd_pckts.append(ip[index + 1])
    index += 2
  ip_fwd = (ip_fwd_bytes, ip_fwd_pckts)
  
  # IP-Drop-bytes  IP-Drop-pckts  (TTL EXPIRED)
  ip = get_next_int_line(content)
  ip_drop_1_bytes = []
  ip_drop_1_pckts = []
  index = 0
  while index < len(ip):
    ip_drop_1_bytes.append(ip[index])
    ip_drop_1_pckts.append(ip[index + 1])
    index += 2
  ip_drop_1 = (ip_drop_1_bytes, ip_drop_1_pckts)
  
  # IP-Drop-bytes  IP-Drop-pckts  (NO ROUTE)
  ip = get_next_int_line(content)
  ip_drop_2_bytes = []
  ip_drop_2_pckts = []
  index = 0
  while index < len(ip):
    ip_drop_2_bytes.append(ip[index])
    ip_drop_2_pckts.append(ip[index + 1])
    index += 2
  ip_drop_2 = (ip_drop_2_bytes, ip_drop_2_pckts)
  
  # IP-Drop-bytes  IP-Drop-pckts  (BAD CHECKSUM)
  ip = get_next_int_line(content)
  ip_drop_3_bytes = []
  ip_drop_3_pckts = []
  index = 0
  while index < len(ip):
    ip_drop_3_bytes.append(ip[index])
    ip_drop_3_pckts.append(ip[index + 1])
    index += 2
  ip_drop_3 = (ip_drop_3_bytes, ip_drop_3_pckts)
  
  # IP-Drop-bytes  IP-Drop-pckts  (INTERFACE DOWN)
  ip = get_next_int_line(content)
  ip_drop_4_bytes = []
  ip_drop_4_pckts = []
  index = 0
  while index < len(ip):
    ip_drop_4_bytes.append(ip[index])
    ip_drop_4_pckts.append(ip[index + 1])
    index += 2
  ip_drop_4 = (ip_drop_4_bytes, ip_drop_4_pckts)
  
  # IP-Drop-bytes  IP-Drop-pckts  (ROUTE ERROR)
  ip = get_next_int_line(content)
  ip_drop_5_bytes = []
  ip_drop_5_pckts = []
  index = 0
  while index < len(ip):
    ip_drop_5_bytes.append(ip[index])
    ip_drop_5_pckts.append(ip[index + 1])
    index += 2
  ip_drop_5 = (ip_drop_5_bytes, ip_drop_5_pckts)
  
  # IP-Drop-bytes  IP-Drop-pckts  (FRAGMENT TIMEOUT)
  ip = get_next_int_line(content)
  ip_drop_6_bytes = []
  ip_drop_6_pckts = []
  index = 0
  while index < len(ip):
    ip_drop_6_bytes.append(ip[index])
    ip_drop_6_pckts.append(ip[index + 1])
    index += 2
  ip_drop_6 = (ip_drop_6_bytes, ip_drop_6_pckts)
  
  # {numHops pcktSize} * {...} // packets histogram (DOWN)
  ip = get_next_int_line(content)
  histogram_down = []
  index = 0
  while index < len(ip):
    histogram_down.append((ip[index], ip[index + 1]))
    index += 2
  
  # {numHops pcktSize} * {...} // packets histogram (DOWN_RSP)
  ip = get_next_int_line(content)
  histogram_down_rsp = []
  index = 0
  while index < len(ip):
    histogram_down_rsp.append((ip[index], ip[index + 1]))
    index += 2
  
  _ip = (ip_rx, ip_tx, ip_fwd, ip_drop_1, ip_drop_2, ip_drop_3, ip_drop_4,
         ip_drop_5, ip_drop_6, histogram_down, histogram_down_rsp)
  
  # {cellId} * {...}
  _cells = get_next_int_line(content)
  
  # ---------------------------------------------------- CTL
  assert (IDX == 21), "%d != 21" % IDX
  
  # {CTL-Rx-bytes	CTL-Rx-pckts} * {...}
  ctrl = get_next_int_line(content)
  ctrl_rx_bytes = []
  ctrl_rx_pckts = []
  index = 0
  while index < len(ctrl):
    ctrl_rx_bytes.append(ctrl[index])
    ctrl_rx_pckts.append(ctrl[index + 1])
    index += 2
  ctrl_rx = (ctrl_rx_bytes, ctrl_rx_pckts)
  
  # {CTL-Tx-bytes	CTL-Tx-pckts} * {...}
  ctrl = get_next_int_line(content)
  ctrl_tx_bytes = []
  ctrl_tx_pckts = []
  index = 0
  while index < len(ctrl):
    ctrl_tx_bytes.append(ctrl[index])
    ctrl_tx_pckts.append(ctrl[index + 1])
    index += 2
  ctrl_tx = (ctrl_tx_bytes, ctrl_tx_pckts)
  
  # {lastGreedy firstPerHop reps} * {...}
  ctrl = get_next_int_line(content)
  reps = {}
  index = 0
  while index < len(ctrl):
    reps[(ctrl[index], ctrl[index + 1])] = ctrl[index + 2]
    index += 3
  
  _ctrl = (ctrl_rx, ctrl_tx, reps)
  
  # ---------------------------------------------------- APP Rx
  assert (IDX == 24), "%d != 24" % IDX
  
  # {APP-Rx-bytes	APP-Rx-pckts} * {...} (PUBLISH)
  app = get_next_int_line(content)
  app_rx_1_bytes = []
  app_rx_1_pckts = []
  index = 0
  while index < len(app):
    app_rx_1_bytes.append(app[index])
    app_rx_1_pckts.append(app[index + 1])
    index += 2
  app_rx_1 = (app_rx_1_bytes, app_rx_1_pckts)
  
  # {APP-Rx-bytes	APP-Rx-pckts} * {...} (UNPUBLISH)
  app = get_next_int_line(content)
  app_rx_2_bytes = []
  app_rx_2_pckts = []
  index = 0
  while index < len(app):
    app_rx_2_bytes.append(app[index])
    app_rx_2_pckts.append(app[index + 1])
    index += 2
  app_rx_2 = (app_rx_2_bytes, app_rx_2_pckts)
  
  # {APP-Rx-bytes	APP-Rx-pckts} * {...} (DOWNLOAD)
  app = get_next_int_line(content)
  app_rx_3_bytes = []
  app_rx_3_pckts = []
  index = 0
  while index < len(app):
    app_rx_3_bytes.append(app[index])
    app_rx_3_pckts.append(app[index + 1])
    index += 2
  app_rx_3 = (app_rx_3_bytes, app_rx_3_pckts)
  
  # {APP-Rx-bytes	APP-Rx-pckts} * {...} (SUBSCRIBE)
  app = get_next_int_line(content)
  app_rx_4_bytes = []
  app_rx_4_pckts = []
  index = 0
  while index < len(app):
    app_rx_4_bytes.append(app[index])
    app_rx_4_pckts.append(app[index + 1])
    index += 2
  app_rx_4 = (app_rx_4_bytes, app_rx_4_pckts)
  
  # {APP-Rx-bytes	APP-Rx-pckts} * {...} (UNSUBSCRIBE)
  app = get_next_int_line(content)
  app_rx_5_bytes = []
  app_rx_5_pckts = []
  index = 0
  while index < len(app):
    app_rx_5_bytes.append(app[index])
    app_rx_5_pckts.append(app[index + 1])
    index += 2
  app_rx_5 = (app_rx_5_bytes, app_rx_5_pckts)
  
  # {APP-Rx-bytes	APP-Rx-pckts} * {...} (NOTIFICATION)
  app = get_next_int_line(content)
  app_rx_6_bytes = []
  app_rx_6_pckts = []
  index = 0
  while index < len(app):
    app_rx_6_bytes.append(app[index])
    app_rx_6_pckts.append(app[index + 1])
    index += 2
  app_rx_6 = (app_rx_6_bytes, app_rx_6_pckts)
  
  # {APP-Rx-bytes	APP-Rx-pckts} * {...} (CONTROL)
  app = get_next_int_line(content)
  app_rx_7_bytes = []
  app_rx_7_pckts = []
  index = 0
  while index < len(app):
    app_rx_7_bytes.append(app[index])
    app_rx_7_pckts.append(app[index + 1])
    index += 2
  app_rx_7 = (app_rx_7_bytes, app_rx_7_pckts)
  
  # {APP-Rx-bytes	APP-Rx-pckts} * {...} (ACTIVE REPLICATION)
  app = get_next_int_line(content)
  app_rx_8_bytes = []
  app_rx_8_pckts = []
  index = 0
  while index < len(app):
    app_rx_8_bytes.append(app[index])
    app_rx_8_pckts.append(app[index + 1])
    index += 2
  app_rx_8 = (app_rx_8_bytes, app_rx_8_pckts)
  
  # {APP-Rx-bytes	APP-Rx-pckts} * {...} (PASSIVE REPLICATION)
  app = get_next_int_line(content)
  app_rx_9_bytes = []
  app_rx_9_pckts = []
  index = 0
  while index < len(app):
    app_rx_9_bytes.append(app[index])
    app_rx_9_pckts.append(app[index + 1])
    index += 2
  app_rx_9 = (app_rx_9_bytes, app_rx_9_pckts)
  
  app_rx = (app_rx_1, app_rx_2, app_rx_3, app_rx_4, app_rx_5, app_rx_6,
            app_rx_7, app_rx_8, app_rx_9)
  
  # ---------------------------------------------------- APP Tx
  assert (IDX == 33), "%d != 33" % IDX
  
  # {APP-Tx-bytes	APP-Tx-pckts} * {...} (PUBLISH)
  app = get_next_int_line(content)
  app_tx_1_bytes = []
  app_tx_1_pckts = []
  index = 0
  while index < len(app):
    app_tx_1_bytes.append(app[index])
    app_tx_1_pckts.append(app[index + 1])
    index += 2
  app_tx_1 = (app_tx_1_bytes, app_tx_1_pckts)
  
  # {APP-Tx-bytes	APP-Tx-pckts} * {...} (UNPUBLISH)
  app = get_next_int_line(content)
  app_tx_2_bytes = []
  app_tx_2_pckts = []
  index = 0
  while index < len(app):
    app_tx_2_bytes.append(app[index])
    app_tx_2_pckts.append(app[index + 1])
    index += 2
  app_tx_2 = (app_tx_2_bytes, app_tx_2_pckts)
  
  # {APP-Tx-bytes	APP-Tx-pckts} * {...} (DOWNLOAD)
  app = get_next_int_line(content)
  app_tx_3_bytes = []
  app_tx_3_pckts = []
  index = 0
  while index < len(app):
    app_tx_3_bytes.append(app[index])
    app_tx_3_pckts.append(app[index + 1])
    index += 2
  app_tx_3 = (app_tx_3_bytes, app_tx_3_pckts)
  
  # {APP-Tx-bytes	APP-Tx-pckts} * {...} (SUBSCRIBE)
  app = get_next_int_line(content)
  app_tx_4_bytes = []
  app_tx_4_pckts = []
  index = 0
  while index < len(app):
    app_tx_4_bytes.append(app[index])
    app_tx_4_pckts.append(app[index + 1])
    index += 2
  app_tx_4 = (app_tx_4_bytes, app_tx_4_pckts)
  
  # {APP-Tx-bytes	APP-Tx-pckts} * {...} (UNSUBSCRIBE)
  app = get_next_int_line(content)
  app_tx_5_bytes = []
  app_tx_5_pckts = []
  index = 0
  while index < len(app):
    app_tx_5_bytes.append(app[index])
    app_tx_5_pckts.append(app[index + 1])
    index += 2
  app_tx_5 = (app_tx_5_bytes, app_tx_5_pckts)
  
  # {APP-Tx-bytes	APP-Tx-pckts} * {...} (NOTIFICATION)
  app = get_next_int_line(content)
  app_tx_6_bytes = []
  app_tx_6_pckts = []
  index = 0
  while index < len(app):
    app_tx_6_bytes.append(app[index])
    app_tx_6_pckts.append(app[index + 1])
    index += 2
  app_tx_6 = (app_tx_6_bytes, app_tx_6_pckts)
  
  # {APP-Tx-bytes	APP-Tx-pckts} * {...} (CONTROL)
  app = get_next_int_line(content)
  app_tx_7_bytes = []
  app_tx_7_pckts = []
  index = 0
  while index < len(app):
    app_tx_7_bytes.append(app[index])
    app_tx_7_pckts.append(app[index + 1])
    index += 2
  app_tx_7 = (app_tx_7_bytes, app_tx_7_pckts)
  
  # {APP-Tx-bytes	APP-Tx-pckts} * {...} (ACTIVE REPLICATION)
  app = get_next_int_line(content)
  app_tx_8_bytes = []
  app_tx_8_pckts = []
  index = 0
  while index < len(app):
    app_tx_8_bytes.append(app[index])
    app_tx_8_pckts.append(app[index + 1])
    index += 2
  app_tx_8 = (app_tx_8_bytes, app_tx_8_pckts)
  
  # {APP-Tx-bytes	APP-Tx-pckts} * {...} (PASSIVE REPLICATION)
  app = get_next_int_line(content)
  app_tx_9_bytes = []
  app_tx_9_pckts = []
  index = 0
  while index < len(app):
    app_tx_9_bytes.append(app[index])
    app_tx_9_pckts.append(app[index + 1])
    index += 2
  app_tx_9 = (app_tx_9_bytes, app_tx_9_pckts)
  
  app_tx = (app_tx_1, app_tx_2, app_tx_3, app_tx_4, app_tx_5, app_tx_6,
            app_tx_7, app_tx_8, app_tx_9)
  
  # ---------------------------------------------------- APP Bcast
  assert (IDX == 42), "%d != 42" % IDX
  
  # {APP-Bcast-bytes	APP-Bcast-pckts} * {...} (PUBLISH)
  app = get_next_int_line(content)
  app_bcast_1_bytes = []
  app_bcast_1_pckts = []
  index = 0
  while index < len(app):
    app_bcast_1_bytes.append(app[index])
    app_bcast_1_pckts.append(app[index + 1])
    index += 2
  app_bcast_1 = (app_bcast_1_bytes, app_bcast_1_pckts)
  
  # {APP-Bcast-bytes	APP-Bcast-pckts} * {...} (UNPUBLISH)
  app = get_next_int_line(content)
  app_bcast_2_bytes = []
  app_bcast_2_pckts = []
  index = 0
  while index < len(app):
    app_bcast_2_bytes.append(app[index])
    app_bcast_2_pckts.append(app[index + 1])
    index += 2
  app_bcast_2 = (app_bcast_2_bytes, app_bcast_2_pckts)
  
  # {APP-Bcast-bytes	APP-Bcast-pckts} * {...} (DOWNLOAD)
  app = get_next_int_line(content)
  app_bcast_3_bytes = []
  app_bcast_3_pckts = []
  index = 0
  while index < len(app):
    app_bcast_3_bytes.append(app[index])
    app_bcast_3_pckts.append(app[index + 1])
    index += 2
  app_bcast_3 = (app_bcast_3_bytes, app_bcast_3_pckts)
  
  # {APP-Bcast-bytes	APP-Bcast-pckts} * {...} (SUBSCRIBE)
  app = get_next_int_line(content)
  app_bcast_4_bytes = []
  app_bcast_4_pckts = []
  index = 0
  while index < len(app):
    app_bcast_4_bytes.append(app[index])
    app_bcast_4_pckts.append(app[index + 1])
    index += 2
  app_bcast_4 = (app_bcast_4_bytes, app_bcast_4_pckts)
  
  # {APP-Bcast-bytes	APP-Bcast-pckts} * {...} (UNSUBSCRIBE)
  app = get_next_int_line(content)
  app_bcast_5_bytes = []
  app_bcast_5_pckts = []
  index = 0
  while index < len(app):
    app_bcast_5_bytes.append(app[index])
    app_bcast_5_pckts.append(app[index + 1])
    index += 2
  app_bcast_5 = (app_bcast_5_bytes, app_bcast_5_pckts)
  
  # {APP-Bcast-bytes	APP-Bcast-pckts} * {...} (NOTIFICATION)
  app = get_next_int_line(content)
  app_bcast_6_bytes = []
  app_bcast_6_pckts = []
  index = 0
  while index < len(app):
    app_bcast_6_bytes.append(app[index])
    app_bcast_6_pckts.append(app[index + 1])
    index += 2
  app_bcast_6 = (app_bcast_6_bytes, app_bcast_6_pckts)
  
  # {APP-Bcast-bytes	APP-Bcast-pckts} * {...} (CONTROL)
  app = get_next_int_line(content)
  app_bcast_7_bytes = []
  app_bcast_7_pckts = []
  index = 0
  while index < len(app):
    app_bcast_7_bytes.append(app[index])
    app_bcast_7_pckts.append(app[index + 1])
    index += 2
  app_bcast_7 = (app_bcast_7_bytes, app_bcast_7_pckts)
  
  # {APP-Bcast-bytes	APP-Bcast-pckts} * {...} (ACTIVE REPLICATION)
  app = get_next_int_line(content)
  app_bcast_8_bytes = []
  app_bcast_8_pckts = []
  index = 0
  while index < len(app):
    app_bcast_8_bytes.append(app[index])
    app_bcast_8_pckts.append(app[index + 1])
    index += 2
  app_bcast_8 = (app_bcast_8_bytes, app_bcast_8_pckts)
  
  # {APP-Bcast-bytes	APP-Bcast-pckts} * {...} (PASSIVE REPLICATION)
  app = get_next_int_line(content)
  app_bcast_9_bytes = []
  app_bcast_9_pckts = []
  index = 0
  while index < len(app):
    app_bcast_9_bytes.append(app[index])
    app_bcast_9_pckts.append(app[index + 1])
    index += 2
  app_bcast_9 = (app_bcast_9_bytes, app_bcast_9_pckts)
  
  app_bcast = (app_bcast_1, app_bcast_2, app_bcast_3, app_bcast_4,
               app_bcast_5, app_bcast_6, app_bcast_7, app_bcast_8, app_bcast_9)
  
  _app = (app_rx, app_tx, app_bcast)
  
  # ---------------------------------------------------- PUBLISH
  assert (IDX == 51), "%d != 51" % IDX
  
  # PUB-succ-amount PUB-fail-amount
  pub = get_next_int_line(content)
  pub_perc = (pub[0], pub[1])
  
  # {PUB-fail-reason  amount} * {...}
  pub = get_next_line(content)
  pub_fails = {}
  index = 0
  while index < len(pub):
    pub_fails[pub[index]] = int(pub[index + 1])
    index += 2
  
  # {PUB-dur} * {...}
  pub = get_next_int_line(content)
  if pub:
    pub_dur = (np.mean(pub), np.std(pub), min(pub), np.percentile(pub, 25),
               np.percentile(pub, 50), np.percentile(pub, 75),
               np.percentile(pub, 90), max(pub), get_conf_int(pub))
  else:
    pub_dur = None
  
  # {PUB-objSize} * {...}
  pub = get_next_int_line(content)
  if pub:
    pub_obj_size = (np.mean(pub), np.std(pub), min(pub), np.percentile(pub, 25),
                    np.percentile(pub, 50), np.percentile(pub, 75),
                    np.percentile(pub, 90), max(pub), get_conf_int(pub))
  else:
    pub_obj_size = None
  
  # {PUB-keySize} * {...}
  pub = get_next_int_line(content)
  if pub:
    pub_key_size = (np.mean(pub), np.std(pub), min(pub), np.percentile(pub, 25),
                    np.percentile(pub, 50), np.percentile(pub, 75),
                    np.percentile(pub, 90), max(pub), get_conf_int(pub))
  else:
    pub_key_size = None
  
  # {PUB-numTags} * {...}
  pub = get_next_int_line(content)
  if pub:
    pub_num_tags = (np.mean(pub), np.std(pub), min(pub), np.percentile(pub, 25),
                    np.percentile(pub, 50), np.percentile(pub, 75),
                    np.percentile(pub, 90), max(pub), get_conf_int(pub))
  else:
    pub_num_tags = None
  
  # {PUB-retries} * {...}
  pub = get_next_int_line(content)
  if pub:
    pub_retries = (np.mean(pub), np.std(pub), min(pub), np.percentile(pub, 25),
                   np.percentile(pub, 50), np.percentile(pub, 75),
                   np.percentile(pub, 90), max(pub), get_conf_int(pub))
  else:
    pub_retries = None
  
  # {PUB-timeout} * {...}
  pub = get_next_int_line(content)
  if pub:
    pub_timeout = (
      np.mean(pub), np.std(pub), np.min(pub), np.percentile(pub, 25),
      np.percentile(pub, 50), np.percentile(pub, 75), np.percentile(pub, 90),
      max(pub), get_conf_int(pub))
  else:
    pub_timeout = None
  
  # {PUB-objsPerNode} * {...}
  pub = get_next_int_line(content)
  if pub:
    pub_objs_node = (
      np.mean(pub), np.std(pub), min(pub), np.percentile(pub, 25),
      np.percentile(pub, 50), np.percentile(pub, 75), np.percentile(pub, 90),
      max(pub), get_conf_int(pub))
  else:
    pub_objs_node = None
  
  # PUB-actRepsPerNode} * {...}
  pub = get_next_int_line(content)
  if pub:
    pub_act_reps_node = (np.mean(pub), np.std(pub), min(pub), np.percentile(pub, 25), np.percentile(pub, 50), np.percentile(pub, 75), np.percentile(pub, 90), max(pub), get_conf_int(pub))
  else:
    pub_act_reps_node = None
  
  # numTagDur
  pub = get_next_int_line(content)
  num_tags = pub[0]
  
  tags = {}
  index = 0
  while index < num_tags:
    line = get_next_int_line(content)  # {PUB-numTags} {dur} * {...}
    pub = line[1:]
    tags[line[0]] = (
      np.mean(pub), np.std(pub), min(pub), np.percentile(pub, 25),
      np.percentile(pub, 50), np.percentile(pub, 75), np.percentile(pub, 90),
      max(pub), get_conf_int(pub))
    index += 1
  
  # numRetryDur
  pub = get_next_int_line(content)
  num_retries = pub[0]
  
  retries = {}
  index = 0
  while index < num_retries:
    line = get_next_int_line(content)  # {PUB-numRetry} {dur} * {...}
    pub = line[1:]
    retries[line[0]] = (
      np.mean(pub), np.std(pub), min(pub), np.percentile(pub, 25),
      np.percentile(pub, 50), np.percentile(pub, 75), np.percentile(pub, 90),
      max(pub), get_conf_int(pub))
    index += 1
  
  _pub = (
    pub_perc, pub_fails, pub_dur, pub_obj_size, pub_key_size, pub_num_tags,
    pub_retries, pub_timeout, pub_objs_node, tags, retries, pub_act_reps_node)
  
  # ---------------------------------------------------- UNPUBLISH
  rest = 63 + num_tags + num_retries
  assert (IDX == rest), "%d != %d" % (IDX, rest)
  
  # UNPUB-succ-amount UNPUB-fail-amount
  unpub = get_next_int_line(content)
  unpub_perc = (unpub[0], unpub[1])
  
  # {UNPUB-fail-reason  amount} * {...}
  unpub = get_next_line(content)
  unpub_fails = {}
  index = 0
  while index < len(unpub):
    unpub_fails[unpub[index]] = int(unpub[index + 1])
    index += 2
  
  # {UNPUB-dur} * {...}
  unpub = get_next_int_line(content)
  if unpub:
    unpub_dur = (
      np.mean(unpub), np.std(unpub), min(unpub), np.percentile(unpub, 25),
      np.percentile(unpub, 50), np.percentile(unpub, 75),
      np.percentile(unpub, 90), max(unpub), get_conf_int(unpub))
  else:
    unpub_dur = None
  
  # {UNPUB-retries} * {...}
  unpub = get_next_int_line(content)
  if unpub:
    unpub_retries = (
      np.mean(unpub), np.std(unpub), min(unpub), np.percentile(unpub, 25),
      np.percentile(unpub, 50), np.percentile(unpub, 75),
      np.percentile(unpub, 90), max(unpub), get_conf_int(unpub))
  else:
    unpub_retries = None
  
  # {UNPUB-timeout} * {...}
  unpub = get_next_int_line(content)
  if unpub:
    unpub_timeout = (
      np.mean(unpub), np.std(unpub), min(unpub), np.percentile(unpub, 25),
      np.percentile(unpub, 50), np.percentile(unpub, 75),
      np.percentile(unpub, 90), max(unpub), get_conf_int(unpub))
  else:
    unpub_timeout = None
  
  # {UNPUB-objsPerNode} * {...}
  unpub = get_next_int_line(content)
  if unpub:
    unpub_objs_node = (
      np.mean(unpub), np.std(unpub), min(unpub), np.percentile(unpub, 25),
      np.percentile(unpub, 50), np.percentile(unpub, 75),
      np.percentile(unpub, 90), max(unpub), get_conf_int(unpub))
  else:
    unpub_objs_node = None
  
  # numTagDur
  unpub = get_next_int_line(content)
  num_tags = unpub[0]
  
  tags = {}
  index = 0
  while index < num_tags:
    line = get_next_int_line(content)  # {UNPUB-numTags} {dur} * {...}
    unpub = line[1:]
    tags[line[0]] = (
      np.mean(unpub), np.std(unpub), min(unpub), np.percentile(unpub, 25),
      np.percentile(unpub, 50), np.percentile(unpub, 75),
      np.percentile(unpub, 90), max(unpub), get_conf_int(unpub))
    index += 1
  
  # numRetryDur
  unpub = get_next_int_line(content)
  num_retries = unpub[0]
  
  retries = {}
  index = 0
  while index < num_retries:
    line = get_next_int_line(content)  # {UNPUB-numRetry} {dur} * {...}
    unpub = line[1:]
    retries[line[0]] = (
      np.mean(unpub), np.std(unpub), min(unpub), np.percentile(unpub, 25),
      np.percentile(unpub, 50), np.percentile(unpub, 75),
      np.percentile(unpub, 90), max(unpub), get_conf_int(unpub))
    index += 1
  
  _unpub = (unpub_perc, unpub_fails, unpub_dur, unpub_retries, unpub_timeout,
            unpub_objs_node, tags, retries)
  
  # ---------------------------------------------------- DOWNLOAD
  rest += 8 + num_tags + num_retries
  assert (IDX == rest), "%d != %d" % (IDX, rest)
  
  # DOWN-succ-amount DOWN-fail-amount
  down = get_next_int_line(content)
  down_prec = (down[0], down[1])
  
  # {DOWN-fail-reason  amount} * {...}
  down = get_next_line(content)
  down_fails = {}
  index = 0
  while index < len(down):
    down_fails[down[index]] = int(down[index + 1])
    index += 2
  
  # {DOWN-dur} * {...}
  down = get_next_int_line(content)
  if down:
    down_dur = (np.mean(down), np.std(down), min(down), np.percentile(down, 25),
                np.percentile(down, 50), np.percentile(down, 75),
                np.percentile(down, 90), max(down), get_conf_int(down))
  else:
    down_dur = None
  
  # {DOWN-objSize} * {...}
  down = get_next_int_line(content)
  if down:
    down_obj_size = (
      np.mean(down), np.std(down), min(down), np.percentile(down, 25),
      np.percentile(down, 50), np.percentile(down, 75), np.percentile(down, 90),
      max(down), get_conf_int(down))
  else:
    down_obj_size = None
  
  # {DOWN-retries} * {...}
  down = get_next_int_line(content)
  if down:
    down_retries = (
      np.mean(down), np.std(down), min(down), np.percentile(down, 25),
      np.percentile(down, 50), np.percentile(down, 75), np.percentile(down, 90),
      max(down), get_conf_int(down))
  else:
    down_retries = None
  
  # {DOWN-timeout} * {...}
  down = get_next_int_line(content)
  if down:
    down_timeout = (
      np.mean(down), np.std(down), min(down), np.percentile(down, 25),
      np.percentile(down, 50), np.percentile(down, 75), np.percentile(down, 90),
      max(down), get_conf_int(down))
  else:
    down_timeout = None
  
  # numRetryDur
  down = get_next_int_line(content)
  num_retries = down[0]
  
  retries = {}
  index = 0
  while index < num_retries:
    line = get_next_int_line(content)  # {DOWN-numRetry} {dur} * {...}
    down = line[1:]
    retries[line[0]] = (
      np.mean(down), np.std(down), min(down), np.percentile(down, 25),
      np.percentile(down, 50), np.percentile(down, 75), np.percentile(down, 90),
      max(down), get_conf_int(down))
    index += 1
  
  _down = (
    down_prec, down_fails, down_dur, down_obj_size, down_retries, down_timeout,
    retries)
  
  # ---------------------------------------------------- SUBSCRIBE
  rest += 7 + num_retries
  assert (IDX == rest), "%d != %d" % (IDX, rest)
  
  # SUB-succ-amount SUB-fail-amount
  sub = get_next_int_line(content)
  sub_prec = (sub[0], sub[1])
  
  # {SUB-fail-reason  amount} * {...}
  sub = get_next_line(content)
  sub_fails = {}
  index = 0
  while index < len(sub):
    sub_fails[sub[index]] = int(sub[index + 1])
    index += 2
  
  # {SUB-dur} * {...}
  sub = get_next_int_line(content)
  if sub:
    sub_dur = (np.mean(sub), np.std(sub), min(sub), np.percentile(sub, 25),
               np.percentile(sub, 50), np.percentile(sub, 75),
               np.percentile(sub, 90), max(sub), get_conf_int(sub))
  else:
    sub_dur = None
  
  # {SUB-retries} * {...}
  sub = get_next_int_line(content)
  if sub:
    sub_retries = (np.mean(sub), np.std(sub), min(sub), np.percentile(sub, 25),
                   np.percentile(sub, 50), np.percentile(sub, 75),
                   np.percentile(sub, 90), max(sub), get_conf_int(sub))
  else:
    sub_retries = None
  
  # {SUB-timeout} * {...}
  sub = get_next_int_line(content)
  if sub:
    sub_timeout = (np.mean(sub), np.std(sub), min(sub), np.percentile(sub, 25),
                   np.percentile(sub, 50), np.percentile(sub, 75),
                   np.percentile(sub, 90), max(sub), get_conf_int(sub))
  else:
    sub_timeout = None
  
  # {SUB-subsPerNode} * {...}
  sub = get_next_int_line(content)
  if sub:
    sub_node = (np.mean(sub), np.std(sub), min(sub), np.percentile(sub, 25),
                np.percentile(sub, 50), np.percentile(sub, 75),
                np.percentile(sub, 90), max(sub), get_conf_int(sub))
  else:
    sub_node = None
  
  # numTagDur
  sub = get_next_int_line(content)
  num_tags = sub[0]
  
  tags = {}
  index = 0
  while index < num_tags:
    line = get_next_int_line(content)  # {SUB-numTags} {dur} * {...}
    sub = line[1:]
    tags[line[0]] = (
      np.mean(sub), np.std(sub), min(sub), np.percentile(sub, 25),
      np.percentile(sub, 50), np.percentile(sub, 75), np.percentile(sub, 90),
      max(sub), get_conf_int(sub))
    index += 1
  
  # numRetryDur
  sub = get_next_int_line(content)
  num_retries = sub[0]
  
  retries = {}
  index = 0
  while index < num_retries:
    line = get_next_int_line(content)  # {SUB-numRetry} {dur} * {...}
    sub = line[1:]
    retries[line[0]] = (
      np.mean(sub), np.std(sub), min(sub), np.percentile(sub, 25),
      np.percentile(sub, 50), np.percentile(sub, 75), np.percentile(sub, 90),
      max(sub), get_conf_int(sub))
    index += 1
  
  _sub = (
    sub_prec, sub_fails, sub_dur, sub_retries, sub_timeout, sub_node, tags,
    retries)
  
  # ---------------------------------------------------- UNSUBSCRIBE
  rest += 8 + num_tags + num_retries
  assert (IDX == rest), "%d != %d" % (IDX, rest)
  
  # UNSUB-succ-amount SUB-fail-amount
  unsub = get_next_int_line(content)
  unsub_prec = (unsub[0], unsub[1])
  
  # {UNSUB-fail-reason  amount} * {...}
  unsub = get_next_line(content)
  unsub_fails = {}
  index = 0
  while index < len(unsub):
    unsub_fails[unsub[index]] = int(unsub[index + 1])
    index += 2
  
  # {UNSUB-dur} * {...}
  unsub = get_next_int_line(content)
  if unsub:
    unsub_dur = (
      np.mean(unsub), np.std(unsub), min(unsub), np.percentile(unsub, 25),
      np.percentile(unsub, 50), np.percentile(unsub, 75),
      np.percentile(unsub, 90), max(unsub), get_conf_int(unsub))
  else:
    unsub_dur = None
  
  # {UNSUB-retries} * {...}
  unsub = get_next_int_line(content)
  if unsub:
    unsub_retries = (
      np.mean(unsub), np.std(unsub), min(sub), np.percentile(unsub, 25),
      np.percentile(unsub, 50), np.percentile(unsub, 75),
      np.percentile(unsub, 90), max(unsub), get_conf_int(unsub))
  else:
    unsub_retries = None
  
  # {UNSUB-timeout} * {...}
  unsub = get_next_int_line(content)
  if unsub:
    unsub_timeout = (
      np.mean(unsub), np.std(unsub), min(unsub), np.percentile(unsub, 25),
      np.percentile(unsub, 50), np.percentile(unsub, 75),
      np.percentile(unsub, 90), max(unsub), get_conf_int(unsub))
  else:
    unsub_timeout = None
  
  # {UNSUB-subsPerNode} * {...}
  unsub = get_next_int_line(content)
  if unsub:
    unsub_node = (
      np.mean(unsub), np.std(unsub), min(unsub), np.percentile(unsub, 25),
      np.percentile(unsub, 50), np.percentile(unsub, 75),
      np.percentile(unsub, 90), max(unsub), get_conf_int(unsub))
  else:
    unsub_node = None
  
  # numTagDur
  unsub = get_next_int_line(content)
  num_tags = unsub[0]
  
  tags = {}
  index = 0
  while index < num_tags:
    line = get_next_int_line(content)  # {UNSUB-numTags} {dur} * {...}
    unsub = line[1:]
    tags[line[0]] = (
      np.mean(unsub), np.std(unsub), min(unsub), np.percentile(unsub, 25),
      np.percentile(unsub, 50), np.percentile(unsub, 75),
      np.percentile(unsub, 90), max(unsub), get_conf_int(unsub))
    index += 1
  
  # numRetryDur
  unsub = get_next_int_line(content)
  num_retries = unsub[0]
  
  retries = {}
  index = 0
  while index < num_tags:
    line = get_next_int_line(content)  # {UNSUB-numRetry} {dur} * {...}
    unsub = line[1:]
    retries[line[0]] = (
      np.mean(unsub), np.std(unsub), min(unsub), np.percentile(unsub, 25),
      np.percentile(unsub, 50), np.percentile(unsub, 75),
      np.percentile(unsub, 90), max(unsub), get_conf_int(unsub))
    index += 1
  
  _unsub = (
    unsub_prec, unsub_fails, unsub_dur, unsub_retries, unsub_timeout,
    unsub_node,
    tags, retries)
  
  # ---------------------------------------------------- NOTS
  rest += 8 + num_tags + num_retries
  assert (IDX == rest), "%d != %d" % (IDX, rest)
  
  # expected-nots nots-sent nots-recvd dup-nots-recvd noid-nots
  nots = get_next_int_line(content)
  nots_prec = (nots[0], nots[1], nots[2], nots[3], nots[4])
  
  # nots-time-amount nots-amount
  nots = get_next_int_line(content)
  nots_time = (nots[0], nots[1])
  
  # {not-time} * {...}
  nots = get_next_int_line(content)
  if nots:
    nots_dur = (np.mean(nots), np.std(nots), min(nots), np.percentile(nots, 25),
                np.percentile(nots, 50), np.percentile(nots, 75),
                np.percentile(nots, 90), max(nots), get_conf_int(nots))
  else:
    nots_dur = None
  
  _nots = (nots_prec, nots_time, nots_dur)
  
  # ----------------------------------------------------
  rest += 3
  assert (IDX == rest), "%d != %d" % (IDX, rest)
  
  return (_phy, _mac, _ip, _cells, _ctrl, _app, _pub, _unpub, _down, _sub,
          _unsub, _nots)


def process_file(filename):
  try:
    with open(filename, 'r') as f:
      content = f.readlines()
    content = [x.strip() for x in content]
  except IOError:
    print 'Unable to open file: %s' % filename
    return None
  
  try:
    idx = content.index(SEP)
    content = content[idx + 1:]
    idx = content.index(SEP)
    content = content[:idx]
    res = process_content(content)
  except ValueError:
    print 'Unable to parse file: %s' % filename
    res = None
  
  return res


def main(argv):
  directory = argv[0]
  nodes = 64
  fieldX = 320
  fieldY = 160
  trace = 'euro2016'
  tags = 1
  runs = 10
  rp = 2
  
  #is_mob = True
  is_mob = False
  
  if is_mob:
    max_speed = 2.5
    min_pause = 30
    max_pause = 30
    move_prob = 75
  else:
    max_speed = 0
    min_pause = 720
    max_pause = 720
    move_prob = 0
  
  #is_crash_churn = True
  is_crash_churn = False
  
  #is_int_churn = True
  is_int_churn = False
  
  if is_int_churn:
    churnDecision = 100
    minChurnFreqUp = 120
    maxChurnFreqUp = 120
    minChurnFreqDown = 60
    maxChurnFreqDown = 60
    churnDownProb = 40
    nodeChurnProb = 100
  else:
    churnDecision = 0
    minChurnFreqUp = 0
    maxChurnFreqUp = 0
    minChurnFreqDown = 0
    maxChurnFreqDown = 0
    churnDownProb = 0
    nodeChurnProb = 0
  
  if is_int_churn:
    minChurnInt = 360
    maxChurnInt = 510
    churnOutProb = 40
  else:
    minChurnInt = 0
    maxChurnInt = 0
    churnOutProb = 0
  
  #is_bcast = True
  is_bcast = False
  
  cell_size = 40
  
  res = []
  
  for run in range(1, runs + 1):
    if is_bcast:
      filename = '%s%s-n%d-rp%d-s%.1f-p%d-%d-mp%d-f%d-%d-cd%d-ciup%d-%d-cidown%d-%d-cpdown%d-cpnode%d-cio%d-%d-conode%d-%s-t%d-r%d.log' % (
        directory, 'broadcast-run', nodes, rp, max_speed, min_pause,
        max_pause, move_prob, fieldX, fieldY, churnDecision, minChurnFreqUp,
        maxChurnFreqUp, minChurnFreqDown, maxChurnFreqDown,
        churnDownProb, nodeChurnProb, minChurnInt, maxChurnInt, churnOutProb,
        trace, tags, run)
    else:
      filename = '%s%s-n%d-s%.1f-p%d-%d-mp%d-f%d-%d-cd%d-ciup%d-%d-cidown%d-%d-cpdown%d-cpnode%d-cio%d-%d-conode%d-%s-t%d-r%d.log' % (
        directory, 'thyme-run', nodes, max_speed, min_pause,
        max_pause,
        move_prob, fieldX, fieldY, churnDecision, minChurnFreqUp, maxChurnFreqUp,
        minChurnFreqDown, maxChurnFreqDown, churnDownProb,
        nodeChurnProb, minChurnInt, maxChurnInt, churnOutProb, trace, tags, run)
    print filename
    
    aux = process_file(filename)
    if aux is not None:  # if file was processed correctly...
      res.append(aux)
  
  phy_rx_bytes_t = []
  phy_rx_pckts_t = []
  phy_rx_drop_bytes_t = []
  phy_rx_drop_pckts_t = []
  phy_tx_bytes_t = []
  phy_tx_pckts_t = []
  phy_tx_drop_bytes_t = []
  phy_tx_drop_pckts_t = []
  
  mac_rx_bytes_t = []
  mac_rx_pckts_t = []
  mac_rx_drop_bytes_t = []
  mac_rx_drop_pckts_t = []
  mac_tx_bytes_t = []
  mac_tx_pckts_t = []
  mac_tx_drop_bytes_t = []
  mac_tx_drop_pckts_t = []
  mac_tx_failed_pckts_t = []
  mac_tx_maxfailed_pckts_t = []
  
  ip_rx_bytes_t = []
  ip_rx_pckts_t = []
  ip_tx_bytes_t = []
  ip_tx_pckts_t = []
  ip_fwd_bytes_t = []
  ip_fwd_pckts_t = []
  ip_fwd_bytes = []
  ip_fwd_pckts = []
  histograms_down = []
  histograms_down_rsp = []
  ip_drop_1_bytes = []
  ip_drop_1_pckts = []
  ip_drop_2_bytes = []
  ip_drop_2_pckts = []
  ip_drop_3_bytes = []
  ip_drop_3_pckts = []
  ip_drop_4_bytes = []
  ip_drop_4_pckts = []
  ip_drop_5_bytes = []
  ip_drop_5_pckts = []
  ip_drop_6_bytes = []
  ip_drop_6_pckts = []
  
  cells_pop_perc = []
  avg_node_per_cell = []
  max_node_per_cell = []
  min_node_per_cell = []
  
  ctrl_rx_bytes_t = []
  ctrl_rx_pckts_t = []
  ctrl_tx_bytes_t = []
  ctrl_tx_pckts_t = []
  ctrl_reps = []
  
  app_rx_1_bytes = []
  app_rx_1_pckts = []
  app_rx_2_bytes = []
  app_rx_2_pckts = []
  app_rx_3_bytes = []
  app_rx_3_pckts = []
  app_rx_4_bytes = []
  app_rx_4_pckts = []
  app_rx_5_bytes = []
  app_rx_5_pckts = []
  app_rx_6_bytes = []
  app_rx_6_pckts = []
  app_rx_7_bytes = []
  app_rx_7_pckts = []
  app_rx_8_bytes = []
  app_rx_8_pckts = []
  app_rx_9_bytes = []
  app_rx_9_pckts = []
  
  app_tx_1_bytes = []
  app_tx_1_pckts = []
  app_tx_2_bytes = []
  app_tx_2_pckts = []
  app_tx_3_bytes = []
  app_tx_3_pckts = []
  app_tx_4_bytes = []
  app_tx_4_pckts = []
  app_tx_5_bytes = []
  app_tx_5_pckts = []
  app_tx_6_bytes = []
  app_tx_6_pckts = []
  app_tx_7_bytes = []
  app_tx_7_pckts = []
  app_tx_8_bytes = []
  app_tx_8_pckts = []
  app_tx_9_bytes = []
  app_tx_9_pckts = []
  
  app_bcast_1_bytes = []
  app_bcast_1_pckts = []
  app_bcast_2_bytes = []
  app_bcast_2_pckts = []
  app_bcast_3_bytes = []
  app_bcast_3_pckts = []
  app_bcast_4_bytes = []
  app_bcast_4_pckts = []
  app_bcast_5_bytes = []
  app_bcast_5_pckts = []
  app_bcast_6_bytes = []
  app_bcast_6_pckts = []
  app_bcast_7_bytes = []
  app_bcast_7_pckts = []
  app_bcast_8_bytes = []
  app_bcast_8_pckts = []
  app_bcast_9_bytes = []
  app_bcast_9_pckts = []
  
  pub_suc = []
  pub_dur_avg = []
  pub_dur_90 = []
  pub_tags = {}
  pub_retries = {}
  
  unpub_suc = []
  unpub_dur_avg = []
  unpub_dur_90 = []
  unpub_tags = {}
  unpub_retries = {}
  
  down_suc = []
  down_dur_avg = []
  down_dur_90 = []
  down_retries = {}
  
  sub_suc = []
  sub_dur_avg = []
  sub_dur_90 = []
  sub_tags = {}
  sub_retries = {}
  
  unsub_suc = []
  unsub_dur_avg = []
  unsub_dur_90 = []
  unsub_tags = {}
  unsub_retries = {}
  
  nots_suc = []
  nots_dur_avg = []
  nots_dur_90 = []
  
  print 'Number of runs: %d' % len(res)
  for run in range(0, len(res)):
    phy = res[run][0]
    # (phy_rx/2, phy_rx_drop/2, phy_tx/2, phy_tx_drop/2)
    phy_rx_bytes_t.append(sum(phy[0][0]))
    phy_rx_pckts_t.append(sum(phy[0][1]))
    
    phy_rx_drop_bytes_t.append(sum(phy[1][0]))
    phy_rx_drop_pckts_t.append(sum(phy[1][1]))
    
    phy_tx_bytes_t.append(sum(phy[2][0]))
    phy_tx_pckts_t.append(sum(phy[2][1]))
    
    phy_tx_drop_bytes_t.append(sum(phy[3][0]))
    phy_tx_drop_pckts_t.append(sum(phy[3][1]))
    
    # ----------------------------------------------------
    
    mac = res[run][1]
    # (mac_rx/2, mac_rx_drop/2, mac_tx/2, mac_tx_drop/2, mac_tx_failed/2)
    mac_rx_bytes_t.append(sum(mac[0][0]))
    mac_rx_pckts_t.append(sum(mac[0][1]))
    
    mac_rx_drop_bytes_t.append(sum(mac[1][0]))
    mac_rx_drop_pckts_t.append(sum(mac[1][1]))
    
    mac_tx_bytes_t.append(sum(mac[2][0]))
    mac_tx_pckts_t.append(sum(mac[2][1]))
    
    mac_tx_drop_bytes_t.append(sum(mac[3][0]))
    mac_tx_drop_pckts_t.append(sum(mac[3][1]))
    
    mac_tx_failed_pckts_t.append(sum(mac[4][0]))
    mac_tx_maxfailed_pckts_t.append(sum(mac[4][1]))
    
    # ----------------------------------------------------
    
    ip = res[run][2]
    # (ip_rx/2, ip_tx/2, ip_fwd/2, ip_drop_1/2, ip_drop_2/2, ip_drop_3/2,
    # ip_drop_4/2, ip_drop_5/2, ip_drop_6/2, histogram_down/1,
    # histogram_down_rsp/1)
    ip_rx_bytes_t.append(sum(ip[0][0]))
    ip_rx_pckts_t.append(sum(ip[0][1]))
    
    ip_tx_bytes_t.append(sum(ip[1][0]))
    ip_tx_pckts_t.append(sum(ip[1][1]))
    
    ip_fwd_bytes_t.append(sum(ip[2][0]))
    ip_fwd_pckts_t.append(sum(ip[2][1]))
    ip_fwd_bytes.append(ip[2][0])
    ip_fwd_pckts.append(ip[2][1])
    
    ip_drop_1_bytes.append(sum(ip[3][0]))
    ip_drop_1_pckts.append(sum(ip[3][1]))
    
    ip_drop_2_bytes.append(sum(ip[4][0]))
    ip_drop_2_pckts.append(sum(ip[4][1]))
    
    ip_drop_3_bytes.append(sum(ip[5][0]))
    ip_drop_3_pckts.append(sum(ip[5][1]))
    
    ip_drop_4_bytes.append(sum(ip[6][0]))
    ip_drop_4_pckts.append(sum(ip[6][1]))
    
    ip_drop_5_bytes.append(sum(ip[7][0]))
    ip_drop_5_pckts.append(sum(ip[7][1]))
    
    ip_drop_6_bytes.append(sum(ip[8][0]))
    ip_drop_6_pckts.append(sum(ip[8][1]))
    
    histograms_down.append(ip[9])
    histograms_down_rsp.append(ip[10])
    
    # ----------------------------------------------------
    
    cells = res[run][3]
    side_widthX = fieldX / cell_size
    side_widthY = fieldY / cell_size
    num_cells = side_widthX * side_widthY
    
    empty = 0
    count = {k: 0 for k in range(num_cells)}
    avg_cells = []
    for cell in range(num_cells):
      _aux = cells.count(cell)
      if _aux == 0: empty += 1
      count[cell] = _aux
      avg_cells.append(_aux)
    
    populated = num_cells - empty
    
    cells_pop_perc.append((populated * 100.0) / num_cells)
    avg_node_per_cell.append(np.mean(avg_cells))
    max_node_per_cell.append(max(avg_cells))
    min_node_per_cell.append(min(avg_cells))
    
    # TODO
    # check disconnex components
    # TODO rule out runs with disconnex components???
    
    # ----------------------------------------------------
    
    ctrl = res[run][4]
    # (ctrl_rx/2, ctrl_tx/2, reps/1)
    ctrl_rx_bytes_t.append(sum(ctrl[0][0]))
    ctrl_rx_pckts_t.append(sum(ctrl[0][1]))
    
    ctrl_tx_bytes_t.append(sum(ctrl[1][0]))
    ctrl_tx_pckts_t.append(sum(ctrl[1][1]))
    
    # TODO
    # ctrl_reps.append()
    
    # ----------------------------------------------------
    
    app = res[run][5]
    # (app_rx/9, app_tx/9, app_bcast/9)
    # (app_rx_1/2, app_rx_2/2, app_rx_3/2, app_rx_4/2, app_rx_5/2, app_rx_6/2,
    # app_rx_7/2, app_rx_8/2, app_rx_9/2)
    app_rx_1_bytes.append(sum(app[0][0][0]))
    app_rx_1_pckts.append(sum(app[0][0][1]))
    
    app_rx_2_bytes.append(sum(app[0][1][0]))
    app_rx_2_pckts.append(sum(app[0][1][1]))
    
    app_rx_3_bytes.append(sum(app[0][2][0]))
    app_rx_3_pckts.append(sum(app[0][2][1]))
    
    app_rx_4_bytes.append(sum(app[0][3][0]))
    app_rx_4_pckts.append(sum(app[0][3][1]))
    
    app_rx_5_bytes.append(sum(app[0][4][0]))
    app_rx_5_pckts.append(sum(app[0][4][1]))
    
    app_rx_6_bytes.append(sum(app[0][5][0]))
    app_rx_6_pckts.append(sum(app[0][5][1]))
    
    app_rx_7_bytes.append(sum(app[0][6][0]))
    app_rx_7_pckts.append(sum(app[0][6][1]))
    
    app_rx_8_bytes.append(sum(app[0][7][0]))
    app_rx_8_pckts.append(sum(app[0][7][1]))
    
    app_rx_9_bytes.append(sum(app[0][8][0]))
    app_rx_9_pckts.append(sum(app[0][8][1]))
    
    # (app_tx_1/2, app_tx_2/2, app_tx_3/2, app_tx_4/2, app_tx_5/2, app_tx_6/2,
    # app_tx_7/2, app_tx_8/2, app_tx_9/1)
    app_tx_1_bytes.append(sum(app[1][0][0]))
    app_tx_1_pckts.append(sum(app[1][0][1]))
    
    app_tx_2_bytes.append(sum(app[1][1][0]))
    app_tx_2_pckts.append(sum(app[1][1][1]))
    
    app_tx_3_bytes.append(sum(app[1][2][0]))
    app_tx_3_pckts.append(sum(app[1][2][1]))
    
    app_tx_4_bytes.append(sum(app[1][3][0]))
    app_tx_4_pckts.append(sum(app[1][3][1]))
    
    app_tx_5_bytes.append(sum(app[1][4][0]))
    app_tx_5_pckts.append(sum(app[1][4][1]))
    
    app_tx_6_bytes.append(sum(app[1][5][0]))
    app_tx_6_pckts.append(sum(app[1][5][1]))
    
    app_tx_7_bytes.append(sum(app[1][6][0]))
    app_tx_7_pckts.append(sum(app[1][6][1]))
    
    app_tx_8_bytes.append(sum(app[1][7][0]))
    app_tx_8_pckts.append(sum(app[1][7][1]))
    
    app_tx_9_bytes.append(sum(app[1][8][0]))
    app_tx_9_pckts.append(sum(app[1][8][1]))
    
    # (app_bcast_1/2, app_bcast_2/2, app_bcast_3/2, app_bcast_4/2,
    # app_bcast_5/2, app_bcast_6/2, app_bcast_7/2, app_bcast_8/2, app_bcast_9/2)
    app_bcast_1_bytes.append(sum(app[2][0][0]))
    app_bcast_1_pckts.append(sum(app[2][0][1]))
    
    app_bcast_2_bytes.append(sum(app[2][1][0]))
    app_bcast_2_pckts.append(sum(app[2][1][1]))
    
    app_bcast_3_bytes.append(sum(app[2][2][0]))
    app_bcast_3_pckts.append(sum(app[2][2][1]))
    
    app_bcast_4_bytes.append(sum(app[2][3][0]))
    app_bcast_4_pckts.append(sum(app[2][3][1]))
    
    app_bcast_5_bytes.append(sum(app[2][4][0]))
    app_bcast_5_pckts.append(sum(app[2][4][1]))
    
    app_bcast_6_bytes.append(sum(app[2][5][0]))
    app_bcast_6_pckts.append(sum(app[2][5][1]))
    
    app_bcast_7_bytes.append(sum(app[2][6][0]))
    app_bcast_7_pckts.append(sum(app[2][6][1]))
    
    app_bcast_8_bytes.append(sum(app[2][7][0]))
    app_bcast_8_pckts.append(sum(app[2][7][1]))
    
    app_bcast_9_bytes.append(sum(app[2][8][0]))
    app_bcast_9_pckts.append(sum(app[2][8][1]))
    
    # ---------------------------------------------------- PUBLISH
    
    pub = res[run][6]
    # (pub_perc/2, pub_fails, pub_dur/8, pub_obj_size/8, pub_key_size/8,
    # pub_num_tags/8, pub_retries/8, pub_timeout/8, pub_objs_node/8,
    # tags, retries)
    pub_suc.append(pub[0][0] * 100.0 / (pub[0][0] + pub[0][1]))
    
    if pub[2] is not None:
      pub_dur_avg.append(pub[2][0])
      pub_dur_90.append(pub[2][6])
    
    for k, v in pub[9].iteritems():
      aux = pub_tags.get(k)
      if aux is None:
        pub_tags[k] = [v[0]]
      else:
        pub_tags[k].append(v[0])
      
      for k, v in pub[10].iteritems():
        aux = pub_retries.get(k)
        if aux is None:
          pub_retries[k] = [v[0]]
        else:
          pub_retries[k].append(v[0])
    
    # ---------------------------------------------------- UNPUBLISH
    
    unpub = res[run][7]
    # (unpub_perc, unpub_fails, unpub_dur, unpub_retries, unpub_timeout,
    # unpub_objs_node, tags, retries)
    unpub_suc.append(unpub[0][0] * 100.0 / (unpub[0][0] + unpub[0][1]))
    
    if unpub[2] is not None:
      unpub_dur_avg.append(unpub[2][0])
      unpub_dur_90.append(unpub[2][6])
    
    for k, v in unpub[6].iteritems():
      aux = unpub_tags.get(k)
      if aux is None:
        unpub_tags[k] = [v[0]]
      else:
        unpub_tags[k].append(v[0])
    
    for k, v in unpub[7].iteritems():
      aux = unpub_retries.get(k)
      if aux is None:
        unpub_retries[k] = [v[0]]
      else:
        unpub_retries[k].append(v[0])
    
    # ---------------------------------------------------- DOWNLOAD
    
    down = res[run][8]
    # (down_prec, down_fails, down_dur, down_obj_size, down_retries,
    # down_timeout, retries)
    down_suc.append(down[0][0] * 100.0 / (down[0][0] + down[0][1]))
    
    if down[2] is not None:
      down_dur_avg.append(down[2][0])
      down_dur_90.append(down[2][6])
    
    # ---------------------------------------------------- SUBSCRIBE
    
    sub = res[run][9]
    # (sub_prec, sub_fails, sub_dur, sub_retries, sub_timeout, sub_node,
    # tags, retries)
    if is_bcast:
      sub_suc.append(sub[5][0] * 100 / sub[0][0])
    else:
      sub_suc.append(sub[0][0] * 100.0 / (sub[0][0] + sub[0][1]))
    
    if sub[2] is not None:
      sub_dur_avg.append(sub[2][0])
      sub_dur_90.append(sub[2][6])
    
    for k, v in sub[6].iteritems():
      aux = sub_tags.get(k)
      if aux is None:
        sub_tags[k] = [v[0]]
      else:
        sub_tags[k].append(v[0])
      
      for k, v in sub[7].iteritems():
        aux = sub_retries.get(k)
        if aux is None:
          sub_retries[k] = [v[0]]
        else:
          sub_retries[k].append(v[0])
    
    # ---------------------------------------------------- UNSUBSCRIBE
    
    unsub = res[run][10]
    # (unsub_prec, unsub_fails, unsub_dur, unsub_retries, unsub_timeout,
    # unsub_node, tags, retries)
    # TODO
    
    if is_bcast:
      unsub_suc.append(unsub[5][0] * 100 / unsub[0][0])
    else:
      unsub_suc.append(unsub[0][0] * 100.0 / (unsub[0][0] + unsub[0][1]))
    
    if unsub[2] is not None:
      unsub_dur_avg.append(unsub[2][0])
      unsub_dur_90.append(unsub[2][6])
    
    for k, v in unsub[6].iteritems():
      aux = unsub_tags.get(k)
      if aux is None:
        unsub_tags[k] = [v[0]]
      else:
        unsub_tags[k].append(v[0])
    
    for k, v in unsub[7].iteritems():
      aux = unsub_retries.get(k)
      if aux is None:
        unsub_retries[k] = [v[0]]
      else:
        unsub_retries[k].append(v[0])
    
    # ----------------------------------------------------
    
    nots = res[run][11]
    # (nots_prec, nots_time, nots_dur)
    nots_suc.append(nots[0][1] * 100.0 / nots[0][0])
    nots_dur_avg.append(nots[2][0])
    nots_dur_90.append(nots[2][6])
    
    # ----------------------------------------------------
  
  print 'PHY'
  print 'Tx Bytes:', b_to_kb(np.mean(phy_tx_bytes_t)), b_to_kb(np.std(phy_tx_bytes_t)), b_to_kb(get_conf_int(phy_tx_bytes_t))
  print 'Tx Pckts:', np.mean(phy_tx_pckts_t), np.std(phy_tx_pckts_t), get_conf_int(phy_tx_pckts_t)

  print ''

  print 'MAC'
  print 'Tx Bytes:', b_to_kb(np.mean(mac_rx_bytes_t)), b_to_kb(np.std(mac_rx_bytes_t)), b_to_kb(get_conf_int(mac_rx_bytes_t))
  print 'Tx Pckts:', np.mean(mac_tx_pckts_t), np.std(mac_tx_pckts_t), get_conf_int(mac_tx_pckts_t)
  print 'TxMaxFailed Pckts:', np.mean(mac_tx_maxfailed_pckts_t), np.std(mac_tx_maxfailed_pckts_t), get_conf_int(mac_tx_maxfailed_pckts_t)
  
  print ''
  
  print 'IP'
  print 'Tx Bytes:', b_to_kb(np.mean(ip_rx_bytes_t)), b_to_kb(np.std(ip_rx_bytes_t)), b_to_kb(get_conf_int(ip_rx_bytes_t))
  print 'Tx Pckts:', np.mean(ip_tx_pckts_t), np.std(ip_tx_pckts_t), get_conf_int(ip_tx_pckts_t)
  print 'Fwd Bytes:', b_to_kb(np.mean(ip_fwd_bytes_t)), b_to_kb(np.std(ip_fwd_bytes_t)), b_to_kb(get_conf_int(ip_fwd_bytes_t))
  print 'Fwd Pckts:', np.mean(ip_fwd_pckts_t), np.std(ip_fwd_pckts_t), get_conf_int(ip_fwd_pckts_t)
  
  print ''
  
  print 'CTRL'
  print 'Tx Bytes:', b_to_kb(np.mean(ctrl_rx_bytes_t)), b_to_kb(np.std(ctrl_rx_bytes_t)), b_to_kb(get_conf_int(ctrl_rx_bytes_t))
  print 'Tx Pckts:', np.mean(ctrl_tx_pckts_t), np.std(ctrl_tx_pckts_t), get_conf_int(ctrl_tx_pckts_t)
  
  print ''
  
  print 'APP Tx'
  print '  PUB Bytes:', b_to_kb(np.mean(app_tx_1_bytes)), b_to_kb(np.std(app_tx_1_bytes)), b_to_kb(get_conf_int(app_tx_1_bytes))
  print '  PUB Pckts:', np.mean(app_tx_1_pckts), np.std(app_tx_1_pckts), get_conf_int(app_tx_1_pckts)
  print '  UNPUB Bytes:', b_to_kb(np.mean(app_tx_2_bytes)), b_to_kb(np.std(app_tx_2_bytes)), b_to_kb(get_conf_int(app_tx_2_bytes))
  print '  UNPUB Pckts:', np.mean(app_tx_2_pckts), np.std(app_tx_2_pckts), get_conf_int(app_tx_2_pckts)
  print '  DOWN Bytes:', b_to_kb(np.mean(app_tx_3_bytes)), b_to_kb(np.std(app_tx_3_bytes)), b_to_kb(get_conf_int(app_tx_3_bytes))
  print '  DOWN Pckts:', np.mean(app_tx_3_pckts), np.std(app_tx_3_pckts), get_conf_int(app_tx_3_pckts)
  print '  SUB Bytes:', b_to_kb(np.mean(app_tx_4_bytes)), b_to_kb(np.std(app_tx_4_bytes)), b_to_kb(get_conf_int(app_tx_4_bytes))
  print '  SUB Pckts:', np.mean(app_tx_4_pckts), np.std(app_tx_4_pckts), get_conf_int(app_tx_4_pckts)
  print '  UNSUB Bytes:', b_to_kb(np.mean(app_tx_5_bytes)), b_to_kb(np.std(app_tx_5_bytes)), b_to_kb(get_conf_int(app_tx_5_bytes))
  print '  UNSUB Pckts:', np.mean(app_tx_5_pckts), np.std(app_tx_5_pckts), get_conf_int(app_tx_5_pckts)
  print '  NOT Bytes:', b_to_kb(np.mean(app_tx_6_bytes)), b_to_kb(np.std(app_tx_6_bytes)), b_to_kb(get_conf_int(app_tx_6_bytes))
  print '  NOT Pckts:', np.mean(app_tx_6_pckts), np.std(app_tx_6_pckts), get_conf_int(app_tx_6_pckts)
  print '  CTRL Bytes:', b_to_kb(np.mean(app_tx_7_bytes)), b_to_kb(np.std(app_tx_7_bytes)), b_to_kb(get_conf_int(app_tx_7_bytes))
  print '  CTRL Pckts:', np.mean(app_tx_7_pckts), np.std(app_tx_7_pckts), get_conf_int(app_tx_7_pckts)
  print '  ACT Bytes:', b_to_kb(np.mean(app_tx_8_bytes)), b_to_kb(np.std(app_tx_8_bytes)), b_to_kb(get_conf_int(app_tx_8_bytes))
  print '  ACT Pckts:', np.mean(app_tx_8_pckts), np.std(app_tx_8_pckts), get_conf_int(app_tx_8_pckts)
  print '  PASS Bytes:', b_to_kb(np.mean(app_tx_9_bytes)), b_to_kb(np.std(app_tx_9_bytes)), b_to_kb(get_conf_int(app_tx_9_bytes))
  print '  PASS Pckts:', np.mean(app_tx_9_pckts), np.std(app_tx_9_pckts), get_conf_int(app_tx_9_pckts)
  
  print ''
  
  print 'APP Bcast'
  print '  PUB Bytes:', b_to_kb(np.mean(app_bcast_1_bytes)), b_to_kb(np.std(app_bcast_1_bytes)), b_to_kb(get_conf_int(app_bcast_1_bytes))
  print '  PUB Pckts:', np.mean(app_bcast_1_pckts), np.std(app_bcast_1_pckts), get_conf_int(app_bcast_1_pckts)
  print '  UNPUB Bytes:', b_to_kb(np.mean(app_bcast_2_bytes)), b_to_kb(np.std(app_bcast_2_bytes)), b_to_kb(get_conf_int(app_bcast_2_bytes))
  print '  UNPUB Pckts:', np.mean(app_bcast_2_pckts), np.std(app_bcast_2_pckts), get_conf_int(app_bcast_2_pckts)
  print '  DOWN Bytes:', b_to_kb(np.mean(app_bcast_3_bytes)), b_to_kb(np.std(app_bcast_3_bytes)), b_to_kb(get_conf_int(app_bcast_3_bytes))
  print '  DOWN Pckts:', np.mean(app_bcast_3_pckts), np.std(app_bcast_3_pckts), get_conf_int(app_bcast_3_pckts)
  print '  SUB Bytes:', b_to_kb(np.mean(app_bcast_4_bytes)), b_to_kb(np.std(app_bcast_4_bytes)), b_to_kb(get_conf_int(app_bcast_4_bytes))
  print '  SUB Pckts:', np.mean(app_bcast_4_pckts), np.std(app_bcast_4_pckts), get_conf_int(app_bcast_4_pckts)
  print '  UNSUB Bytes:', b_to_kb(np.mean(app_bcast_5_bytes)), b_to_kb(np.std(app_bcast_5_bytes)), b_to_kb(get_conf_int(app_bcast_5_bytes))
  print '  UNSUB Pckts:', np.mean(app_bcast_5_pckts), np.std(app_bcast_5_pckts), get_conf_int(app_bcast_5_pckts)
  print '  NOT Bytes:', b_to_kb(np.mean(app_bcast_6_bytes)), b_to_kb(np.std(app_bcast_6_bytes)), b_to_kb(get_conf_int(app_bcast_6_bytes))
  print '  NOT Pckts:', np.mean(app_bcast_6_pckts), np.std(app_bcast_6_pckts), get_conf_int(app_bcast_6_pckts)
  print '  CTRL Bytes:', b_to_kb(np.mean(app_bcast_7_bytes)), b_to_kb(np.std(app_bcast_7_bytes)), b_to_kb(get_conf_int(app_bcast_7_bytes))
  print '  CTRL Pckts:', np.mean(app_bcast_7_pckts), np.std(app_bcast_7_pckts), get_conf_int(app_bcast_7_pckts)
  print '  ACT Bytes:', b_to_kb(np.mean(app_bcast_8_bytes)), b_to_kb(np.std(app_bcast_8_bytes)), b_to_kb(get_conf_int(app_bcast_8_bytes))
  print '  ACT Pckts:', np.mean(app_bcast_8_pckts), np.std(app_bcast_8_pckts), get_conf_int(app_bcast_8_pckts)
  print '  PASS Bytes:', b_to_kb(np.mean(app_bcast_9_bytes)), b_to_kb(np.std(app_bcast_9_bytes)), b_to_kb(get_conf_int(app_bcast_9_bytes))
  print '  PASS Pckts:', np.mean(app_bcast_9_pckts), np.std(app_bcast_9_pckts), get_conf_int(app_bcast_9_pckts)
  
  print ''
  
  print 'PUB', np.mean(pub_suc), ns_to_ms(np.mean(pub_dur_avg)), \
    ns_to_ms(np.std(pub_dur_avg)), ns_to_ms(np.mean(pub_dur_90)), ns_to_ms(get_conf_int(pub_dur_avg))
  print 'Tags:'
  for k, v in pub_tags.iteritems():
    print k, ns_to_ms(np.mean(v)), ns_to_ms(np.std(v)), ns_to_ms(get_conf_int(v))
  print 'Retries:'
  for k, v in pub_retries.iteritems():
    print k, ns_to_ms(np.mean(v)), ns_to_ms(np.std(v)), ns_to_ms(get_conf_int(v))
  
  print ''
  
  print 'UNPUB', np.mean(unpub_suc), ns_to_ms(np.mean(unpub_dur_avg)), \
    ns_to_ms(np.std(unpub_dur_avg)), ns_to_ms(np.mean(unpub_dur_90)), ns_to_ms(get_conf_int(unpub_dur_avg))
  print 'Tags:'
  for k, v in unpub_tags.iteritems():
    print k, ns_to_ms(np.mean(v)), ns_to_ms(np.std(v)), ns_to_ms(get_conf_int(v))
  print 'Retries:'
  for k, v in unpub_retries.iteritems():
    print k, ns_to_ms(np.mean(v)), ns_to_ms(np.std(v)), ns_to_ms(get_conf_int(v))

  print ''
  
  print 'DOWN', np.mean(down_suc), ns_to_ms(np.mean(down_dur_avg)), \
    ns_to_ms(np.std(down_dur_avg)), ns_to_ms(np.mean(down_dur_90)), ns_to_ms(get_conf_int(down_dur_avg))

  print ''
  
  print 'SUB', np.mean(sub_suc), ns_to_ms(np.mean(sub_dur_avg)), \
    ns_to_ms(np.std(sub_dur_avg)), ns_to_ms(np.mean(sub_dur_90)), ns_to_ms(get_conf_int(sub_dur_avg))
  print 'Tags:'
  for k, v in sub_tags.iteritems():
    print k, ns_to_ms(np.mean(v)), ns_to_ms(np.std(v)), ns_to_ms(get_conf_int(v))
  print 'Retries:'
  for k, v in sub_retries.iteritems():
    print k, ns_to_ms(np.mean(v)), ns_to_ms(np.std(v)), ns_to_ms(get_conf_int(v))

  print ''
  
  print 'UNSUB', np.mean(unsub_suc), ns_to_ms(np.mean(unsub_dur_avg)), \
    ns_to_ms(np.std(unsub_dur_avg)), ns_to_ms(np.mean(unsub_dur_90)), ns_to_ms(get_conf_int(unsub_dur_avg))
  print 'Tags:'
  for k, v in unsub_tags.iteritems():
    print k, ns_to_ms(np.mean(v)), ns_to_ms(np.std(v)), ns_to_ms(get_conf_int(v))
  print 'Retries:'
  for k, v in unsub_retries.iteritems():
    print k, ns_to_ms(np.mean(v)), ns_to_ms(np.std(v)), ns_to_ms(get_conf_int(v))

  print ''

  print 'NOTS', np.mean(nots_suc), ns_to_ms(np.mean(nots_dur_avg)), \
    ns_to_ms(np.std(nots_dur_avg)), ns_to_ms(np.mean(nots_dur_90)), ns_to_ms(get_conf_int(nots_dur_avg))


if __name__ == '__main__':
  main(sys.argv[1:])
