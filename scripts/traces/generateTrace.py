#!/usr/bin/python
import math
import operator
import os
import random
import sys
from datetime import datetime
from os.path import isfile, join

import matplotlib.pyplot as plt
import scipy.stats as stats

# SHOW_PLOT = True
SHOW_PLOT = False
PRINT_OUT = True
# PRINT_OUT = False

CRAWL_DIR = './get-old-tweets/export/euro2016/'

REAL_TIME = 10800.0  # 3 hours in seconds
SIM_TIME = 600.0  # 10 minutes in seconds
WARMUP_TIME = 30.0  # in seconds
START_TIME = 60.0  # in seconds
COOLDOWN_TIME = START_TIME + SIM_TIME  # in seconds (no more ops onwards)
TOTAL_SIM_TIME = 720.0  # 12 minutes in seconds

NODES = [400, 324, 256, 196, 144, 100, 64, 36, 16]
MAX_X = {400: 800, 324: 720, 256: 640, 196: 560, 144: 480, 100: 400, 64: 320,
         36: 240, 16: 160}
MAX_Y = {400: 400, 324: 360, 256: 320, 196: 280, 144: 240, 100: 200, 64: 160,
         36: 120, 16: 80}
CELL_SIZE = 40  # in meters
EMPTY_CELLS = 25  # in percentage; amount of empty cells allowed

MOB_PAUSE = [30, 60, 120]  # in seconds
MOB_SPEED = [1.0, 2.5, 5.0]  # in meters/second
MOV_PROB = 75  # in percentage; prob of a node moving
NODES_MOB_PROB = 65  # in percentage; amount of mobile nodes

MIN_CHURN_DOWN = 60  # in seconds
MAX_CHURN_DOWN = 60  # in seconds
MIN_CHURN_UP = 120  # in seconds
MAX_CHURN_UP = 120  # in seconds
CHANGE_UP_PROB = 75  # in percentage
CHANGE_DOWN_PROB = 75  # in percentage
# in percentage; amount of intermittent nodes
INT_CHURN_PROB = [5, 10, 20, 30, 40, 50]

MIN_CHURN_INT = 300 + START_TIME  # in seconds
MAX_CHURN_INT = 600 + START_TIME  # in seconds
# in percentage; amount of crash nodes
CRASH_CHURN_PROB = [5, 10, 20, 30, 40, 50]

TRACE = 'euro2016'
# TRACE = 'superbowl50'
# TRACE = 'superbowl51'
ONE_TAG_PER_PUB = True
# ONE_TAG_PER_PUB = False
PAST_SUBS_PROB = 60  # in percentage; amount of subs in past
SUB_TAGS = 60  # percentage of used tags (in publish) to subscribe from

FIRST_HALF_SUBS_RATE = 3  # subs per hour
SECOND_HALF_SUBS_RATE = 1  # subs per hour
UNPUBS_RATE = 0.5  # unpubs per hour
UNSUBS_RATE = 0.2  # unsubs per hour

EURO2016_START = datetime.strptime('2016-07-10 20:00', '%Y-%m-%d %H:%M')
EURO2016_END = datetime.strptime('2016-07-10 23:00', '%Y-%m-%d %H:%M')

SUPERBOWL50_START = datetime.strptime('2016-02-07 23:30', '%Y-%m-%d %H:%M')
SUPERBOWL50_END = datetime.strptime('2016-02-08 02:30', '%Y-%m-%d %H:%M')

SUPERBOWL51_START = datetime.strptime('2016-02-05 23:30', '%Y-%m-%d %H:%M')
SUPERBOWL51_END = datetime.strptime('2016-02-06 02:30', '%Y-%m-%d %H:%M')

SEARCH_TAGS = [
  'euro2016final',
  'eurofinal2016',
  '2016eurofinal',
  '2016finaleuro',
  'finaleuro2016',
  'final2016euro',
  # 'euro2016',
  'superbowl50',
  'superbowl51',
  'superbowl',
]


def generate_plot(ops_per_second, num_nodes):
  ops2 = set(ops_per_second)  # remove duplicate times
  # count num of ops in each second
  ops_freq = {op: ops_per_second.count(op) for op in ops2}
  
  for i in xrange(int(SIM_TIME)):
    get = ops_freq.get(i)
    if get is None:  # insert 0 in no-op seconds
      ops_freq[i] = 0
  
  ops_freq = sorted(ops_freq.items(), key=operator.itemgetter(0))
  
  x = [a[0] for a in ops_freq]
  y = [a[1] for a in ops_freq]
  
  if PRINT_OUT:
    print '\\addplot[ybar interval, color=black, fill] coordinates {'
    for i in xrange(len(x)):
      print '\t(%d,%d)' % (x[i], y[i])
    print '}\closedcycle;'
    print '\\addlegendentry{%d nodes}' % num_nodes
  
  plt.plot(x, y)
  plt.ylabel('Num ops')
  plt.xlabel('Time (s)')
  plt.title('Num ops during the event (%d nodes)' % num_nodes)
  plt.ylim(ymin=0)
  plt.grid(True)
  plt.show()


def get_tags_freq(tags):  # receives a list of tags
  _tags = set(tags)  # remove duplicates
  tags_freq = {t: tags.count(t) for t in _tags}  # get tags frequency
  
  sorted_tags_freq = sorted(tags_freq.items(), key=operator.itemgetter(1))
  sorted_tags_freq.reverse()  # most used tags first
  
  return [t[0] for t in sorted_tags_freq]  # extract only the tags


def get_cell(pos, max_cells_x):  # pos[0] - X, pos[1] - Y
  return int(max_cells_x * math.floor(pos[1] / CELL_SIZE)
             + math.floor(pos[0] / CELL_SIZE))


def get_cell_center(cell, max_cells_x):
  x = CELL_SIZE * (cell % max_cells_x + 0.5)
  y = CELL_SIZE * (math.floor(cell / max_cells_x) + 0.5)
  return x, y


def get_rand_pos_in_cell(cell, max_cells_x):
  half = CELL_SIZE / 2.0
  
  cell_center = get_cell_center(cell, max_cells_x)
  x_left = cell_center[0] - half
  x_right = cell_center[0] + half
  y_bottom = cell_center[1] - half
  y_top = cell_center[1] + half
  
  x = random.uniform(x_left, x_right)
  y = random.uniform(y_bottom, y_top)
  return x, y


def calc_distance(pos1, pos2):  # euclidean distance
  return math.sqrt((pos1[0] - pos2[0]) ** 2 + (pos1[1] - pos2[1]) ** 2)


def get_next_node(nodes_time):  # returns node with smaller timestamp
  sorted_ts = sorted(nodes_time.items(), key=operator.itemgetter(1))
  return sorted_ts[0]  # returns id and time of next node (in a tuple)


def get_nodes_in_cell(me, cell, nodes_pos, max_cells_x, nodes_moving):
  res = []
  for k, v in nodes_pos.iteritems():
    if k == me or nodes_moving[k]:  # if it's me, or node is moving
      continue  # skip node
    
    if get_cell(v, max_cells_x) == cell:  # if node is in my cell
      res.append(k)
  
  return res


def get_empty_cells_percentage(population):
  empty_cells = 0
  for _, nodes in population.iteritems():
    if len(nodes) is 0:
      empty_cells += 1
  
  res = (empty_cells * 100.0) / len(population)
  print 'Empty cells: %.2f%% (%d/%d)' % (res, empty_cells, len(population))
  
  return res


def get_adjacent_cells(cell, num_cells, max_cells_x):
  array_pos = [1, max_cells_x + 1, max_cells_x, max_cells_x - 1, -1,
               -max_cells_x - 1, -max_cells_x, -max_cells_x + 1]
  
  neighbors = []
  for i in xrange(8):  # get adjacent neighbor cells
    curr = cell + array_pos[i]
    horiz_diff = (curr % max_cells_x) - (cell % max_cells_x)
    
    if 0 <= curr < num_cells and -1 <= horiz_diff <= 1:
      neighbors.append(curr)
  
  return neighbors


def have_disconnected_cells(num_cells, pop_cells, max_cells_x):
  disconnected_cells = 0
  for c in pop_cells:  # for each populated cell
    neighbors = get_adjacent_cells(c, num_cells, max_cells_x)
    
    pop_neighbors = 0
    for n in neighbors:  # for each neighbor of the current cell
      if n in pop_cells:  # check if it is populated
        pop_neighbors += 1
    
    if pop_neighbors is 0:  # if cell has no populated neighbors
      disconnected_cells += 1
  
  percent = (disconnected_cells * 100.0) / num_cells
  print 'Disconnected cells: %.2f%% (%d/%d)' % (percent, disconnected_cells,
                                                num_cells)
  
  return disconnected_cells > 0


def get_mob_pos(old_pos, new_pos, start_time, curr_time, speed):
  dist = calc_distance(old_pos, new_pos)  # total distance to move
  time_length = curr_time - start_time  # time spent already moving
  
  dist_mov = time_length * speed  # distance already moved
  dist_left = dist - dist_mov  # distance still to move
  
  f = (dist - dist_left) / dist
  x = old_pos[0] + f * (new_pos[0] - old_pos[0])
  y = old_pos[1] + f * (new_pos[1] - old_pos[1])
  
  return x, y


def generate_first_positions(num_nodes):
  first_pos = {}  # node_id: pos
  
  for n in xrange(num_nodes):  # for each node
    x = random.uniform(0, MAX_X[num_nodes])
    y = random.uniform(0, MAX_Y[num_nodes])
    first_pos[n] = (x, y)  # get random position inside max boundaries
  
  return first_pos


def generate_ingress(num_nodes):
  ingress = {}  # node_id: time
  
  for n in xrange(num_nodes):  # for each node
    # get random start time between warmup time and start time [30;60]s
    ingress[n] = random.uniform(WARMUP_TIME, START_TIME - 5.0)
  
  return ingress


def generate_mobility(num_nodes, first_pos, pop_cells, ingress, pause, speed):
  # node_id: [(curr_time, new_time, old_pos, new_pos, speed)]
  mob = {n: [] for n in xrange(num_nodes)}
  
  max_cells_x = math.ceil(MAX_X[num_nodes] / CELL_SIZE)
  
  # choose which nodes are going to move
  num_moving_nodes = int(num_nodes * (NODES_MOB_PROB / 100.0))
  moving_nodes = random.sample(xrange(num_nodes), num_moving_nodes)
  
  pop_cells = list(pop_cells)  # to allow random.choice()
  
  nodes_pos = first_pos.copy()  # current node position
  # True = moving out of cell; False = not moving or moving in same cell
  nodes_moving = {n: False for n in xrange(num_nodes)}
  nodes_pause = {n: True for n in moving_nodes}
  nodes_time = {n: ingress[n] for n in moving_nodes}
  
  while True:
    curr_node, curr_time = get_next_node(nodes_time)
    
    if curr_time >= COOLDOWN_TIME:
      break  # stop generating movement
    
    if nodes_pause[curr_node]:  # node is pausing
      nodes_pause[curr_node] = False  # next moment is a moving moment
      nodes_time[curr_node] += pause
      nodes_moving[curr_node] = False  # node is not moving
    
    else:  # node is moving
      old_pos = nodes_pos[curr_node]
      old_cell = get_cell(old_pos, max_cells_x)
      neighbors = get_nodes_in_cell(curr_node, old_cell, nodes_pos,
                                    max_cells_x, nodes_moving)
      
      movement_prob = MOV_PROB
      if len(neighbors) >= 2:  # if pop. inside cell is greater than 2
        movement_prob += 15  # node has higher prob. of moving
      
      if roll_dice() < movement_prob:  # node moves
        _speed = random.uniform(0, speed)
        
        if len(neighbors) > 0:  # I have neighbors; can move out of my cell
          new_cell = random.choice(pop_cells)  # choose random pop. cell
        else:  # move in my current cell
          new_cell = old_cell
        
        new_pos = get_rand_pos_in_cell(new_cell, max_cells_x)
        
        # calculate travel delay
        dist = calc_distance(old_pos, new_pos)  # distance to move
        new_time = dist / _speed  # duration of movement
        travel_time = curr_time + new_time  # time at the end of movement
        
        move = (curr_time, travel_time, old_pos, new_pos, _speed)
        mob[curr_node].append(move)
        
        nodes_pause[curr_node] = True  # next moment is a pausing moment
        nodes_time[curr_node] = travel_time
        nodes_moving[curr_node] = new_cell != old_cell
        nodes_pos[curr_node] = new_pos  # update node position
      
      else:  # node pauses again
        nodes_pause[curr_node] = False  # next moment is a moving moment
        nodes_time[curr_node] += pause
        nodes_moving[curr_node] = False  # node is not moving
  
  return mob


def generate_crash_churn(num_nodes, crash_prob, population, first_pos):
  crash_churn = {}  # node_id: time
  
  max_cells_x = math.ceil(MAX_X[num_nodes] / CELL_SIZE)
  pop = population.copy()
  
  num_crash_nodes = int(round(num_nodes * (crash_prob / 100.0)))
  nodes = list(xrange(num_nodes))
  crash_nodes = 0
  
  while crash_nodes < num_crash_nodes and len(nodes) > 0:
    chosen_node = random.choice(nodes)  # choose node at random
    cell = get_cell(first_pos[chosen_node], max_cells_x)
    nodes_in_cell = pop[cell]
    
    if len(nodes_in_cell) > 1:
      # only if this node doesn't leave its cell empty
      nodes_in_cell.remove(chosen_node)  # remove node from cell
      crash_churn[chosen_node] = random.uniform(MIN_CHURN_INT, MAX_CHURN_INT)
      crash_nodes += 1
    
    nodes.remove(chosen_node)  # remove node from possible crash nodes
  
  percent = (crash_nodes * 100.0) / num_nodes
  if crash_nodes != num_crash_nodes:
    print 'Crash nodes: %.2f%% (%d/%d) expected: %d%% - %d' \
          % (percent, crash_nodes, num_nodes, crash_prob, num_crash_nodes)
  else:
    print 'Crash nodes: %.2f%% (%d/%d)' % (percent, crash_nodes, num_nodes)
  
  return crash_churn


def generate_intermittent_churn(num_nodes, int_prob, ingress, population,
                                first_pos):
  int_churn = {n: [] for n in xrange(num_nodes)}  # node_id: [(time, is_pause)]
  
  max_cells_x = math.ceil(MAX_X[num_nodes] / CELL_SIZE)
  pop = population.copy()
  
  # choose which nodes are intermittent nodes
  num_int_nodes = int(round(num_nodes * (int_prob / 100.0)))
  int_nodes = random.sample(xrange(num_nodes), num_int_nodes)
  
  is_up = {n: True for n in int_nodes}
  first_time = {n: True for n in int_nodes}
  last_time_was_down = {n: False for n in int_nodes}
  nodes_time = {n: ingress[n] for n in int_nodes}
  
  while True:
    curr_node, curr_time = get_next_node(nodes_time)
    
    if curr_time >= COOLDOWN_TIME:
      break  # stop generating churn
    
    if is_up[curr_node]:  # is up
      if not first_time[curr_node] and last_time_was_down[curr_node]:
        int_churn[curr_node].append((curr_time, False))  # node resumes
      
      first_time[curr_node] = False
      last_time_was_down[curr_node] = False
      nodes_time[curr_node] += random.uniform(MIN_CHURN_UP, MAX_CHURN_UP)
      
      # only if this node doesn't leave its cell empty
      nodes_in_cell = pop[get_cell(first_pos[curr_node], max_cells_x)]
      if roll_dice() < CHANGE_DOWN_PROB and len(nodes_in_cell) > 1:
        nodes_in_cell.remove(curr_node)
        is_up[curr_node] = False
    
    else:  # is down
      if not last_time_was_down[curr_node]:
        int_churn[curr_node].append((curr_time, True))  # node pauses
      
      last_time_was_down[curr_node] = True
      nodes_time[curr_node] += random.uniform(MIN_CHURN_DOWN, MAX_CHURN_DOWN)
      
      if roll_dice() < CHANGE_UP_PROB:
        cell = get_cell(first_pos[curr_node], max_cells_x)
        pop[cell].append(curr_node)
        is_up[curr_node] = True
  
  return int_churn


def generate_publications(num_nodes, tweets_by_user, nodes):
  # node_id: [(time, obj_id, obj_val, summary, [tags])]
  pubs = {n: [] for n in xrange(num_nodes)}
  
  for n in xrange(num_nodes):  # for each node
    for pub in tweets_by_user[nodes[n]]:  # for each tweet of the user
      time = convert_time(pub[1], TRACE) + START_TIME
      obj_id = pub[4]
      obj_val = pub[2]
      summary = obj_val[:int(len(obj_val) / 3)]  # the first third of the tweet
      
      if ONE_TAG_PER_PUB:
        tags = [random.choice(pub[3])]  # choose one tag at random
      else:
        tags = pub[3]  # use all tags
      
      pubs[n].append([time, obj_id, obj_val, summary, tags])
    
    # sort pubs by time
    pubs[n] = sorted(pubs[n], key=(lambda _x: _x[0]))
    
    # adjust pubs with same time
    for i in xrange(len(pubs[n]) - 1):
      if pubs[n][i][0] == pubs[n][i + 1][0]:  # if times are the same
        pubs[n][i][0] += random.uniform(-0.5, 0.5)  # change current pub time
    
    # sort pubs by time again
    pubs[n] = sorted(pubs[n], key=(lambda _x: _x[0]))
  
  return pubs


def get_tags_used_in_pubs(pubs):
  tags = []
  for _, node_pubs in pubs.iteritems():  # for each node
    for pub in node_pubs:  # for each node's publication
      tags.extend(pub[4])  # gather all the tags used in publications
  
  return tags


def generate_subscriptions(num_nodes, pubs):
  # node_id: [(time, sub_id, in_past, tag)]
  subs = {k: [] for k in xrange(num_nodes)}
  tags_used_in_pubs = get_tags_used_in_pubs(pubs)
  tags_for_subs = get_tags_freq(tags_used_in_pubs)
  tags_for_subs = tags_for_subs[:int((SUB_TAGS / 100.0) * len(tags_for_subs))]
  
  for n in xrange(num_nodes):  # for each node
    sub_id = 0
    curr_time = 0.0
    node_tags = list(tags_for_subs)
    
    while curr_time < REAL_TIME / 2.0:  # during the first half of the event
      s = 1.0 / (FIRST_HALF_SUBS_RATE / 3600.0)  # subscription rate
      delay = stats.expon.rvs(scale=s)  # scale = 1 / lambda
      
      curr_time += delay
      time = convert_time(curr_time) + START_TIME
      
      if curr_time < REAL_TIME / 2.0:  # check time
        tag = random.choice(node_tags)
        node_tags.remove(tag)
        in_past = roll_dice() < PAST_SUBS_PROB
        subs[n].append((time, sub_id, in_past, tag))
        
        sub_id += 1
    
    while curr_time < REAL_TIME:  # during the rest of the event
      s = 1.0 / (SECOND_HALF_SUBS_RATE / 3600.0)  # subscription rate
      delay = stats.expon.rvs(scale=s)  # scale = 1 / lambda
      
      curr_time += delay
      time = convert_time(curr_time) + START_TIME
      
      if curr_time < REAL_TIME:  # check time
        tag = random.choice(node_tags)
        node_tags.remove(tag)
        in_past = roll_dice() < (PAST_SUBS_PROB + 10)  # 70%
        subs[n].append((time, sub_id, in_past, tag))
        
        sub_id += 1
    
    # sort subs by time
    subs[n] = sorted(subs[n], key=(lambda _x: _x[0]))
  
  return subs


def generate_unpublications(num_nodes, pubs):
  unpubs = {k: [] for k in xrange(num_nodes)}  # node_id: [(time, obj_id)]
  
  for n in xrange(num_nodes):  # for each node
    if len(pubs[n]) is 0:
      continue  # node with no publications. skip
    
    node_pubs = list(pubs[n])  # copy of node's publications
    # during the second half of the event
    curr_time = max(REAL_TIME / 2.0, node_pubs[0][0])
    
    while curr_time < REAL_TIME:
      s = 1.0 / (UNPUBS_RATE / 3600.0)  # unpublication rate
      delay = stats.expon.rvs(scale=s)  # scale = 1 / lambda
      
      curr_time += delay
      time = convert_time(curr_time) + START_TIME
      
      if curr_time < REAL_TIME:  # check time
        possible_pubs = get_ops_before(node_pubs, time)
        
        if len(possible_pubs) is 0:
          continue  # if no pubs to unpublish at this time, skip this one
        
        chosen_pub = random.choice(possible_pubs)  # choose pub at random
        unpubs[n].append((time, chosen_pub[1]))
        
        node_pubs_size = len(node_pubs)
        node_pubs.remove(chosen_pub)
        assert len(node_pubs) == (node_pubs_size - 1)
    
    # sort unpubs by time
    unpubs[n] = sorted(unpubs[n], key=(lambda _x: _x[0]))
  
  return unpubs


def generate_unsubscriptions(num_nodes, subs):
  unsubs = {k: [] for k in xrange(num_nodes)}  # node_id: [(time, sub_id)]
  
  for n in xrange(num_nodes):  # for each node
    if len(subs[n]) is 0:
      continue  # node with no subscriptions. skip
    
    node_subs = list(subs[n])  # copy of node's subscriptions
    # during the second half of the event
    curr_time = max(REAL_TIME / 2.0, node_subs[0][0])
    
    while curr_time < REAL_TIME:
      s = 1.0 / (UNSUBS_RATE / 3600.0)  # unsubscription rate
      delay = stats.expon.rvs(scale=s)  # scale = 1 / lambda
      
      curr_time += delay
      time = convert_time(curr_time) + START_TIME
      
      if curr_time < REAL_TIME:  # check time
        possible_subs = get_ops_before(node_subs, time)
        
        if len(possible_subs) is 0:
          continue  # if no subs to unsubscribe at this time, skip this one
        
        chosen_sub = random.choice(possible_subs)
        unsubs[n].append((time, chosen_sub[1]))
        
        node_subs_size = len(node_subs)
        node_subs.remove(chosen_sub)
        assert len(node_subs) == (node_subs_size - 1)
    
    # sort unsubs by time
    unsubs[n] = sorted(unsubs[n], key=(lambda _x: _x[0]))
  
  return unsubs


def output_trace_to_file(num_nodes, first_pos, ingress, speed, pause, mob,
                         crash_prob, crash_churn, int_prob, int_churn, pubs,
                         subs, unpubs, unsubs):
  trace_filename = 'n%d-%s-t%d-p%d-s%.1f-c%d-i%d.txt' % (
    num_nodes, TRACE, ONE_TAG_PER_PUB, pause, speed, crash_prob, int_prob)
  trace_file = open(trace_filename, 'w')
  
  max_cells_x = math.ceil(MAX_X[num_nodes] / CELL_SIZE)
  
  # set nodes first position
  for node_id, pos in first_pos.iteritems():
    trace_file.write("$node_(%d) set X_ %f # cell %d\n" % (
      node_id, pos[0], get_cell(pos, max_cells_x)))
    trace_file.write("$node_(%d) set Y_ %f\n" % (node_id, pos[1]))
    # $node_(0) set X_ 329.82427591159615
    # $node_(0) set Y_ 66.06016140869389
  
  trace_file.write('\n')
  
  # nodes ingress
  # sort ingress by time
  _ingress = sorted(ingress.items(), key=operator.itemgetter(1))
  for node_id, time in _ingress:
    trace_file.write('%s$|$%f$|$%d\n' % ('NODE', time, node_id))
  
  trace_file.write('\n')
  
  # mobility
  # node_id: [(curr_time, new_time, old_pos, new_pos, speed)]
  if speed > 0 and COOLDOWN_TIME > pause > 0:
    for node_id, moves in mob.iteritems():
      for v in moves:
        trace_file.write('$ns_ at %f "$node_(%d) setdest %f %f %f"\n' % (
          v[0], node_id, v[3][0], v[3][1], v[4]))
        trace_file.write('# $ns_ at %f "$node_(%d) setdest %f %f 0.0"\n' % (
          v[1], node_id, v[3][0], v[3][1]))
        # $ns_ at 0.0 "$node_(0) setdest 378.375 45.592 0.573"
        # # $ns_ at 91.877459 "$node_(0) setdest 378.375 45.592 0.0"
      
      if len(moves) > 0:
        trace_file.write('\n')
    
    trace_file.write('\n')
  
  # crash churn
  # sort churn by time
  _crash_churn = sorted(crash_churn.items(), key=operator.itemgetter(1))
  if crash_prob > 0:
    for node_id, time in _crash_churn:
      trace_file.write('%s$|$%f$|$%d\n' % ('EXIT', time, node_id))
    
    trace_file.write('\n')
  
  # intermittent churn
  if int_prob > 0:
    for node_id, churn in int_churn.iteritems():  # node_id: [(time, is_pause)]
      for v in churn:
        if v[1]:  # pause
          trace_file.write('%s$|$%f$|$%d\n' % ('PAUSE', v[0], node_id))
        else:  # resume
          trace_file.write('%s$|$%f$|$%d\n' % ('RESUME', v[0], node_id))
      
      if len(churn) > 0:
        trace_file.write('\n')
    
    trace_file.write('\n')
  
  # publications
  # node_id: [(time, obj_id, obj_val, summary, [tags])]
  for node_id, node_pubs in pubs.iteritems():
    for v in node_pubs:
      trace_file.write('%s$|$%f$|$%d$|$%s$|$%s$|$%s$|$%s\n' % (
        'PUB', v[0], node_id, v[1], v[2], v[3], ' '.join(v[4])))
    
    if len(node_pubs) > 0:
      trace_file.write('\n')
  
  trace_file.write('\n')
  
  # subscriptions
  # node_id: [(time, sub_id, in_past, tag)]
  for node_id, node_subs in subs.iteritems():
    for v in node_subs:
      if v[2]:  # in the past
        trace_file.write(
          '%s$|$%f$|$%d$|$%s\n' % ('SUB_P', v[0], node_id, v[3]))
      else:  # in the future
        trace_file.write(
          '%s$|$%f$|$%d$|$%s\n' % ('SUB_F', v[0], node_id, v[3]))
    
    if len(node_subs) > 0:
      trace_file.write('\n')
  
  trace_file.write('\n')
  
  # unpublications
  # node_id: [(time, obj_id)]
  for node_id, node_unpubs in unpubs.iteritems():
    for v in node_unpubs:
      trace_file.write('%s$|$%f$|$%d$|$%s\n' % ('UNPUB', v[0], node_id, v[1]))
    
    if len(node_unpubs) > 0:
      trace_file.write('\n')
  
  trace_file.write('\n')
  
  # unsubscriptions
  # node_id: [(time, sub_id)]
  for node_id, node_unsubs in unsubs.iteritems():
    for v in node_unsubs:
      trace_file.write('%s$|$%f$|$%d$|$%d\n' % ('UNSUB', v[0], node_id, v[1]))
    
    if len(node_unsubs) > 0:
      trace_file.write('\n')
  
  trace_file.close()


def generate_trace(nodes, tweets_by_user, pause, speed, int_prob, crash_prob):
  num_nodes = len(nodes)
  is_mob_on = speed > 0 and COOLDOWN_TIME > pause > 0  # mobility is ON
  is_crash_churn_on = crash_prob > 0  # crash churn is ON
  is_int_churn_on = int_prob > 0  # intermittent churn is ON
  
  # cell related metrics
  max_cells_x = math.ceil(MAX_X[num_nodes] / CELL_SIZE)
  max_cells_y = math.ceil(MAX_Y[num_nodes] / CELL_SIZE)
  num_cells = int(max_cells_x * max_cells_y)
  
  #############################################################################
  # NODES' FIRST POSITION
  while True:  # to emulate a do while loop
    first_pos = generate_first_positions(num_nodes)  # node_id: pos
    
    pop_cells = set()  # populated cells
    population = {c: [] for c in xrange(num_cells)}  # nodes in each cell
    
    for node, pos in first_pos.iteritems():  # find populated cells
      cell = get_cell(pos, max_cells_x)
      pop_cells.add(cell)
      population[cell].append(node)
    
    # check for acceptable conditions
    # get_empty_cells_percentage(population) < EMPTY_CELLS and not \
    get_empty_cells_percentage(population)
    if not have_disconnected_cells(num_cells, pop_cells, max_cells_x):
      break
  
  #############################################################################
  # NODES INGRESS
  ingress = generate_ingress(num_nodes)  # node_id: time
  
  #############################################################################
  # MOBILITY
  # node_id: [(curr_time, new_time, old_pos, new_pos, speed)]
  mob = {n: [] for n in xrange(num_nodes)}
  
  if is_mob_on:
    mob = generate_mobility(num_nodes, first_pos, pop_cells, ingress, pause,
                            speed)
  
  #############################################################################
  # CRASH CHURN
  crash_churn = {}  # node_id: time
  
  if is_crash_churn_on:
    crash_churn = generate_crash_churn(num_nodes, crash_prob, population,
                                       first_pos)
  
  #############################################################################
  # INTERMITTENT CHURN
  int_churn = {k: [] for k in xrange(num_nodes)}  # node_id: [(time, is_pause)]
  
  if is_int_churn_on:
    int_churn = generate_intermittent_churn(num_nodes, int_prob, ingress,
                                            population, first_pos)
  
  #############################################################################
  # PUBLICATIONS
  # node_id: [(time, obj_id, obj_val, summary, [tags])]
  pubs = generate_publications(num_nodes, tweets_by_user, nodes)
  
  if is_mob_on:
    # for node_id, node_pubs in pubs.iteritems():
    #   for pub in node_pubs:
    #     pub_time = pub[0]
    #
    #     for move in mob[node_id]:
    #       if move[0] > pub_time:  # moves are ordered by start time
    #         break
    #
    #       if move[0] <= pub_time <= move[1]:  # publication during movement
    #         start_diff = abs(pub_time - move[0])
    #         end_diff = abs(pub_time - move[1])
    #         if start_diff < end_diff:  # start diff is smaller
    #           pub[0] = move[0] - 3  # anticipate pub before movement by 3s
    #         elif end_diff < start_diff:  # end diff is smaller or are equal
    #           pub[0] = move[1] + 1  # delay pub after movement ends
    #
    #         print 'start:%f end:%f pub:%f new_pub:%f' % (
    #           move[0], move[1], pub_time, pub[0])
    
    pass  # TODO!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  if is_crash_churn_on:  # remove ops after crash time
    for node_id, time in crash_churn.iteritems():
      pubs[node_id] = [x for x in pubs[node_id] if x[0] < time]
  
  if is_int_churn_on:  # remove ops in down moments
    for node_id, churn in int_churn.iteritems():
      pauses = get_intermittent_churn_moments(churn)
      for p in pauses:
        pubs[node_id] = [x for x in pubs[node_id] if x[0] < p[0] or x[0] > p[1]]
  
  #############################################################################
  # SUBSCRIPTIONS
  # node_id: [(time, sub_id, in_past, tag)]
  subs = generate_subscriptions(num_nodes, pubs)
  
  if is_mob_on:
    pass  # TODO!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  if is_crash_churn_on:  # remove ops after crash time
    for node_id, time in crash_churn.iteritems():
      subs[node_id] = [x for x in subs[node_id] if x[0] < time]
  
  if is_int_churn_on:  # remove ops in down moments
    for node_id, churn in int_churn.iteritems():
      pauses = get_intermittent_churn_moments(churn)
      for p in pauses:
        subs[node_id] = [x for x in subs[node_id] if x[0] < p[0] or x[0] > p[1]]
  
  #############################################################################
  # UNPUBLICATIONS
  # node_id: [(time, obj_id)]
  unpubs = generate_unpublications(num_nodes, pubs)
  
  if is_mob_on:
    pass  # TODO!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  if is_crash_churn_on:  # remove ops after crash time
    for node_id, time in crash_churn.iteritems():
      unpubs[node_id] = [x for x in unpubs[node_id] if x[0] < time]
  
  if is_int_churn_on:  # remove ops in down moments
    for node_id, churn in int_churn.iteritems():
      pauses = get_intermittent_churn_moments(churn)
      for p in pauses:
        unpubs[node_id] = [x for x in unpubs[node_id] if
                           x[0] < p[0] or x[0] > p[1]]
  
  #############################################################################
  # UNSUBSCRIPTIONS
  # node_id: [(time, sub_id)]
  unsubs = generate_unsubscriptions(num_nodes, subs)
  
  if is_mob_on:
    pass  # TODO!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  if is_crash_churn_on:  # remove ops after crash time
    for node_id, time in crash_churn.iteritems():
      unsubs[node_id] = [x for x in unsubs[node_id] if x[0] < time]
  
  if is_int_churn_on:  # remove ops in down moments
    for node_id, churn in int_churn.iteritems():
      pauses = get_intermittent_churn_moments(churn)
      for p in pauses:
        unsubs[node_id] = [x for x in unsubs[node_id] if
                           x[0] < p[0] or x[0] > p[1]]
  
  #############################################################################
  # OUTPUT TO FILE
  output_trace_to_file(num_nodes, first_pos, ingress, speed, pause, mob,
                       crash_prob, crash_churn, int_prob, int_churn, pubs, subs,
                       unpubs, unsubs)
  
  return first_pos, ingress, mob, crash_churn, int_churn, pubs, subs, unpubs, \
         unsubs


def get_intermittent_churn_moments(churn):
  res = []
  i = 0
  for _ in xrange(int(len(churn) / 2)):
    res.append((churn[i][0], churn[i + 1][0]))
    i += 2
  
  if len(churn) % 2 != 0:  # is odd
    res.append((churn[len(churn) - 1][0], TOTAL_SIM_TIME))
  
  return res


def get_ops_before(ops, time):
  res = []
  for op in ops:
    if op[0] < time:
      res.append(op)
  
  return res


def roll_dice():  # return number between 0 and 99
  return random.randint(0, 100)


def convert_time(time, trace='realtime'):
  if trace == 'euro2016':
    delta = time - EURO2016_START
  elif trace == 'superbowl50':
    delta = time - SUPERBOWL50_START
  elif trace == 'superbowl51':
    delta = time - SUPERBOWL51_START
  elif trace == 'realtime':  # convert real_time to sim_time
    return (time * SIM_TIME) / REAL_TIME
  else:
    print 'ERROR: trace %s unknown!!!' % trace
    sys.exit()
  
  assert delta.total_seconds() < REAL_TIME
  return (delta.total_seconds() * SIM_TIME) / REAL_TIME


def extract_tweets_info(lines):
  # get all usernames and remove duplicates
  usernames = set([l[0] for l in lines])
  # convert usernames to ids (ints) {int: str}
  user_ids = dict(enumerate(usernames))
  user_ids = {v: k for k, v in user_ids.iteritems()}  # invert dict {str: int}
  print 'Processed a total of %d unique users...' % len(usernames)
  
  tids = set()
  # init map with empty lists
  tweets_by_user_id = {v: [] for _, v in user_ids.iteritems()}
  for line in lines:  # get tweets by user id
    if line[4] not in tids:  # filter duplicate tweets (if need be)
      tids.add(line[4])
      user_id = user_ids[line[0]]  # convert username to user id
      
      # (user_id, date, text, hashtags, tid)
      t = (user_id, line[1], line[2], line[3], line[4])
      tweets_by_user_id[user_id].append(t)
  
  # no more usernames from here on... (just user ids)
  print 'Processed a total of %d unique tweets...' % len(tids)
  
  # get users tweet frequency
  user_tweets_freq = {k: len(v) for k, v in tweets_by_user_id.iteritems()}
  sorted_tweets_freq = sorted(user_tweets_freq.items(),
                              key=operator.itemgetter(1))
  sorted_tweets_freq.reverse()  # most active users first
  
  user_freq = [u[0] for u in sorted_tweets_freq]  # extract only the user_ids
  
  return tweets_by_user_id, user_freq


def filter_lines(lines):
  filtered_lines = []
  for line in lines:
    if EURO2016_START <= line[1] < EURO2016_END:
      # SUPERBOWL50_START <= line[1] < SUPERBOWL50_END or \
      # SUPERBOWL51_START <= line[1] < SUPERBOWL51_END:
      hashtags = [h for h in line[3] if h not in SEARCH_TAGS]  # filter no tags
      
      if len(hashtags) is 0:
        continue  # filter tweets with no hashtags
      
      # (username, date, text, hashtags, tid)
      t = (line[0], line[1], line[2], hashtags, line[4])
      filtered_lines.append(t)
  
  return filtered_lines


def process_line(line):
  # username;date;retweets;favorites;text;geo;mentions;hashtags;id;permalink
  # pearlisaterf;
  # 2016-07-11 00:56;
  # 0;
  # 0;
  # "Even with #france playing such a dirty game in the (...) #sorelosers tho";
  # ;
  # ;
  # #france #FinalEuro2016 #portugal #sorelosers;
  # "752290349129891841";
  # https://twitter.com/pearlisaterf/status/752290349129891841
  row = line.split(';')
  
  username = row[0]
  if username == 'username':
    return None  # skip first (header) line
  
  date = datetime.strptime(row[1], '%Y-%m-%d %H:%M')
  
  idx1 = line.find(';"') + 2
  idx2 = line.find('";')
  text = line[idx1:idx2]  # tweet text
  
  row2 = line[idx2:].split(';')
  
  hashtags = row2[3].lower()  # convert to lower case
  hashtags = hashtags.split(' ')
  hashtags = [h for h in hashtags if len(h) > 0]  # filter blank string
  # remove '#' char (and filter hashtags with only the '#' char)
  hashtags = [h[1:] for h in hashtags if len(h) > 1]
  if len(hashtags) is 0:
    return None  # filter tweets with no hashtags
  
  tid = row2[4]
  tid = tid[1:len(tid) - 1]  # tweet id
  
  return username, date, text, hashtags, tid


def process_file(filename):
  log = open(filename, 'r')  # open file
  
  processed_lines = []
  for line in log:  # process line by line
    res = process_line(line)  # (username, date, text, hashtags, tid)
    if res is not None:
      processed_lines.append(res)  # collect processed lines
  
  log.close()  # close file
  
  return processed_lines


def main(argv):
  if len(argv) != 3:
    print 'Args: <mob> <int_churn> <crash_churn>'
    sys.exit()
  
  directory = CRAWL_DIR  # directory with tweets files to process
  mob = bool(int(argv[0]))  # 0 - false; 1 - true
  int_churn = bool(int(argv[1]))  # 0 - false; 1 - true
  crash_churn = bool(int(argv[2]))  # 0 - false; 1 - true
  
  if int(mob) + int(int_churn) + int(crash_churn) > 1:
    print 'Mob, Int_churn, and Crash_churn are mutually exclusive!'
    sys.exit()
  
  dirs_and_files = os.listdir(directory)
  # filter dirs
  only_files = [f for f in dirs_and_files if isfile(join(directory, f))]
  print 'Files to process:'
  for f in only_files:
    print ' %s' % f
  
  # get files full path names
  only_files = [join(directory, f) for f in only_files]
  
  print ''
  print 'Processing files:'
  processed_lines = []
  i = 1
  tot = len(only_files)
  for f in only_files:
    res = process_file(f)
    print '%d/%d > %d lines processed...' % (i, tot, len(res))
    i += 1
    processed_lines.extend(res)
  
  print ''
  print 'Total lines processed: %d' % len(processed_lines)
  processed_lines = filter_lines(processed_lines)
  print 'Total lines after filtering: %d' % len(processed_lines)
  
  tweets_by_user, user_freq = extract_tweets_info(processed_lines)
  print ''
  print 'Top 5 most active users:'
  for user in user_freq[:5]:
    print ' ', user, len(tweets_by_user[user])
  
  print ''
  print 'Trace: %s' % TRACE
  print ''
  
  print '########## No mobility, No churn:'
  for n in NODES:
    generate_trace(user_freq[:n], tweets_by_user, 720, 0, 0, 0)
    print ''
  
  if mob:
    print '########## Mobility, No churn:'
    for n in NODES:
      for p in MOB_PAUSE:
        for s in MOB_SPEED:
          generate_trace(user_freq[:n], tweets_by_user, p, s, 0, 0)
          print ''
  
  if crash_churn:
    print '########## No mobility, Crash churn:'
    for n in NODES:
      for prob in CRASH_CHURN_PROB:
        generate_trace(user_freq[:n], tweets_by_user, 720, 0, 0, prob)
        print ''
  
  if int_churn:
    print '########## No mobility, Intermittent churn:'
    for n in NODES:
      for prob in INT_CHURN_PROB:
        generate_trace(user_freq[:n], tweets_by_user, 720, 0, prob, 0)
        print ''
        
        # if SHOW_PLOT:
        #   generate_plot(ops, SIM_TIME, n, PRINT_OUT)


if __name__ == '__main__':
  main(sys.argv[1:])  # remove called script file from args list
