from fabric.api import *

env.hosts = [
  'node1',
  'node2',
  'node3',
  'node4',
  'node5',
  'node6',
  'node7',
  'node8',
  'node10',
  'node11',
  'node12',
  'node13',
]
env.user = 'jaasilva'
env.warn_only = True
env.colorize_errors = True

def build_debug():
  with cd('/localhome/jaasilva/thyme/ns-3.24'):
    run('make debug')
    run('make')

def build():
  with cd('/localhome/jaasilva/thyme/ns-3.24'):
    run('make release')
    run('make')

def clone_repo():
  with cd('/localhome/jaasilva'):
    run('git clone git@bitbucket.org:hyrax-nova/thyme.git')

def update_repo():
  with cd('/localhome/jaasilva/thyme'):
    run('git pull')

def compress_logs():
  with cd('/localhome/jaasilva/thyme'):
    with hide('output', 'running'):
      host = run('hostname')
      date = run('date +%d-%m-%y')
    filename = '%s-%s.tar.xz' % (host, date)
    run('tar cJvf %s logs/*.log' % filename)

def move_logs():
  with cd('/localhome/jaasilva/thyme'):
    run('mv *.tar.xz ~/thyme/logs/')

def clean_logs():
  with cd('/localhome/jaasilva/thyme'):
    run('rm ./logs/*.log')

def rm_repo():
  with cd('/localhome/jaasilva'):
    run('rm -rf thyme')

