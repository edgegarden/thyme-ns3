#!/usr/bin/python
import sys
import numpy as np
import matplotlib.pyplot as plt

f = open('6mbps', 'r')

lines = []
for line in f:
	lines.append(line.strip())

occurrences = []
for i in range(9):
	idx = lines.index('e')
	occurrences.append(lines[:idx])
	lines = lines[idx+1:]
occurrences.append(lines)

occs2 = []
for occ in occurrences:
	m = {}
	for x in occ:
		split = x.split(' ')
		m[int(split[0])] = float(split[1])
	occs2.append(m)

m = {i:[] for i in range(5,210)}
for i in range(5, 210):
	for occ in occs2:
		m[i].append(occ[i])

for i in range(5, 210):
	print '(%d,%f)' % (i, np.mean(m[i]))

