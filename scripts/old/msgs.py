#!/usr/bin/python
import sys
import math
import numpy as np

# 1260.01s 15 RP:ProcessPrimaryDataPacket(): [WARN ] #TRACE# 10.0.0.16 recv pckt 115190 PUBLISH_TAG from 10.0.0.18 size: 130 time: 1260005269042 lastHop: 84 myCell: 99 dst: 160 homeCell: 65535
# 1260.01s 15 RP:OneHopSend(): [WARN ] #TRACE# 10.0.0.16 send pckt 115190 PUBLISH_TAG to 10.0.0.18 size: 130 time: 1260005269042 lastHop: 99 myCell: 99 dst: 160 homeCell: 65535
# 855s -1 RP:CellBroadcast(): [WARN ] #TRACE# 10.0.0.3 cbcast pckt 45115 ACTIVE_REP size: 142 time: 855000000000 dst: 95 homeCell: 65535

PCKTS = {}
_PUBS = 0
_SUBS = 0
_DOWNS = 0
_UNPUBS = 0
ps = []

def process_line(line):
  global _PUBS, _SUBS, _DOWNS, _UNPUBS
  if '#TRACE#' in line:
    tokens = line.split(' ')
    tokens = tokens[tokens.index('#TRACE#'):]
    if ' send pckt ' in line:
      pid = int(tokens[4])
      sender = tokens[1]
      recvr = tokens[7]
      time = int(tokens[11])
      lasthop = int(tokens[13])
      mycell = int(tokens[15])
      dst = int(tokens[17])
      homecell = int(tokens[19])
      target = int(tokens[20])
      pos = tokens[21].split(':')
      
      a = (time, 2, sender, recvr, lasthop, mycell, dst, homecell, 'S', target)
      
      if PCKTS.get(pid, -1) != -1:
        PCKTS[pid].append(a)
      else:
        PCKTS[pid] = [a]
    elif ' recv pckt ' in line :#and ' ACTIVE_REP ' not in line:
      
      pid = int(tokens[4])
      op = tokens[5]
      recvr = tokens[1]
      sender = tokens[7]
      time = int(tokens[11])
      lasthop = int(tokens[13])
      mycell = int(tokens[15])
      dst = int(tokens[17])
      homecell = int(tokens[19])
      target = int(tokens[20])
      neighbors = int(tokens[21])
      incell = int(tokens[22])
      dsts = int(tokens[23])
      inrec = int(tokens[24])
      lastgreedy = int(tokens[25])
      firstperhop = int(tokens[26])
      
      if pid not in ps:
        ps.append(pid)
        if 'UNPUBLISH' in op:
          _UNPUBS += 1
        elif 'DOWNLOAD' in op:
          _DOWNS += 1
        elif 'SUBSCRIBE' in op:
          _SUBS += 1
        elif 'PUBLISH' in op:
          _PUBS += 1
      
      a = (time, 1, recvr, sender, lasthop, mycell, dst, homecell, 'R', target, neighbors, incell, dsts, inrec, lastgreedy, firstperhop, op, pid)
      
      if PCKTS.get(pid, -1) != -1:
        PCKTS[pid].append(a)
      else:
        PCKTS[pid] = [a]
    elif ' cbcast pckt ' in line :#and ' ACTIVE_REP ' not in line:
      pid = int(tokens[4])
      sender = tokens[1]
      time = int(tokens[9])
      dst = int(tokens[11])
      homecell = int(tokens[13])
      mycell = int(tokens[14])
      
      a = (time, 3, sender, dst, homecell, mycell, 0, 0, 'B')
      
      if PCKTS.get(pid, -1) != -1:
        PCKTS[pid].append(a)
      else:
        PCKTS[pid] = [a]

def process_file(filename):
  log = open(filename, 'r')
  for line in log:
    process_line(line.strip()) # trim line
  log.close()

def main(argv):
  global _PUBS, _SUBS, _DOWNS, _UNPUBS
  directory = argv[0]
  speed = 0
  pause = 1320
  trace = 'euro2016'
  NODES = 25
  runs = 3
  filenum = 1
  run = 1
  field = 400
  
  filename = directory + 'hyrax-run-example' + '-n' + `NODES` + \
    '-rp' + '-s' + `speed` + '-p' + `pause` + '-f' + `field` + \
    ':' + `field` + '-' + trace + '-' + `filenum` + '-r' + `run` + '.log'
  print filename
  process_file(filename)
  
  fail=0
  for k, v in PCKTS.iteritems():
    s = sorted(v)
    last = len(s) - 1
    
    yes = False
    if s[last][8] == 'R' or s[last][8] == 'B':
    #for step in s:
      # if mycell == dst or mycell == homecell
      #if (step[8] == 'R' and (step[5] == step[6] or step[5] == step[7]) ) or (step[8] == 'B' and (step[5] == step[4])) or (step[8] == 'R' and step[16] == 'DOWNLOAD_LIST'):
      yes = True
      #break
     
    #if not yes:
    if not yes:
      print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>"
    fail += 1
    print len(s)
    for x in s:
      print x
    print 
  
  print 'TOTAL', len(PCKTS), 'FAILS', fail, (fail * 100.0 / len(PCKTS))
  print _PUBS, _UNPUBS, _DOWNS, _SUBS

if __name__ == '__main__':
  main(sys.argv[1:])

