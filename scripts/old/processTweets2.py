#!/usr/bin/python
from datetime import datetime
import operator
import scipy.stats as stats
import random

start = datetime.strptime('2016-02-07 22:30', '%Y-%m-%d %H:%M')
end = datetime.strptime('2016-02-08 03:00', '%Y-%m-%d %H:%M')

csvfile = open('/home/jaasilva/Downloads/GetOldTweets-python-master/Untitled Folder/output_got.csv', 'r')

table={}
users={}
tags={}
i=0
tids=set()

for line in csvfile: #username;date;retweets;favorites;text;geo;mentions;hashtags;id;permalink
  row = line.split(';')
  username = row[0]
  if username == 'username':
    continue
  
  date = datetime.strptime(row[1], '%Y-%m-%d %H:%M') # 2016-07-10 21:45
  idx1 = line.find(';"') + 2
  idx2 = line.find('";')
  text = line[idx1:idx2]
  row2 = line[idx2:].split(';')
  hashtags = row2[3].lower()
  tid = row2[4]
  tid = tid[1:len(tid) - 1]
  
  if date >= start and date <= end and hashtags != '#superbowl':
    hashs = hashtags.split(' ')
    res = []
    for h in hashs:
      h1 = h[1:].lower()
      if h1 != '' and h1 != 'superbowl':
        res.append(h1)
        if tags.get(h1, None) != None:
          tags[h1] += 1
        else:
          tags[h1] = 1
    
    if len(res) > 0:
      idx = users.get(username, None)
      if idx != None:
        username = idx
      else:
        users[username] = i;
        username = i
        i += 1
      
      t = (username, date, text, res, tid);
      
      if tid not in tids:
        tids.add(tid)
        if table.get(username, None) != None:
          table[username].append(t)
        else:
          table[username] = [t]
      else:
        pass

print(len(table))
csvfile.close()

###############################################################################

csvfile = open('/home/jaasilva/Downloads/GetOldTweets-python-master/Untitled Folder/--querysearch "#superbowl50" --since 2016-02-07 --until 2016-02-09.csv', 'r')

for line in csvfile: #username;date;retweets;favorites;text;geo;mentions;hashtags;id;permalink
  row = line.split(';')
  username = row[0]
  if username == 'username':
    continue
  
  date = datetime.strptime(row[1], '%Y-%m-%d %H:%M') # 2016-07-10 21:45
  idx1 = line.find(';"') + 2
  idx2 = line.find('";')
  text = line[idx1:idx2]
  row2 = line[idx2:].split(';')
  hashtags = row2[3].lower()
  tid = row2[4]
  tid = tid[1:len(tid) - 1]
  
  if date >= start and date <= end and hashtags != '#superbowl50':
    hashs = hashtags.split(' ')
    res = []
    for h in hashs:
      h1 = h[1:].lower()
      if h1 != '' and h1 != 'superbowl50':
        res.append(h1)
        if tags.get(h1, None) != None:
          tags[h1] += 1
        else:
          tags[h1] = 1
    
    if len(res) > 0:
      idx = users.get(username, None)
      if idx != None:
        username = idx
      else:
        users[username] = i;
        username = i
        i += 1
      
      t = (username, date, text, res, tid);
      
      if tid not in tids:
        tids.add(tid)
        if table.get(username, None) != None:
          table[username].append(t)
        else:
          table[username] = [t]
      else:
        pass

print(len(table))
csvfile.close()

###############################################################################

csvfile = open('/home/jaasilva/Downloads/GetOldTweets-python-master/--querysearch "#superbowl50" --since 2016-02-07 --until 2016-02-08.csv', 'r')

for line in csvfile: #username;date;retweets;favorites;text;geo;mentions;hashtags;id;permalink
  row = line.split(';')
  username = row[0]
  if username == 'username':
    continue
  
  date = datetime.strptime(row[1], '%Y-%m-%d %H:%M') # 2016-07-10 21:45
  idx1 = line.find(';"') + 2
  idx2 = line.find('";')
  text = line[idx1:idx2]
  row2 = line[idx2:].split(';')
  hashtags = row2[3].lower()
  tid = row2[4]
  tid = tid[1:len(tid) - 1]
  
  if date >= start and date <= end and hashtags != '#superbowl50':
    hashs = hashtags.split(' ')
    res = []
    for h in hashs:
      h1 = h[1:].lower()
      if h1 != '' and h1 != 'superbowl50':
        res.append(h1)
        if tags.get(h1, None) != None:
          tags[h1] += 1
        else:
          tags[h1] = 1
    
    if len(res) > 0:
      idx = users.get(username, None)
      if idx != None:
        username = idx
      else:
        users[username] = i;
        username = i
        i += 1
      
      t = (username, date, text, res, tid);
      
      if tid not in tids:
        tids.add(tid)
        if table.get(username, None) != None:
          table[username].append(t)
        else:
          table[username] = [t]
      else:
        pass

print(len(table))
csvfile.close()

###############################################################################

res={}
for k,v in table.items():
  res[k] = len(v)

sorted_tweets = sorted(res.items(), key=operator.itemgetter(1))
sorted_tweets.reverse()

###############################################################################

sorted_tags = sorted(tags.items(), key=operator.itemgetter(1))
sorted_tags.reverse()

###############################################################################

nodes=400
sim_time=1200 # seconds (20 min.)
tot_time = (end - start).total_seconds()
tracefile = open('trace.txt', 'w')

nodes_tags = {}

for n in range(nodes):
  pubs = table[sorted_tweets[n][0]]
  for pub in pubs:
    tags = pub[3]
    for tag in tags:
      if nodes_tags.get(tag, None) != None:
        nodes_tags[tag] += 1
      else:
        nodes_tags[tag] = 1

nodes_tags = sorted(nodes_tags.items(), key=operator.itemgetter(1))
nodes_tags.reverse()
nodes_tags = nodes_tags[:min(len(nodes_tags), 40)]

###############################################################################

_pubs = 0
_subs = 0
_unpubs = 0

for n in range(nodes):
  # publications
  pubs = table[sorted_tweets[n][0]]
  for pub in pubs:
    delta = pub[1] - start
    t = sim_time / tot_time * delta.total_seconds()
    tid = pub[4]
    text = pub[2]
    summary_len = int(len(text) / 3)
    summary = text[:summary_len]
    tags = pub[3]
    tags_str = ''
    for tag in tags:
      tags_str += ' ' + tag
    tracefile.write('%s$|$%f$|$%d$|$%s$|$%s$|$%s$|$%s\n' % ('PUB', t, n, tid, text, summary, tags_str[1:]))
    _pubs += 1
  
  # subscriptions
  t = 0
  subs = set()
  while t <= sim_time / 3.0:
    delay = stats.expon.rvs(scale=360) # lambda = 10 per hour
    t += delay
    if t <= sim_time:
      tag = random.choice(nodes_tags)
      while tag in subs:
        tag = random.choice(nodes_tags)
      subs.add(tag)
      tracefile.write('%s$|$%f$|$%d$|$%s\n' % ('SUB', t, n, tag[0]))
      _subs += 1
  
  while t <= sim_time:
    delay = stats.expon.rvs(scale=720) # lambda = 5 per hour
    t += delay
    if t <= sim_time:
      tag = random.choice(nodes_tags)
      while tag in subs:
        tag = random.choice(nodes_tags)
      subs.add(tag)
      tracefile.write('%s$|$%f$|$%d$|$%s\n' % ('SUB', t, n, tag[0]))
      _subs += 1
  
  # unpublications
  t = sim_time / 3.0
  while t <= sim_time:
    delay = stats.expon.rvs(scale=3600) # lambda = 1 per hour
    t += delay
    if t <= sim_time:
      tracefile.write('%s$|$%f$|$%d\n' % ('UNPUB', t, n))
      _unpubs += 1

tracefile.close()

print "pubs: %d, subs: %d, unpubs: %d" % (_pubs, _subs, _unpubs)

