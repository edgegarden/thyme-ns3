#!/usr/bin/python
import sys
import numpy as np

class FileProcesser:
  
  def __init__(self):
    pass
  
  def ns_to_ms(self, val):
    return val / 1000000.0
  
  def proc_publish_op(self, line): # 10.0.0.12 PUB 18 752214222428315648 [fiersdetrebleus finaleuro2016 frapor porfra] 63 135000000000
    tokens = line.split(' ')
    source = tokens[0]
    #key_size = int(tokens[2])
    key = tokens[3]
    sub = line.split('[')[1].split(']')
    tags = sub[0].split(' ')
    line2 = sub[1].split(' ')
    obj_size = int(line2[1])
    time = int(line2[2])
    
    self.PUBS_OBJ_SIZE.append(obj_size)
    self.PUBS[(key, source)] = (tags, time)
    
    for tag in tags:
      if self.PUBS_TAGS.get(tag, -1) == -1: # new tag
        self.PUBS_TAGS[tag] = [(key, source)]
      else: # existent tag
        self.PUBS_TAGS[tag].append((key, source))

  def process_unpublish_op(self.line): # 10.0.0.71 UNPUB 752254835966214146 952959852999
    tokens = line.split(' ')
    source = tokens[0]
    key = tokens[2]
    time = int(tokens[3])
    
    UNPUBS[(key, source)] = time













