#!/usr/bin/python
import sys
import numpy as np

NODES = 50
PUBS = {}
PUBS_TAGS = {}
PUBS_OBJ_SIZE = []

UNPUBS = {}

DOWN_TIME = []
DOWN_FAIL_NOT_FOUND = 0
DOWN_FAILS = {}
DOWN_FAILS_TIMEOUT = []

SUB_SUC = []
SUBS = {}
SUBS_B = {}

SUB_NOT = {}

RX_APP_P = [[], [], [], [], [], [], [], []]
TX_APP_P = [[], [], [], [], [], [], [], []]
BCAST_APP_P = [[], [], [], [], [], [], [], []]
FWD_P = []
RX_ROUT_P = []
TX_ROUT_P = []

RX_APP_B = [[], [], [], [], [], [], [], []]
TX_APP_B = [[], [], [], [], [], [], [], []]
BCAST_APP_B = [[], [], [], [], [], [], [], []]
FWD_B = []
RX_ROUT_B = []
TX_ROUT_B = []

# Codes
_ROUTING = ['Rx:', 'Tx:', 'Bcast:', 'Fwd:', 'CtlRx:', 'CtlTx:', 'Nots:']
_APP = ['PUB', 'UNPUB', 'DOWN', 'SUB', 'UNSUB']
_BG_MSG_TYPE = {1:'DOWN_REQ', 2:'DOWN_REP', 10:'SUB', 11:'SUB_NOT', 20:'UNSUB', 30:'JOIN_REQ', 31:'JOIN_REP'}
_MSG_STATUS = {1:'SUCCESS', 2:'FAILURE', 3:'OBJECT_FOUND', 4:'OBJECT_NOT_FOUND', 5:'OWNER_NOT_FOUND', 6:'USED_KEY', 7:'MOVING', 8:'TIMEOUT', 9:'NOT_OWNER', 10:'INVALID_ARG'}

_down_succ_perc = []
_down_time_avg = []
_down_time_std = []
_down_time_max = []
_down_time_min = []
_down_time_50 = []
_down_time_90 = []
_down_time_99 = []

_down_timeout_avg = []
_down_timeout_std = []
_down_timeout_max = []
_down_timeout_min = []
_down_timeout_50 = []
_down_timeout_90 = []
_down_timeout_99 = []

_obj_size_avg = []
_obj_size_std = []
_obj_size_max = []
_obj_size_min = []
_obj_size_50 = []
_obj_size_90 = []
_obj_size_99 = []

_pubs_count = []
_pubs_count_avg = []
_pubs_count_std = []
_pubs_perc = []

_sub_succ_prec = []
_sub_time_avg = []
_sub_time_std = []
_sub_time_max = []
_sub_time_min = []
_sub_time_50 = []
_sub_time_90 = []
_sub_time_99 = []
_sub_spread_avg = []
_sub_spread_std = []
_subs_count = []
_subs_count_avg = []

_not_succ = []
_sub_not_time_avg = []
_sub_not_time_std = []
_sub_not_time_max = []
_sub_not_time_min = []
_sub_not_time_50 = []
_sub_not_time_90 = []
_sub_not_time_99 = []

_rx_app_pckts = [[], [], [], [], [], [], [], []]
_rx_app_bytes = [[], [], [], [], [], [], [], []]
_tx_app_pckts = [[], [], [], [], [], [], [], []]
_tx_app_bytes = [[], [], [], [], [], [], [], []]
_bcast_app_pckts = [[], [], [], [], [], [], [], []]
_bcast_app_bytes = [[], [], [], [], [], [], [], []]
_fwd_pckts = []
_fwd_bytes = []
_rx_rout_pckts = []
_rx_rout_bytes = []
_tx_rout_pckts = []
_tx_rout_bytes = []

def ns_to_ms(val):
  return val / 1000000.0

def process_publish_op(line): # 10.0.0.12 PUB 18 752214222428315648 [fiersdetrebleus finaleuro2016 frapor porfra] 63 135000000000
  tokens = line.split(' ')
  source = tokens[0]
  #key_size = int(tokens[2])
  key = tokens[3]
  sub = line.split('[')[1].split(']')
  tags = sub[0].split(' ')
  line2 = sub[1].split(' ')
  obj_size = int(line2[1])
  time = int(line2[2])
  
  PUBS_OBJ_SIZE.append(obj_size)
  PUBS[(key, source)] = (tags, time)
  
  for tag in tags:
    if PUBS_TAGS.get(tag, -1) == -1: # new tag
      PUBS_TAGS[tag] = [(key, source)]
    else: # existent tag
      PUBS_TAGS[tag].append((key, source))

def process_unpublish_op(line): # 10.0.0.71 UNPUB 752254835966214146 952959852999
  tokens = line.split(' ')
  source = tokens[0]
  key = tokens[2]
  time = int(tokens[3])
  
  UNPUBS[(key, source)] = time

def process_download_op(line):
  global DOWN_FAIL_NOT_FOUND
  tokens = line.split(' ')
  source = tokens[0]
  if ' SUC ' in line: # 10.0.0.87 DOWN SUC 3 25548734
    #status = int(tokens[3])
    duration = int(tokens[4])
    
    DOWN_TIME.append(duration)
  elif ' FAIL ' in line: # 10.0.0.15 DOWN FAIL 8 0 177544544343
    status = int(tokens[3])
    if status == 5: # timeout
      reqId = int(tokens[4])
      time = int(tokens[5])
      
      DOWN_FAILS[(reqId, source)] = time
    else: # status == 4 (OBJECT_NOT_FOUND)
      #status = int(tokens[3])
      #duration = int(tokens[4])
      
      DOWN_FAIL_NOT_FOUND += 1
  elif ' NOID ' in line: # 10.0.0.45 DOWN NOID 31 3 872741322733
    reqId = int(tokens[3])
    #status = int(tokens[4])
    time = int(tokens[5])
    
    DOWN_FAILS_TIMEOUT.append(time - DOWN_FAILS[(reqId, source)])

def process_subscribe_op(line):
  tokens = line.split(' ')
  source = tokens[0]
  if ' B ' in line: # 10.0.0.30 SUB B 1 10.0.0.59 172400100165
    subId = int(tokens[3])
    subscriber = tokens[4]
    time = int(tokens[5])
    
    if SUBS_B.get((subId, subscriber), -1) == -1:
      SUBS_B[(subId, subscriber)] = [time]
    else:
      SUBS_B[(subId, subscriber)].append(time)
  elif ' NOT ' in line and ' NOID ' not in line: # 10.0.0.7 SUB NOT 0 752215940117438464 10.0.0.88 173445930070
    subId = int(tokens[3])
    key = tokens[4]
    owner = tokens[5]
    time = int(tokens[6])
    
    if SUB_NOT.get((key, owner), -1) == -1:
      SUB_NOT[(key, owner)] = [(source, subId, time)]
    else:
      SUB_NOT[(key, owner)].append((source, subId, time))
  elif ' NOT NOID ' in line: # 10.0.0.7 SUB NOT NOID 0 173445930070
    subId = int(tokens[4])
    time = int(tokens[5])
    
    # TODO
  else: # 10.0.0.43 SUB 172487366000 0 'andymurray' 0 172487366000
    start = int(tokens[2])
    end = int(tokens[3])
    query = tokens[4]
    query = query[1:len(query) - 1]
    subId = int(tokens[5])
    time = int(tokens[6])
    
    SUBS[(subId, source)] = (start, end, query, time)

def process_unsubscribe_op(line):
  pass # TODO

def process_app_line(line):
  if ' PUB ' in line:
    process_publish_op(line)
  elif ' UNPUB ' in line:
    process_unpublish_op(line)
  elif ' DOWN ' in line:
    process_download_op(line)
  elif ' SUB ' in line:
    process_subscribe_op(line)
  elif ' UNSUB ' in line:
    process_unsubscribe_op(line)

def process_rx_app_data(line):
  tokens = line.split(' - ')
  pckts = tokens[0].split(' ')
  bytes = tokens[1].split(' ')
  
  RX_APP_P[0].append(int(pckts[0]))
  RX_APP_P[1].append(int(pckts[1]))
  RX_APP_P[2].append(int(pckts[2]))
  RX_APP_P[3].append(int(pckts[3]))
  RX_APP_P[4].append(int(pckts[4]))
  RX_APP_P[5].append(int(pckts[5]))
  RX_APP_P[6].append(int(pckts[6]))
  RX_APP_P[7].append(int(pckts[7])) 
  
  RX_APP_B[0].append(int(bytes[0]))
  RX_APP_B[1].append(int(bytes[1]))
  RX_APP_B[2].append(int(bytes[2]))
  RX_APP_B[3].append(int(bytes[3]))
  RX_APP_B[4].append(int(bytes[4]))
  RX_APP_B[5].append(int(bytes[5]))
  RX_APP_B[6].append(int(bytes[6]))
  RX_APP_B[7].append(int(bytes[7]))

def process_tx_app_data(line):
  tokens = line.split(' - ')
  pckts = tokens[0].split(' ')
  bytes = tokens[1].split(' ')
  
  TX_APP_P[0].append(int(pckts[0]))
  TX_APP_P[1].append(int(pckts[1]))
  TX_APP_P[2].append(int(pckts[2]))
  TX_APP_P[3].append(int(pckts[3]))
  TX_APP_P[4].append(int(pckts[4]))
  TX_APP_P[5].append(int(pckts[5]))
  TX_APP_P[6].append(int(pckts[6]))
  TX_APP_P[7].append(int(pckts[7]))
  
  TX_APP_B[0].append(int(bytes[0]))
  TX_APP_B[1].append(int(bytes[1]))
  TX_APP_B[2].append(int(bytes[2]))
  TX_APP_B[3].append(int(bytes[3]))
  TX_APP_B[4].append(int(bytes[4]))
  TX_APP_B[5].append(int(bytes[5]))
  TX_APP_B[6].append(int(bytes[6]))
  TX_APP_B[7].append(int(bytes[7]))

def process_bcast_app_data(line):
  tokens = line.split(' - ')
  pckts = tokens[0].split(' ')
  bytes = tokens[1].split(' ')
  
  BCAST_APP_P[0].append(int(pckts[0]))
  BCAST_APP_P[1].append(int(pckts[1]))
  BCAST_APP_P[2].append(int(pckts[2]))
  BCAST_APP_P[3].append(int(pckts[3]))
  BCAST_APP_P[4].append(int(pckts[4]))
  BCAST_APP_P[5].append(int(pckts[5]))
  BCAST_APP_P[6].append(int(pckts[6]))
  BCAST_APP_P[7].append(int(pckts[7]))
  
  BCAST_APP_B[0].append(int(bytes[0]))
  BCAST_APP_B[1].append(int(bytes[1]))
  BCAST_APP_B[2].append(int(bytes[2]))
  BCAST_APP_B[3].append(int(bytes[3]))
  BCAST_APP_B[4].append(int(bytes[4]))
  BCAST_APP_B[5].append(int(bytes[5]))
  BCAST_APP_B[6].append(int(bytes[6]))
  BCAST_APP_B[7].append(int(bytes[7]))

def process_fwd_routing_data(line):
  global FWD_P, FWD_B
  tokens = line.split(' ')
  FWD_P.append(int(tokens[0]))
  FWD_B.append(int(tokens[1]))

def process_rx_routing_data(line):
  global RX_ROUT_P, RX_ROUT_B
  tokens = line.split(' ')
  RX_ROUT_P.append(int(tokens[0]))
  RX_ROUT_B.append(int(tokens[1]))

def process_tx_routing_data(line):
  global TX_ROUT_P, TX_ROUT_B
  tokens = line.split(' ')
  TX_ROUT_P.append(int(tokens[0]))
  TX_ROUT_B.append(int(tokens[1]))

def process_nots_routing_data(line):
  tokens = line.split(' ')
  print 'count', tokens[0]

def process_routing_line(line):
  if 'Rx:' in line and 'CtlRx:' not in line:
    process_rx_app_data(line.split('Rx: ')[1])
  elif 'Tx:' in line and 'CtlTx:' not in line:
    process_tx_app_data(line.split('Tx: ')[1])
  elif 'Bcast:' in line:
    process_bcast_app_data(line.split('Bcast: ')[1])
  elif 'Fwd:' in line:
    process_fwd_routing_data(line.split('Fwd: ')[1])
  elif 'CtlRx:' in line:
    process_rx_routing_data(line.split('CtlRx: ')[1])
  elif 'CtlTx:' in line:
    process_tx_routing_data(line.split('CtlTx: ')[1])
  elif 'Nots:' in line:
    process_nots_routing_data(line.split('Nots: ')[1])

def process_line(line):
  global _ROUTING, _APP
  tokens = line.split(' ')

  if tokens[0] in _ROUTING:
    process_routing_line(line)
  elif tokens[1] in _APP:
    process_app_line(line)

def process_file(filename):
  clear_data()
  log = open(filename, 'r')
  for line in log:
    process_line(line.strip()) # trim line
  log.close()

def get_download_general_stats():
  down_succ_size = len(DOWN_TIME)
  down_fail_size = len(DOWN_FAILS) + DOWN_FAIL_NOT_FOUND
  down_total_size = down_succ_size + down_fail_size
  print down_succ_size, down_fail_size
  _down_succ_perc.append(down_succ_size * 100.0 / down_total_size)
  _down_time_avg.append(np.mean(DOWN_TIME))
  _down_time_std.append(np.std(DOWN_TIME))
  _down_time_max.append(max(DOWN_TIME))
  _down_time_min.append(min(DOWN_TIME))
  _down_time_50.append(np.percentile(DOWN_TIME, 50))
  _down_time_90.append(np.percentile(DOWN_TIME, 90))
  _down_time_99.append(np.percentile(DOWN_TIME, 99))

def get_download_timeouts_stats():
  _down_timeout_avg.append(np.mean(DOWN_FAILS_TIMEOUT))
  _down_timeout_std.append(np.std(DOWN_FAILS_TIMEOUT))
  _down_timeout_max.append(max(DOWN_FAILS_TIMEOUT))
  _down_timeout_min.append(min(DOWN_FAILS_TIMEOUT))
  _down_timeout_50.append(np.percentile(DOWN_FAILS_TIMEOUT, 50))
  _down_timeout_90.append(np.percentile(DOWN_FAILS_TIMEOUT, 90))
  _down_timeout_99.append(np.percentile(DOWN_FAILS_TIMEOUT, 99))

def get_object_stats():
  _obj_size_avg.append(np.mean(PUBS_OBJ_SIZE))
  _obj_size_std.append(np.std(PUBS_OBJ_SIZE))
  _obj_size_max.append(max(PUBS_OBJ_SIZE))
  _obj_size_min.append(min(PUBS_OBJ_SIZE))
  _obj_size_50.append(np.percentile(PUBS_OBJ_SIZE, 50))
  _obj_size_90.append(np.percentile(PUBS_OBJ_SIZE, 90))
  _obj_size_99.append(np.percentile(PUBS_OBJ_SIZE, 99))

def get_publish_stats():
  pub_count = {}
  for k, v in PUBS.iteritems():
    if pub_count.get(k[1], -1) == -1:
      pub_count[k[1]] = 1
    else:
      pub_count[k[1]] += 1
  
  _pubs_count.append(sum(pub_count.values()))
  _pubs_count_avg.append(np.mean(pub_count.values()))
  _pubs_count_std.append(np.std(pub_count.values()))
  _pubs_perc.append(np.mean(pub_count.values()) * 100.0 / sum(pub_count.values()))

def get_subscribe_stats():
  subs_fail = 0
  subs_dur = []
  subs_perc = []
  for k, v in SUBS.iteritems():
    if SUBS_B.get(k, -1) == -1:
      subs_fail += 1
    else:
      if len(SUBS_B[k]) < int(NODES * 0.90)-1:
        subs_fail +=1
      else:
        subs_dur.append(max(SUBS_B[k]) - v[3])
        subs_perc.append(len(SUBS_B[k]) * 100.0 / (NODES - 1))
  
  _sub_succ_prec.append(100.0 - (subs_fail * 100.0 / len(SUBS)))
  _sub_time_avg.append(np.mean(subs_dur))
  _sub_time_std.append(np.std(subs_dur))
  _sub_time_max.append(max(subs_dur))
  _sub_time_min.append(min(subs_dur))
  _sub_time_50.append(np.percentile(subs_dur, 50))
  _sub_time_90.append(np.percentile(subs_dur, 90))
  _sub_time_99.append(np.percentile(subs_dur, 99))
  _sub_spread_avg.append(np.mean(subs_perc))
  _sub_spread_std.append(np.std(subs_perc))
  
  sub_count = 0
  for k, v in SUBS.iteritems():
    if SUBS_B.get(k, -1) != -1:
      sub_count += len(SUBS_B[k])
  
  _subs_count.append(len(SUBS))
  _subs_count_avg.append(sub_count * 1.0 / NODES)

def get_notifications_stats():
  not_fail = 0
  not_succ = 0
  for k, v in SUBS.iteritems():
    query = v[2]
    if PUBS_TAGS.get(query, -1) != -1: # tag has publications
      for obj in PUBS_TAGS[query]:
        if sub_match(PUBS[obj], v) and not unpubed(obj, v[3]) and k[1] != obj[1]: # if sub match pub
          if exists_notification(obj, k[1], k[0]):
            not_succ += 1
          else: # no notifications exist
            not_fail += 1
  _not_succ.append((not_succ * 1.0 / (not_succ + not_fail)) * 100.0)
  
  print 'nots', (not_succ + not_fail)
  
  subs_not = []
  for k, v in SUB_NOT.iteritems():
    if PUBS.get(k, -1) != -1:
      pub_time = PUBS[k][1]
      for x in v:
        if SUBS.get((x[1], x[0]), -1) != -1:
          sub_time = SUBS[(x[1], x[0])][3]
          if sub_time <= pub_time:
            subs_not.append(x[2] - pub_time)
          else:
            subs_not.append(x[2] - sub_time)
  
  _sub_not_time_avg.append(np.mean(subs_not))
  _sub_not_time_std.append(np.std(subs_not))
  _sub_not_time_max.append(max(subs_not))
  _sub_not_time_min.append(min(subs_not))
  _sub_not_time_50.append(np.percentile(subs_not, 50))
  _sub_not_time_90.append(np.percentile(subs_not, 90))
  _sub_not_time_99.append(np.percentile(subs_not, 99))

def unpubed(obj, sub_time):
  if UNPUBS.get(obj, -1) != -1:
    return sub_time >= UNPUBS[obj]
  return False

def exists_notification(obj, nid, subid):
  if SUB_NOT.get(obj, -1) != -1: # exist notifications
    for _not in SUB_NOT[obj]: # check if I received notification for this publication
      if _not[0] == nid and _not[1] == subid:
        return True
  return False

def sub_match(pub, sub):
  tags = pub[0]
  pub_time = pub[1]
  start = sub[0]
  end = sub[1]
  query = sub[2]
  
  valid_time = (start == 0 or pub_time >= start) and (end == 0 or pub_time <= end)
  valid_query = query in tags
  
  return valid_time and valid_query

def clear_data():
  global PUBS, PUBS_TAGS, PUBS_OBJ_SIZE, UNPUBS, DOWN_TIME, DOWN_FAIL_NOT_FOUND, DOWN_FAILS, DOWN_FAILS_TIMEOUT, SUB_SUC, SUBS, SUBS_B, SUB_NOT
  PUBS = {}
  PUBS_TAGS = {}
  PUBS_OBJ_SIZE = []

  UNPUBS = {}

  DOWN_TIME = []
  DOWN_FAIL_NOT_FOUND = 0
  DOWN_FAILS = {}
  DOWN_FAILS_TIMEOUT = []

  SUB_SUC = []
  SUBS = {}
  SUBS_B = {}

  SUB_NOT = {}

def print_report():
  print "##### DOWNLOAD #####"
  print "success:", np.mean(_down_succ_perc), "(", np.std(_down_succ_perc), ")"
  print "avg:", ns_to_ms(np.mean(_down_time_avg)), "(", ns_to_ms(np.std(_down_time_avg)), ")", "std:", ns_to_ms(np.mean(_down_time_std)), "(", ns_to_ms(np.std(_down_time_std)), ")"
  print "max:", ns_to_ms(np.mean(_down_time_max)), "(", ns_to_ms(np.std(_down_time_max)), ")", "min:", ns_to_ms(np.mean(_down_time_min)), "(", ns_to_ms(np.std(_down_time_min)), ")"
  print "percentile 50th:", ns_to_ms(np.mean(_down_time_50)), "(", ns_to_ms(np.std(_down_time_50)), ")", "90th:", ns_to_ms(np.mean(_down_time_90)), "(", ns_to_ms(np.std(_down_time_90)), ")", "99th:", ns_to_ms(np.mean(_down_time_99)), "(", ns_to_ms(np.std(_down_time_99)), ")"
  
  if len(_down_timeout_avg) > 0:
    print "=== Timeouts:"
    print "avg:", ns_to_ms(np.mean(_down_timeout_avg)), "(", ns_to_ms(np.std(_down_timeout_avg)), ")", "std:", ns_to_ms(np.mean(_down_timeout_std)), "(", ns_to_ms(np.std(_down_timeout_std)), ")"
    print "max:", ns_to_ms(np.mean(_down_timeout_max)), "(", ns_to_ms(np.std(_down_timeout_max)), ")", "min:", ns_to_ms(np.mean(_down_timeout_min)), "(", ns_to_ms(np.std(_down_timeout_min)), ")"
    print "percentile 50th:", ns_to_ms(np.mean(_down_timeout_50)), "(", ns_to_ms(np.std(_down_timeout_50)), ")", "90th:", ns_to_ms(np.mean(_down_timeout_90)), "(", ns_to_ms(np.std(_down_timeout_90)), ")", "99th:", ns_to_ms(np.mean(_down_timeout_99)), "(", ns_to_ms(np.std(_down_timeout_99)), ")"
  
  print "##### PUBLISH #####"
  print "=== Obj Size:"
  print "avg:", np.mean(_obj_size_avg), "std:", np.mean(_obj_size_std)
  print "max:", np.mean(_obj_size_max), "min:", np.mean(_obj_size_min)
  print "percentile 50th:", np.mean(_obj_size_50), "90th:", np.mean(_obj_size_90), "99th:", np.mean(_obj_size_99)
  
  print "=== Storage:"
  print "total pubs:", np.mean(_pubs_count)
  print "avg/node:", np.mean(_pubs_count_avg), "std:", np.mean(_pubs_count_std)
  print "percentage:", np.mean(_pubs_perc)
  
  print "##### SUBSCRIBE #####"
  print "success:", np.mean(_sub_succ_prec), "(", np.std(_sub_succ_prec), ")"
  print "avg:", ns_to_ms(np.mean(_sub_time_avg)), "(", ns_to_ms(np.std(_sub_time_avg)), ")", "std:", ns_to_ms(np.mean(_sub_time_std)), "(", ns_to_ms(np.std(_sub_time_std)), ")"
  print "max:", ns_to_ms(np.mean(_sub_time_max)), "(", ns_to_ms(np.std(_sub_time_max)), ")", "min:", ns_to_ms(np.mean(_sub_time_min)), "(", ns_to_ms(np.std(_sub_time_min)), ")"
  print "percentile 50th:", ns_to_ms(np.mean(_sub_time_50)), "(", ns_to_ms(np.std(_sub_time_50)), ")", "90th:", ns_to_ms(np.mean(_sub_time_90)), "(", ns_to_ms(np.std(_sub_time_90)), ")", "99th:", ns_to_ms(np.mean(_sub_time_99)), "(", ns_to_ms(np.std(_sub_time_99)), ")"
  print "spread avg:", np.mean(_sub_spread_avg), "(", np.std(_sub_spread_avg), ")", "std:", np.mean(_sub_spread_std), "(", np.std(_sub_spread_std), ")"
  print "=== Storage:"
  print "total subs:", np.mean(_subs_count), "(", np.std(_subs_count), ")"
  print "avg/node:", np.mean(_subs_count_avg), "(", np.std(_subs_count_avg), ")"
  
  print "=== Notifications:"
  print "success:", np.mean(_not_succ), "(", np.std(_not_succ), ")"
  print "avg:", ns_to_ms(np.mean(_sub_not_time_avg)), "(", ns_to_ms(np.std(_sub_not_time_avg)), ")", "std:", ns_to_ms(np.mean(_sub_not_time_std)), "(", ns_to_ms(np.std(_sub_not_time_std)), ")"
  print "max:", ns_to_ms(np.mean(_sub_not_time_max)), "(", ns_to_ms(np.std(_sub_not_time_max)), ")", "min:", ns_to_ms(np.mean(_sub_not_time_min)), "(", ns_to_ms(np.std(_sub_not_time_min)), ")"
  print "percentile 50th:", ns_to_ms(np.mean(_sub_not_time_50)), "(", ns_to_ms(np.std(_sub_not_time_50)), ")", "90th:", ns_to_ms(np.mean(_sub_not_time_90)), "(", ns_to_ms(np.std(_sub_not_time_90)), ")", "99th:", ns_to_ms(np.mean(_sub_not_time_99)), "(", ns_to_ms(np.std(_sub_not_time_99)), ")"
  
  print
  
  print np.mean(RX_APP_P[0]), np.mean(RX_APP_P[1]), np.mean(RX_APP_P[2]), np.mean(RX_APP_P[3]), np.mean(RX_APP_P[4]), np.mean(RX_APP_P[5]), np.mean(RX_APP_P[6]), np.mean(RX_APP_P[7])
  print np.mean(TX_APP_P[0]), np.mean(TX_APP_P[1]), np.mean(TX_APP_P[2]), np.mean(TX_APP_P[3]), np.mean(TX_APP_P[4]), np.mean(TX_APP_P[5]), np.mean(TX_APP_P[6]), np.mean(TX_APP_P[7])
  print np.mean(BCAST_APP_P[0]), np.mean(BCAST_APP_P[1]), np.mean(BCAST_APP_P[2]), np.mean(BCAST_APP_P[3]), np.mean(BCAST_APP_P[4]), np.mean(BCAST_APP_P[5]), np.mean(BCAST_APP_P[6]), np.mean(BCAST_APP_P[7])
  print np.mean(FWD_P), np.std(FWD_P)
  print np.mean(RX_ROUT_P), np.std(RX_ROUT_P)
  print np.mean(TX_ROUT_P), np.std(TX_ROUT_P)
  
  print
  
  print np.mean(RX_APP_B[0]), np.mean(RX_APP_B[1]), np.mean(RX_APP_B[2]), np.mean(RX_APP_B[3]), np.mean(RX_APP_B[4]), np.mean(RX_APP_B[5]), np.mean(RX_APP_B[6]), np.mean(RX_APP_B[7])
  print np.mean(TX_APP_B[0]), np.mean(TX_APP_B[1]), np.mean(TX_APP_B[2]), np.mean(TX_APP_B[3]), np.mean(TX_APP_B[4]), np.mean(TX_APP_B[5]), np.mean(TX_APP_B[6]), np.mean(TX_APP_B[7])
  print np.mean(BCAST_APP_B[0]), np.mean(BCAST_APP_B[1]), np.mean(BCAST_APP_B[2]), np.mean(BCAST_APP_B[3]), np.mean(BCAST_APP_B[4]), np.mean(BCAST_APP_B[5]), np.mean(BCAST_APP_B[6]), np.mean(BCAST_APP_B[7])
  print np.mean(FWD_B), np.std(FWD_B)
  print np.mean(RX_ROUT_B), np.std(RX_ROUT_B)
  print np.mean(TX_ROUT_B), np.std(TX_ROUT_B)
  

def main(argv):
  global NODES
  directory = argv[0]
  speed = 0
  pause = 1320
  trace = 'euro2016'
  NODES = 100
  runs = 3
  filenums = 1
  field = 200
  rp = int(argv[1])
  
  for filenum in range(1, filenums + 1):
    for run in range(1, runs + 1):
      filename = directory + 'broadcast-gnutella-example' + '-n' + `NODES` + \
        '-rp' + `rp` + '-s' + `speed` + '-p' + `pause` + '-f' + `field` + \
        ':' + `field` + '-' + trace + '-' + `filenum` + '-r' + `run` + '.log'
      print filename
      process_file(filename)
      
      get_download_general_stats()
      if len(DOWN_FAILS_TIMEOUT) > 0:
        get_download_timeouts_stats()
      get_object_stats()
      get_publish_stats()
      get_subscribe_stats()
      get_notifications_stats()
      
  print_report()

if __name__ == '__main__':
  main(sys.argv[1:])

### Broadcast

# publish - <address> PUB <keySize> <key> [tags] <objSize> <time>

# unpublish - <address> UNPUB <key> <time>

# download - <address> DOWN SUC/FAIL <status> [<duration>] (if status == TIMEOUT : no duration, but reqId and time)
# download noId - <address> DOWN NOID <reqId> <status> <time>

# subscribe - <address> SUB <start> <end> 'filter' <subId> <time>
# subscribe bcast - <address> SUB B <subId> <subscriber> <time>

# unsubscribe - <address> UNSUB <subId> <time>
# unsubscribe bcast - <address> UNSUB B <subId> <subscriber> <time>

# subscribe notification - <address> SUB NOT <subId> <key> <owner> <time>
# subscribe notification noId - <address> SUB NOT NOID <subId> <time>

# Rx: (pckts) <pub> <unpub> <down> <sub> <unsub> <sub_not> <ctrl> <rep> - (bytes) <pub> <unpub> <down> <sub> <unsub> <sub_not> <ctrl> <rep>
# Tx: (pckts) <pub> <unpub> <down> <sub> <unsub> <sub_not> <ctrl> <rep> - (bytes) <pub> <unpub> <down> <sub> <unsub> <sub_not> <ctrl> <rep>
# Bcast: (pckts) <pub> <unpub> <down> <sub> <unsub> <sub_not> <ctrl> <rep> - (bytes) <pub> <unpub> <down> <sub> <unsub> <sub_not> <ctrl> <rep>
# Fwd: <pckts> <bytes>
# CtlRx: <pckts> <bytes>
# CtlTx: <pckts> <bytes>
# Nots: <num>

# msgType: DOWN_REQ=1, DOWN_REP=2, SUB=10, SUB_NOT=11, UNSUB=20, JOIN_REQ=30, JOIN_REP=31
# status: OBJECT_FOUND=3, OBJECT_NOT_FOUND=4, TIMEOUT=8

### Thyme

# publish - <address> PUB <keySize> <key> [tags] <objSize> <time> <reqId>
# publish success - <address> PUB SUC <reqId> <duration>
# publish failure - <address> PUB FAIL <reqId> <time> <reqState> (always TIMEOUT)
# publish noId - <address> PUB NOID <reqId> <time>

# unpublish - <address> UNPUB <key> <time> <reqId>
# unpublish success - <address> UNPUB SUC <reqId> <duration>
# unpublish failure - <address> UNPUB FAIL <reqId> <time> <reqState> (always TIMEOUT)
# unpublish noId - <address> UNPUB NOID <reqId> <time>

# download - <address> DOWN <reqId> <time>
# download rsp - <address> DOWN RSP <reqId> <status> <duration>
# download success - <address> DOWN SUC <reqId> <duration>
# download failure - <address> DOWN FAIL <reqId> <status> [<duration>] (if status == TIMEOUT : no duration, but time)
# download new location - <address> DOWN INTER <reqId> <duration>
# download noId - <address> DOWN NOID <reqId> <time>

# subscribe - <address> SUB <start> <end> 'filter' <subId> <time> <reqId>
# subscribe success - <address> SUB SUC <reqId> <duration>
# subscribe failure - <address> SUB FAIL <reqId> <time> <reqState> (always TIMEOUT)
# subscribe noId - <address> SUB NOID <reqId> <time>

# unsubscribe - <address> UNSUB <subId> <time> <reqId>
# unsubscribe success - <address> UNSUB SUC <reqId> <duration>
# unsubscribe failure - <address> UNSUB FAIL <reqId> <time> <reqState> (always TIMEOUT)
# unsubscribe noId - <address> UNSUB NOID <reqId> <time>

# subscribe notification - <address> SUB NOT <subId> <key> <owner> <time> <status>
# subscribe notification noId - <address> SUB NOT NOID <subId> <time>

