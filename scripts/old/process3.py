#!/usr/bin/python
import sys
import numpy as np

NODES = 50

REQS = {}

PUBS = {}
PUBS_FAILS = 0
PUBS_TIME = []
PUBS_TAGS = {}
PUBS_FAILS2 = {}
PUBS_FAILS_TIMEOUT = []

UNPUBS = {}
UNPUBS_FAILS = 0
UNPUBS_TIME = []

DOWN_TIME = []
DOWN_FAILS = 0

SUBS_TIME = []
SUBS = {}
SUBS_FAILS = 0

SUB_NOT = {}

RX_APP_P = [[], [], [], [], [], [], [], []]
TX_APP_P = [[], [], [], [], [], [], [], []]
BCAST_APP_P = [[], [], [], [], [], [], [], []]
FWD_P = []
RX_ROUT_P = []
TX_ROUT_P = []

RX_APP_B = [[], [], [], [], [], [], [], []]
TX_APP_B = [[], [], [], [], [], [], [], []]
BCAST_APP_B = [[], [], [], [], [], [], [], []]
FWD_B = []
RX_ROUT_B = []
TX_ROUT_B = []

# Codes
_ROUTING = ['Rx:', 'Tx:', 'Bcast:', 'Fwd:', 'CtlRx:', 'CtlTx:']
_APP = ['PUB', 'UNPUB', 'DOWN', 'SUB', 'UNSUB']
_BG_MSG_TYPE = {1:'DOWN_REQ', 2:'DOWN_REP', 10:'SUB', 11:'SUB_NOT', 20:'UNSUB', 30:'JOIN_REQ', 31:'JOIN_REP'}
_MSG_STATUS = {1:'SUCCESS', 2:'FAILURE', 3:'OBJECT_FOUND', 4:'OBJECT_NOT_FOUND', 5:'OWNER_NOT_FOUND', 6:'USED_KEY', 7:'MOVING', 8:'TIMEOUT', 9:'NOT_OWNER', 10:'INVALID_ARG'}

_down_succ_perc = []
_down_time_avg = []
_down_time_std = []
_down_time_max = []
_down_time_min = []
_down_time_50 = []
_down_time_90 = []
_down_time_99 = []

_pub_succ_perc = []
_pub_time_avg = []
_pub_time_std = []
_pub_time_max = []
_pub_time_min = []
_pub_time_50 = []
_pub_time_90 = []
_pub_time_99 = []

_sub_succ_perc = []
_sub_time_avg = []
_sub_time_std = []
_sub_time_max = []
_sub_time_min = []
_sub_time_50 = []
_sub_time_90 = []
_sub_time_99 = []

_not_succ = []
_sub_not_time_avg = []
_sub_not_time_std = []
_sub_not_time_max = []
_sub_not_time_min = []
_sub_not_time_50 = []
_sub_not_time_90 = []
_sub_not_time_99 = []

_rx_app_pckts = [[], [], [], [], [], [], [], []]
_rx_app_bytes = [[], [], [], [], [], [], [], []]
_tx_app_pckts = [[], [], [], [], [], [], [], []]
_tx_app_bytes = [[], [], [], [], [], [], [], []]
_bcast_app_pckts = [[], [], [], [], [], [], [], []]
_bcast_app_bytes = [[], [], [], [], [], [], [], []]
_fwd_pckts = []
_fwd_bytes = []
_rx_rout_pckts = []
_rx_rout_bytes = []
_tx_rout_pckts = []
_tx_rout_bytes = []

def ns_to_ms(val):
  return val / 1000000.0

def process_publish_op(line): # 10.0.0.12 PUB 18 752214222428315648 [fiersdetrebleus finaleuro2016 frapor porfra] 63 135000000000
  global PUBS_FAILS, LOL
  tokens = line.split(' ')
  source = tokens[0]
  
  if  ' SUC ' in line: # 10.0.0.10 PUB SUC 0 12930953
    reqId = int(tokens[3])
    dur = int(tokens[4])
    
    PUBS_TIME.append(dur)
    del REQS[(reqId, source)]
  elif ' FAIL ' in line: # 10.0.0.11 PUB FAIL 3 627500000000 1/1 4/4
    reqId = int(tokens[3])
    
    
    for tag in REQS[(reqId, source)][1][1]:
      PUBS_TAGS[tag].remove(REQS[(reqId, source)][0])
    del PUBS[REQS[(reqId, source)][0]]
    del REQS[(reqId, source)]
    PUBS_FAILS += 1
  elif ' NOID ' in line:
    pass #TODO
  else: # 10.0.0.10 PUB 18 752208793124175873 [finaleuro2016] 46 70000000000 0
    #key_size = int(tokens[2])
    key = tokens[3]
    sub = line.split('[')[1].split(']')
    tags = sub[0].split(' ')
    line2 = sub[1].split(' ')
    time = int(line2[2])
    reqId = int(line2[3])
    
    PUBS[(key, source)] = (reqId, tags, time)
    REQS[(reqId, source)] = ((key, source), (reqId, tags, time))
    
    for tag in tags:
      if PUBS_TAGS.get(tag, -1) == -1: # new tag
        PUBS_TAGS[tag] = [(key, source)]
      else: # existent tag
        PUBS_TAGS[tag].append((key, source))

def process_unpublish_op(line): # 10.0.0.71 UNPUB 752254835966214146 952959852999
  pass

def process_download_op(line):
  pass

def process_subscribe_op(line):
  pass

def process_unsubscribe_op(line):
  pass # TODO

def process_app_line(line):
  #if ' #TRACE# ' not in line:
  if ' PUB ' in line:
    process_publish_op(line)
  elif ' UNPUB ' in line:
    process_unpublish_op(line)
  elif ' DOWN ' in line:
    process_download_op(line)
  elif ' SUB ' in line:
    process_subscribe_op(line)
  elif ' UNSUB ' in line:
    process_unsubscribe_op(line)

def process_rx_app_data(line):
  pass

def process_tx_app_data(line):
  pass

def process_bcast_app_data(line):
  pass

def process_fwd_routing_data(line):
  pass

def process_rx_routing_data(line):
  pass

def process_tx_routing_data(line):
  pass

def process_nots_routing_data(line):
  pass

def process_routing_line(line):
  if 'Rx:' in line and 'CtlRx:' not in line:
    process_rx_app_data(line.split('Rx: ')[1])
  elif 'Tx:' in line and 'CtlTx:' not in line:
    process_tx_app_data(line.split('Tx: ')[1])
  elif 'Bcast:' in line:
    process_bcast_app_data(line.split('Bcast: ')[1])
  elif 'Fwd:' in line:
    process_fwd_routing_data(line.split('Fwd: ')[1])
  elif 'CtlRx:' in line:
    process_rx_routing_data(line.split('CtlRx: ')[1])
  elif 'CtlTx:' in line:
    process_tx_routing_data(line.split('CtlTx: ')[1])
  elif 'Nots:' in line:
    process_nots_routing_data(line.split('Nots: ')[1])

def process_line(line):
  global _ROUTING, _APP
  tokens = line.split(' ')
  try:
    if 's' not in tokens[0]:
      if tokens[0] in _ROUTING:
        process_routing_line(line)
      elif tokens[1] in _APP:
        process_app_line(line)
  except:
    pass

def process_file(filename):
  clear_data()
  log = open(filename, 'r')
  for line in log:
    process_line(line.strip()) # trim line
  log.close()

def get_download_general_stats():
  pass

def get_publish_stats():
  pub_succ_size = len(PUBS_TIME)
  pub_fail_size = PUBS_FAILS
  pub_total_size = pub_succ_size + pub_fail_size
  print 'pubs', pub_succ_size, pub_fail_size, pub_total_size
  _pub_succ_perc.append(pub_succ_size * 100.0 / pub_total_size)
  _pub_time_avg.append(np.mean(PUBS_TIME))
  _pub_time_std.append(np.std(PUBS_TIME))
  _pub_time_max.append(max(PUBS_TIME))
  _pub_time_min.append(min(PUBS_TIME))
  _pub_time_50.append(np.percentile(PUBS_TIME, 50))
  _pub_time_90.append(np.percentile(PUBS_TIME, 90))
  _pub_time_99.append(np.percentile(PUBS_TIME, 99))

def get_subscribe_stats():
  pass

def get_notifications_stats():
  pass

def unpubed(obj, sub_time):
  if UNPUBS.get(obj, -1) != -1:
    return sub_time >= UNPUBS[obj][1]
  return False

def exists_notification(obj, nid, subid):
  if SUB_NOT.get(obj, -1) != -1: # exist notifications
    for _not in SUB_NOT[obj]: # check if I received notification for this publication
      if _not[0] == nid and _not[1] == subid:
        return True
  return False

def sub_match(pub, sub):
  tags = pub[1]
  pub_time = pub[2]
  start = sub[0]
  end = sub[1]
  query = sub[2]
  
  valid_time = (start == 0 or pub_time >= start) and (end == 0 or pub_time <= end)
  valid_query = query in tags
  
  return valid_time and valid_query

def clear_data():
  global REQS, PUBS, PUBS_FAILS, PUBS_TIME, PUBS_TAGS, UNPUBS, UNPUBS_FAILS, UNPUBS_TIME, DOWN_TIME, DOWN_FAILS, SUBS_TIME, SUBS, SUBS_B, SUBS_FAILS
  REQS = {}

  PUBS = {}
  PUBS_FAILS = 0
  PUBS_TIME = []
  PUBS_TAGS = {}

  UNPUBS = {}
  UNPUBS_FAILS = 0
  UNPUBS_TIME = []

  DOWN_TIME = []
  DOWN_FAILS = 0

  SUBS_TIME = []
  SUBS = {}
  SUBS_FAILS = 0

def print_report():
  print "##### PUBLISH #####"
  print "success:", np.mean(_pub_succ_perc), "(", np.std(_pub_succ_perc), ")"
  print "avg:", ns_to_ms(np.mean(_pub_time_avg)), "(", ns_to_ms(np.std(_pub_time_avg)), ")", "std:", ns_to_ms(np.mean(_pub_time_std)), "(", ns_to_ms(np.std(_pub_time_std)), ")"
  print "max:", ns_to_ms(np.mean(_pub_time_max)), "(", ns_to_ms(np.std(_pub_time_max)), ")", "min:", ns_to_ms(np.mean(_pub_time_min)), "(", ns_to_ms(np.std(_pub_time_min)), ")"
  print "percentile 50th:", ns_to_ms(np.mean(_pub_time_50)), "(", ns_to_ms(np.std(_pub_time_50)), ")", "90th:", ns_to_ms(np.mean(_pub_time_90)), "(", ns_to_ms(np.std(_pub_time_90)), ")", "99th:", ns_to_ms(np.mean(_pub_time_99)), "(", ns_to_ms(np.std(_pub_time_99)), ")"

def main(argv):
  global NODES
  directory = argv[0]
  speed = 0
  pause = 1320
  trace = 'euro2016'
  NODES = 25
  runs = 1
  filenums = 1
  field = 400
  
  for filenum in range(1, filenums + 1):
    for run in range(1, runs + 1):
      filename = directory + 'hyrax-run-example' + '-n' + `NODES` + \
        '-rp' + '-s' + `speed` + '-p' + `pause` + '-f' + `field` + \
        ':' + `field` + '-' + trace + '-' + `filenum` + '-r' + `run` + '.log'
      print filename
      process_file(filename)
      
      get_download_general_stats()
      get_publish_stats()
      get_subscribe_stats()
      get_notifications_stats()
      
  print_report()

if __name__ == '__main__':
  main(sys.argv[1:])

### Broadcast

# publish - <address> PUB <keySize> <key> [tags] <objSize> <time>

# unpublish - <address> UNPUB <key> <time>

# download - <address> DOWN SUC/FAIL <status> [<duration>] (if status == TIMEOUT : no duration, but reqId and time)
# download noId - <address> DOWN NOID <reqId> <status> <time>

# subscribe - <address> SUB <start> <end> 'filter' <subId> <time>
# subscribe bcast - <address> SUB B <subId> <subscriber> <time>

# unsubscribe - <address> UNSUB <subId> <time>
# unsubscribe bcast - <address> UNSUB B <subId> <subscriber> <time>

# subscribe notification - <address> SUB NOT <subId> <key> <owner> <time>
# subscribe notification noId - <address> SUB NOT NOID <subId> <time>

# Rx: (pckts) <pub> <unpub> <down> <sub> <unsub> <sub_not> <ctrl> <rep> - (bytes) <pub> <unpub> <down> <sub> <unsub> <sub_not> <ctrl> <rep>
# Tx: (pckts) <pub> <unpub> <down> <sub> <unsub> <sub_not> <ctrl> <rep> - (bytes) <pub> <unpub> <down> <sub> <unsub> <sub_not> <ctrl> <rep>
# Bcast: (pckts) <pub> <unpub> <down> <sub> <unsub> <sub_not> <ctrl> <rep> - (bytes) <pub> <unpub> <down> <sub> <unsub> <sub_not> <ctrl> <rep>
# Fwd: <pckts> <bytes>
# CtlRx: <pckts> <bytes>
# CtlTx: <pckts> <bytes>
# Nots: <num>

# msgType: DOWN_REQ=1, DOWN_REP=2, SUB=10, SUB_NOT=11, UNSUB=20, JOIN_REQ=30, JOIN_REP=31
# status: OBJECT_FOUND=3, OBJECT_NOT_FOUND=4, TIMEOUT=8

### Thyme

# publish - <address> PUB <keySize> <key> [tags] <objSize> <time> <reqId>
# publish success - <address> PUB SUC <reqId> <duration>
# publish failure - <address> PUB FAIL <reqId> <time> <reqState> (always TIMEOUT)
# publish noId - <address> PUB NOID <reqId> <time>

# unpublish - <address> UNPUB <key> <time> <reqId>
# unpublish success - <address> UNPUB SUC <reqId> <duration>
# unpublish failure - <address> UNPUB FAIL <reqId> <time> <reqState> (always TIMEOUT)
# unpublish noId - <address> UNPUB NOID <reqId> <time>

# download - <address> DOWN <reqId> <time>
# download rsp - <address> DOWN RSP <reqId> <status> <duration>
# download success - <address> DOWN SUC <reqId> <duration>
# download failure - <address> DOWN FAIL <reqId> <status> [<duration>] (if status == TIMEOUT : no duration, but time)
# download new location - <address> DOWN INTER <reqId> <duration>
# download noId - <address> DOWN NOID <reqId> <time>

# subscribe - <address> SUB <start> <end> 'filter' <subId> <time> <reqId>
# subscribe success - <address> SUB SUC <reqId> <duration>
# subscribe failure - <address> SUB FAIL <reqId> <time> <reqState> (always TIMEOUT)
# subscribe noId - <address> SUB NOID <reqId> <time>

# unsubscribe - <address> UNSUB <subId> <time> <reqId>
# unsubscribe success - <address> UNSUB SUC <reqId> <duration>
# unsubscribe failure - <address> UNSUB FAIL <reqId> <time> <reqState> (always TIMEOUT)
# unsubscribe noId - <address> UNSUB NOID <reqId> <time>

# subscribe notification - <address> SUB NOT <subId> <key> <owner> <time> <status>
# subscribe notification noId - <address> SUB NOT NOID <subId> <time>

