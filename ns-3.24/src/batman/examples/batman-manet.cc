/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2010 Hemanth Narra
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Hemanth Narra <hemanth@ittc.ku.com>
 *
 * James P.G. Sterbenz <jpgs@ittc.ku.edu>, director
 * ResiliNets Research Group  http://wiki.ittc.ku.edu/resilinets
 * Information and Telecommunication Technology Center (ITTC)
 * and Department of Electrical Engineering and Computer Science
 * The University of Kansas Lawrence, KS USA.
 *
 * Work supported in part by NSF FIND (Future Internet Design) Program
 * under grant CNS-0626918 (Postmodern Internet Architecture),
 * NSF grant CNS-1050226 (Multilayer Network Resilience Analysis and Experimentation on GENI),
 * US Department of Defense (DoD), and ITTC at The University of Kansas.
 */
#include "ns3/core-module.h"
#include "ns3/mobility-module.h"
#include "ns3/application-container.h"
#include "ns3/flow-monitor.h"
#include "ns3/flow-monitor-helper.h"
#include "ns3/ipv4-flow-classifier.h"
#include "ns3/random-variable-stream.h"
#include "ns3/node-container.h"
#include "ns3/wifi-module.h"
#include "ns3/batman-helper.h"
#include "ns3/internet-stack-helper.h"
#include "ns3/ipv4-address-helper.h"
#include "ns3/ipv4-interface-container.h"
#include "ns3/double.h"
#include "ns3/on-off-helper.h"
#include <iostream>
#include <cmath>

using namespace ns3;

uint16_t port = 9;

NS_LOG_COMPONENT_DEFINE ("BatmanManetExample");

class BatmanManetExample
{
public:
  BatmanManetExample ();
  void CaseRun (uint32_t nWifis,
    uint32_t nSinks,
    double totalTime,
    std::string rate,
    std::string phyMode,
    uint32_t nodeSpeed,
    uint32_t periodicUpdateInterval,
    uint32_t settlingTime,
    double dataStart,
    bool printRoutes,
    std::string CSVfileName);

private:
  uint32_t m_nWifis;
  uint32_t m_nSinks;
  double m_totalTime;
  std::string m_rate;
  std::string m_phyMode;
  uint32_t m_nodeSpeed;
  uint32_t m_periodicUpdateInterval;
  uint32_t m_settlingTime;
  double m_dataStart;
  uint32_t bytesTotal;
  uint32_t packetsReceived;
  bool m_printRoutes;
  std::string m_CSVfileName;

  NodeContainer nodes;
  NetDeviceContainer devices;
  Ipv4InterfaceContainer interfaces;

  Ptr<UniformRandomVariable> start;

private:
  void CreateNodes ();
  void CreateDevices (std::string tr_name);
  void InstallInternetStack (std::string tr_name);
  void InstallApplications ();
  void SetupMobility ();
  void ReceivePacket (Ptr <Socket>);
  Ptr <Socket> SetupPacketReceive (Ipv4Address, Ptr <Node>);
  void CheckThroughput ();

};

int
main (int argc, char **argv)
{
  BatmanManetExample test;
  uint32_t nWifis = 30;
  uint32_t nSinks = 10;
  double totalTime = 100.0;
  std::string rate ("8kbps");
  std::string phyMode ("DsssRate11Mbps");
  uint32_t nodeSpeed = 10; // in m/s
  std::string appl = "all";
  uint32_t periodicUpdateInterval = 15;
  uint32_t settlingTime = 6;
  double dataStart = 50.0;
  bool printRoutingTable = true;
  std::string CSVfileName = "BatmanManetExample.csv";

  CommandLine cmd;
  cmd.AddValue ("nWifis", "Number of wifi nodes[Default:30]", nWifis);
  cmd.AddValue ("nSinks", "Number of wifi sink nodes[Default:10]", nSinks);
  cmd.AddValue ("totalTime", "Total Simulation time[Default:100]", totalTime);
  cmd.AddValue ("phyMode", "Wifi Phy mode[Default:DsssRate11Mbps]", phyMode);
  cmd.AddValue ("rate", "CBR traffic rate[Default:8kbps]", rate);
  cmd.AddValue ("nodeSpeed", "Node speed in RandomWayPoint model[Default:10]", nodeSpeed);
  cmd.AddValue ("periodicUpdateInterval", "Periodic Interval Time[Default=15]", periodicUpdateInterval);
  cmd.AddValue ("settlingTime", "Settling Time before sending out an update for changed metric[Default=6]", settlingTime);
  cmd.AddValue ("dataStart", "Time at which nodes start to transmit data[Default=50.0]", dataStart);
  cmd.AddValue ("printRoutingTable", "print routing table for nodes[Default:1]", printRoutingTable);
  cmd.AddValue ("CSVfileName", "The name of the CSV output file name[Default:BatmanManetExample.csv]", CSVfileName);
  cmd.Parse (argc, argv);

  std::ofstream out (CSVfileName.c_str ());
  out << "SimulationSecond," <<
    "ReceiveRate," <<
    "PacketsReceived," <<
    "NumberOfSinks," <<
    std::endl;
  out.close ();

  SeedManager::SetSeed (12345);

  Config::SetDefault ("ns3::OnOffApplication::PacketSize", StringValue ("1000"));
  Config::SetDefault ("ns3::OnOffApplication::DataRate", StringValue (rate));
  Config::SetDefault ("ns3::WifiRemoteStationManager::NonUnicastMode", StringValue (phyMode));
  Config::SetDefault ("ns3::WifiRemoteStationManager::RtsCtsThreshold", StringValue ("2000"));

  test = BatmanManetExample ();
  test.CaseRun (nWifis, nSinks, totalTime, rate, phyMode, nodeSpeed, periodicUpdateInterval,
    settlingTime, dataStart, printRoutingTable, CSVfileName);

  return 0;
}

BatmanManetExample::BatmanManetExample ()
  : bytesTotal (0),
  packetsReceived (0)
{
  start = CreateObject<UniformRandomVariable> ();
}

void
BatmanManetExample::ReceivePacket (Ptr <Socket> socket)
{
  NS_LOG_UNCOND (Simulator::Now ().GetSeconds () << " Received one packet!");
  Ptr <Packet> packet;
  while (packet = socket->Recv ())
   {
    bytesTotal += packet->GetSize ();
    packetsReceived += 1;
   }
}

void
BatmanManetExample::CheckThroughput ()
{
  double kbs = (bytesTotal * 8.0) / 1000;
  bytesTotal = 0;

  std::ofstream out (m_CSVfileName.c_str (), std::ios::app);

  out << (Simulator::Now ()).GetSeconds () << "," << kbs << "," << packetsReceived << "," << m_nSinks << std::endl;

  out.close ();
  packetsReceived = 0;
  Simulator::Schedule (Seconds (1.0), &BatmanManetExample::CheckThroughput, this);
}

Ptr <Socket>
BatmanManetExample::SetupPacketReceive (Ipv4Address addr, Ptr <Node> node)
{

  TypeId tid = TypeId::LookupByName ("ns3::UdpSocketFactory");
  Ptr <Socket> sink = Socket::CreateSocket (node, tid);
  InetSocketAddress local = InetSocketAddress (addr, port);
  sink->Bind (local);
  sink->SetRecvCallback (MakeCallback (&BatmanManetExample::ReceivePacket, this));

  return sink;
}

void
BatmanManetExample::CaseRun (uint32_t nWifis, uint32_t nSinks, double totalTime, std::string rate,
  std::string phyMode, uint32_t nodeSpeed, uint32_t periodicUpdateInterval, uint32_t settlingTime,
  double dataStart, bool printRoutes, std::string CSVfileName)
{
  m_nWifis = nWifis;
  m_nSinks = nSinks;
  m_totalTime = totalTime;
  m_rate = rate;
  m_phyMode = phyMode;
  m_nodeSpeed = nodeSpeed;
  m_periodicUpdateInterval = periodicUpdateInterval;
  m_settlingTime = settlingTime;
  m_dataStart = dataStart;
  m_printRoutes = printRoutes;
  m_CSVfileName = CSVfileName;

  std::stringstream ss;
  ss << m_nWifis;
  std::string t_nodes = ss.str ();

  std::stringstream ss3;
  ss3 << m_totalTime;
  std::string sTotalTime = ss3.str ();

  //LogComponentEnable ("BatmanRoutingProtocol", (LogLevel) (LOG_LEVEL_ALL | LOG_PREFIX_ALL));
  //LogComponentEnable ("OnOffApplication", (LogLevel) (LOG_LEVEL_ALL | LOG_PREFIX_ALL));
  LogComponentEnable ("BatmanManetExample", (LogLevel) (LOG_LEVEL_ALL | LOG_PREFIX_ALL));
  
  std::string tr_name = "Batman_Manet_" + t_nodes + "Nodes_" + sTotalTime + "SimTime";
  std::cout << "Trace file generated is " << tr_name << ".tr\n";

  CreateNodes ();
  CreateDevices (tr_name);
  SetupMobility ();
  InstallInternetStack (tr_name);
  InstallApplications ();

  Ptr<FlowMonitor> monitor;
  FlowMonitorHelper flowHelper;
  monitor = flowHelper.InstallAll ();

  std::cout << "\nStarting simulation for " << m_totalTime << " s ...\n";

  CheckThroughput ();

  Simulator::Stop (Seconds (m_totalTime));
  Simulator::Run ();

  monitor->CheckForLostPackets ();
  Ptr<Ipv4FlowClassifier> classifier = DynamicCast<Ipv4FlowClassifier> (flowHelper.GetClassifier ());
  std::map<FlowId, FlowMonitor::FlowStats> stats = monitor->GetFlowStats ();
  uint64_t totalPacketsTx = 0, totalPacketsRx = 0, numberOfFlows = 0;
  Time totalDelay = Seconds (0.0);
  for (auto i = stats.begin (); i != stats.end (); ++i)
   {
    Ipv4FlowClassifier::FiveTuple t = classifier->FindFlow (i->first);

    //    std::cout << "Flow " << i->first << " (" << t.sourceAddress << " -> " << t.destinationAddress << ")\n";
    //    std::cout << "  Tx Bytes:   " << i->second.txBytes << "\n";
    //    std::cout << "  Rx Bytes:   " << i->second.rxBytes << "\n";
    //    std::cout << "  Tx Packets:   " << i->second.txPackets << "\n";
    //    std::cout << "  Rx Packets:   " << i->second.rxPackets << "\n";
    //    std::cout << "  Packets lost:   " << i->second.lostPackets << "\n";
    //    std::cout << "  Throughput: " << i->second.rxBytes * 8.0 / 10.0 / 1024 << " Kbps\n";
    totalPacketsTx += i->second.txPackets;
    totalPacketsRx += i->second.rxPackets;
    numberOfFlows++;
    totalDelay += i->second.delaySum;
    //    std::cout << "  Sum Delay:   " << i->second.delaySum.GetSeconds () << "s\n\n";
   }

  std::cout << "Number of flows:   " << numberOfFlows << "\n";
  std::cout << "Flow monitor total # Packets Tx:   " << totalPacketsTx << "\n";
  std::cout << "Flow monitor total # Packets Rx:   " << totalPacketsRx << "\n\n";

  std::cout << "Flow monitor Avg Delay:   " << totalDelay.GetSeconds () << "/" << totalPacketsRx << " = " << (totalDelay.GetSeconds ()) / totalPacketsRx << "\n\n";

  monitor->SerializeToXmlFile ("NameOfFile.xml", true, true);

  Simulator::Destroy ();
}

void
BatmanManetExample::CreateNodes ()
{
  std::cout << "Creating " << (unsigned) m_nWifis << " nodes.\n";
  nodes.Create (m_nWifis);
  NS_ASSERT_MSG (m_nWifis > m_nSinks, "Sinks must be less or equal to the number of nodes in network");
}

void
BatmanManetExample::SetupMobility ()
{
  MobilityHelper mobility;
  ObjectFactory pos;
  pos.SetTypeId ("ns3::RandomRectanglePositionAllocator");
  pos.Set ("X", StringValue ("ns3::UniformRandomVariable[Min=0.0|Max=1000.0]"));
  pos.Set ("Y", StringValue ("ns3::UniformRandomVariable[Min=0.0|Max=1000.0]"));

  Ptr <PositionAllocator> taPositionAlloc = pos.Create ()->GetObject <PositionAllocator> ();
  mobility.SetMobilityModel ("ns3::RandomWaypointMobilityModel", "Speed", StringValue ("ns3::ConstantRandomVariable[Constant=10.0]"),
    "Pause", StringValue ("ns3::ConstantRandomVariable[Constant=2.0]"), "PositionAllocator", PointerValue (taPositionAlloc));
  mobility.SetPositionAllocator (taPositionAlloc);
  mobility.Install (nodes);
}

void
BatmanManetExample::CreateDevices (std::string tr_name)
{
  NqosWifiMacHelper wifiMac = NqosWifiMacHelper::Default ();
  wifiMac.SetType ("ns3::AdhocWifiMac");
  YansWifiPhyHelper wifiPhy = YansWifiPhyHelper::Default ();
  YansWifiChannelHelper wifiChannel;
  wifiChannel.SetPropagationDelay ("ns3::ConstantSpeedPropagationDelayModel");
  wifiChannel.AddPropagationLoss ("ns3::FriisPropagationLossModel");
  wifiPhy.SetChannel (wifiChannel.Create ());
  WifiHelper wifi;
  wifi.SetStandard (WIFI_PHY_STANDARD_80211b);
  wifi.SetRemoteStationManager ("ns3::ConstantRateWifiManager", "DataMode", StringValue (m_phyMode), "ControlMode",
    StringValue (m_phyMode));
  devices = wifi.Install (wifiPhy, wifiMac, nodes);

  AsciiTraceHelper ascii;
  wifiPhy.EnableAsciiAll (ascii.CreateFileStream (tr_name + ".tr"));
  //wifiPhy.EnablePcapAll (tr_name);
}

void
BatmanManetExample::InstallInternetStack (std::string tr_name)
{
  BatmanHelper batman;
  //batman.Set ("PeriodicUpdateInterval", TimeValue (Seconds (m_periodicUpdateInterval)));
  //batman.Set ("SettlingTime", TimeValue (Seconds (m_settlingTime)));
  InternetStackHelper stack;
  stack.SetRoutingHelper (batman);
  stack.Install (nodes);
  Ipv4AddressHelper address;
  address.SetBase ("10.1.1.0", "255.255.255.0");
  interfaces = address.Assign (devices);
  if (m_printRoutes)
   {
    Ptr<OutputStreamWrapper> routingStream = Create<OutputStreamWrapper> ((tr_name + ".routes"), std::ios::out);
    batman.PrintRoutingTableAllAt (Seconds (m_periodicUpdateInterval), routingStream);
   }
}

void
BatmanManetExample::InstallApplications ()
{
  for (uint32_t i = 0; i <= m_nSinks - 1; i++)
   {
    Ptr<Node> node = nodes.Get (i);
    Ipv4Address nodeAddress = node->GetObject<Ipv4> ()->GetAddress (1, 0).GetLocal ();
    Ptr<Socket> sink = SetupPacketReceive (nodeAddress, node);
   }

  for (uint32_t clientNode = 0; clientNode <= m_nWifis - 1; clientNode++)
   {
    for (uint32_t j = 0; j <= m_nSinks - 1; j++)
     {
      OnOffHelper onoff1 ("ns3::UdpSocketFactory", Address (InetSocketAddress (interfaces.GetAddress (j), port)));
      onoff1.SetAttribute ("OnTime", StringValue ("ns3::ConstantRandomVariable[Constant=1.0]"));
      onoff1.SetAttribute ("OffTime", StringValue ("ns3::ConstantRandomVariable[Constant=0.0]"));

      if (j != clientNode)
       {
        ApplicationContainer apps1 = onoff1.Install (nodes.Get (clientNode));
        apps1.Start (Seconds (start->GetValue (m_dataStart, m_dataStart + 1)));
        apps1.Stop (Seconds (m_totalTime));
       }
     }
   }
}

