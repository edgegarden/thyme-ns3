/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#include <fstream>
#include <stdlib.h>
#include <stdint.h>
#include <iostream>
#include <string.h>
#include <vector>
#include <openssl/sha.h>
#include "ns3/application-container.h"
#include "ns3/core-module.h"
#include "ns3/mobility-module.h"
#include "ns3/wifi-module.h" 
#include "ns3/aodv-module.h"
#include "ns3/csma-module.h"
#include "ns3/node-container.h"
#include "ns3/internet-stack-helper.h"
#include "ns3/ipv4-address-helper.h"
#include "ns3/batman-helper.h"
#include "ns3/chord-ps.h"
#include "ns3/chord-ipv4-helper.h"
#include "ns3/object.h"
#include "ns3/nstime.h"
#include "ns3/olsr-routing-protocol.h"
#include "ns3/olsr-helper.h"
#include "ns3/dsdv-helper.h"
#include "ns3/dsr-module.h"
#include "ns3/flow-monitor.h"
#include "ns3/flow-monitor-helper.h"
#include "ns3/ipv4-flow-classifier.h"

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("ChordRun");

class ChordRun
{
public:
  void Start (std::string scriptFile, NodeContainer nodeContainer);

  //Chord
  void InsertVNode (Ptr<ChordIpv4> chordApplication, std::string vNodeName);
  void Lookup (Ptr<ChordIpv4> chordApplication, std::string resourceName);

  //DHash
  void Insert (Ptr<ChordIpv4> chordApplication, std::string resourceName, std::string resourceValue);
  void Retrieve (Ptr<ChordIpv4> chordApplication, std::string resourceName);

  //Crash Testing
  void DetachNode (uint16_t nodeNumber);
  void ReAttachNode (uint16_t nodeNumber);

  // Call backs by Chord Layer
  void JoinSuccess (std::string vNodeName, uint8_t* key, uint8_t numBytes);
  void LookupSuccess (uint8_t* lookupKey, uint8_t lookupKeyBytes, Ipv4Address ipAddress, uint16_t port);
  void LookupFailure (uint8_t* lookupKey, uint8_t lookupKeyBytes);
  void InsertSuccess (uint8_t* key, uint8_t numBytes, uint8_t* object, uint32_t objectBytes);
  void RetrieveSuccess (uint8_t* key, uint8_t numBytes, uint8_t* object, uint32_t objectBytes);
  void InsertFailure (uint8_t* key, uint8_t numBytes, uint8_t* object, uint32_t objectBytes);
  void RetrieveFailure (uint8_t* key, uint8_t numBytes);
  void VNodeKeyOwnership (std::string vNodeName, uint8_t* key, uint8_t keyBytes, uint8_t* predecessorKey, uint8_t predecessorKeyBytes
    , uint8_t* oldPredecessorKey, uint8_t oldPredecessorKeyBytes, Ipv4Address predecessorIp, uint16_t predecessorPort);

  //Statistics
  void TraceRing (std::string vNodeName, uint8_t* key, uint8_t numBytes);
  void VNodeFailure (std::string vNodeName, uint8_t* key, uint8_t numBytes);
  void DumpVNodeInfo (Ptr<ChordIpv4> chordApplication, std::string vNodeName);
  void DumpDHashInfo (Ptr<ChordIpv4> chordApplication);

  //Keyboard Handlers
  void Tokenize (const std::string& str, std::vector<std::string>& tokens, const std::string& delimiters);
  void ProcessCommandTokens (std::vector<std::string> tokens, Time time);

private:
  ChordRun* m_chordRun;
  std::string m_scriptFile;
  NodeContainer m_nodeContainer;
  std::vector<std::string> m_tokens;
  bool m_readyToRead;

  //Print
  void PrintCharArray (uint8_t*, uint32_t, std::ostream&);
  void PrintHexArray (uint8_t*, uint32_t, std::ostream&);
};

void
ChordRun::Start (std::string scriptFile, NodeContainer nodeContainer)
{
  NS_LOG_FUNCTION (this << scriptFile);
  this->m_chordRun = this;
  this->m_nodeContainer = nodeContainer;

  m_readyToRead = false;
  //process script-file
  std::ifstream file;
  file.open (scriptFile.c_str ());
  if (file.is_open ())
   {
    NS_LOG_INFO ("\nReading Script File: " << scriptFile);
    Time time = MilliSeconds (0.0);
    std::string commandLine;
    while (!file.eof ())
     {
      std::getline (file, commandLine, '\n');
      std::cout << "Adding Command: " << commandLine << std::endl;
      m_chordRun->Tokenize (commandLine, m_chordRun -> m_tokens, " ");
      if (m_chordRun -> m_tokens.size () == 0)
       {
        NS_LOG_INFO ("Failed to Tokenize...");
        continue;
       }
      //check for time command
      std::vector<std::string>::iterator iterator = m_chordRun -> m_tokens.begin ();
      if (*iterator == "Time")
       {
        if (m_chordRun -> m_tokens.size () < 2)
         {
          continue;
         }
        iterator++;
        std::istringstream sin (*iterator);
        uint64_t delta;
        sin >> delta;
        time = MilliSeconds (time.GetMilliSeconds () + delta);
        std::cout << "Time Pointer: " << time.GetMilliSeconds () << std::endl;
        m_chordRun -> m_tokens.clear ();
        NS_LOG_INFO ("\n");
        continue;
       }
      //NS_LOG_INFO ("Processing...");
      m_chordRun->ProcessCommandTokens (m_chordRun -> m_tokens, MilliSeconds (time.GetMilliSeconds ()));
      m_chordRun -> m_tokens.clear ();
      NS_LOG_INFO ("\n");
     }
   }
}

void
ChordRun::ProcessCommandTokens (std::vector<std::string> tokens, Time time)
{
  //NS_LOG_INFO ("Processing Command Token...");
  //Process tokens
  std::vector<std::string>::iterator iterator = tokens.begin ();

  std::istringstream sin (*iterator);
  uint16_t nodeNumber;
  sin >> nodeNumber;
  //this command can be in script file
  if (*iterator == "quit")
   {
    NS_LOG_INFO ("Scheduling Command quit...");
    Simulator::Stop (MilliSeconds (time.GetMilliSeconds ()));
    return;
   }
  else if (tokens.size () < 2)
   {
    return;
   }
  Ptr<ChordIpv4> chordApplication = m_nodeContainer.Get (nodeNumber)->GetApplication (0)->GetObject<ChordIpv4> ();

  iterator++;
  if (*iterator == "InsertVNode")
   {
    if (tokens.size () < 3)
     {
      return;
     }
    //extract node name
    iterator++;
    std::string vNodeName = std::string (*iterator);
    NS_LOG_INFO ("Scheduling Command InsertVNode...");
    Simulator::Schedule (MilliSeconds (time.GetMilliSeconds ()), &ChordRun::InsertVNode, this, chordApplication, vNodeName);
    return;
   }
  else if (*iterator == "DumpVNodeInfo")
   {
    if (tokens.size () < 3)
     {
      return;
     }
    //extract node name
    iterator++;
    std::string vNodeName = std::string (*iterator);
    NS_LOG_INFO ("Scheduling Command DumpVNodeInfo...");
    Simulator::Schedule (MilliSeconds (time.GetMilliSeconds ()), &ChordRun::DumpVNodeInfo, this, chordApplication, vNodeName);
   }

  else if (*iterator == "DumpDHashInfo")
   {
    NS_LOG_INFO ("Scheduling Command DumpDHashInfo...");
    Simulator::Schedule (MilliSeconds (time.GetMilliSeconds ()), &ChordRun::DumpDHashInfo, this, chordApplication);
   }

  else if (*iterator == "TraceRing")
   {
    if (tokens.size () < 3)
     {
      return;
     }
    //extract node name
    iterator++;
    std::string vNodeName = std::string (*iterator);
    NS_LOG_INFO ("Scheduling Command TraceRing...");
    Simulator::Schedule (MilliSeconds (time.GetMilliSeconds ()), &ChordIpv4::FireTraceRing, chordApplication, vNodeName);
   }
  else if (*iterator == "Lookup")
   {
    if (tokens.size () < 3)
     {
      return;
     }
    //extract node resourceName
    iterator++;
    std::string resourceName = std::string (*iterator);
    Simulator::Schedule (MilliSeconds (time.GetMilliSeconds ()), &ChordRun::Lookup, this, chordApplication, resourceName);
    return;
   }
  else if (*iterator == "Retrieve")
   {
    if (tokens.size () < 3)
     {
      return;
     }
    iterator++;
    std::string resourceName = std::string (*iterator);
    Simulator::Schedule (MilliSeconds (time.GetMilliSeconds ()), &ChordRun::Retrieve, this, chordApplication, resourceName);
   }
  else if (*iterator == "RemoveVNode")
   {
    if (tokens.size () < 3)
     {
      return;
     }
    //extract node resourceName
    iterator++;
    std::string vNodeName = std::string (*iterator);
    NS_LOG_INFO ("Scheduling Command RemoveVNode...");
    Simulator::Schedule (MilliSeconds (time.GetMilliSeconds ()), &ChordIpv4::RemoveVNode, chordApplication, vNodeName);
   }
  else if (*iterator == "Detach")
   {
    NS_LOG_INFO ("Scheduling Command Detach...");
    Simulator::Schedule (MilliSeconds (time.GetMilliSeconds ()), &ChordRun::DetachNode, this, nodeNumber);
   }
  else if (*iterator == "ReAttach")
   {
    NS_LOG_INFO ("Scheduling Command ReAttach...");
    Simulator::Schedule (MilliSeconds (time.GetMilliSeconds ()), &ChordRun::ReAttachNode, this, nodeNumber);
   }
  else if (*iterator == "FixFinger")
   {
    iterator++;
    std::string vNodeName = std::string (*iterator);
    NS_LOG_INFO ("Scheduling Command FixFinger...");
    Simulator::Schedule (MilliSeconds (time.GetMilliSeconds ()), &ChordIpv4::FixFingers, chordApplication, vNodeName);
   }
  else if (*iterator == "Insert")
   {
    if (tokens.size () < 4)
     {
      return;
     }
    iterator++;
    std::string resourceName = std::string (*iterator);

    iterator++;
    std::string resourceValue = std::string (*iterator);
    NS_LOG_INFO ("Scheduling Command Insert...");
    Simulator::Schedule (MilliSeconds (time.GetMilliSeconds ()), &ChordRun::Insert, this, chordApplication, resourceName, resourceValue);
   }
  else
   {
    std::cout << "Unrecognized command\n";
   }
}

void
ChordRun::InsertVNode (Ptr<ChordIpv4> chordApplication, std::string vNodeName)
{
  NS_LOG_FUNCTION (this << vNodeName);
  //std::cout << "Current Simulation Time: " << Simulator::Now ().GetMilliSeconds () << std::endl;
  unsigned char* md = (unsigned char*) malloc (20);
  const unsigned char* message = (const unsigned char*) vNodeName.c_str ();
  SHA1 (message, vNodeName.length (), md);

  //NS_LOG_INFO ("Scheduling Command InsertVNode...");
  //chordApplication->InsertVNode (vNodeName, md, 20);
  free (md);
  std::cout << "\n" << std::endl;
}

void
ChordRun::Lookup (Ptr<ChordIpv4> chordApplication, std::string resourceName)
{
  NS_LOG_FUNCTION (this << resourceName);
  //std::cout << "Current Simulation Time: " << Simulator::Now ().GetMilliSeconds () << std::endl;
  unsigned char* md = (unsigned char*) malloc (20);
  const unsigned char* message = (const unsigned char*) resourceName.c_str ();
  SHA1 (message, resourceName.length (), md);
  //chordApplication->PrivLookupKey (md, 20);
  free (md);
  std::cout << "\n" << std::endl;
}

void
ChordRun::Insert (Ptr<ChordIpv4> chordApplication, std::string resourceName, std::string resourceValue)
{
  NS_LOG_FUNCTION (this << resourceName << resourceValue);
  //std::cout << "Current Simulation Time: " << Simulator::Now ().GetMilliSeconds () << std::endl;
  //NS_LOG_INFO ("Insert ResourceName : " << resourceName);
  //NS_LOG_INFO ("Insert Resourcevalue : " << resourceValue);
  unsigned char* md = (unsigned char*) malloc (20);
  const unsigned char* message = (const unsigned char*) resourceName.c_str ();
  SHA1 (message, resourceName.length (), md);
  unsigned char* value = (unsigned char *) (resourceValue.c_str ());
  //chordApplication->Insert (md, 20, value, resourceValue.length ());
  free (md);
  std::cout << "\n" << std::endl;
}

void
ChordRun::Retrieve (Ptr<ChordIpv4> chordApplication, std::string resourceName)
{
  NS_LOG_FUNCTION (this << resourceName);
  //std::cout << "Current Simulation Time: " << Simulator::Now ().GetMilliSeconds () << std::endl;
  unsigned char* md = (unsigned char*) malloc (20);
  const unsigned char* message = (const unsigned char*) resourceName.c_str ();
  SHA1 (message, resourceName.length (), md);
  //chordApplication->Retrieve (md, 20);
  free (md);
  std::cout << "\n" << std::endl;
}

void
ChordRun::DetachNode (uint16_t nodeNumber)
{
  NS_LOG_FUNCTION_NOARGS ();
  std::cout << "Current Simulation Time: " << Simulator::Now ().GetMilliSeconds () << std::endl;
  Ptr<NetDevice> netDevice = m_nodeContainer.Get (nodeNumber)->GetDevice (1);
  //  Ptr<CsmaChannel> channel = netDevice->GetChannel ()->GetObject<CsmaChannel> ();
  //  channel->Detach (nodeNumber);
  std::cout << "\n" << std::endl;
}

void
ChordRun::ReAttachNode (uint16_t nodeNumber)
{
  NS_LOG_FUNCTION_NOARGS ();
  std::cout << "Current Simulation Time: " << Simulator::Now ().GetMilliSeconds () << std::endl;
  //  Ptr<NetDevice> netDevice = m_nodeContainer.Get (nodeNumber)->GetDevice (1);
  //  Ptr<CsmaChannel> channel = netDevice->GetChannel ()->GetObject<CsmaChannel> ();
  //  if (channel->Reattach (nodeNumber) == false)
  //   std::cout << "Reattach success" << std::endl;
  //  else
  //   std::cout << "Reattach failed" << std::endl;
  std::cout << "\n" << std::endl;
}

void
ChordRun::DumpVNodeInfo (Ptr<ChordIpv4> chordApplication, std::string vNodeName)
{
  NS_LOG_FUNCTION_NOARGS ();
  std::cout << "Current Simulation Time: " << Simulator::Now ().GetMilliSeconds () << std::endl;
  //chordApplication->DumpVNodeInfo (vNodeName, std::cout);
  std::cout << "\n" << std::endl;
}

void
ChordRun::DumpDHashInfo (Ptr<ChordIpv4> chordApplication)
{
  NS_LOG_FUNCTION_NOARGS ();
  std::cout << "Current Simulation Time: " << Simulator::Now ().GetMilliSeconds () << std::endl;
  //chordApplication->DumpDHashInfo (std::cout);
  std::cout << "\n" << std::endl;
}
//----------------------------------------------------------------

void
ChordRun::JoinSuccess (std::string vNodeName, uint8_t* key, uint8_t numBytes)
{
  NS_LOG_FUNCTION (this << vNodeName);
  //std::cout << "Current Simulation Time: " << Simulator::Now ().GetMilliSeconds () << std::endl;
  //std::cout << "VNode: " << vNodeName << " Joined successfully\n" << std::endl;
  //PrintHexArray (key, numBytes, std::cout);
  std::cout << "\n" << std::endl;
}

void
ChordRun::LookupSuccess (uint8_t* lookupKey, uint8_t lookupKeyBytes, Ipv4Address ipAddress, uint16_t port)
{
  NS_LOG_FUNCTION (this << std::string ((char*) lookupKey, lookupKeyBytes) << ipAddress);
  //std::cout << "Current Simulation Time: " << Simulator::Now ().GetMilliSeconds () << std::endl;
  //std::cout << "Lookup Success Ip: " << ipAddress << " Port: " << port << std::endl;
  //PrintHexArray (lookupKey, lookupKeyBytes, std::cout);
  //PrintCharArray (lookupKey, lookupKeyBytes, std::cout);
  std::cout << "\n" << std::endl;
}

void
ChordRun::LookupFailure (uint8_t* lookupKey, uint8_t lookupKeyBytes)
{
  NS_LOG_FUNCTION (this << std::string ((char*) lookupKey, lookupKeyBytes));
  //std::cout << "Current Simulation Time: " << Simulator::Now ().GetMilliSeconds () << std::endl;
  //std::cout << "Key Lookup failed" << std::endl;
  //PrintHexArray (lookupKey, lookupKeyBytes, std::cout);
  //PrintCharArray (lookupKey, lookupKeyBytes, std::cout);
  std::cout << "\n" << std::endl;
}

void
ChordRun::VNodeKeyOwnership (std::string vNodeName, uint8_t* key, uint8_t keyBytes, uint8_t* predecessorKey, uint8_t predecessorKeyBytes, uint8_t* oldPredecessorKey, uint8_t oldPredecessorKeyBytes, Ipv4Address predecessorIp, uint16_t predecessorPort)
{
  NS_LOG_FUNCTION (this << vNodeName << predecessorIp);
  //std::cout << "Current Simulation Time: " << Simulator::Now ().GetMilliSeconds () << std::endl;
  //std::cout << "VNode: " << vNodeName << " Key Space Ownership change reported" << std::endl;
  //std::cout << "New predecessor Ip: " << predecessorIp << " Port: " << predecessorPort << "\n" << std::endl;
  std::cout << "\n" << std::endl;
}

void
ChordRun::VNodeFailure (std::string vNodeName, uint8_t* key, uint8_t numBytes)
{
  NS_LOG_FUNCTION (this << vNodeName);
  //std::cout << "Current Simulation Time: " << Simulator::Now ().GetMilliSeconds () << std::endl;
  //std::cout << "VNode: " << vNodeName << " Failed\n" << std::endl;
  std::cout << "\n" << std::endl;
}

void
ChordRun::InsertSuccess (uint8_t* key, uint8_t numBytes, uint8_t* object, uint32_t objectBytes)
{
  NS_LOG_FUNCTION (this << std::string ((char*) key, numBytes) << std::string ((char*) object, objectBytes));
  //std::cout << "Current Simulation Time: " << Simulator::Now ().GetMilliSeconds () << std::endl;
  //std::cout << "Insert Success!";
  //PrintHexArray (key, numBytes, std::cout);
  //PrintCharArray (object, objectBytes, std::cout);
  std::cout << "\n" << std::endl;
}

void
ChordRun::RetrieveSuccess (uint8_t* key, uint8_t numBytes, uint8_t* object, uint32_t objectBytes)
{
  NS_LOG_FUNCTION (this << std::string ((char*) key, numBytes) << std::string ((char*) object, objectBytes));
  //std::cout << "Current Simulation Time: " << Simulator::Now ().GetMilliSeconds () << std::endl;
  //std::cout << "Retrieve Success!";
  //PrintHexArray (key, numBytes, std::cout);
  //PrintCharArray (object, objectBytes, std::cout);
  std::cout << "\n" << std::endl;
}

void
ChordRun::InsertFailure (uint8_t* key, uint8_t numBytes, uint8_t* object, uint32_t objectBytes)
{
  NS_LOG_FUNCTION (this << std::string ((char*) key, numBytes) << std::string ((char*) object, objectBytes));
  //std::cout << "Current Simulation Time: " << Simulator::Now ().GetMilliSeconds () << std::endl;
  //std::cout << "Insert Failure Reported...";
  //PrintHexArray (key, numBytes, std::cout);
  //PrintCharArray (object, objectBytes, std::cout);
  std::cout << "\n" << std::endl;
}

void
ChordRun::RetrieveFailure (uint8_t* key, uint8_t keyBytes)
{
  NS_LOG_FUNCTION (this << std::string ((char*) key, keyBytes));
  //std::cout << "Current Simulation Time: " << Simulator::Now ().GetMilliSeconds () << std::endl;
  //std::cout << "Retrieve Failure Reported...";
  //PrintHexArray (key, keyBytes, std::cout);
  //PrintCharArray (key, keyBytes, std::cout);
  std::cout << "\n" << std::endl;
}

void
ChordRun::TraceRing (std::string vNodeName, uint8_t* key, uint8_t numBytes)
{
  std::cout << "<" << vNodeName << ">" << std::endl;
}

void
ChordRun::Tokenize (const std::string& str,
  std::vector<std::string>& tokens,
  const std::string& delimiters)
{
  // Skip delimiters at beginning.
  std::string::size_type lastPos = str.find_first_not_of (delimiters, 0);
  // Find first "non-delimiter".
  std::string::size_type pos = str.find_first_of (delimiters, lastPos);

  while (std::string::npos != pos || std::string::npos != lastPos)
   {
    // Found a token, add it to the vector.
    tokens.push_back (str.substr (lastPos, pos - lastPos));
    // Skip delimiters.  Note the "not_of"
    lastPos = str.find_first_not_of (delimiters, pos);
    // Find next "non-delimiter"
    pos = str.find_first_of (delimiters, lastPos);
   }
}

void
ChordRun::PrintCharArray (uint8_t* array, uint32_t size, std::ostream &os)
{
  os << "Char Array: ";
  for (uint32_t i = 0; i < size; i++)
   os << array[i];
  os << "\n";
}

void
ChordRun::PrintHexArray (uint8_t* array, uint32_t size, std::ostream &os)
{
  os << "Bytes: " << (uint16_t) size << "\n";
  os << "Array: \n";
  os << "[ ";
  for (uint8_t j = 0; j < size; j++)
   {
    os << std::hex << "0x" << (uint16_t) array[j] << " ";
   }
  os << std::dec << "]\n";
}

int
main (int argc, char *argv[])
{
  uint16_t nodes;
  uint16_t bootStrapNodeNum;
  std::string scriptFile = "";
  if (argc < 3)
   {
    std::cout << "Usage: chord-run <nodes> <bootstrapNodeNumber> <OPTIONAL: script-file>. Please input number of nodes to simulate and bootstrap node number\n";
    exit (1);
   }
  else
   {
    nodes = atoi (argv[1]);
    bootStrapNodeNum = atoi (argv[2]);
    if (argc == 4)
     {
      scriptFile = argv[3];
     }
    std::cout << "Number of nodes to simulate: " << (uint16_t) nodes << "\n";
   }

  LogComponentEnable ("ChordPS", (LogLevel) (LOG_LEVEL_ALL | LOG_PREFIX_ALL));
  //LogComponentEnable ("ChordRun", (LogLevel) (LOG_LEVEL_ALL | LOG_PREFIX_ALL));
  LogComponentEnable ("ChordIpv4", (LogLevel) (LOG_LEVEL_DEBUG | LOG_PREFIX_ALL));
  //LogComponentEnable("ChordMessage", LOG_LEVEL_ALL);
  LogComponentEnable ("ChordIdentifier", LOG_LEVEL_ERROR);
  LogComponentEnable ("ChordTransaction", LOG_LEVEL_ERROR);
  LogComponentEnable ("ChordVNode", LOG_LEVEL_ERROR);
  LogComponentEnable ("ChordNodeTable", LOG_LEVEL_ERROR);
  LogComponentEnable ("AodvRoutingProtocol", (LogLevel) (LOG_LEVEL_WARN | LOG_PREFIX_ALL));

  // Allow the user to override any of the defaults and the above Bind() at
  // run-time, via command-line arguments
  CommandLine cmd;
  cmd.Parse (argc, argv);

  // Explicitly create the nodes
  NS_LOG_INFO ("Creating nodes.");
  NodeContainer nodeContainer;
  nodeContainer.Create (nodes);

  // Create static grid
  MobilityHelper mobility;
  mobility.SetPositionAllocator ("ns3::GridPositionAllocator",
    "MinX", DoubleValue (0.0),
    "MinY", DoubleValue (0.0),
    "DeltaX", DoubleValue (10),
    "DeltaY", DoubleValue (10),
    "GridWidth", UintegerValue (4),
    "LayoutType", StringValue ("RowFirst"));
  mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobility.Install (nodeContainer);

  // create devices
  NS_LOG_INFO ("Create channels.");
  WifiMacHelper wifiMac;
  wifiMac.SetType ("ns3::AdhocWifiMac");
  YansWifiPhyHelper wifiPhy = YansWifiPhyHelper::Default ();
  YansWifiChannelHelper wifiChannel = YansWifiChannelHelper::Default ();
  wifiPhy.SetChannel (wifiChannel.Create ());
  WifiHelper wifi;
  wifi.SetRemoteStationManager ("ns3::ConstantRateWifiManager", "DataMode", StringValue ("OfdmRate6Mbps"), "RtsCtsThreshold", UintegerValue (100));
  NetDeviceContainer d = wifi.Install (wifiPhy, wifiMac, nodeContainer);

  // install internet stack
  NS_LOG_INFO ("Assign IP Addresses.");
  AodvHelper aodv;
  //OlsrHelper olsr;
  //DsdvHelper dsdv;
  //DsrMainHelper dsrMain;
  //DsrHelper dsr;
  //BatmanHelper batman;
  InternetStackHelper stack;
  stack.SetRoutingHelper (aodv); // has effect on the next Install ()
  //stack.SetRoutingHelper (olsr); // has effect on the next Install ()
  //stack.SetRoutingHelper (dsdv); // has effect on the next Install ()
  //stack.SetRoutingHelper (batman); // has effect on the next Install ()
  stack.Install (nodeContainer);
  //dsrMain.Install (dsr, nodeContainer);
  Ipv4AddressHelper address;
  address.SetBase ("10.0.0.0", "255.255.255.0");
  Ipv4InterfaceContainer i = address.Assign (d);

  NS_LOG_INFO ("Create Applications.");
  ChordRun chordRun;
  // Create a ChordIpv4 application on all nodes.
  uint16_t port = 2000;
  for (int j = 0; j < nodes; j++)
   {
    ChordIpv4Helper server;
    ApplicationContainer apps = server.Install (nodeContainer.Get (j));
    apps.Start (Seconds (j / 10.0));
   }

  Ptr<hyrax::ChordPS> chordApp = nodeContainer.Get (0)->GetApplication (0)->GetObject<hyrax::ChordPS> ();
  Simulator::Schedule (Seconds (60), &hyrax::ChordPS::TraceRing, chordApp);

  chordApp = nodeContainer.Get (12)->GetApplication (0)->GetObject<hyrax::ChordPS> ();
  Simulator::Schedule (Seconds (227), &hyrax::ChordPS::TraceRing, chordApp);

  Ptr<FlowMonitor> monitor;
  FlowMonitorHelper flowHelper;
  monitor = flowHelper.InstallAll ();

  //Start Chord-Run 
  //chordRun.Start (scriptFile, nodeContainer);
  // Now, do the actual simulation.
  NS_LOG_INFO ("########################################### Run Simulation...");
  Simulator::Run ();

  monitor->CheckForLostPackets ();
  Ptr<Ipv4FlowClassifier> classifier = DynamicCast<Ipv4FlowClassifier> (flowHelper.GetClassifier ());
  std::map<FlowId, FlowMonitor::FlowStats> stats = monitor->GetFlowStats ();
  uint64_t totalPacketsTx = 0, totalPacketsRx = 0, numberOfFlows = 0;
  Time totalDelay = Seconds (0.0);
  for (auto i = stats.begin (); i != stats.end (); ++i)
   {
    Ipv4FlowClassifier::FiveTuple t = classifier->FindFlow (i->first);
    totalPacketsTx += i->second.txPackets;
    totalPacketsRx += i->second.rxPackets;
    numberOfFlows++;
    totalDelay += i->second.delaySum;
   }

  std::cout << "Number of flows:   " << numberOfFlows << "\n";
  std::cout << "Flow monitor total # Packets Tx:   " << totalPacketsTx << "\n";
  std::cout << "Flow monitor total # Packets Rx:   " << totalPacketsRx << "\n\n";

  std::cout << "Flow monitor Avg Delay:   " << totalDelay.GetSeconds () << "/" << totalPacketsRx << " = " << (totalDelay.GetSeconds ()) / totalPacketsRx << "\n\n";

  Simulator::Destroy ();
  NS_LOG_INFO ("Done.");
  return 0;

}
