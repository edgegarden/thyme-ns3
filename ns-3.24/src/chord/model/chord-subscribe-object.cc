/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2015 NOVA LINCS DI-FCT-UNL <http://nova-lincs.di.fct.unl.pt/>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Joao A. Silva <jaa.silva@campus.fct.unl.pt>
 */
#include "chord-subscribe-object.h"

namespace ns3 {
namespace hyrax {

Ptr<UniformRandomVariable> CSubscriptionObject::RAND = CreateObject<UniformRandomVariable> ();

CSubscriptionObject::CSubscriptionObject () : Subscription ()
{
}

CSubscriptionObject::CSubscriptionObject (uint32_t subId, Ipv4Address subscriber,
  Time start, Time end, Conjunction tags) :
  Subscription (subId, subscriber, start, end)//, m_tags (tags)
{
}

CSubscriptionObject::CSubscriptionObject (uint32_t subId, Ipv4Address subscriber,
  Time start, Time end, std::string filter) :
  Subscription (subId, subscriber, start, end), m_filter (filter)
{
}

uint32_t
CSubscriptionObject::GetSerializedSize () const
{
  uint32_t size = Subscription::GetSerializedSize ();
//  size += sizeof (uint8_t);
//  for (int i = 0; i < m_tags.size (); ++i)
//   {
//    size += sizeof (uint8_t) + GetSerializedStringSize (m_tags[i].first);
//   }
  return size;
}

void
CSubscriptionObject::Serialize (Buffer::Iterator &start) const
{
  Subscription::Serialize (start);

//  start.WriteU8 (m_tags.size ());
//  for (int i = 0; i < m_tags.size (); ++i)
//   {
//    start.WriteU8 ((uint8_t) m_tags[i].second);
//    SerializeString (m_tags[i].first, start);
//   }
}

uint32_t
CSubscriptionObject::Deserialize (Buffer::Iterator &start)
{
  Subscription::Deserialize (start);

//  uint8_t numTags = start.ReadU8 ();
//  for (int i = 0; i < numTags; ++i)
//   {
//    bool b = (bool) start.ReadU8 ();
//    std::string str = DeserializeString (start);
//    m_tags.push_back ({str, b});
//   }

  return GetSerializedSize ();
}

void
CSubscriptionObject::Print (std::ostream &os) const
{
  Subscription::Print (os);
  //os << " #AndTags: " << m_tags.size ();
}

//AndTags
//CSubscriptionObject::GetTags () const
//{
//  //return m_tags;
//}

//void
//CSubscriptionObject::SetTags (AndTags tags)
//{
//  m_tags = tags;
//}

std::vector<std::string>
CSubscriptionObject::GetPositiveTags () const
{
  std::vector<std::string> posTags;
//  for (int i = 0; i < m_tags.size (); ++i)
//   {
//    if (!m_tags[i].second)
//     { // if tag is positive
//      posTags.push_back (m_tags[i].first);
//     }
//   }
  return posTags;
}

std::string
CSubscriptionObject::GetRandPositiveTag () const
{
  std::vector<std::string> posTags = GetPositiveTags ();
  return posTags[RAND->GetInteger (0, posTags.size () - 1)];
}

std::string
CSubscriptionObject::GetFilter () const
{
  return m_filter;
}

Filter
CSubscriptionObject::GetParsedFilter ()
{
  return ParseFilter (m_filter);
}

std::vector <std::string>
CSubscriptionObject::GetSendTags () const
{
  return m_sendTags;
}

void
CSubscriptionObject::AddSendTag (std::string tag)
{
  m_sendTags.push_back (tag);
}

bool
CSubscriptionObject::ValidFilter (std::set<std::string> tags) const
{
//  for (int i = 0; i < m_tags.size (); ++i)
//   {
//    bool found = tags.find (m_tags[i].first) != tags.end ();
//    bool isNeg = m_tags[i].second;
//    if ((found && isNeg) || (!found && !isNeg))
//     { // (found tag and tag is neg) OR (didn't find tag and tag is pos)
//      return false;
//     }
//   }
  return true;
}

}
}
