/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2009 University of Pennsylvania
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#include "ns3/chord-identifier.h"

#include <stdlib.h>
#include "ns3/abort.h"
#include "ns3/log.h"

NS_LOG_COMPONENT_DEFINE ("ChordIdentifier");

namespace ns3 {

ChordIdentifier::ChordIdentifier ()
{
  NS_LOG_FUNCTION (this);
  m_key = 0;
  m_numBytes = 0;
}

ChordIdentifier::ChordIdentifier (const uint8_t* key, uint8_t numBytes)
{
  NS_LOG_FUNCTION (this << key << numBytes);
  // Allocate memory to store key
  m_key = AllocateBuffer (numBytes);
  memcpy (m_key, key, numBytes); // Copy entire key to this area
  m_numBytes = numBytes; // Save numBytes
}

ChordIdentifier::ChordIdentifier (Ptr<ChordIdentifier> identifier)
{
  NS_LOG_FUNCTION (this << identifier);
  CopyIdentifier (identifier);
}

ChordIdentifier::ChordIdentifier (const ChordIdentifier& identifier)
{
  NS_LOG_FUNCTION (this << &identifier);
  Ptr<ChordIdentifier> chordIdentifier =
    const_cast<ChordIdentifier *> (&identifier);
  CopyIdentifier (chordIdentifier);
}

ChordIdentifier::~ChordIdentifier ()
{
  NS_LOG_FUNCTION (this);
  if (m_key != 0)
   { // Free memory
    free (m_key);
    m_numBytes = 0;
    m_key = 0;
   }
}

void
ChordIdentifier::CopyIdentifier (Ptr<ChordIdentifier> identifier)
{
  NS_LOG_FUNCTION (this << identifier);
  uint8_t *key = identifier->GetKey ();
  m_numBytes = identifier->GetNumBytes ();
  // Allocate memory to store key
  m_key = AllocateBuffer (m_numBytes);
  memcpy (m_key, key, m_numBytes); // Copy entire key to this area
}

bool
ChordIdentifier::IsEqual (Ptr<const ChordIdentifier> identifier) const
{
  NS_LOG_FUNCTION (this << identifier);
  uint8_t* key = identifier->GetKey (); // Retrieve key
  for (int i = 0; i < m_numBytes; i++)
   { // Check for contents
    if (key[i] != m_key[i])
     {
      return false;
     }
   }
  return true; // Keys are equal, return true
}

bool
ChordIdentifier::IsLess (Ptr<const ChordIdentifier> identifier) const
{
  NS_LOG_FUNCTION (this << identifier);
  uint8_t* key = identifier->GetKey (); // Retrieve key
  for (int i = m_numBytes - 1; i >= 0; i--)
   { // Compare two keys byte by byte
    if (m_key[i] < key[i])
     {
      return true;
     }
    else if (m_key[i] > key[i])
     {
      return false;
     }
   }
  return false; // Keys are equal, return false
}

bool
ChordIdentifier::IsGreater (Ptr<const ChordIdentifier> identifier) const
{
  NS_LOG_FUNCTION (this << identifier);
  uint8_t* key = identifier->GetKey (); // Retrieve key
  for (int i = m_numBytes - 1; i >= 0; i--)
   { // Compare two keys byte by byte
    if (m_key[i] > key[i])
     {
      return true;
     }
    else if (m_key[i] < key[i])
     {
      return false;
     }
   }
  return false; // Keys are equal, return false
}

bool
ChordIdentifier::IsInBetween (Ptr<const ChordIdentifier> identifierLow,
  Ptr<const ChordIdentifier> identifierHigh) const
{
  NS_LOG_FUNCTION (this << identifierLow << identifierHigh);
  // Check for existence in between range (keyLow,keyHigh], taken on circular identifier space, in clockwise direction
  if (identifierHigh->IsGreater (identifierLow))
   { // Check for wrap-around
    if (!IsGreater (identifierLow))
     { // No wrap-around
      return false;
     }
    if (IsLess (identifierHigh) || IsEqual (identifierHigh))
     {
      return true;
     }
    else
     {
      return false;
     }
   }
  else if (identifierHigh->IsLess (identifierLow))
   {
    // Wrap-around is there
    // Either key lies on left of 0 or on right. So check for both scenarios
    if (IsGreater (identifierHigh) || IsEqual (identifierHigh))
     { // Key lies on left of 0?
      if (IsGreater (identifierLow))
       {
        return true;
       }
      else
       {
        return false;
       }
     }
    if (IsLess (identifierHigh) || IsEqual (identifierHigh))
     { // Check if key lies on right of 0 and in between
      if (IsLess (identifierLow))
       { // Key lies on right of 0?
        return true;
       }
      else
       {
        return false;
       }
     }
   }
  else if (identifierHigh->IsEqual (identifierLow))
   {
    if (!IsEqual (identifierHigh))
     { // Everything is in between! (except keyHigh)
      return true;
     }
    return false;
   }
  NS_LOG_ERROR ("Something is seriously is wrong..");
  return false; // Something is seriously wrong, cannot be here
}

void
ChordIdentifier::AddPowerOfTwo (uint16_t powerOfTwo)
{
  NS_LOG_FUNCTION (this << powerOfTwo);
  uint8_t powZero = 0x01;
  uint8_t position = powerOfTwo / 8; // Find byte position
  NS_ASSERT (position < m_numBytes);
  uint8_t shift = powerOfTwo % 8;
  // Add power
  uint8_t prevVal = m_key[position];
  m_key[position] = m_key[position] + (powZero << shift);
  // if carry is there
  while ((m_key[position] < prevVal) && (position <= (m_numBytes - 1)))
   {
    position++;
    prevVal = m_key[position];
    m_key[position] = m_key[position] + 0x01;
   }
}

uint8_t*
ChordIdentifier::GetKey (void) const
{
  NS_LOG_FUNCTION (this);
  return m_key;
}

uint8_t
ChordIdentifier::GetNumBytes ()
{
  NS_LOG_FUNCTION (this);
  return m_numBytes;
}

void
ChordIdentifier::SetKey (const uint8_t* key, uint8_t numBytes)
{
  NS_LOG_FUNCTION (this << key << numBytes);
  if (m_key != 0)
   { // Free previously stored key
    free (m_key);
    m_key = 0;
    m_numBytes = 0;
   }
  // Allocate memory to store key
  m_key = AllocateBuffer (numBytes);
  memcpy (m_key, key, numBytes); // Copy entire key to this area
  m_numBytes = numBytes; // Save numBytes
}

void
ChordIdentifier::Serialize (Buffer::Iterator &start)
{
  NS_LOG_FUNCTION (this);
  start.WriteU8 (m_numBytes);
  start.Write (m_key, m_numBytes);
}

uint32_t
ChordIdentifier::Deserialize (Buffer::Iterator &start)
{
  NS_LOG_FUNCTION (this);
  m_numBytes = start.ReadU8 ();
  // Allocate memory
  m_key = AllocateBuffer (m_numBytes);
  start.Read (m_key, m_numBytes); // Retrieve key from buffer
  return GetSerializedSize ();
}

uint32_t
ChordIdentifier::GetSerializedSize ()
{
  NS_LOG_FUNCTION (this);
  uint32_t size = GetNumBytes () + sizeof (uint8_t);
  return size;

}

void
ChordIdentifier::Print (std::ostream &os)
{
  os << "Bytes: " << (uint16_t) m_numBytes << "\n";
  os << "Key: ";
  os << "[ ";
  for (uint8_t j = 0; j < m_numBytes; j++)
   {
    os << std::hex << "0x" << (uint16_t) m_key[j] << " ";
   }
  os << std::dec << "]";
}

std::ostream & operator<< (std::ostream& os, Ptr<ChordIdentifier> const &identifier)
{
  identifier->Print (os);
  return os;
}

bool operator== (ChordIdentifier &chordIdentifierL, ChordIdentifier &chordIdentifierR)
{
  Ptr<ChordIdentifier> identifierL = const_cast<ChordIdentifier *> (&chordIdentifierL);
  Ptr<ChordIdentifier> identifierR = const_cast<ChordIdentifier *> (&chordIdentifierR);
  if (identifierL->IsEqual (identifierR))
   {
    return true;
   }
  else
   {
    return false;
   }
}

bool operator< (const ChordIdentifier &chordIdentifierL, const ChordIdentifier &chordIdentifierR)
{
  Ptr<ChordIdentifier> identifierL = const_cast<ChordIdentifier *> (&chordIdentifierL);
  Ptr<ChordIdentifier> identifierR = const_cast<ChordIdentifier *> (&chordIdentifierR);
  if (identifierL->IsLess (identifierR))
   {
    return true;
   }
  else
   {
    return false;
   }
}

uint8_t*
ChordIdentifier::AllocateBuffer (uint8_t numBytes)
{
  uint8_t* keybuf = (uint8_t*) malloc ((numBytes * sizeof (uint8_t)) + 1);
  NS_ASSERT_MSG (keybuf != 0, "Malloc failed");
  memset (keybuf, 0, numBytes + 1);
  return keybuf;
}

} // namespace ns3
