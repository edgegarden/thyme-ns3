/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2009 University of Pennsylvania
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#include "chord-ipv4.h"

#include "stdint.h"
#include "stdlib.h"
#include "ns3/log.h"
#include "ns3/ipv4-address.h"
#include "ns3/double.h"
#include "ns3/nstime.h"
#include "ns3/inet-socket-address.h"
#include "ns3/address-utils.h"
#include "ns3/socket.h"
#include "ns3/simulator.h"
#include "ns3/socket-factory.h"
#include "ns3/packet.h"
#include "ns3/uinteger.h"
#include "ns3/boolean.h"
#include "ns3/trace-source-accessor.h"
#include "ns3/callback.h"
#include "ns3/object-factory.h"

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("ChordIpv4");
NS_OBJECT_ENSURE_REGISTERED (ChordIpv4);

TypeId
ChordIpv4::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::ChordIpv4")
    .SetParent<Object> ()
    .AddConstructor<ChordIpv4> ()
    .AddAttribute ("ListeningPort",
    "Chord Protocol port (mandatory)",
    UintegerValue (12153),
    MakeUintegerAccessor (&ChordIpv4::m_listeningPort),
    MakeUintegerChecker<uint16_t> ())
    .AddAttribute ("ApplicationPort",
    "Port to be sent in response to Lookup requests (mandatory)",
    UintegerValue (12154),
    MakeUintegerAccessor (&ChordIpv4::m_applicationPort),
    MakeUintegerChecker<uint16_t> ())
    .AddAttribute ("MaxVNodeSuccessorListSize",
    "Max Size of successor list to maintain in a VNode",
    UintegerValue (DEFAULT_MAX_VNODE_SUCCESSOR_LIST_SIZE),
    MakeUintegerAccessor (&ChordIpv4::m_maxVNodeSuccessorListSize),
    MakeUintegerChecker<uint8_t> ())
    .AddAttribute ("MaxVNodePredecessorListSize",
    "Max Size of predessor list to maintain in a VNode",
    UintegerValue (DEFAULT_MAX_VNODE_PREDECESSOR_LIST_SIZE),
    MakeUintegerAccessor (&ChordIpv4::m_maxVNodePredecessorListSize),
    MakeUintegerChecker<uint8_t> ())
    .AddAttribute ("StabilizeInterval",
    "Stabilize Interval in milli seconds",
    TimeValue (MilliSeconds (DEFAULT_STABILIZE_INTERVAL)),
    MakeTimeAccessor (&ChordIpv4::m_stabilizeInterval),
    MakeTimeChecker ())
    .AddAttribute ("HeartbeatInterval",
    "Heartbeat Interval in milli seconds",
    TimeValue (MilliSeconds (DEFAULT_HEARTBEAT_INTERVAL)),
    MakeTimeAccessor (&ChordIpv4::m_heartbeatInterval),
    MakeTimeChecker ())
    .AddAttribute ("MaxMissedKeepAlives",
    "Number of missed Heartbeats and Stabilize requests before declaring node dead",
    UintegerValue (DEFAULT_MAX_MISSED_KEEP_ALIVES),
    MakeUintegerAccessor (&ChordIpv4::m_maxMissedKeepAlives),
    MakeUintegerChecker<uint16_t> ())
    .AddAttribute ("MaxRequestRetries",
    "Number of request retries before giving up",
    UintegerValue (DEFAULT_MAX_REQUEST_RETRIES),
    MakeUintegerAccessor (&ChordIpv4::m_maxRequestRetries),
    MakeUintegerChecker<uint8_t> ())
    .AddAttribute ("RequestTimeout",
    "Timeout value for request retransmission in milli seconds",
    TimeValue (MilliSeconds (DEFAULT_REQUEST_TIMEOUT)),
    MakeTimeAccessor (&ChordIpv4::m_requestTimeout),
    MakeTimeChecker ())
    .AddAttribute ("FixFingerInterval",
    "Fix Finger Interval in milli seconds",
    TimeValue (MilliSeconds (DEFAULT_FIX_FINGER_INTERVAL)),
    MakeTimeAccessor (&ChordIpv4::m_fixFingerInterval),
    MakeTimeChecker ())
    .AddAttribute ("PacketTTL",
    "Maximum HopCounter(TimeToLive) for Chord Requests",
    UintegerValue (DEFAULT_MAX_PACKET_TTL),
    MakeUintegerAccessor (&ChordIpv4::m_maxPacketTTL),
    MakeUintegerChecker<uint8_t> ())
    ;
  return tid;
}

ChordIpv4::ChordIpv4 () : m_stabilizeTimer (Timer::CANCEL_ON_DESTROY),
  m_heartbeatTimer (Timer::CANCEL_ON_DESTROY), isBootStrapNode (false),
  m_fixFingerTimer (Timer::CANCEL_ON_DESTROY), m_socket (0), m_started (false)
{
  NS_LOG_FUNCTION (this);
}

ChordIpv4::~ChordIpv4 ()
{
  NS_LOG_FUNCTION (this);
  m_socket = 0;
}

void
ChordIpv4::Start (Ptr<Node> node, Ipv4Address localIp, uint16_t dhashPort)
{
  NS_LOG_FUNCTION (this << node << localIp << dhashPort);
  if (m_started)
   {
    return;
   }

  m_started = true;
  m_node = node;
  m_localIpAddress = localIp;
  m_dHashPort = dhashPort;

  NS_LOG_INFO ("***ChordIpv4 starting on Node: " << m_node->GetId ()
    << "\n***Parameters: "
    << "\n***listeningPort: " << m_listeningPort
    << "\n***localIp: " << m_localIpAddress
    );

  // Configure socket
  TypeId tid = TypeId::LookupByName ("ns3::UdpSocketFactory");
  m_socket = Socket::CreateSocket (m_node, tid);
  InetSocketAddress local = InetSocketAddress (m_localIpAddress, m_listeningPort);
  m_socket->Bind (local);
  m_socket->SetRecvCallback (MakeCallback (&ChordIpv4::ProcessUdpPacket, this));

  // Configure timers
  m_stabilizeTimer.SetFunction (&ChordIpv4::DoPeriodicStabilize, this);
  m_heartbeatTimer.SetFunction (&ChordIpv4::DoPeriodicHeartbeat, this);
  m_fixFingerTimer.SetFunction (&ChordIpv4::DoPeriodicFixFinger, this);

  // Start timers
  m_stabilizeTimer.Schedule (m_stabilizeInterval);
  m_heartbeatTimer.Schedule (m_heartbeatInterval);
  m_fixFingerTimer.Schedule (m_fixFingerInterval);
}

void
ChordIpv4::SetBootstrapIp (Ipv4Address bootstrapIp)
{
  NS_LOG_FUNCTION (this << bootstrapIp);
  m_bootStrapIp = bootstrapIp;
  isBootStrapNode = m_bootStrapIp == m_localIpAddress;
}

void
ChordIpv4::InsertVNode (std::string vNodeName, uint8_t* key, uint8_t keyBytes)
{
  NS_LOG_FUNCTION (this << vNodeName << key << (uint16_t) keyBytes);
  Ptr<ChordIdentifier> chordIdentifier = Create<ChordIdentifier> (key, keyBytes);
  // Create VNode object
  Ptr<ChordNode> node = Create<ChordNode> (chordIdentifier, vNodeName, m_localIpAddress, m_listeningPort, m_applicationPort, m_dHashPort);
  Ptr<ChordVNode> vNode = Create<ChordVNode> (node, m_maxVNodeSuccessorListSize, m_maxVNodePredecessorListSize);
  // Own up entire key-space
  vNode->SetSuccessor (Create<ChordNode> (vNode));
  vNode->SetPredecessor (Create<ChordNode> (vNode));
  // Set routable = false
  vNode->SetRoutable (false);
  /* bootStrapIp is same as local Ip and no v-nodes exist. In that case we need to create a new chord */
  if (isBootStrapNode && m_vNodeMap.GetSize () == 0 && m_started)
   {
    NS_LOG_INFO ("Bootstrap new Chord network");
    // Create a new chord network
    // Insert VNode into list
    Ptr<ChordNode> chordNode = DynamicCast<ChordNode> (vNode);
    m_vNodeMap.UpdateNode (chordNode);
    DoFixFinger (vNode);
    NotifyJoinSuccess (vNode->GetVNodeName (), vNode->GetChordIdentifier ());
    return;
   }
  else if (isBootStrapNode && m_vNodeMap.GetSize () == 1 && m_started)
   {
    NS_LOG_INFO ("Bootstrap case, InsertVNode");
    vNode->SetSuccessor (Create<ChordNode> (*(m_vNodeMap.GetNodes ().begin ())));
    vNode->SetRoutable (true);
    // Insert VNode into list
    Ptr<ChordNode> chordNode = DynamicCast<ChordNode> (vNode);
    m_vNodeMap.UpdateNode (chordNode);
    DoStabilize (vNode);
    DoFixFinger (vNode);
    NotifyJoinSuccess (vNode->GetVNodeName (), vNode->GetChordIdentifier ());
    return;
   }

  // Insert VNode into list
  Ptr<ChordNode> chordNode = DynamicCast<ChordNode> (vNode);
  // Do not store vNode if ChordIpv4 is inactive.
  if (m_started)
   {
    m_vNodeMap.UpdateNode (chordNode);
   }
  // Send this request to bootstrap IP
  ChordMessage chordMessage = ChordMessage (m_maxPacketTTL);
  vNode->PackJoinReq (chordMessage);
  // Add transaction
  Ptr<ChordTransaction> chordTransaction = Create<ChordTransaction> (chordMessage.GetTransactionId (), chordMessage, m_requestTimeout, m_maxRequestRetries);
  // Add to vNode
  vNode->AddTransaction (chordMessage.GetTransactionId (), chordTransaction);
  // Start transaction timer
  EventId requestTimeoutId = Simulator::Schedule (chordTransaction->GetRequestTimeout (), &ChordIpv4::HandleRequestTimeout, this, vNode, chordMessage.GetTransactionId ());
  chordTransaction->SetRequestTimeoutEventId (requestTimeoutId);
  NS_LOG_INFO ("Sending JoinReq" << chordMessage);
  if (m_vNodeMap.GetSize () > 1)
   {
    if (RouteChordMessage (vNode->GetChordIdentifier (), chordMessage))
     {
      return;
     }
   }
  // Default: Send to bootstrap node
  SendChordMessage (chordMessage, m_bootStrapIp, m_listeningPort);
}

bool
ChordIpv4::CheckOwnership (uint8_t* lookupKey, uint8_t lookupKeyBytes)
{
  NS_LOG_FUNCTION (this << lookupKey << lookupKeyBytes);
  Ptr<ChordIdentifier> lookupIdentifier = Create<ChordIdentifier> (lookupKey, lookupKeyBytes);
  Ptr<ChordVNode> chordVNode = LookupLocal (lookupIdentifier);
  return chordVNode != 0;
}

void
ChordIpv4::RemoveVNode (std::string vNodeName)
{
  NS_LOG_FUNCTION (this << vNodeName);
  // Iterate through the  m_vNodeMap and find the specified Vnode which has to leave.
  // Iterate VNode list and check if v node exists
  Ptr<ChordNode> chordNode = m_vNodeMap.FindNode (vNodeName);
  if (!chordNode)
   {
    return;
   }
  Ptr<ChordVNode> virtualNode = DynamicCast<ChordVNode> (chordNode);

  // Send this request to bootstrap IP
  ChordMessage chordMessage = ChordMessage (m_maxPacketTTL);
  virtualNode->PackLeaveReq (chordMessage);

  NS_LOG_INFO ("Sending LeaveReq\n" << chordMessage);
  SendChordMessage (chordMessage, virtualNode->GetSuccessor ()->GetIpAddress (), virtualNode->GetSuccessor ()->GetPort ());
  SendChordMessage (chordMessage, virtualNode->GetPredecessor ()->GetIpAddress (), virtualNode->GetPredecessor ()->GetPort ());

  DeleteVNode (vNodeName); // delete VNode from m_vNodeMap
}

void
ChordIpv4::LookupKey (uint8_t* lookupKey, uint8_t lookupKeyBytes)
{
  NS_LOG_FUNCTION (this << lookupKey << lookupKeyBytes);
  Ptr<ChordIdentifier> requestedIdentifier = Create<ChordIdentifier> (lookupKey, lookupKeyBytes);
  LookupKey (requestedIdentifier);
}

void
ChordIpv4::LookupKey (Ptr<ChordIdentifier> identifier)
{
  NS_LOG_FUNCTION (this << identifier);
  DoLookup (identifier, ChordTransaction::DHASH);
}

/******* Diagnostics  *******/
void
ChordIpv4::DumpVNodeInfo (std::string vNodeName, std::ostream &os)
{
  Ptr<ChordVNode> virtualNode = FindVNode (vNodeName);
  if (virtualNode)
   { // Dump stats
    os << "**** Info for VNode: " << virtualNode->GetVNodeName () << " ****\n";
    os << "Local IP: " << m_localIpAddress << "\n";
    os << "Local Port: " << m_listeningPort << "\n";
    os << "VNode Identifier: " << virtualNode->GetChordIdentifier () << "\n";
    os << "Successor Ip: " << virtualNode->GetSuccessor ()->GetIpAddress () << "\n";
    os << "Successor Port: " << virtualNode->GetSuccessor ()->GetPort () << "\n";
    os << "Successor Identifier: " << virtualNode->GetSuccessor ()->GetChordIdentifier () << "\n";
    os << "Predecessor Ip: " << virtualNode->GetPredecessor ()->GetIpAddress () << "\n";
    os << "Predecessor Port: " << virtualNode->GetPredecessor ()->GetPort () << "\n";
    os << "Predecessor Identifier: " << virtualNode->GetPredecessor ()->GetChordIdentifier () << "\n";

    virtualNode->PrintSuccessorList (os);
    virtualNode->PrintPredecessorList (os);
    virtualNode->PrintFingerTable (os);
    // virtualNode -> PrintFingerIdentifierList (os);
    os << "Fingers actually looked up: " << virtualNode->GetStats ().fingersLookedUp << "\n";
   }
  else
   {
    os << "No Such VNode Exists" << "\n";
   }
}

void
ChordIpv4::FireTraceRing (std::string vNodeName)
{
  Ptr<ChordVNode> virtualNode = FindVNode (vNodeName);
  if (virtualNode)
   {
    NotifyTraceRing (virtualNode->GetVNodeName (), virtualNode->GetChordIdentifier ());
    ChordMessage chordMessageFwd = ChordMessage (m_maxPacketTTL);
    virtualNode->PackTraceRing (virtualNode, chordMessageFwd);
    NS_LOG_INFO ("Forwarding TraceRing: " << chordMessageFwd);
    SendChordMessage (chordMessageFwd, virtualNode->GetSuccessor ()->GetIpAddress (), virtualNode->GetSuccessor ()->GetPort ());
   }
}

void
ChordIpv4::FixFingers (std::string vNodeName)
{
  Ptr<ChordVNode> virtualNode = FindVNode (vNodeName);
  if (virtualNode)
   {
    DoFixFinger (virtualNode);
   }
}

void
ChordIpv4::DoDispose (void)
{
  NS_LOG_FUNCTION (this);
  m_started = false;
  if (m_socket != 0)
   {
    m_socket->SetRecvCallback (MakeNullCallback<void, Ptr<Socket> > ());
   }
  // Cancel Timers
  m_stabilizeTimer.Cancel ();
  m_heartbeatTimer.Cancel ();
  m_fixFingerTimer.Cancel ();
  // Delete vNodes
  m_vNodeMap.Clear ();
}

void
ChordIpv4::DoPeriodicStabilize ()
{
  // Loop for all v-nodes
  vector<Ptr<ChordNode> > chordNodes = m_vNodeMap.GetNodes ();
  for (vector<Ptr<ChordNode> >::iterator vNodeIter = chordNodes.begin (); vNodeIter != chordNodes.end (); vNodeIter++)
   {
    Ptr<ChordVNode> vNode = DynamicCast<ChordVNode> (*vNodeIter);
    //Do not process for nodes still in joining process
    if (vNode->GetSuccessor ()->GetChordIdentifier ()->IsEqual (vNode->GetChordIdentifier ()))
     {
      continue;
     }

    // Check if successor is alive. Shift successor if necessary. If all else fails, send CHORD_FAILURE to user and remove vNode
    // Compare timestamp and check if current successor has died
    if (vNode->GetSuccessor ()->GetTimestamp ().GetMilliSeconds () + m_stabilizeInterval.GetMilliSeconds () * m_maxMissedKeepAlives < Simulator::Now ().GetMilliSeconds ())
     {
      // Successor has failed
      // Shift vNode successor
      if (vNode->ShiftSuccessor () == false)
       {
        // If this is last node and we are bootstrap node, do not report failure or remove this node. This can be only removed manually.
        if (isBootStrapNode && m_vNodeMap.GetSize () == 1)
         {
          // Reset successor as self
          vNode->SetSuccessor (Create<ChordNode> (vNode));
          vNode->SetRoutable (false);
          continue;
         }
        // Delete vNode
        DeleteVNode (vNode->GetChordIdentifier ());
        // No successor(s) in list, report failure and remove vNode
        NotifyVNodeFailure (vNode->GetVNodeName (), vNode->GetChordIdentifier ());
        if (m_vNodeMap.GetSize () == 0)
         {
          break;
         }
        else
         {
          continue;
         }
       }
     }
    DoStabilize (vNode); // Fire stablize req
   }
  m_stabilizeTimer.Schedule (m_stabilizeInterval); // RescheduleTimer
}

void
ChordIpv4::DoPeriodicHeartbeat ()
{
  // Loop for all v-nodes
  vector<Ptr<ChordNode> > chordNodes = m_vNodeMap.GetNodes ();
  for (vector<Ptr<ChordNode> >::iterator vNodeIter = chordNodes.begin (); vNodeIter != chordNodes.end (); vNodeIter++)
   {
    Ptr<ChordVNode> vNode = DynamicCast<ChordVNode> (*vNodeIter);

    if (vNode->GetPredecessor ()->GetTimestamp ().GetMilliSeconds () + m_heartbeatInterval.GetMilliSeconds () * m_maxMissedKeepAlives < Simulator::Now ().GetMilliSeconds ())
     {
      Ptr<ChordNode> oldPredecessorNode = vNode->GetPredecessor ();
      // Predecessor has failed. Shift vNode predecessor
      if (vNode->ShiftPredecessor () == false)
       { // Reset predecessor as self node
        vNode->SetPredecessor (Create<ChordNode> (vNode));
        continue;
       }
      else
       { // Predecessor shift success, trigger key space change
        NotifyVNodeKeyOwnership (vNode->GetVNodeName (), vNode->GetChordIdentifier (), vNode->GetPredecessor (), oldPredecessorNode->GetChordIdentifier ());
       }
     }
    DoHeartbeat (vNode); // Fire heartbeat req
   }
  m_heartbeatTimer.Schedule (m_heartbeatInterval); // RescheduleTimer
}

void
ChordIpv4::DoPeriodicFixFinger ()
{
  vector<Ptr<ChordNode> > chordNodes = m_vNodeMap.GetNodes ();
  for (vector<Ptr<ChordNode> >::iterator vNodeIter = chordNodes.begin (); vNodeIter != chordNodes.end (); vNodeIter++)
   {
    Ptr<ChordVNode> vNode = DynamicCast<ChordVNode> (*vNodeIter);
    DoFixFinger (vNode);
   }
  // RescheduleTimer
  // Use random variable and introduce variance of 100ms
  //Ptr<NormalRandomVariable> interval = Create<NormalRandomVariable> ();
  //m_fixFingerTimer.Schedule (MilliSeconds (interval->GetValue (m_fixFingerInterval.GetMilliSeconds (), 100)));
  m_fixFingerTimer.Schedule (MilliSeconds (m_fixFingerInterval.GetMilliSeconds ()));
}

void
ChordIpv4::NotifyJoinSuccess (std::string vNodeName, Ptr<ChordIdentifier> chordIdentifier)
{
  NS_LOG_FUNCTION (this << vNodeName << chordIdentifier);
  if (!m_joinSuccessFn.IsNull ())
   {
    m_joinSuccessFn (vNodeName, chordIdentifier);
   }
}

void
ChordIpv4::NotifyLookupSuccess (Ptr<ChordIdentifier> lookupIdentifier, Ptr<ChordNode> resolvedNode, ChordTransaction::Originator originator)
{
  NS_LOG_FUNCTION (this << lookupIdentifier << resolvedNode << originator);
  if (!m_lookupSuccessFn.IsNull () && originator == ChordTransaction::APPLICATION)
   {
    m_lookupSuccessFn (lookupIdentifier, resolvedNode->GetIpAddress (), resolvedNode->GetApplicationPort ());
   }
  else if (!m_dHashLookupSuccessFn.IsNull () && originator == ChordTransaction::DHASH)
   {
    m_dHashLookupSuccessFn (lookupIdentifier, resolvedNode->GetIpAddress (), resolvedNode->GetDHashPort ());
   }
}

void
ChordIpv4::NotifyLookupFailure (Ptr<ChordIdentifier> chordIdentifier, ChordTransaction::Originator originator)
{
  NS_LOG_FUNCTION (this << chordIdentifier << originator);
  if (!m_lookupFailureFn.IsNull () && originator == ChordTransaction::APPLICATION)
   {
    m_lookupFailureFn (chordIdentifier);
   }
  else if (!m_dHashLookupFailureFn.IsNull () && originator == ChordTransaction::DHASH)
   {
    m_dHashLookupFailureFn (chordIdentifier);
   }
}

void
ChordIpv4::NotifyVNodeKeyOwnership (std::string vNodeName, Ptr<ChordIdentifier> chordIdentifier, Ptr<ChordNode> predecessorNode, Ptr<ChordIdentifier> oldPredecessorIdentifier)
{
  NS_LOG_FUNCTION (this << vNodeName << chordIdentifier << predecessorNode << oldPredecessorIdentifier);
  if (!m_vNodeKeyOwnershipFn.IsNull ())
   {
    m_vNodeKeyOwnershipFn (vNodeName, chordIdentifier, predecessorNode->GetChordIdentifier (), oldPredecessorIdentifier, predecessorNode->GetIpAddress (), predecessorNode->GetApplicationPort ());
   }
  if (!m_dHashVNodeKeyOwnershipFn.IsNull ())
   {
    m_dHashVNodeKeyOwnershipFn (chordIdentifier, predecessorNode->GetChordIdentifier (), oldPredecessorIdentifier, predecessorNode->GetIpAddress (), predecessorNode->GetDHashPort ());
   }
}

void
ChordIpv4::NotifyTraceRing (std::string vNodeName, Ptr<ChordIdentifier> chordIdentifier)
{
  NS_LOG_FUNCTION (this << vNodeName << chordIdentifier);
  if (!m_traceRingFn.IsNull ())
   {
    m_traceRingFn (vNodeName, chordIdentifier);
   }
}

void
ChordIpv4::NotifyVNodeFailure (std::string vNodeName, Ptr<ChordIdentifier> chordIdentifier)
{
  NS_LOG_FUNCTION (this << vNodeName << chordIdentifier);
  if (!m_vNodeFailureFn.IsNull ())
   {
    m_vNodeFailureFn (vNodeName, chordIdentifier);
   }
}

void
ChordIpv4::ProcessMessage (Ptr<Packet> packet)
{
  NS_LOG_FUNCTION (this << packet);
  ChordMessage chordMessage = ChordMessage (m_maxPacketTTL);
  // Retrieve and Deserialize chord message
  packet->RemoveHeader (chordMessage);
  //NS_LOG_INFO ("ChordMessage: " << chordMessage);
  switch (chordMessage.GetMessageType ())
  {
  case ChordMessage::JOIN_REQ:
    ProcessJoinReq (chordMessage);
    break;
  case ChordMessage::JOIN_RSP:
    ProcessJoinRsp (chordMessage);
    break;
  case ChordMessage::LEAVE_REQ:
    ProcessLeaveReq (chordMessage);
    break;
  case ChordMessage::LEAVE_RSP:
    ProcessLeaveRsp (chordMessage);
    break;
  case ChordMessage::LOOKUP_REQ:
    ProcessLookupReq (chordMessage);
    break;
  case ChordMessage::LOOKUP_RSP:
    ProcessLookupRsp (chordMessage);
    break;
  case ChordMessage::STABILIZE_REQ:
    ProcessStabilizeReq (chordMessage);
    break;
  case ChordMessage::STABILIZE_RSP:
    ProcessStabilizeRsp (chordMessage);
    break;
  case ChordMessage::HEARTBEAT_REQ:
    ProcessHeartbeatReq (chordMessage);
    break;
  case ChordMessage::HEARTBEAT_RSP:
    ProcessHeartbeatRsp (chordMessage);
    break;
  case ChordMessage::FINGER_REQ:
    ProcessFingerReq (chordMessage);
    break;
  case ChordMessage::FINGER_RSP:
    ProcessFingerRsp (chordMessage);
    break;
  case ChordMessage::TRACE_RING:
    ProcessTraceRing (chordMessage);
    break;
  default:
    break;
  }
}

void
ChordIpv4::ProcessUdpPacket (Ptr<Socket> socket)
{
  NS_LOG_FUNCTION (this << socket);
  Ptr<Packet> packet;
  Address from;
  if ((packet = socket->RecvFrom (from)) != 0)
   {
    if (InetSocketAddress::IsMatchingType (from))
     {
      InetSocketAddress address = InetSocketAddress::ConvertFrom (from);
      NS_LOG_INFO ("ChordIpv4: Received " << packet->GetSize () << " bytes packet from " << address.GetIpv4 ());
      ProcessMessage (packet);
     }
   }
}

void
ChordIpv4::ProcessJoinReq (ChordMessage chordMessage)
{
  NS_LOG_FUNCTION (this << &chordMessage);
  Ptr<ChordNode> requestorNode = chordMessage.GetRequestorNode ();
  uint32_t transactionId = chordMessage.GetTransactionId ();
  if (m_vNodeMap.GetSize () == 0)
   { // No vNode exists as yet, drop this request.
    return;
   }
  // Check if we can be this node's successor
  Ptr<ChordVNode> virtualNode = LookupLocal (requestorNode->GetChordIdentifier ());
  if (virtualNode)
   {
    ChordMessage chordMessageRsp = ChordMessage (m_maxPacketTTL);
    virtualNode->PackJoinRsp (requestorNode, transactionId, chordMessageRsp);
    NS_LOG_INFO ("Sending JoinRsp: " << chordMessageRsp);
    SendChordMessage (chordMessageRsp, requestorNode->GetIpAddress (), requestorNode->GetPort ());
    return;
   }
  // Could not resolve join request, forward to nearest successor
  RouteChordMessage (requestorNode->GetChordIdentifier (), chordMessage);
}

void
ChordIpv4::ProcessJoinRsp (ChordMessage chordMessage)
{
  NS_LOG_FUNCTION (this << &chordMessage);
  // Extract info from packet
  Ptr<ChordNode> requestorNode = chordMessage.GetRequestorNode ();
  Ptr<ChordNode> successorNode = chordMessage.GetJoinRsp ().successorNode;
  // Find virtual node which sent this message
  Ptr<ChordVNode> virtualNode = FindVNode (requestorNode->GetChordIdentifier ());

  if (virtualNode)
   {
    // Find Transaction
    Ptr<ChordTransaction> chordTransaction = virtualNode->FindTransaction (chordMessage.GetTransactionId ());
    if (!chordTransaction)
     { // No transaction exists, return from here
      return;
     }
    // VNode found, set its successor and stabilize
    virtualNode->SetSuccessor (Create<ChordNode> (successorNode));
    // Make this node routable
    virtualNode->SetRoutable (true);
    // cancel transaction
    virtualNode->RemoveTransaction (chordTransaction->GetTransactionId ());
    DoStabilize (virtualNode);
    DoFixFinger (virtualNode);
    // notify application about join success
    NotifyJoinSuccess (virtualNode->GetVNodeName (), requestorNode->GetChordIdentifier ());
   }
}

void
ChordIpv4::ProcessLeaveReq (ChordMessage chordMessage)
{
  NS_LOG_FUNCTION (this << &chordMessage);
  Ptr<ChordNode> requestorNode = chordMessage.GetRequestorNode ();
  // Read payload and get vnode identifier
  Ptr<ChordNode> successorNode = chordMessage.GetLeaveReq ().successorNode;
  Ptr<ChordNode> predecessorNode = chordMessage.GetLeaveReq ().predecessorNode;
  Ptr<ChordVNode> virtualNode;

  // Find VNode
  virtualNode = FindVNode (successorNode->GetChordIdentifier ());

  // Are we successor node?
  if (virtualNode && virtualNode->GetPredecessor ()->GetChordIdentifier ()->IsEqual (requestorNode->GetChordIdentifier ()))
   {
    // Reset own predecessor
    Ptr<ChordNode> oldPredecessorNode = virtualNode->GetPredecessor ();
    virtualNode->SetPredecessor (Create<ChordNode> (predecessorNode));
    NotifyVNodeKeyOwnership (virtualNode->GetVNodeName (), virtualNode->GetChordIdentifier (), virtualNode->GetPredecessor (), oldPredecessorNode->GetChordIdentifier ());
    // Send Leave Rsp (only required in case of successor)
    ChordMessage respMessage = ChordMessage (m_maxPacketTTL);
    virtualNode->PackLeaveRsp (requestorNode, successorNode, predecessorNode, chordMessage);
    SendChordMessage (chordMessage, requestorNode->GetIpAddress (), requestorNode->GetPort ());
    NS_LOG_INFO ("Predecessor changed for VNode");
   }

  // Are we predecessor node?
  virtualNode = FindVNode (predecessorNode->GetChordIdentifier ());
  if (virtualNode && virtualNode->GetSuccessor ()->GetChordIdentifier ()->IsEqual (requestorNode->GetChordIdentifier ()))
   { // Reset own successor
    virtualNode->SetSuccessor (Create<ChordNode> (successorNode));
    DoFixFinger (virtualNode);
    virtualNode->SetRoutable (true);
    NS_LOG_INFO ("Successor changed for VNode");
   }
}

void
ChordIpv4::ProcessLeaveRsp (ChordMessage chordMessage)
{
  NS_LOG_FUNCTION (this << &chordMessage);
  Ptr<ChordNode> requestorNode = chordMessage.GetRequestorNode ();
  Ptr<ChordNode> successorNode = chordMessage.GetLeaveRsp ().successorNode;
  Ptr<ChordNode> predecessorNode = chordMessage.GetLeaveRsp ().predecessorNode;

  Ptr<ChordIdentifier> requestorIdentifier = requestorNode->GetChordIdentifier ();
  Ptr<ChordIdentifier> successorIdentifier = successorNode->GetChordIdentifier ();
  Ptr<ChordIdentifier> predecessorIdentifier = predecessorNode->GetChordIdentifier ();

  if (!m_dHashVNodeKeyOwnershipFn.IsNull ())
   { // Send trigger to dHash
    m_dHashVNodeKeyOwnershipFn (successorIdentifier, requestorIdentifier, predecessorIdentifier, successorNode->GetIpAddress (), successorNode->GetDHashPort ());
   }
}

void
ChordIpv4::ProcessLookupReq (ChordMessage chordMessage)
{
  NS_LOG_FUNCTION (this << &chordMessage);
  Ptr<ChordNode> requestorNode = chordMessage.GetRequestorNode ();
  Ptr<ChordIdentifier> requestedIdentifier = chordMessage.GetLookupReq ().requestedIdentifier;
  uint32_t transactionId = chordMessage.GetTransactionId ();
  if (m_vNodeMap.GetSize () == 0)
   { // No vNode exists as yet, drop this request.
    return;
   }
  // Check if we are owner of requestedIdentifier
  Ptr<ChordVNode> virtualNode = LookupLocal (requestedIdentifier);
  if (virtualNode)
   {
    ChordMessage chordMessageRsp = ChordMessage (m_maxPacketTTL);
    virtualNode->PackLookupRsp (requestorNode, transactionId, chordMessageRsp);
    NS_LOG_INFO ("Sending LookupRsp: " << chordMessageRsp);
    SendChordMessage (chordMessageRsp, requestorNode->GetIpAddress (), requestorNode->GetPort ());
    return;
   }
  // Could not resolve lookup request, forward to nearest successor
  RouteChordMessage (requestedIdentifier, chordMessage);
}

void
ChordIpv4::ProcessLookupRsp (ChordMessage chordMessage)
{
  NS_LOG_FUNCTION (this << &chordMessage);
  // Extract info from packet
  Ptr<ChordNode> requestorNode = chordMessage.GetRequestorNode ();
  Ptr<ChordNode> resolvedNode = chordMessage.GetLookupRsp ().resolvedNode;
  // Find virtual node which sent this message
  Ptr<ChordVNode> virtualNode = FindVNode (requestorNode->GetChordIdentifier ());
  if (virtualNode)
   {
    // Find Transaction
    Ptr<ChordTransaction> chordTransaction = virtualNode->FindTransaction (chordMessage.GetTransactionId ());
    if (!chordTransaction)
     { // No transaction exists, return from here
      return;
     }
    Ptr<ChordIdentifier> requestedIdentifier = chordTransaction->GetRequestedIdentifier ();
    ChordTransaction::Originator originator = chordTransaction->GetOriginator ();
    // cancel transaction
    virtualNode->RemoveTransaction (chordTransaction->GetTransactionId ());
    // notify application about lookup success
    NotifyLookupSuccess (requestedIdentifier, resolvedNode, originator);
   }
}

void
ChordIpv4::ProcessStabilizeReq (ChordMessage chordMessage)
{
  NS_LOG_FUNCTION (this << &chordMessage);
  Ptr<ChordNode> requestorNode = chordMessage.GetRequestorNode ();

  // Read payload and get vnode identifier
  Ptr<ChordIdentifier> vNodeIdentifier = chordMessage.GetStabilizeReq ().successorIdentifier;

  // Find VNode
  Ptr<ChordVNode> virtualNode = FindVNode (vNodeIdentifier);
  if (!virtualNode)
   { // VNode does not exist here, drop packet
    return;
   }

  // Stabilize
  if (requestorNode->GetChordIdentifier ()->IsInBetween (virtualNode->GetPredecessor ()->GetChordIdentifier (), virtualNode->GetChordIdentifier ()))
   {
    // Reset own predecessor
    Ptr<ChordNode> predecessorNode = Create<ChordNode> (requestorNode);
    Ptr<ChordNode> oldPredecessorNode = virtualNode->GetPredecessor ();
    virtualNode->SetPredecessor (predecessorNode);
    // Check if requestor can be our successor as well (bootstrap case)
    if (virtualNode->GetSuccessor ()->GetChordIdentifier ()->IsEqual (virtualNode->GetChordIdentifier ()))
     {
      // Reset Successor as well
      Ptr<ChordNode> successorNode = Create<ChordNode> (requestorNode);
      virtualNode->SetSuccessor (successorNode);
      virtualNode->SetRoutable (true);
      // Stabilize
      DoStabilize (virtualNode);
      DoFixFinger (virtualNode);
     }
    NotifyVNodeKeyOwnership (virtualNode->GetVNodeName (), virtualNode->GetChordIdentifier (), virtualNode->GetPredecessor (), oldPredecessorNode->GetChordIdentifier ());
    NS_LOG_INFO ("Predecessor changed for VNode");
   }
  // Send Response
  ChordMessage chordMessageRsp = ChordMessage (m_maxPacketTTL);
  virtualNode->PackStabilizeRsp (requestorNode, chordMessageRsp);
  NS_LOG_INFO ("Sending StabilizeRsp: " << chordMessageRsp);
  SendChordMessage (chordMessageRsp, requestorNode->GetIpAddress (), requestorNode->GetPort ());
  return;
}

void
ChordIpv4::ProcessStabilizeRsp (ChordMessage chordMessage)
{
  NS_LOG_FUNCTION (this << &chordMessage);
  // Extract info
  Ptr<ChordNode> requestorNode = chordMessage.GetRequestorNode ();
  // Read payload
  Ptr<ChordNode> predecessorNode = chordMessage.GetStabilizeRsp ().predecessorNode;

  // Find VNode
  Ptr<ChordVNode> virtualNode = FindVNode (requestorNode->GetChordIdentifier ());
  if (!virtualNode)
   {
    // VNode does not exist here, drop packet
    return;
   }

  // Reset Successor if needed
  if (!virtualNode->GetChordIdentifier ()->IsEqual (predecessorNode->GetChordIdentifier ()))
   {
    // We need to reset successor and restabilize new successor
    Ptr<ChordNode> successorNode = Create<ChordNode> (predecessorNode);
    virtualNode->SetSuccessor (successorNode);
    virtualNode->SetRoutable (true);
    NS_LOG_INFO ("Successor changed for VNode");
    // Trigger stabilization
    DoStabilize (virtualNode);
    DoFixFinger (virtualNode);
    return;
   }
  // Reset timestamp
  virtualNode->GetSuccessor ()->SetTimestamp (Simulator::Now ());
  // Synch successor list
  virtualNode->SynchSuccessorList (chordMessage.GetStabilizeRsp ().successorList);
}

void
ChordIpv4::ProcessHeartbeatReq (ChordMessage chordMessage)
{
  NS_LOG_FUNCTION (this << &chordMessage);
  Ptr<ChordNode> requestorNode = chordMessage.GetRequestorNode ();

  // Read payload and get vnode identifier
  Ptr<ChordIdentifier> vNodeIdentifier = chordMessage.GetHeartbeatReq ().predecessorIdentifier;

  // Find VNode
  Ptr<ChordVNode> virtualNode = FindVNode (vNodeIdentifier);
  if (!virtualNode)
   {
    // VNode does not exist here, drop packet
    return;
   }

  // Reply to heartbeat
  ChordMessage chordMessageRsp = ChordMessage (m_maxPacketTTL);
  virtualNode->PackHeartbeatRsp (requestorNode, chordMessageRsp);
  NS_LOG_INFO ("Sending StabilizeRsp: " << chordMessageRsp);
  SendChordMessage (chordMessageRsp, requestorNode->GetIpAddress (), requestorNode->GetPort ());
  return;
}

void
ChordIpv4::ProcessHeartbeatRsp (ChordMessage chordMessage)
{
  NS_LOG_FUNCTION (this << &chordMessage);
  // Extract info
  Ptr<ChordNode> requestorNode = chordMessage.GetRequestorNode ();
  // Read payload -- Not needed for Heartbeats
  /*
   */
  // Find VNode
  Ptr<ChordVNode> virtualNode = FindVNode (requestorNode->GetChordIdentifier ());
  if (!virtualNode)
   {
    // VNode does not exist here, drop packet
    return;
   }
  // Reset timestamp
  virtualNode->GetPredecessor ()->SetTimestamp (Simulator::Now ());
  // Synch predecessor list
  virtualNode->SynchPredecessorList (chordMessage.GetHeartbeatRsp ().predecessorList);
}

void
ChordIpv4::ProcessFingerReq (ChordMessage chordMessage)
{
  NS_LOG_FUNCTION (this << &chordMessage);
  Ptr<ChordNode> requestorNode = chordMessage.GetRequestorNode ();
  Ptr<ChordIdentifier> requestedIdentifier = chordMessage.GetFingerReq ().requestedIdentifier;
  if (m_vNodeMap.GetSize () == 0)
   {
    // No vNode exists as yet, drop this request.
    return;
   }
  // Check if we are owner of requestedIdentifier
  Ptr<ChordVNode> virtualNode = LookupLocal (requestedIdentifier);
  if (virtualNode)
   {
    ChordMessage chordMessageRsp = ChordMessage (m_maxPacketTTL);
    virtualNode->PackFingerRsp (requestorNode, requestedIdentifier, chordMessageRsp);
    NS_LOG_INFO ("Sending FingerRsp: " << chordMessageRsp);
    SendChordMessage (chordMessageRsp, requestorNode->GetIpAddress (), requestorNode->GetPort ());
    return;
   }
  // Could not resolve finger request, forward to successor
  Ptr<ChordVNode> vNode = FindNearestVNode (requestedIdentifier);
  if (vNode)
   {
    SendChordMessage (chordMessage, vNode->GetSuccessor ()->GetIpAddress (), vNode->GetSuccessor ()->GetPort ());
   }
  else
   {
    SendChordMessageViaAnyVNode (chordMessage);
   }
}

void
ChordIpv4::ProcessFingerRsp (ChordMessage chordMessage)
{
  NS_LOG_FUNCTION (this << &chordMessage);
  // Extract info from packet
  Ptr<ChordNode> requestorNode = chordMessage.GetRequestorNode ();
  Ptr<ChordIdentifier> requestedIdentifier = chordMessage.GetFingerRsp ().requestedIdentifier;
  Ptr<ChordNode> fingerNode = chordMessage.GetFingerRsp ().fingerNode;
  // Find virtual node which sent this message
  Ptr<ChordVNode> virtualNode = FindVNode (requestorNode->GetChordIdentifier ());
  if (virtualNode)
   {
    // Save finger lookup in table
    Ptr<ChordNode> finger = Create<ChordNode> (fingerNode);
    virtualNode->GetFingerTable ().UpdateNode (fingerNode);
   }
}

void
ChordIpv4::ProcessTraceRing (ChordMessage chordMessage)
{
  NS_LOG_FUNCTION (this << &chordMessage);
  // Make Up-call
  NS_LOG_FUNCTION_NOARGS ();
  Ptr<ChordNode> requestorNode = chordMessage.GetRequestorNode ();

  // Read payload and get vnode identifier
  Ptr<ChordIdentifier> vNodeIdentifier = chordMessage.GetTraceRing ().successorIdentifier;

  // Find VNode
  Ptr<ChordVNode> virtualNode = FindVNode (vNodeIdentifier);
  if (!virtualNode)
   {
    // VNode does not exist here, drop packet
    return;
   }
  NotifyTraceRing (virtualNode->GetVNodeName (), virtualNode->GetChordIdentifier ());
  // Forward Trace Ring
  // Originator has to remove the packet
  if (requestorNode->GetChordIdentifier ()->IsEqual (vNodeIdentifier))
   {
    return;
   }
  ChordMessage chordMessageFwd = ChordMessage (m_maxPacketTTL);
  virtualNode->PackTraceRing (requestorNode, chordMessageFwd);
  NS_LOG_INFO ("Forwarding TraceRing: " << chordMessageFwd);
  SendChordMessage (chordMessageFwd, virtualNode->GetSuccessor ()->GetIpAddress (), virtualNode->GetSuccessor ()->GetPort ());
}

void
ChordIpv4::DoLookup (Ptr<ChordIdentifier> requestedIdentifier, ChordTransaction::Originator originator)
{
  NS_LOG_FUNCTION (this << requestedIdentifier << originator);
  Ptr<ChordVNode> virtualNode = LookupLocal (requestedIdentifier); // Find local
  if (virtualNode)
   { // We are owner, report success
    NotifyLookupSuccess (requestedIdentifier, virtualNode, originator);
    return;
   }
  // Initiate lookup request
  virtualNode = FindNearestVNode (requestedIdentifier);
  if (virtualNode)
   {
    ChordMessage chordMessage = ChordMessage (m_maxPacketTTL);
    virtualNode->PackLookupReq (requestedIdentifier, chordMessage);
    // Add transaction
    Ptr<ChordTransaction> chordTransaction = Create<ChordTransaction> (chordMessage.GetTransactionId (), chordMessage, m_requestTimeout, m_maxRequestRetries);
    chordTransaction->SetOriginator (originator);
    chordTransaction->SetRequestedIdentifier (requestedIdentifier);
    // Add to vNode
    virtualNode->AddTransaction (chordMessage.GetTransactionId (), chordTransaction);
    // Start transaction timer
    EventId requestTimeoutId = Simulator::Schedule (chordTransaction->GetRequestTimeout (), &ChordIpv4::HandleRequestTimeout, this, virtualNode, chordMessage.GetTransactionId ());
    chordTransaction->SetRequestTimeoutEventId (requestTimeoutId);
    RouteChordMessageViaFinger (requestedIdentifier, virtualNode, chordMessage);
   }
  else
   {
    NotifyLookupFailure (requestedIdentifier, originator);
    return;
   }
}

void
ChordIpv4::DoStabilize (Ptr<ChordVNode> virtualNode)
{
  if (virtualNode->GetSuccessor ()->GetChordIdentifier ()->IsEqual (virtualNode->GetChordIdentifier ()))
   {
    // Reset timestamp
    virtualNode->GetSuccessor ()->SetTimestamp (Simulator::Now ());
    return;
   }
  ChordMessage chordMessage = ChordMessage (m_maxPacketTTL);
  virtualNode->PackStabilizeReq (chordMessage);
  NS_LOG_INFO ("Sending StabilizeReq: " << chordMessage);
  SendChordMessage (chordMessage, virtualNode->GetSuccessor ()->GetIpAddress (), virtualNode->GetSuccessor ()->GetPort ());
}

void
ChordIpv4::DoHeartbeat (Ptr<ChordVNode> virtualNode)
{
  if (virtualNode->GetPredecessor ()->GetChordIdentifier ()->IsEqual (virtualNode->GetChordIdentifier ()))
   {
    // Reset timestamp
    virtualNode->GetPredecessor ()->SetTimestamp (Simulator::Now ());
    return;
   }
  ChordMessage chordMessage = ChordMessage (m_maxPacketTTL);
  virtualNode->PackHeartbeatReq (chordMessage);
  NS_LOG_INFO ("Sending HeartbeatReq: " << chordMessage);
  SendChordMessage (chordMessage, virtualNode->GetPredecessor ()->GetIpAddress (), virtualNode->GetPredecessor ()->GetPort ());
}

void
ChordIpv4::DoFixFinger (Ptr<ChordVNode> virtualNode)
{
  // Do not fix fingers for unstable v-nodes
  if (virtualNode->GetSuccessor ()->GetChordIdentifier ()->IsEqual (virtualNode->GetChordIdentifier ()))
   {
    return;
   }
  // Remove stale entries from finger table; Remove even if finger request failed in last try. TODO: Use transactions for finger requests??
  virtualNode->GetFingerTable ().Audit (m_fixFingerInterval);

  virtualNode->GetStats ().fingersLookedUp = 0;

  for (std::vector<Ptr<ChordIdentifier> >::iterator fingerIter = virtualNode->GetFingerIdentifierList ().begin (); fingerIter != virtualNode->GetFingerIdentifierList ().end (); fingerIter++)
   {
    Ptr<ChordIdentifier> fingerIdentifier = *fingerIter;
    // Do not lookup local identifiers
    Ptr<ChordVNode> vNode = LookupLocal (fingerIdentifier);
    if (vNode)
     {
      continue;
     }
    // Do not lookup fingers between successor and this node
    if (fingerIdentifier->IsInBetween (virtualNode->GetChordIdentifier (), virtualNode->GetSuccessor ()->GetChordIdentifier ()))
     {
      // Make routing entry
      Ptr<ChordNode> fingerNode = Create<ChordNode> (fingerIdentifier, virtualNode->GetSuccessor ()->GetIpAddress (), virtualNode->GetSuccessor ()->GetPort (), virtualNode->GetSuccessor ()->GetApplicationPort (), virtualNode->GetSuccessor ()->GetDHashPort ());
      virtualNode->GetFingerTable ().UpdateNode (fingerNode);
      continue;
     }
    ChordMessage chordMessage = ChordMessage (m_maxPacketTTL);
    virtualNode->PackFingerReq (fingerIdentifier, chordMessage);
    virtualNode->GetStats ().fingersLookedUp++;
    NS_LOG_INFO ("Sending FingerReq: " << chordMessage);
    // Route using successor for finger fixing
    SendChordMessage (chordMessage, virtualNode->GetSuccessor ()->GetIpAddress (), virtualNode->GetSuccessor ()->GetPort ());
   }
}

Ptr<ChordVNode>
ChordIpv4::FindVNode (Ptr<ChordIdentifier> chordIdentifier)
{
  NS_LOG_FUNCTION (this << chordIdentifier);
  // Iterate VNode list and check if v node exists
  Ptr<ChordNode> chordNode = m_vNodeMap.FindNode (chordIdentifier);
  if (!chordNode)
   {
    return 0;
   }
  return DynamicCast<ChordVNode> (chordNode);
}

Ptr<ChordVNode>
ChordIpv4::FindVNode (std::string vNodeName)
{
  NS_LOG_FUNCTION (this << vNodeName);
  Ptr<ChordNode> chordNode = m_vNodeMap.FindNode (vNodeName);
  if (!chordNode)
   {
    return 0;
   }
  return DynamicCast<ChordVNode> (chordNode);
}

/*  Logic: We need to send packet via virtual node whose key is nearest to 
 * the requested key. Our aim is to minimize lookup hops.
 *
 *  Step 1: Iterate for all virtual nodes and maximize identifier for v-nodes 
 * which satisfies: vnode lies inBetween (0,key] <closestVNodeOnRight>
 *  Step 2: If none found in step 1, send to v-node with highest key 
 * number <closestVNodeOnLeft>
 */
Ptr<ChordVNode>
ChordIpv4::FindNearestVNode (Ptr<ChordIdentifier> targetIdentifier)
{
  NS_LOG_FUNCTION (this << targetIdentifier);
  Ptr<ChordNode> chordNode = m_vNodeMap.FindNearestNode (targetIdentifier);
  if (!chordNode)
   {
    return 0;
   }
  return DynamicCast<ChordVNode> (chordNode);
}

void
ChordIpv4::DeleteVNode (Ptr<ChordIdentifier> chordIdentifier)
{
  NS_LOG_FUNCTION (this << chordIdentifier);
  m_vNodeMap.RemoveNode (chordIdentifier);
}

void
ChordIpv4::DeleteVNode (std::string vNodeName)
{
  NS_LOG_FUNCTION (this << vNodeName);
  m_vNodeMap.RemoveNode (vNodeName);
}

Ptr<ChordVNode>
ChordIpv4::LookupLocal (Ptr<ChordIdentifier> chordIdentifier)
{
  NS_LOG_FUNCTION (this << chordIdentifier);
  // Iterate VNode list and check if we are owner
  vector<Ptr<ChordNode> > chordNodes = m_vNodeMap.GetNodes ();
  for (vector<Ptr<ChordNode> >::iterator vNodeIter = chordNodes.begin (); vNodeIter != chordNodes.end (); vNodeIter++)
   {
    Ptr<ChordVNode> vNode = DynamicCast<ChordVNode> (*vNodeIter);

    if (vNode->GetPredecessor () == 0)
     {
      continue;
     }
    if (chordIdentifier->IsInBetween (vNode->GetPredecessor ()->GetChordIdentifier (), vNode->GetChordIdentifier ()))
     {
      // Do not accept ownership if we have set ourselves as predecessor, but accept in bootstrap case. This means our predecessor recently died and we are waiting for someone to send us stabilize.
      if (vNode->GetPredecessor ()->GetChordIdentifier ()->IsEqual (vNode->GetChordIdentifier ()) && !(isBootStrapNode && m_vNodeMap.GetSize () == 1))
       {
        continue;
       }
      else
       { // We are the owner. Set virtualNode pointer and return success
        return DynamicCast<ChordVNode> (*vNodeIter);
       }
     }
   }
  NS_LOG_INFO ("NOT OWNER, localIpAddress: " << m_localIpAddress << " listeningPort" << m_listeningPort);
  return 0; // We are not owner, return failure
}

void
ChordIpv4::SendChordMessage (ChordMessage &chordMessage, Ipv4Address destinationIp, uint16_t destinationPort)
{
  NS_LOG_FUNCTION (this << &chordMessage << destinationIp << destinationPort);
  if (!UpdateTTL (chordMessage))
   {
    NS_LOG_INFO ("Dropping packet: TTL expiry");
    return;
   }
  Ptr<Packet> packet = Create<Packet> ();
  packet->AddHeader (chordMessage);
  if (packet->GetSize ())
   {
    SendPacket (packet, destinationIp, destinationPort);
   }
}

void
ChordIpv4::SendPacket (Ptr<Packet> packet, Ipv4Address destinationIp, uint16_t destinationPort)
{
  NS_LOG_FUNCTION (this << packet << destinationIp << destinationPort);
  if (m_started)
   {
    if (destinationIp == m_localIpAddress && destinationPort == m_listeningPort)
     {
      Simulator::ScheduleNow (&ChordIpv4::ProcessMessage, this, packet);
     }
    else
     {
      m_socket->SendTo (packet, 0, InetSocketAddress (destinationIp, destinationPort));
     }
   }
  else
   NS_LOG_ERROR ("Error: ChordIpv4 layer inactive");
}

bool
ChordIpv4::SendChordMessageViaAnyVNode (ChordMessage &chordMessage)
{
  NS_LOG_FUNCTION (this << &chordMessage);
  if (!UpdateTTL (chordMessage))
   {
    NS_LOG_INFO ("Dropping packet: TTL expiry");
    return true;
   }
  Ptr<Packet> packet = Create<Packet> ();
  packet->AddHeader (chordMessage);
  if (packet->GetSize ())
   {
    return SendViaAnyVNode (packet);
   }
  else
   {
    return false;
   }
}

bool
ChordIpv4::SendViaAnyVNode (Ptr<Packet> packet)
{
  NS_LOG_FUNCTION (this << packet);
  // Find first v-node with valid successor
  vector<Ptr<ChordNode> > chordNodes = m_vNodeMap.GetNodes ();
  for (vector<Ptr<ChordNode> >::iterator vNodeIter = chordNodes.begin (); vNodeIter != chordNodes.end (); vNodeIter++)
   {
    Ptr<ChordVNode> vNode = DynamicCast<ChordVNode> (*vNodeIter);
    // Choose any node whose successor is not set as self
    if (!vNode->GetSuccessor ()->GetChordIdentifier ()->IsEqual (vNode->GetChordIdentifier ()))
     {
      SendPacket (packet, vNode->GetSuccessor ()->GetIpAddress (), vNode->GetSuccessor ()->GetPort ());
      return true;
     }
   }
  return false;
}

bool
ChordIpv4::RouteChordMessage (Ptr<ChordIdentifier> targetIdentifier, ChordMessage &chordMessage)
{
  NS_LOG_FUNCTION (this << targetIdentifier << &chordMessage);
  if (!UpdateTTL (chordMessage))
   {
    NS_LOG_INFO ("Dropping packet: TTL expiry");
    return true;
   }
  Ptr<Packet> packet = Create<Packet> ();
  packet->AddHeader (chordMessage);
  if (packet->GetSize ())
   {
    return RoutePacket (targetIdentifier, packet);
   }
  else
   {
    return false;
   }
}

bool
ChordIpv4::RoutePacket (Ptr<ChordIdentifier> targetIdentifier, Ptr<Packet> packet)
{
  NS_LOG_FUNCTION (this << targetIdentifier << packet);
  if (packet->GetSize ())
   {
    Ptr<ChordVNode> vNode = FindNearestVNode (targetIdentifier);
    // Choose best vNode
    if (vNode)
     {
      if (RouteViaFinger (targetIdentifier, vNode, packet) == true)
       {
        return true;
       }
     }
    else
     {
      return SendViaAnyVNode (packet);
     }
   }
  return false;
}

bool
ChordIpv4::RouteChordMessageViaFinger (Ptr<ChordIdentifier> targetIdentifier, Ptr<ChordVNode> vNode, ChordMessage &chordMessage)
{
  NS_LOG_FUNCTION (this << targetIdentifier << vNode << &chordMessage);
  if (!UpdateTTL (chordMessage))
   {
    NS_LOG_INFO ("Dropping packet: TTL expiry");
    return true;
   }
  Ptr<Packet> packet = Create<Packet> ();
  packet->AddHeader (chordMessage);
  if (packet->GetSize ())
   {
    return RouteViaFinger (targetIdentifier, vNode, packet);
   }
  else
   {
    return false;
   }
}

bool
ChordIpv4::RouteViaFinger (Ptr<ChordIdentifier> targetIdentifier, Ptr<ChordVNode> vNode, Ptr<Packet> packet)
{
  NS_LOG_FUNCTION (this << targetIdentifier << vNode << packet);
  if (packet->GetSize ())
   {
    Ptr<ChordNode> remoteNode = vNode->GetFingerTable ().FindNearestNode (targetIdentifier);
    // Choose nearest finger
    if (remoteNode)
     {
      SendPacket (packet, remoteNode->GetIpAddress (), remoteNode->GetPort ());
      return true;
     }
    else
     {
      // Send to successor
      SendPacket (packet, vNode->GetSuccessor ()->GetIpAddress (), vNode->GetSuccessor ()->GetPort ());
      return true;
     }
   }
  return false;
}

bool
ChordIpv4::UpdateTTL (ChordMessage &chordMessage)
{
  if (chordMessage.GetTTL () <= 0)
   {
    return false;
   }
  chordMessage.SetTTL (chordMessage.GetTTL () - 1);
  return true;
}

void
ChordIpv4::HandleRequestTimeout (Ptr<ChordVNode> vNode, uint32_t transactionId)
{
  NS_LOG_FUNCTION (this << vNode << transactionId);
  // Find transaction
  Ptr<ChordTransaction> chordTransaction = vNode->FindTransaction (transactionId);
  if (!chordTransaction)
   {
    // Transaction does not exist
    return;
   }
  // Retransmit and reschedule if needed
  if (chordTransaction->GetRetries () > chordTransaction->GetMaxRetries ())
   {
    // Report failure
    if (chordTransaction->GetChordMessage ().GetMessageType () == ChordMessage::JOIN_REQ)
     {
      NS_LOG_ERROR ("Join request failed! " << (uint16_t) chordTransaction->GetRetries () << " " << (uint16_t) chordTransaction->GetMaxRetries ());
      // Delete vNode
      DeleteVNode (vNode->GetChordIdentifier ());
      NotifyVNodeFailure (vNode->GetVNodeName (), vNode->GetChordIdentifier ());
     }
    else if (chordTransaction->GetChordMessage ().GetMessageType () == ChordMessage::LOOKUP_REQ)
     {
      NS_LOG_ERROR ("Lookup Request failed!");
      // cancel transaction
      vNode->RemoveTransaction (chordTransaction->GetTransactionId ());
      NotifyLookupFailure (chordTransaction->GetChordMessage ().GetLookupReq ().requestedIdentifier, chordTransaction->GetOriginator ());
     }
    return;
   }
  else
   {
    ChordMessage chordMessage = chordTransaction->GetChordMessage ();
    // Retransmit
    uint8_t retries = chordTransaction->GetRetries ();
    chordTransaction->SetRetries (retries + 1);
    NS_LOG_INFO ("Retransmission Req\n" << chordTransaction->GetChordMessage ());
    SendChordMessage (chordMessage, m_bootStrapIp, m_listeningPort);
    // Reschedule
    // Start transaction timer
    EventId requestTimeoutId = Simulator::Schedule (chordTransaction->GetRequestTimeout (), &ChordIpv4::HandleRequestTimeout, this, vNode, transactionId);
    chordTransaction->SetRequestTimeoutEventId (requestTimeoutId);
   }
}

} // namespace ns3
