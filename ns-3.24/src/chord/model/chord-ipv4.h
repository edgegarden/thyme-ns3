/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2009 University of Pennsylvania
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#ifndef CHORD_IPV4_H
#define CHORD_IPV4_H

#include "ns3/application.h"
#include "ns3/event-id.h"
#include "ns3/ptr.h"
#include "ns3/ipv4-address.h"
#include "ns3/traced-callback.h"
#include "ns3/timer.h"
#include "ns3/random-variable-stream.h"
#include "ns3/chord-identifier.h"
#include "ns3/chord-node.h"
#include "ns3/chord-vnode.h"
#include "ns3/chord-message.h"
#include "ns3/chord-node-table.h"
#include "ns3/dhash-ipv4.h"

/* Static defines */
// m = 160 bits i.e. 20 bytes
#define DEFAULT_CHORD_IDENTIFIER_BYTES 20
// Stabilize interval
#define DEFAULT_STABILIZE_INTERVAL 2000 // 500
// Heartbeat interval
#define DEFAULT_HEARTBEAT_INTERVAL 2000 // 500
// Fix Finger interval
#define DEFAULT_FIX_FINGER_INTERVAL 15000 // 10000
// Max missed keep alives (Stabilize and Heartbeat)
#define DEFAULT_MAX_MISSED_KEEP_ALIVES 4
// Request timeout
#define DEFAULT_REQUEST_TIMEOUT 5000 // 1000
// Max request retries
#define DEFAULT_MAX_REQUEST_RETRIES 3
// Max Successor List Size
#define DEFAULT_MAX_VNODE_SUCCESSOR_LIST_SIZE 8
// Max Predecessor List Size
#define DEFAULT_MAX_VNODE_PREDECESSOR_LIST_SIZE 8
// Max Packet TimeToLive
#define DEFAULT_MAX_PACKET_TTL 32

namespace ns3 {

class Socket;
class Packet;

/**
 *  \ingroup applications
 *  \defgroup chordipv4 ChordIpv4
 */

/**
 *  \ingroup chordipv4
 *  \brief Implementation of Chord/DHash DHT (http://pdos.csail.mit.edu/chord/)
 *
 *  Provides API for Configuring and Operating Chord/DHash DHT
 *
 */

class ChordIpv4 : public Object
{
public:
  static TypeId GetTypeId (void);

  ChordIpv4 ();

  virtual ~ChordIpv4 ();

  void Start (Ptr<Node> node, Ipv4Address localIp, uint16_t dhashPort);
  void SetBootstrapIp (Ipv4Address bootstrapIp);

  /* Application interface (ChordIpv4 Service User) */

  /**
   *  \brief Create and Insert VirtualNode(ChordVNode) in Chord overlay network
   *  \param vNodeName Name of VirtualNode(ChordVNode). ChordIpv4 layer makes 
   * upcalls based on both VirtualNode(ChordVNode) name as well as key
   * \param key Pointer to key array (VirtualNode(ChordVNode) identifier)
   * \param keyBytes Number of bytes in key (max 255)
   *
   * On invocation, Join request is sent to Boot Strap node (default). If 
   * VirtualNode(ChordVNode)(s) already exist on local physical node, then 
   * Join Request is sent via node closest (in terms of identifier distance) 
   * to the VirtualNode(ChordVNode) identifier.
   * A Chord VirtualNode(ChordVNode) (which is already part of the network), 
   * on receiving Join Request responds if the identifier of requesting node 
   * lies under its purview <lies in range (n.predecessor, n]> . If the 
   * identifier of requestor is out of purview then the request is forwarded 
   * to nearest remote node.
   *
   * On reception of Join Response, ChordIpv4 makes upcall to application if 
   * callback is function is set via SetJoinSuccessCallback. Please Note that 
   * this notification upcall is made even before initial stabilization 
   * (actual insertion into network) procedure. This upcall merely states 
   * that lookup involved with Join procedure was a success and possible 
   * successor was resolved.
   *
   * This request is retransmitted later in case response is not received 
   * on time (configurable). ChordIpv4 gives up retransmission after 
   * configurable number of retries.
   *
   * On failure to resolve successor (e.g. boot strap node is down) after 
   * a few retries, ChordIpv4 makes notification upcall to function registered 
   * via SetInsertFailureCallback by user. On Join failure, 
   * VirtualNode(ChordVNode) object is deleted from ChordIpv4 layer.
   */
  void InsertVNode (std::string vNodeName, uint8_t * key, uint8_t keyBytes);

  /**
   *  \brief Check whether the any VirtualNode(ChordVNode) running on local 
   * physical node owns particular identifier.
   *  \param key Pointer to key array (identifier)
   *  \param lookupKeyBytes Number of bytes in key (max 255)
   *
   *  \returns true if local ChordIpv4 is owner of identifier, false if local 
   * ChordIpv4 is not owner.
   *
   *  On invocation, ChordIpv4 layer checks and returns ownership status of 
   * given identifier.
   */
  bool CheckOwnership (uint8_t * lookupKey, uint8_t lookupKeyBytes);

  /**
   *  \brief Lookup owner node of an identifier in Chord Network
   *  \param key Pointer to key array (identifier)
   *  \param lookupKeyBytes Number of bytes in key (max 255)
   *
   *  On invocation, ChordIpv4 layer sends Lookup Request via 
   * VirtualNode(ChordVNode) closest to requested identifer (if it is not owner).
   *  A VirtualNode(ChordVNode) which is owner of requested identifier responds 
   * with its IP address and Application Port. On reception of Response, a 
   * notification upcall is made to the function registered via 
   * SetLookupSuccessCallback.
   * This request is retransmitted later in case response is not received on 
   * time (configurable). ChordIpv4 gives up retransmission after configurable 
   * number of retries.
   * On failure to resolve identifier, ChordIpv4 makes a notification upcall 
   * to function registered via SetLookupFailureCallback by user.
   */
  void LookupKey (uint8_t * lookupKey, uint8_t lookupKeyBytes);
  void LookupKey (Ptr<ChordIdentifier> identifier);

  /**
   *  \brief Remove VirtualNode(ChordVNode) from Chord Network
   *  \param vNodeName Name of VirtualNode(ChordVNode)
   *
   *  On invocation, ChordIpv4 layer removes given VirtualNode(ChordVNode) 
   * from the Chord Network. Leave Requests are sent to successor and 
   * predecessor of the given VirtualNode(ChordVNode), notifying them of removal.
   *  On receiving a Leave Request, successor node sends back Leave Response. 
   * When Leave Response is received back, ChordIpv4 layer triggers DHash 
   * (DHashIpv4) layer to transfer stored DHashObjects belonging to given 
   * VirtualNode(ChordVNode) to its successor.
   *
   */
  void RemoveVNode (std::string vNodeName);

  /**
   *  \brief Registers Callback function for Join Success Notifications.
   *  \param joinSuccessFn Callback function
   */
  void
  SetJoinSuccessCallback (Callback <void, std::string, Ptr<ChordIdentifier> > joinSuccessFn)
  {
    m_joinSuccessFn = joinSuccessFn;
  }

  /**
   *  \brief Registers Callback function for Lookup Success Notifications.
   *  \param lookupSuccessFn This Callback is passed vNodeName, key and 
   * numBytes of key as parameters.
   */
  void
  SetLookupSuccessCallback (Callback <void, Ptr<ChordIdentifier>, Ipv4Address, uint16_t> lookupSuccessFn)
  {
    m_lookupSuccessFn = lookupSuccessFn;
  }

  /**
   *  \brief Registers Callback function for Lookup Failure Notifications.
   *  \param lookupFailureFn This Callback is passed lookup key, numBytes of 
   * lookup key, resolved IP and resolved port as parameters.
   */
  void
  SetLookupFailureCallback (Callback <void, Ptr<ChordIdentifier> > lookupFailureFn)
  {
    m_lookupFailureFn = lookupFailureFn;
  }

  /**
   *  \brief Registers Callback function for VirtualNode(ChordVNode) key 
   * space ownership change notifications.
   *  \param vNodeKeyOwnershipFn This Callback is passed vNodeName, vNode 
   * key, numBytes in vNode key, predecessor key, numBytes in predecessor key, 
   * oldPredecessor key, numBytes in oldPredecessor key, IP address of 
   * predecessor and application port of predecessor.
   *
   *  This upcall is made when predecessor of a VirtualNode(ChordVNode) 
   * changes (key space partitions).
   *  A similar upcall is made to DHash (DHashIpv4) layer, prompting it to 
   * shift stored objects to new owner.
   */
  void
  SetVNodeKeyOwnershipCallback (Callback <void, std::string, Ptr<ChordIdentifier>, Ptr<ChordIdentifier>, Ptr<ChordIdentifier>, Ipv4Address, uint16_t> vNodeKeyOwnershipFn)
  {
    m_vNodeKeyOwnershipFn = vNodeKeyOwnershipFn;
  }

  /**
   *  \brief Registers Callback function for Trace Ring packet notification 
   * (Diagnostics packet)
   *  \param traceRingFn This Callback is passed vNodeName, vNode key and 
   * numBytes in vNode key as parameters
   */
  void
  SetTraceRingCallback (Callback <void, std::string, Ptr<ChordIdentifier> > traceRingFn)
  {
    m_traceRingFn = traceRingFn;
  }

  /**
   *  \brief Registers Callback function for VirtualNode(ChordVNode) failure 
   * notification
   *  \param vNodeFailureCallback This Callback is passed vNodeName, vNode 
   * key and numBytes in vNode key as parameters
   *  This upcall is made when all successors of a VirtualNode(ChordVNode) 
   * fail (stabilization failure) and it cannot stay in Chord Network anymore.
   *  Please note that a bootstrap node (a VirtualNode(ChordVNode) which 
   * bootstrapped itself and created a new chord network) will never make 
   * this upcall or leave Chord Network.
   */
  void
  SetVNodeFailureCallback (Callback <void, std::string, Ptr<ChordIdentifier> > vNodeFailureFn)
  {
    m_vNodeFailureFn = vNodeFailureFn;
  }

  void
  SetDHashLookupSuccessCallback (Callback <void, Ptr<ChordIdentifier>, Ipv4Address, uint16_t> dHashLookupSuccessFn)
  {
    m_dHashLookupSuccessFn = dHashLookupSuccessFn;
  }

  void
  SetDHashLookupFailureCallback (Callback <void, Ptr<ChordIdentifier> > dHashLookupFailureFn)
  {
    m_dHashLookupFailureFn = dHashLookupFailureFn;
  }

  void
  SetDHashVNodeKeyOwnershipCallback (Callback <void, Ptr<ChordIdentifier>, Ptr<ChordIdentifier>, Ptr<ChordIdentifier>, Ipv4Address, uint16_t> dHashVNodeKeyOwnershipFn)
  {
    m_dHashVNodeKeyOwnershipFn = dHashVNodeKeyOwnershipFn;
  }

  // Diagnostics Interface
  /**
   *  \brief Dumps VirtualNode(ChordVNode) information
   *  \param vNodeName VirtualNode(ChordVNode) name
   *  \param os Output stream
   */
  void DumpVNodeInfo (std::string vNodeName, std::ostream &os);

  /**
   *  \brief Fires Trace Ring packet
   *  \param vNodeName VirtualNode(ChordVNode) name
   *
   *  On invocation, a VirtualNode(ChordVNode) sends Trace Ring packet towards 
   * its successor. On reception of such a packet, application on that physical 
   * node is notified of reception and the packet is forwarded further. The 
   * originator of this packet removes it from the network once it has 
   * travelled around the ring. This packet can be used to trace the ring 
   * structure in simulator.
   */
  void FireTraceRing (std::string vNodeName);

  /**
   *  \brief Manually fix fingers
   *  \param vNodeName VirtualNode(ChordVNode) name
   */
  void FixFingers (std::string vNodeName);

protected:
  virtual void DoDispose (void);

private:
  bool m_started;

  Ptr<Node> m_node;
  Ptr<Socket> m_socket;
  Ipv4Address m_bootStrapIp;
  Ipv4Address m_localIpAddress;
  uint16_t m_listeningPort;
  uint16_t m_applicationPort;
  uint16_t m_dHashPort;

  uint8_t m_maxVNodeSuccessorListSize;
  uint8_t m_maxVNodePredecessorListSize;

  bool isBootStrapNode;

  ChordNodeTable m_vNodeMap;

  // Timers
  Timer m_stabilizeTimer;
  Time m_stabilizeInterval;
  Timer m_heartbeatTimer;
  Time m_heartbeatInterval;
  Timer m_fixFingerTimer;
  Time m_fixFingerInterval;

  Time m_requestTimeout;
  uint8_t m_maxMissedKeepAlives;

  uint8_t m_maxRequestRetries;
  uint8_t m_maxPacketTTL;

  void StabilizeTimerExpire ();
  void HeartBeatTimerExpire ();

  // Periodic task methods
  void DoPeriodicStabilize ();
  void DoPeriodicHeartbeat ();
  void DoPeriodicFixFinger ();

  // Callbacks
  Callback<void, std::string, Ptr<ChordIdentifier> > m_joinSuccessFn;
  Callback<void, Ptr<ChordIdentifier>, Ipv4Address, uint16_t> m_lookupSuccessFn;
  Callback<void, Ptr<ChordIdentifier> > m_lookupFailureFn;
  Callback<void, std::string, Ptr<ChordIdentifier>, Ptr<ChordIdentifier>, Ptr<ChordIdentifier>, Ipv4Address, uint16_t> m_vNodeKeyOwnershipFn;
  Callback<void, std::string, Ptr<ChordIdentifier> > m_traceRingFn;
  Callback<void, std::string, Ptr<ChordIdentifier> > m_vNodeFailureFn;

  // DHash callbacks
  Callback<void, Ptr<ChordIdentifier>, Ipv4Address, uint16_t> m_dHashLookupSuccessFn;
  Callback<void, Ptr<ChordIdentifier> > m_dHashLookupFailureFn;
  Callback <void, Ptr<ChordIdentifier>, Ptr<ChordIdentifier>, Ptr<ChordIdentifier>, Ipv4Address, uint16_t> m_dHashVNodeKeyOwnershipFn;

  // Upcall (notify) methods
  void NotifyJoinSuccess (std::string vNodeName, Ptr<ChordIdentifier> chordIdentifier);
  void NotifyLookupSuccess (Ptr<ChordIdentifier> lookupIdentifier,
                            Ptr<ChordNode> resolvedNode, ChordTransaction::Originator originator);
  void NotifyLookupFailure (Ptr<ChordIdentifier> chordIdentifier, ChordTransaction::Originator originator);
  void NotifyVNodeKeyOwnership (std::string vNodeName, Ptr<ChordIdentifier> chordIdentifier,
                                Ptr<ChordNode> predecessorNode, Ptr<ChordIdentifier> oldPredecessorIdentifier);
  void NotifyTraceRing (std::string vNodeName, Ptr<ChordIdentifier> chordIdentifier);
  void NotifyVNodeFailure (std::string vNodeName, Ptr<ChordIdentifier> chordIdentifier);

  // Message processing methods
  void ProcessMessage (Ptr<Packet> packet);
  void ProcessUdpPacket (Ptr<Socket> socket);
  void ProcessJoinReq (ChordMessage chordMessage);
  void ProcessJoinRsp (ChordMessage chordMessage);
  void ProcessLeaveReq (ChordMessage chordMessage);
  void ProcessLeaveRsp (ChordMessage chordMessage);
  void ProcessLookupReq (ChordMessage chordMessage);
  void ProcessLookupRsp (ChordMessage chordMessage);
  void ProcessStabilizeReq (ChordMessage chordMessage);
  void ProcessStabilizeRsp (ChordMessage chordMessage);
  void ProcessHeartbeatReq (ChordMessage chordMessage);
  void ProcessHeartbeatRsp (ChordMessage chordMessage);
  void ProcessFingerReq (ChordMessage chordMessage);
  void ProcessFingerRsp (ChordMessage chordMessage);
  void ProcessTraceRing (ChordMessage chordMessage);

  // DHT operations
  void DoLookup (Ptr<ChordIdentifier> requestedIdentifier, ChordTransaction::Originator orginator);
  void DoStabilize (Ptr<ChordVNode> virtualNode);
  void DoHeartbeat (Ptr<ChordVNode> virtualNode);
  void DoFixFinger (Ptr<ChordVNode> virtualNode);

  // Find nodes in DHT
  Ptr<ChordVNode> FindVNode (Ptr<ChordIdentifier> chordIdentifier);
  Ptr<ChordVNode> FindVNode (std::string vNodeName);
  Ptr<ChordVNode> FindNearestVNode (Ptr<ChordIdentifier> targetIdentifier);
  void DeleteVNode (Ptr<ChordIdentifier> chordIdentifier);
  void DeleteVNode (std::string vNodeName);
  Ptr<ChordVNode> LookupLocal (Ptr<ChordIdentifier> chordIdentifier);

  // Send/Routing methods
  void SendChordMessage (ChordMessage &chordMessage, Ipv4Address destination, uint16_t destinationPort);
  void SendPacket (Ptr<Packet> packet, Ipv4Address destinationIp, uint16_t destinationPort);
  bool SendChordMessageViaAnyVNode (ChordMessage &chordMessage);
  bool SendViaAnyVNode (Ptr<Packet> packet);
  bool RouteChordMessage (Ptr<ChordIdentifier> targetIdentifier, ChordMessage &chordMessage);
  bool RoutePacket (Ptr<ChordIdentifier> targetIdentifier, Ptr<Packet> packet);
  bool RouteChordMessageViaFinger (Ptr<ChordIdentifier> targetIdentifier,
                                   Ptr<ChordVNode> vNode, ChordMessage &chordMessage);
  bool RouteViaFinger (Ptr<ChordIdentifier> targetIdentifier,
                       Ptr<ChordVNode> vNode, Ptr<Packet> packet);
  bool UpdateTTL (ChordMessage &chordMessage);

  // Timeouts
  void HandleRequestTimeout (Ptr<ChordVNode> chordVNode, uint32_t transactionId);
};

} // namespace ns3

#endif // CHORD_IPV4_H
