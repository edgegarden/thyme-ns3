/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2015 NOVA LINCS DI-FCT-UNL <http://nova-lincs.di.fct.unl.pt/>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Joao A. Silva <jaa.silva@campus.fct.unl.pt>
 */
#include "chord-ps.h"

//#include <openssl/sha.h>
#include "ns3/log.h"
#include "chord-ipv4.h"
#include "ns3/uinteger.h"

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("ChordPS");

namespace hyrax {

NS_OBJECT_ENSURE_REGISTERED (ChordPS);

TypeId
ChordPS::GetTypeId (void)
{
  NS_LOG_FUNCTION_NOARGS ();
  static TypeId tid = TypeId ("ns3::hyrax::ChordPS")
    .SetParent<HyraxApi> ()
    .AddConstructor<ChordPS> ()

    .AddAttribute ("DHashPort",
    "Listening Port to be used with DHash layer.",
    UintegerValue (12155),
    MakeUintegerAccessor (&ChordPS::m_dHashPort),
    MakeUintegerChecker<uint16_t> ())

    .AddAttribute ("AuditObjectsTimeout",
    "Timeout value for auditing objects in milli seconds.",
    TimeValue (MilliSeconds (DEFAULT_AUDIT_OBJECTS_TIMEOUT)),
    MakeTimeAccessor (&ChordPS::m_auditObjectsTimeout),
    MakeTimeChecker ())

    .AddAttribute ("JoinTimeout",
    "Join timeout interval.",
    TimeValue (MilliSeconds (JOIN_TIMEOUT)),
    MakeTimeAccessor (&ChordPS::m_joinTimeout),
    MakeTimeChecker ())
    ;

  return tid;
}

ChordPS::ChordPS ()
{
  NS_LOG_FUNCTION (this);
}

ChordPS::~ChordPS ()
{
  NS_LOG_FUNCTION (this);
}

void
ChordPS::DoPublish (Ptr<ThymeObject> obj)
{
  NS_LOG_FUNCTION (this << obj);
  // save publication locally
  Ptr<ObjectMetadata> objMeta = obj->GetObjectMetadata ();
  Ptr<HyraxIdentifier> objId = objMeta->GetObjectIdentifier ();
  HyraxIdentifier id = *(PeekPointer (objId));
  m_myPubs.insert ({id, obj}); // indexed by objId

  Ptr<PublishRequest> req = CreatePublishRequest (obj);
  req->ReceivedPubKeyReply ();

  // publish object in tag brokers - index object by tag (CPUBLISH_REQ)
  std::set<std::string> tags = objMeta->GetTags ();
  for (auto it = tags.begin (); it != tags.end (); ++it)
   {
    Ptr<ChordIdentifier> id = CreateChordIdentifier (*it);
    m_routing->LookupKey (id); // request lookups and wait for replies
    CMessage msg;
    msg.SetPublishReq (req->GetRequestId (), objMeta, *it);

    InsertRequest (id, req);
   }
}

void
ChordPS::DoUnpublish (Ptr<ThymeObject> obj)
{
  NS_LOG_FUNCTION (this << obj);
  Ptr<ObjectMetadata> objMeta = obj->GetObjectMetadata ();
  Ptr<HyraxIdentifier> objId = objMeta->GetObjectIdentifier ();
  HyraxIdentifier id = *(PeekPointer (objId));
  Ptr<UnpublishRequest> unpubReq; // @TODO = CreateUnpublishRequest (obj);

  // unpublish object in tag brokers (CUNPUBLISH_REQ)
  std::set<std::string> tags = objMeta->GetTags ();
  for (auto it = tags.begin (); it != tags.end (); ++it)
   {
    Ptr<ChordIdentifier> id = CreateChordIdentifier (*it);
    m_routing->LookupKey (id); // request lookups and wait for replies
    InsertRequest (id, unpubReq);
   }
}

void
ChordPS::DoDownload (Ptr<HyraxIdentifier> identifier, Ipv4Address owner)
{
  NS_LOG_FUNCTION (this << identifier << owner);
  // send CDOWNLOAD_REQ to owner
  Ptr<DownloadRequest> req = CreateDownloadRequest (identifier, owner);
  uint32_t reqId = req->GetRequestId ();
  CMessage msg;
  msg.SetDownloadReq (reqId, identifier);
  Send (msg, owner);
  StartDownloadObjectTimeoutHandler (req);
}

void
ChordPS::DoSubscribe (std::string filter, Time startTs, Time endTs)
{
  NS_LOG_FUNCTION (this << filter << startTs << endTs);
  uint32_t subId = GetNextSubscriptionId ();
  Ptr<CSubscriptionObject> subObj =
    Create<CSubscriptionObject> (subId, m_addr, startTs, endTs, filter);

  // @TODO
}

void
ChordPS::Unsubscribe (Ptr<Subscription> subObj)
{
  NS_LOG_FUNCTION (this << subObj);
  uint32_t subId = subObj->GetSubscriptionId ();
  // check subscription ownership
  if (subObj->GetSubscriber () != m_addr)
   {
    NotifyUnsubscribeFailure (subObj, NOT_OWNER);
    return;
   }
//  auto found = m_mySubs.find (subObj->GetSubscriptionId ());
//  if (found == m_mySubs.end ())
//   { // subObj is not in mySubs ("invalid" object)
//    NotifyUnsubscribeFailure (subObj, OBJECT_NOT_FOUND);
//    return;
//   }

  // check subscription validity
  if (subObj->IsEndTimeExpired ())
   { // endTs is in the past (it will be purged eventually)
    NotifyUnsubscribeFailure (subObj, TIMEOUT);
    return;
   }

  Ptr<CSubscriptionObject> so = dynamic_cast<CSubscriptionObject*> (PeekPointer (subObj));
  std::vector<std::string> sendTags = so->GetSendTags ();
  uint32_t sendTagsNum = sendTags.size ();
  Ptr<UnsubscribeRequest> unsubReq = CreateUnsubscribeRequest (so, sendTagsNum);


  // @TODO
}

Ptr<Subscription>
ChordPS::GetSubscription (uint32_t subId)
{
  NS_LOG_FUNCTION (this << subId);
//  auto found = m_mySubs.find (subId);
//  return found != m_mySubs.end () ? found->second : 0;
  return 0;
}

std::vector<uint32_t>
ChordPS::GetSubscriptionIds ()
{
  NS_LOG_FUNCTION (this);
  std::vector<uint32_t> vec;
//  for (auto it = m_mySubs.begin (); it != m_mySubs.end (); ++it)
//   {
//    vec.push_back (it->first);
//   }
  return vec;
}

void
ChordPS::IfaceDown ()
{
  // @TODO
}

void
ChordPS::IfaceUp ()
{
  // @TODO
}

void
ChordPS::TraceRing ()
{
  NS_LOG_FUNCTION (this);
  m_routing->FireTraceRing (m_name);
}

void
ChordPS::DoDispose ()
{
  NS_LOG_FUNCTION (this);
  Application::DoDispose ();
}

void
ChordPS::DoInitialize ()
{
  NS_LOG_FUNCTION (this);
  m_node = GetNode ();
  m_ipv4 = m_node->GetObject<Ipv4> ();
  m_rand = CreateObject<UniformRandomVariable> ();

  Application::DoInitialize ();
}

void
ChordPS::StartApplication ()
{
  NS_LOG_FUNCTION (this);
  SetupRoutingLayer ();

  std::stringstream ss;
  ss << m_addr;
  m_name = ss.str ();
  m_key = Hash (m_name);

  ObjectFactory factory;
  factory.SetTypeId (ChordIpv4::GetTypeId ());
  m_routing = factory.Create<ChordIpv4> ();
  m_routing->SetDHashLookupSuccessCallback (MakeCallback (&ChordPS::HandleLookupSuccess, this));
  m_routing->SetDHashLookupFailureCallback (MakeCallback (&ChordPS::HandleLookupFailure, this));
  m_routing->SetDHashVNodeKeyOwnershipCallback (MakeCallback (&ChordPS::HandleKeyOwnershipChanged, this));
  m_routing->SetVNodeFailureCallback (MakeCallback (&ChordPS::HandleVNodeFailure, this));
  m_routing->SetJoinSuccessCallback (MakeCallback (&ChordPS::HandleVNodeJoinSucces, this));
  m_routing->SetTraceRingCallback (MakeCallback (&ChordPS::HandleTraceRing, this));
  m_routing->Start (m_node, m_addr, m_dHashPort);

  Join ();

  // @TODO start purge subscriptions timer
}

void
ChordPS::StopApplication ()
{
  NS_LOG_FUNCTION (this);
  m_socket->Close ();
  m_node = 0;
  m_routing = 0;
  m_ipv4 = 0;
  m_rand = 0;
  m_iface = 0;
  m_socket = 0;
  RemoveAllRequests ();
}

void
ChordPS::SetupRoutingLayer ()
{
  NS_LOG_FUNCTION (this);
  uint32_t devs = m_node->GetNDevices (); // get interfaces
  NS_LOG_INFO (devs << " device(s) found");

  for (int i = 0; i < devs; ++i)
   {
    Ptr<NetDevice> dev = m_node->GetDevice (i);
    NS_LOG_INFO ("Dev " << i << " " << dev->GetAddress () << " "
      << dev->GetInstanceTypeId ().GetName () << " "
      << m_ipv4->GetAddress (m_ipv4->GetInterfaceForDevice (dev), 0).GetLocal ()
      << " " << m_ipv4->GetInterfaceForDevice (dev));

    if (dev->GetInstanceTypeId ().GetName ().compare ("ns3::LoopbackNetDevice") != 0)
     { // if not the loopback iface
      if (m_iface == 0)
       { // first iface is considered the primary iface
        m_iface = dev;
        m_addr = m_ipv4->GetAddress (m_ipv4->GetInterfaceForDevice (dev), 0).GetLocal ();
        NS_LOG_INFO ("Assigned iface: " << m_iface->GetAddress ());
        break;
       }
     }
   }

  NS_ABORT_MSG_UNLESS (m_iface != 0
    && m_ipv4->IsUp (m_ipv4->GetInterfaceForDevice (m_iface)),
    "Iface not assigned or not up");

  m_socket = Socket::CreateSocket (m_node, UdpSocketFactory::GetTypeId ());
  NS_ABORT_MSG_UNLESS (m_socket != 0, "Socket not created");
  m_socket->SetAllowBroadcast (true); // allow broadcast
  m_socket->SetRecvCallback (MakeCallback (&ChordPS::ProcessPacket, this));
  Ipv4Address bindAddr = Ipv4Address::GetAny ();
  int bind = m_socket->Bind (InetSocketAddress (bindAddr, m_dHashPort));
  NS_ABORT_MSG_UNLESS (bind != -1, "Socket not bound to addr");
  m_socket->BindToNetDevice (m_iface);
  NS_LOG_INFO ("Bind socket to " << bindAddr << ":" << m_dHashPort);
}

void
ChordPS::ProcessPacket (Ptr<Socket> socket)
{
  NS_LOG_FUNCTION (this << socket);
  Address sender;
  Ptr<Packet> packet = socket->RecvFrom (sender);
  Ipv4Address src = InetSocketAddress::ConvertFrom (sender).GetIpv4 ();

  CMessage msg;
  packet->RemoveHeader (msg);
  MType type = msg.GetMsgType ();

  switch (type)
  {
  case CPUBLISH_REQ:
    ProcessPublishReq (msg.GetPublishReq (), src);
    break;
  case CPUBLISH_REP:
    ProcessPublishRep (msg.GetPublishRep ());
    break;
  case CUNPUBLISH_REQ:
    ProcessUnpublishReq (msg.GetUnpublishReq (), src);
    break;
  case CUNPUBLISH_REP:
    ProcessUnpublishRep (msg.GetUnpublishRep ());
    break;
  case CDOWNLOAD_REQ:
    ProcessDownloadReq (msg.GetDownloadReq (), src);
    break;
  case CDOWNLOAD_REP:
    ProcessDownloadRep (msg.GetDownloadRep ());
    break;
  case CSUBSCRIBE_REQ:
    ProcessSubscribeReq (msg.GetSubscribeReq (), src);
    break;
  case CSUBSCRIBE_REP:
    ProcessSubscribeRep (msg.GetSubscribeRep ());
    break;
  case CSUBSCRIBE_NOT:
    ProcessSubscribeNot (msg.GetSubscribeNot ());
    break;
  case CUNSUBSCRIBE_REQ:
    ProcessUnsubscribeReq (msg.GetUnsubscribeReq (), src);
    break;
  case CUNSUBSCRIBE_REP:
    ProcessUnsubscribeRep (msg.GetUnsubscribeRep ());
    break;
  case CSTATE_TRANSFER:
    ProcessStateTransfer (msg.GetStateTransfer ());
    break;
  case CJOIN_REQ:
    ProcessJoinReq (src);
    break;
  case CJOIN_REP:
    ProcessJoinRep (src);
    break;
  default:
    NS_LOG_ERROR ("Unknown msg type " << type);
  }
}

void
ChordPS::ProcessPublishReq (CMessage::CPublishReq msg, Ipv4Address src)
{
  NS_LOG_FUNCTION (this << &msg << src);
  // @TODO
}

void
ChordPS::ProcessPublishRep (CMessage::CPublishRep msg)
{
  NS_LOG_FUNCTION (this << &msg);
  // @TODO
}

void
ChordPS::ProcessUnpublishReq (CMessage::CUnpublishReq msg, Ipv4Address src)
{
  NS_LOG_FUNCTION (this << &msg << src);
  // @TODO
}

void
ChordPS::ProcessUnpublishRep (CMessage::CUnpublishRep msg)
{
  NS_LOG_FUNCTION (this << &msg);
  // @TODO
}

void
ChordPS::ProcessDownloadReq (CMessage::CDownloadReq msg, Ipv4Address src)
{
  NS_LOG_FUNCTION (this << &msg << src);
  auto found = m_myPubs.find (*(PeekPointer (msg.objId)));
  Ptr<ThymeObject> obj = found != m_myPubs.end () ? found->second : 0;
  MessageStatus st = obj == 0 ? OBJECT_NOT_FOUND : OBJECT_FOUND;

  CMessage rep;
  rep.SetDownloadRep (msg.requestId, st, obj);
  Send (rep, src);
}

void
ChordPS::ProcessDownloadRep (CMessage::CDownloadRep msg)
{
  NS_LOG_FUNCTION (this << &msg);
  Ptr<DownloadRequest> req = dynamic_cast<DownloadRequest *> (PeekPointer (FindRequest (msg.requestId)));
  MessageStatus st = msg.status;

  if (req != 0)
   {
    req->CancelDownloadObjectTimeout ();
    if (st == OBJECT_FOUND)
     { // object found. notify upper layer
      Ptr<ObjectMetadata> meta = msg.object->GetObjectMetadata ();
      ObjectKey objKey = {meta->GetObjectIdentifier (), meta->GetOwner ()};
      auto found = m_passReps.find (objKey);
      if (found != m_passReps.end ())
       {
        m_passReps.erase (found);
       }
      m_passReps.insert ({objKey, msg.object});
      NotifyDownloadSuccess (msg.object);
     }
    else
     { // st == OBJECT_NOT_FOUND
      NotifyDownloadFailure (req->GetObjectIdentifier (), req->GetOwner (), st);
     }
    RemoveRequest (req->GetRequestId ());
   }
  else
   {
    NS_LOG_WARN ("DownloadRep with unknown requestId " << msg.requestId);
   }
}

void
ChordPS::ProcessSubscribeReq (CMessage::CSubscribeReq msg, Ipv4Address src)
{
  NS_LOG_FUNCTION (this << &msg << src);
  // @TODO
}

void
ChordPS::ProcessSubscribeRep (CMessage::CSubscribeRep msg)
{
  NS_LOG_FUNCTION (this << &msg);
  // @TODO
}

void
ChordPS::ProcessSubscribeNot (CMessage::CSubscribeNot msg)
{
  NS_LOG_FUNCTION (this << &msg);
  // @TODO
}

void
ChordPS::ProcessUnsubscribeReq (CMessage::CUnsubscribeReq msg, Ipv4Address src)
{
  NS_LOG_FUNCTION (this << &msg << src);
  // @TODO
}

void
ChordPS::ProcessUnsubscribeRep (CMessage::CUnsubscribeRep msg)
{
  NS_LOG_FUNCTION (this << &msg);
  // @TODO
}

void
ChordPS::ProcessStateTransfer (CMessage::CStateTransfer msg)
{
  NS_LOG_FUNCTION (this << &msg);
  // @TODO
}

void
ChordPS::ProcessJoinReq (Ipv4Address src)
{
  NS_LOG_FUNCTION (this << src);
  CMessage msg;
  msg.SetJoinRep ();
  Send (msg, src);
}

void
ChordPS::ProcessJoinRep (Ipv4Address src)
{
  NS_LOG_FUNCTION (this << src);
  if (!m_joined)
   {
    m_joinTimer.Cancel ();
    m_joined = true;
    m_routing->SetBootstrapIp (src);

    m_joinRingRetries = 0;
    NS_LOG_DEBUG ("Node " << m_name << " trying to join Chord ring!");
    // each node has only one Vnode
    m_routing->InsertVNode (m_name, m_key, 20);
   }
}

void
ChordPS::Broadcast (CMessage msg)
{
  NS_LOG_FUNCTION (this << &msg);
  Ptr<Packet> packet = Create<Packet>();
  packet->AddHeader (msg);
  int sent = m_socket->SendTo (packet, 0,
    InetSocketAddress (Ipv4Address::GetBroadcast (), m_dHashPort));
  NS_ABORT_MSG_UNLESS (sent != -1, "Error while bcast pckt " << packet->GetUid ());
}

void
ChordPS::Send (CMessage msg, Ipv4Address dst)
{
  NS_LOG_FUNCTION (this << &msg << dst);
  Ptr<Packet> packet = Create<Packet> ();
  packet->AddHeader (msg);
  int sent = m_socket->SendTo (packet, 0, InetSocketAddress (dst, m_dHashPort));
  NS_ABORT_MSG_UNLESS (sent != -1, "Error while send pckt " << packet->GetUid ());
}

void
ChordPS::HandleLookupSuccess (Ptr<ChordIdentifier> key, Ipv4Address addr,
  uint16_t port)
{
  NS_LOG_FUNCTION (this);
  // @TODO
}

void
ChordPS::HandleLookupFailure (Ptr<ChordIdentifier> key)
{
  NS_LOG_FUNCTION (this);
  // @TODO
}

void
ChordPS::HandleKeyOwnershipChanged (Ptr<ChordIdentifier> vNodeKey,
  Ptr<ChordIdentifier> predecessorKey, Ptr<ChordIdentifier> oldPredecessorKey,
  Ipv4Address predecessorAddr, uint16_t port)
{
  NS_LOG_FUNCTION (this);
  // @TODO
}

void
ChordPS::HandleVNodeFailure (std::string name, Ptr<ChordIdentifier> key)
{
  NS_LOG_FUNCTION (this << name);
  if (m_joinRingRetries < DEFAULT_MAX_JOIN_RING_RETRIES)
   {
    m_joinRingRetries++;
    NS_LOG_DEBUG ("Node " << m_name << " retrying to join Chord ring! retry: "
      << (uint16_t) m_joinRingRetries);
    Simulator::ScheduleNow (&ChordIpv4::InsertVNode, m_routing, m_name, m_key, 20);
   }
  else
   {
    NS_LOG_ERROR ("Node " << m_name << " unable to join Chord ring. Give up!");
   }
}

void
ChordPS::HandleVNodeJoinSucces (std::string name, Ptr<ChordIdentifier> key)
{
  NS_LOG_FUNCTION (this << name);
  m_joinRingRetries = 0;
}

void
ChordPS::HandleTraceRing (std::string name, Ptr<ChordIdentifier> key)
{
  NS_LOG_FUNCTION (this << name);
  NS_LOG_DEBUG ("Trace Ring from node " << name << ". I'm node " << m_name);
}

void
ChordPS::StartDownloadObjectTimeoutHandler (Ptr<DownloadRequest> req)
{
  EventId eventId = Simulator::Schedule (m_reqTimeout,
    &ChordPS::HandleDownloadObjectTimeout, this, req);
  req->SetDownloadObjectTimeoutEventId (eventId);
}

void
ChordPS::HandleDownloadObjectTimeout (Ptr<DownloadRequest> req)
{
  Ptr<HyraxIdentifier> objId = req->GetObjectIdentifier ();
  Ipv4Address owner = req->GetOwner ();
  RemoveRequest (req->GetRequestId ());
  NotifyDownloadFailure (objId, owner, TIMEOUT);
}

uint8_t*
ChordPS::Hash (std::string key)
{
  NS_LOG_FUNCTION (this << key);
  unsigned char* hash = (unsigned char*) malloc (20); // sha1 outputs 160 bits
  //SHA1 ((const unsigned char*) key.c_str (), key.length (), hash);
  return (uint8_t*) hash;
}

Ptr<ChordIdentifier>
ChordPS::CreateChordIdentifier (std::string key)
{
  NS_LOG_FUNCTION (this << key);
  uint8_t* hash = Hash (key);
  Ptr<ChordIdentifier> id = Create<ChordIdentifier> (hash, 20);
  free (hash);
  return id;
}

void
ChordPS::Join ()
{
  NS_LOG_FUNCTION (this);
  CMessage msg;
  msg.SetJoinReq ();
  Broadcast (msg);
  m_joinTimer = Simulator::Schedule (m_joinTimeout, &ChordPS::JoinTimerExpired, this);
}

void
ChordPS::JoinTimerExpired ()
{
  NS_LOG_FUNCTION (this);
  ProcessJoinRep (m_addr);
}

void
ChordPS::InsertRequest (Ptr<ChordIdentifier> key, Ptr<Request> req)
{
  NS_LOG_FUNCTION (this << key << req);
  ChordIdentifier id = *PeekPointer (key);

  auto found = m_ongoingLookups.find (id);
  if (found != m_ongoingLookups.end ())
   { // id found
    found->second.push_back (req);
   }
  else
   { // id not found
    std::vector<Ptr<Request> > aux;
    aux.push_back (req);
    m_ongoingLookups.insert ({id, aux});
   }
}

}
}
