/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2015 NOVA LINCS DI-FCT-UNL <http://nova-lincs.di.fct.unl.pt/>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Joao A. Silva <jaa.silva@campus.fct.unl.pt>
 */
#ifndef CHORD_MESSAGE2_H
#define CHORD_MESSAGE2_H

#include "ns3/header.h"
#include "ns3/buffer.h"
#include "ns3/ipv4-address.h"
#include "ns3/chord-common.h"
#include "ns3/hyrax-common.h"
#include "ns3/chord-subscribe-object.h"
#include "ns3/hyrax-object.h"

namespace ns3 {
namespace hyrax {

class CMessage : public Header
{
public:
  static TypeId GetTypeId (void);

  virtual TypeId
  GetInstanceTypeId (void) const
  {
    return GetTypeId ();
  }

  CMessage () : m_type ((MType) 0)
  {
  }

  MType
  GetMsgType () const
  {
    return m_type;
  }

  void
  SetMsgType (MType type)
  {
    m_type = type;
  }

  virtual uint32_t GetSerializedSize (void) const;
  virtual void Serialize (Buffer::Iterator start) const;
  virtual uint32_t Deserialize (Buffer::Iterator start);
  virtual void
  Print (std::ostream &os) const;

  // CPublishReq -------------------------------------------------------------

  struct CPublishReq
  {
    Ptr<ObjectMetadata> objMetadata;
    std::string tag;
    uint32_t requestId;

    uint32_t GetSerializedSize () const;
    void Serialize (Buffer::Iterator &start) const;
    uint32_t Deserialize (Buffer::Iterator &start);
    void Print (std::ostream &os) const;
  };

  CPublishReq
  GetPublishReq () const
  {
    return m_message.pubReq;
  }

  void
  SetPublishReq (uint32_t reqId, Ptr<ObjectMetadata> meta, std::string t);

  // CPublishRep -------------------------------------------------------------

  struct CPublishRep
  {
    uint32_t requestId;

    uint32_t GetSerializedSize () const;
    void Serialize (Buffer::Iterator &start) const;
    uint32_t Deserialize (Buffer::Iterator &start);
    void Print (std::ostream &os) const;
  };

  CPublishRep
  GetPublishRep () const
  {
    return m_message.pubRep;
  }

  void
  SetPublishRep (uint32_t reqId);

  // CUnpublishReq ------------------------------------------------------------

  struct CUnpublishReq
  {
    Ptr<HyraxIdentifier> objId;
    std::string tag;
    uint32_t requestId;

    uint32_t GetSerializedSize () const;
    void Serialize (Buffer::Iterator &start) const;
    uint32_t Deserialize (Buffer::Iterator &start);
    void Print (std::ostream &os) const;
  };

  CUnpublishReq
  GetUnpublishReq () const
  {
    return m_message.unpubReq;
  }

  void
  SetUnpublishReq (uint32_t reqId, Ptr<HyraxIdentifier> id, std::string str);

  // CUnpublishRep ------------------------------------------------------------

  struct CUnpublishRep
  {
    uint32_t requestId;

    uint32_t GetSerializedSize () const;
    void Serialize (Buffer::Iterator &start) const;
    uint32_t Deserialize (Buffer::Iterator &start);
    void Print (std::ostream &os) const;
  };

  CUnpublishRep
  GetUnpublishRep () const
  {
    return m_message.unpubRep;
  }

  void
  SetUnpublishRep (uint32_t reqId);

  // CDownloadReq -------------------------------------------------------------

  struct CDownloadReq
  {
    Ptr<HyraxIdentifier> objId;
    uint32_t requestId;

    uint32_t GetSerializedSize () const;
    void Serialize (Buffer::Iterator &start) const;
    uint32_t Deserialize (Buffer::Iterator &start);
    void Print (std::ostream &os) const;
  };

  CDownloadReq
  GetDownloadReq () const
  {
    return m_message.downReq;
  }

  void
  SetDownloadReq (uint32_t reqId, Ptr<HyraxIdentifier> id);

  // CDownloadRep -------------------------------------------------------------

  struct CDownloadRep
  {
    MessageStatus status;
    Ptr<ThymeObject> object;
    uint32_t requestId;

    uint32_t GetSerializedSize () const;
    void Serialize (Buffer::Iterator &start) const;
    uint32_t Deserialize (Buffer::Iterator &start);
    void Print (std::ostream &os) const;
  };

  CDownloadRep
  GetDownloadRep () const
  {
    return m_message.downRep;
  }

  void
  SetDownloadRep (uint32_t reqId, MessageStatus st, Ptr<ThymeObject> obj);

  // CSubscribeReq ------------------------------------------------------------

  struct CSubscribeReq
  {
    Ptr<CSubscriptionObject> subObj;
    uint32_t requestId;

    uint32_t GetSerializedSize () const;
    void Serialize (Buffer::Iterator &start) const;
    uint32_t Deserialize (Buffer::Iterator &start);
    void Print (std::ostream &os) const;
  };

  CSubscribeReq
  GetSubscribeReq () const
  {
    return m_message.subReq;
  }

  void
  SetSubscribeReq (uint32_t reqId, Ptr<CSubscriptionObject> obj);

  // CSubscribeRep ------------------------------------------------------------

  struct CSubscribeRep
  {
    uint32_t requestId;

    uint32_t GetSerializedSize () const;
    void Serialize (Buffer::Iterator &start) const;
    uint32_t Deserialize (Buffer::Iterator &start);
    void Print (std::ostream &os) const;
  };

  CSubscribeRep
  GetSubscribeRep () const
  {
    return m_message.subRep;
  }

  void
  SetSubscribeRep (uint32_t reqId);

  // CSubscribeNot ------------------------------------------------------------

  struct CSubscribeNot
  {
    std::vector<uint32_t> subscriptionId;
    Ptr<ObjectMetadata> objectMetadata;

    uint32_t GetSerializedSize (void) const;
    void Serialize (Buffer::Iterator &start) const;
    uint32_t Deserialize (Buffer::Iterator &start);
    void Print (std::ostream &os) const;
  };

  CSubscribeNot
  GetSubscribeNot () const
  {
    return m_message.subNot;
  }

  void
  SetSubscribeNot (std::vector<uint32_t> subId, Ptr<ObjectMetadata> objMeta);

  // CUnsubscribeReq ----------------------------------------------------------

  struct CUnsubscribeReq
  {
    uint32_t subscriptionId;
    uint32_t requestId;

    uint32_t GetSerializedSize () const;
    void Serialize (Buffer::Iterator &start) const;
    uint32_t Deserialize (Buffer::Iterator &start);
    void Print (std::ostream &os) const;
  };

  CUnsubscribeReq
  GetUnsubscribeReq () const
  {
    return m_message.unsubReq;
  }

  void
  SetUnsubscribeReq (uint32_t reqId, uint32_t subId);

  // CUnsubscribeRep ----------------------------------------------------------

  struct CUnsubscribeRep
  {
    uint32_t requestId;

    uint32_t GetSerializedSize () const;
    void Serialize (Buffer::Iterator &start) const;
    uint32_t Deserialize (Buffer::Iterator &start);
    void Print (std::ostream &os) const;
  };

  CUnsubscribeRep
  GetUnsubscribeRep () const
  {
    return m_message.unsubRep;
  }

  void
  SetUnsubscribeRep (uint32_t reqId);

  // CStateTransfer -----------------------------------------------------------

  struct CStateTransfer
  {
    // @TODO

    uint32_t GetSerializedSize () const;
    void Serialize (Buffer::Iterator &start) const;
    uint32_t Deserialize (Buffer::Iterator &start);
    void Print (std::ostream &os) const;
  };

  CStateTransfer
  GetStateTransfer () const
  {
    return m_message.stransfer;
  }

  void
  SetStateTransfer ();

  // CJoinReq -----------------------------------------------------------------

  void
  SetJoinReq ();

  // CJoinRep -----------------------------------------------------------------

  void
  SetJoinRep ();

private:
  MType m_type;

  struct
  {
    CPublishReq pubReq;
    CPublishRep pubRep;
    CUnpublishReq unpubReq;
    CUnpublishRep unpubRep;
    CDownloadReq downReq;
    CDownloadRep downRep;
    CSubscribeReq subReq;
    CSubscribeRep subRep;
    CSubscribeNot subNot;
    CUnsubscribeReq unsubReq;
    CUnsubscribeRep unsubRep;
    CStateTransfer stransfer;
  } m_message;
};

std::ostream& operator<< (std::ostream &os, CMessage::CPublishReq const &x);
std::ostream& operator<< (std::ostream &os, CMessage::CPublishRep const &x);
std::ostream& operator<< (std::ostream &os, CMessage::CUnpublishReq const &x);
std::ostream& operator<< (std::ostream &os, CMessage::CUnpublishRep const &x);
std::ostream& operator<< (std::ostream &os, CMessage::CDownloadReq const &x);
std::ostream& operator<< (std::ostream &os, CMessage::CDownloadRep const &x);
std::ostream& operator<< (std::ostream &os, CMessage::CSubscribeReq const &x);
std::ostream& operator<< (std::ostream &os, CMessage::CSubscribeRep const &x);
std::ostream& operator<< (std::ostream &os, CMessage::CSubscribeNot const &x);
std::ostream& operator<< (std::ostream &os, CMessage::CUnsubscribeReq const &x);
std::ostream& operator<< (std::ostream &os, CMessage::CUnsubscribeRep const &x);
std::ostream& operator<< (std::ostream &os, CMessage::CStateTransfer const &x);

}
}

#endif /* CHORD_MESSAGE2_H */
