/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2015 NOVA LINCS DI-FCT-UNL <http://nova-lincs.di.fct.unl.pt/>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Joao A. Silva <jaa.silva@campus.fct.unl.pt>
 */
#ifndef CHORD_SUBSCRIPTION_OBJECT_H
#define CHORD_SUBSCRIPTION_OBJECT_H

#include "ns3/hyrax-common.h"
#include "ns3/random-variable-stream.h"
#include "ns3/hyrax-subscription.h"

namespace ns3 {
namespace hyrax {

class CSubscriptionObject : public Subscription
{
public:
  CSubscriptionObject ();
  CSubscriptionObject (uint32_t subId, Ipv4Address subscriber, Time start,
                       Time end, Conjunction tags);
  CSubscriptionObject (uint32_t subId, Ipv4Address subscriber, Time start,
                       Time end, std::string filter);

  virtual uint32_t GetSerializedSize () const;
  virtual void Serialize (Buffer::Iterator &start) const;
  virtual uint32_t Deserialize (Buffer::Iterator &start);
  virtual void Print (std::ostream &os) const;

//  AndTags GetTags () const;
//  void SetTags (AndTags tags);
  std::vector<std::string> GetPositiveTags () const;
  std::string GetRandPositiveTag () const;

  std::string GetFilter () const;
  Filter GetParsedFilter ();

  std::vector <std::string> GetSendTags () const;
  void AddSendTag (std::string tag);

  static Ptr<UniformRandomVariable> RAND;

private:
  virtual bool ValidFilter (std::set<std::string> tags) const;

  // ----- VARIABLES -----
  //AndTags m_tags; // individual AND clause
  std::string m_filter; // don't serialize
  std::vector <std::string> m_sendTags; // don't serialize
};

}
}

#endif /* CHORD_SUBSCRIPTION_OBJECT_H */
