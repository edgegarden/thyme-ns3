/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2015 NOVA LINCS DI-FCT-UNL <http://nova-lincs.di.fct.unl.pt/>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Joao A. Silva <jaa.silva@campus.fct.unl.pt>
 */
#ifndef CHORD_COMMON_H
#define CHORD_COMMON_H

#include <map>

namespace ns3 {
namespace hyrax {

// Constants ------------------------------------------------------------------

#define JOIN_TIMEOUT 500 // ms
#define DEFAULT_MAX_JOIN_RING_RETRIES 3

// Enums ----------------------------------------------------------------------

enum MType
{
  CPUBLISH_REQ = 1,
  CPUBLISH_REP = 2,

  CUNPUBLISH_REQ = 10,
  CUNPUBLISH_REP = 11,

  CDOWNLOAD_REQ = 20,
  CDOWNLOAD_REP = 21,

  CSUBSCRIBE_REQ = 30,
  CSUBSCRIBE_REP = 31,
  CSUBSCRIBE_NOT = 32,

  CUNSUBSCRIBE_REQ = 40,
  CUNSUBSCRIBE_REP = 41,

  CSTATE_TRANSFER = 50,

  CJOIN_REQ = 60,
  CJOIN_REP = 61,
};

// Typedefs -------------------------------------------------------------------

class CSubscriptionObject;

typedef std::map<uint32_t, Ptr<CSubscriptionObject> > COwnSubscriptions;
typedef std::map<Ipv4Address, COwnSubscriptions> CSubscriptions;

}
}

#endif /* CHORD_COMMON_H */
