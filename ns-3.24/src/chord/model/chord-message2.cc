/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2015 NOVA LINCS DI-FCT-UNL <http://nova-lincs.di.fct.unl.pt/>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Joao A. Silva <jaa.silva@campus.fct.unl.pt>
 */
#include "chord-message2.h"

#include "ns3/log.h"
#include "ns3/hyrax-common.h"
#include "ns3/hyrax-utils.h"

namespace ns3 {
namespace hyrax {

NS_OBJECT_ENSURE_REGISTERED (CMessage);

TypeId
CMessage::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::hyrax::CMessage")
    .SetParent<Header> ()
    .AddConstructor<CMessage> ();

  return tid;
}

uint32_t
CMessage::GetSerializedSize (void) const
{
  uint32_t size = sizeof (uint8_t);
  switch (m_type)
  {
  case CPUBLISH_REQ:
    size += m_message.pubReq.GetSerializedSize ();
    break;
  case CPUBLISH_REP:
    size += m_message.pubRep.GetSerializedSize ();
    break;
  case CUNPUBLISH_REQ:
    size += m_message.unpubReq.GetSerializedSize ();
    break;
  case CUNPUBLISH_REP:
    size += m_message.unpubRep.GetSerializedSize ();
    break;
  case CDOWNLOAD_REQ:
    size += m_message.downReq.GetSerializedSize ();
    break;
  case CDOWNLOAD_REP:
    size += m_message.downRep.GetSerializedSize ();
    break;
  case CSUBSCRIBE_REQ:
    size += m_message.subReq.GetSerializedSize ();
    break;
  case CSUBSCRIBE_REP:
    size += m_message.subRep.GetSerializedSize ();
    break;
  case CSUBSCRIBE_NOT:
    size += m_message.subNot.GetSerializedSize ();
    break;
  case CUNSUBSCRIBE_REQ:
    size += m_message.unsubReq.GetSerializedSize ();
    break;
  case CUNSUBSCRIBE_REP:
    size += m_message.unsubRep.GetSerializedSize ();
    break;
  case CSTATE_TRANSFER:
    size += m_message.stransfer.GetSerializedSize ();
    break;
  case CJOIN_REQ:
  case CJOIN_REP:
    size += 0;
    break;
  default:
    NS_ABORT_UNLESS (false);
  }
  return size;
}

void
CMessage::Serialize (Buffer::Iterator start) const
{
  start.WriteU8 ((uint8_t) m_type);
  switch (m_type)
  {
  case CPUBLISH_REQ:
    m_message.pubReq.Serialize (start);
    break;
  case CPUBLISH_REP:
    m_message.pubRep.Serialize (start);
    break;
  case CUNPUBLISH_REQ:
    m_message.unpubReq.Serialize (start);
    break;
  case CUNPUBLISH_REP:
    m_message.unpubRep.Serialize (start);
    break;
  case CDOWNLOAD_REQ:
    m_message.downReq.Serialize (start);
    break;
  case CDOWNLOAD_REP:
    m_message.downRep.Serialize (start);
    break;
  case CSUBSCRIBE_REQ:
    m_message.subReq.Serialize (start);
    break;
  case CSUBSCRIBE_REP:
    m_message.subRep.Serialize (start);
    break;
  case CSUBSCRIBE_NOT:
    m_message.subNot.Serialize (start);
    break;
  case CUNSUBSCRIBE_REQ:
    m_message.unsubReq.Serialize (start);
    break;
  case CUNSUBSCRIBE_REP:
    m_message.unsubRep.Serialize (start);
    break;
  case CSTATE_TRANSFER:
    m_message.stransfer.Serialize (start);
    break;
  case CJOIN_REQ:
  case CJOIN_REP:
    break;
  default:
    NS_ABORT_UNLESS (false);
  }
}

uint32_t
CMessage::Deserialize (Buffer::Iterator start)
{
  m_type = (MType) start.ReadU8 ();
  switch (m_type)
  {
  case CPUBLISH_REQ:
    m_message.pubReq.Deserialize (start);
    break;
  case CPUBLISH_REP:
    m_message.pubRep.Deserialize (start);
    break;
  case CUNPUBLISH_REQ:
    m_message.unpubReq.Deserialize (start);
    break;
  case CUNPUBLISH_REP:
    m_message.unpubRep.Deserialize (start);
    break;
  case CDOWNLOAD_REQ:
    m_message.downReq.Deserialize (start);
    break;
  case CDOWNLOAD_REP:
    m_message.downRep.Deserialize (start);
    break;
  case CSUBSCRIBE_REQ:
    m_message.subReq.Deserialize (start);
    break;
  case CSUBSCRIBE_REP:
    m_message.subRep.Deserialize (start);
    break;
  case CSUBSCRIBE_NOT:
    m_message.subNot.Deserialize (start);
    break;
  case CUNSUBSCRIBE_REQ:
    m_message.unsubReq.Deserialize (start);
    break;
  case CUNSUBSCRIBE_REP:
    m_message.unsubRep.Deserialize (start);
    break;
  case CSTATE_TRANSFER:
    m_message.stransfer.Deserialize (start);
    break;
  case CJOIN_REQ:
  case CJOIN_REP:
    break;
  default:
    NS_ABORT_UNLESS (false);
  }
  return GetSerializedSize ();
}

void
CMessage::Print (std::ostream &os) const
{
  os << "\n:::Header:::\nMsgType: " << m_type
    << "\n:::Payload:::\n";
  switch (m_type)
  {
  case CPUBLISH_REQ:
    m_message.pubReq.Print (os);
    break;
  case CPUBLISH_REP:
    m_message.pubRep.Print (os);
    break;
  case CUNPUBLISH_REQ:
    m_message.unpubReq.Print (os);
    break;
  case CUNPUBLISH_REP:
    m_message.unpubRep.Print (os);
    break;
  case CDOWNLOAD_REQ:
    m_message.downReq.Print (os);
    break;
  case CDOWNLOAD_REP:
    m_message.downRep.Print (os);
    break;
  case CSUBSCRIBE_REQ:
    m_message.subReq.Print (os);
    break;
  case CSUBSCRIBE_REP:
    m_message.subRep.Print (os);
    break;
  case CSUBSCRIBE_NOT:
    m_message.subNot.Print (os);
    break;
  case CUNSUBSCRIBE_REQ:
    m_message.unsubReq.Print (os);
    break;
  case CUNSUBSCRIBE_REP:
    m_message.unsubRep.Print (os);
    break;
  case CSTATE_TRANSFER:
    m_message.stransfer.Print (os);
    break;
  case CJOIN_REQ:
    os << "::JoinReq::";
    break;
  case CJOIN_REP:
    os << "::JoinRep::";
    break;
  default:
    NS_ABORT_UNLESS (false);
  }
}

// CPublishReq ---------------------------------------------------------------

uint32_t
CMessage::CPublishReq::GetSerializedSize () const
{
  uint32_t size = objMetadata->GetSerializedSize ();
  size += sizeof (uint32_t) + Utils::GetSerializedStringSize (tag);
  return size;
}

void
CMessage::CPublishReq::Serialize (Buffer::Iterator &start) const
{
  start.WriteHtonU32 (requestId);
  Utils::SerializeString (tag, start);
  objMetadata->Serialize (start);
}

uint32_t
CMessage::CPublishReq::Deserialize (Buffer::Iterator &start)
{
  requestId = start.ReadNtohU32 ();
  tag = Utils::DeserializeString (start);
  objMetadata = Create<ObjectMetadata> ();
  objMetadata->Deserialize (start);
  return GetSerializedSize ();
}

void
CMessage::CPublishReq::Print (std::ostream &os) const
{
  os << "::PubReq:: ReqId: " << requestId << " Tag: " << tag << " ";
  objMetadata->Print (os);
}

void
CMessage::SetPublishReq (uint32_t reqId, Ptr<ObjectMetadata> meta,
  std::string t)
{
  if (m_type == 0)
   {
    m_type = CPUBLISH_REQ;
   }
  else
   {
    NS_ABORT_UNLESS (m_type == CPUBLISH_REQ);
   }
  m_message.pubReq.requestId = reqId;
  m_message.pubReq.objMetadata = meta;
  m_message.pubReq.tag = t;
}

// CPublishRep ---------------------------------------------------------------

uint32_t
CMessage::CPublishRep::GetSerializedSize () const
{
  uint32_t size = sizeof (uint32_t);
  return size;
}

void
CMessage::CPublishRep::Serialize (Buffer::Iterator &start) const
{
  start.WriteHtonU32 (requestId);
}

uint32_t
CMessage::CPublishRep::Deserialize (Buffer::Iterator &start)
{
  requestId = start.ReadNtohU32 ();
  return GetSerializedSize ();
}

void
CMessage::CPublishRep::Print (std::ostream &os) const
{
  os << "::PubRep:: ReqId: " << requestId;
}

void
CMessage::SetPublishRep (uint32_t reqId)
{
  if (m_type == 0)
   {
    m_type = CPUBLISH_REP;
   }
  else
   {
    NS_ABORT_UNLESS (m_type == CPUBLISH_REP);
   }
  m_message.pubRep.requestId = reqId;
}

// CUnpublishReq --------------------------------------------------------------

uint32_t
CMessage::CUnpublishReq::GetSerializedSize () const
{
  uint32_t size = objId->GetSerializedSize ();
  size += Utils::GetSerializedStringSize (tag) + sizeof (uint32_t);
  return size;
}

void
CMessage::CUnpublishReq::Serialize (Buffer::Iterator &start) const
{
  objId->Serialize (start);
  Utils::SerializeString (tag, start);
  start.WriteHtonU32 (requestId);
}

uint32_t
CMessage::CUnpublishReq::Deserialize (Buffer::Iterator &start)
{
  objId = Create<HyraxIdentifier> ();
  objId->Deserialize (start);
  tag = Utils::DeserializeString (start);
  requestId = start.ReadNtohU32 ();
  return GetSerializedSize ();
}

void
CMessage::CUnpublishReq::Print (std::ostream &os) const
{
  os << "::UnpubReq:: ReqId: " << requestId
    << " Tag: " << tag;
  objId->Print (os);
}

void
CMessage::SetUnpublishReq (uint32_t reqId, Ptr<HyraxIdentifier> id,
  std::string str)
{
  if (m_type == 0)
   {
    m_type = CUNPUBLISH_REQ;
   }
  else
   {
    NS_ABORT_UNLESS (m_type == CUNPUBLISH_REQ);
   }
  m_message.unpubReq.requestId = reqId;
  m_message.unpubReq.objId = id;
  m_message.unpubReq.tag = str;
}

// CUnpublishRep --------------------------------------------------------------

uint32_t
CMessage::CUnpublishRep::GetSerializedSize () const
{
  uint32_t size = sizeof (uint32_t);
  return size;
}

void
CMessage::CUnpublishRep::Serialize (Buffer::Iterator &start) const
{
  start.WriteHtonU32 (requestId);
}

uint32_t
CMessage::CUnpublishRep::Deserialize (Buffer::Iterator &start)
{
  requestId = start.ReadNtohU32 ();
  return GetSerializedSize ();
}

void
CMessage::CUnpublishRep::Print (std::ostream &os) const
{
  os << "::UnpubRep:: ReqId: " << requestId;
}

void
CMessage::SetUnpublishRep (uint32_t reqId)
{
  if (m_type == 0)
   {
    m_type = CUNPUBLISH_REP;
   }
  else
   {
    NS_ABORT_UNLESS (m_type == CUNPUBLISH_REP);
   }
  m_message.unpubRep.requestId = reqId;
}

// CDownloadReq ---------------------------------------------------------------

uint32_t
CMessage::CDownloadReq::GetSerializedSize () const
{

}

void
CMessage::CDownloadReq::Serialize (Buffer::Iterator &start) const
{

}

uint32_t
CMessage::CDownloadReq::Deserialize (Buffer::Iterator &start)
{

}

void
CMessage::CDownloadReq::Print (std::ostream &os) const
{

}

void
CMessage::SetDownloadReq (uint32_t reqId, Ptr<HyraxIdentifier> id)
{

}

// CDownloadRep ---------------------------------------------------------------

uint32_t
CMessage::CDownloadRep::GetSerializedSize () const
{

}

void
CMessage::CDownloadRep::Serialize (Buffer::Iterator &start) const
{

}

uint32_t
CMessage::CDownloadRep::Deserialize (Buffer::Iterator &start)
{

}

void
CMessage::CDownloadRep::Print (std::ostream &os) const
{

}

void
CMessage::SetDownloadRep (uint32_t reqId, MessageStatus st,
  Ptr<ThymeObject> obj)
{

}

// CSubscribeReq --------------------------------------------------------------

uint32_t
CMessage::CSubscribeReq::GetSerializedSize () const
{

}

void
CMessage::CSubscribeReq::Serialize (Buffer::Iterator &start) const
{

}

uint32_t
CMessage::CSubscribeReq::Deserialize (Buffer::Iterator &start)
{

}

void
CMessage::CSubscribeReq::Print (std::ostream &os) const
{

}

void
CMessage::SetSubscribeReq (uint32_t reqId, Ptr<CSubscriptionObject> obj)
{

}

// CSubscribeRep --------------------------------------------------------------

uint32_t
CMessage::CSubscribeRep::GetSerializedSize () const
{

}

void
CMessage::CSubscribeRep::Serialize (Buffer::Iterator &start) const
{

}

uint32_t
CMessage::CSubscribeRep::Deserialize (Buffer::Iterator &start)
{

}

void
CMessage::CSubscribeRep::Print (std::ostream &os) const
{

}

void
CMessage::SetSubscribeRep (uint32_t reqId)
{

}

// CSubscribeNot --------------------------------------------------------------

uint32_t
CMessage::CSubscribeNot::GetSerializedSize () const
{

}

void
CMessage::CSubscribeNot::Serialize (Buffer::Iterator &start) const
{

}

uint32_t
CMessage::CSubscribeNot::Deserialize (Buffer::Iterator &start)
{

}

void
CMessage::CSubscribeNot::Print (std::ostream &os) const
{

}

void
CMessage::SetSubscribeNot (std::vector<uint32_t> subId,
  Ptr<ObjectMetadata> objMeta)
{

}

// CUnsubscribeReq ------------------------------------------------------------

uint32_t
CMessage::CUnsubscribeReq::GetSerializedSize () const
{

}

void
CMessage::CUnsubscribeReq::Serialize (Buffer::Iterator &start) const
{

}

uint32_t
CMessage::CUnsubscribeReq::Deserialize (Buffer::Iterator &start)
{

}

void
CMessage::CUnsubscribeReq::Print (std::ostream &os) const
{

}

void
CMessage::SetUnsubscribeReq (uint32_t reqId, uint32_t subId)
{

}

// CUnsubscribeRep ------------------------------------------------------------

uint32_t
CMessage::CUnsubscribeRep::GetSerializedSize () const
{

}

void
CMessage::CUnsubscribeRep::Serialize (Buffer::Iterator &start) const
{

}

uint32_t
CMessage::CUnsubscribeRep::Deserialize (Buffer::Iterator &start)
{

}

void
CMessage::CUnsubscribeRep::Print (std::ostream &os) const
{

}

void
CMessage::SetUnsubscribeRep (uint32_t reqId)
{

}

// CStateTransfer -------------------------------------------------------------

uint32_t
CMessage::CStateTransfer::GetSerializedSize () const
{

}

void
CMessage::CStateTransfer::Serialize (Buffer::Iterator &start) const
{

}

uint32_t
CMessage::CStateTransfer::Deserialize (Buffer::Iterator &start)
{

}

void
CMessage::CStateTransfer::Print (std::ostream &os) const
{

}

void
CMessage::SetStateTransfer ()
{

}

// CJoinReq -------------------------------------------------------------------

void
CMessage::SetJoinReq ()
{
  if (m_type == 0)
   {
    m_type = CJOIN_REQ;
   }
  else
   {
    NS_ABORT_UNLESS (m_type == CJOIN_REQ);
   }
}

// CJoinRep -------------------------------------------------------------------

void
CMessage::SetJoinRep ()
{
  if (m_type == 0)
   {
    m_type = CJOIN_REP;
   }
  else
   {
    NS_ABORT_UNLESS (m_type == CJOIN_REP);
   }
}

std::ostream&
operator<< (std::ostream &os, CMessage::CPublishReq const &x)
{
  x.Print (os);
  return os;
}

std::ostream&
operator<< (std::ostream &os, CMessage::CPublishRep const &x)
{
  x.Print (os);
  return os;
}

std::ostream&
operator<< (std::ostream &os, CMessage::CUnpublishReq const &x)
{
  x.Print (os);
  return os;
}

std::ostream&
operator<< (std::ostream &os, CMessage::CUnpublishRep const &x)
{
  x.Print (os);
  return os;
}

std::ostream&
operator<< (std::ostream &os, CMessage::CDownloadReq const &x)
{
  x.Print (os);
  return os;
}

std::ostream&
operator<< (std::ostream &os, CMessage::CDownloadRep const &x)
{
  x.Print (os);
  return os;
}

std::ostream&
operator<< (std::ostream &os, CMessage::CSubscribeReq const &x)
{
  x.Print (os);
  return os;
}

std::ostream&
operator<< (std::ostream &os, CMessage::CSubscribeRep const &x)
{
  x.Print (os);
  return os;
}

std::ostream&
operator<< (std::ostream &os, CMessage::CSubscribeNot const &x)
{
  x.Print (os);
  return os;
}

std::ostream&
operator<< (std::ostream &os, CMessage::CUnsubscribeReq const &x)
{
  x.Print (os);
  return os;
}

std::ostream&
operator<< (std::ostream &os, CMessage::CUnsubscribeRep const &x)
{
  x.Print (os);
  return os;
}

std::ostream&
operator<< (std::ostream &os, CMessage::CStateTransfer const &x)
{
  x.Print (os);
  return os;
}

}
}
