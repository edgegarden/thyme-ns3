/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2015 NOVA LINCS DI-FCT-UNL <http://nova-lincs.di.fct.unl.pt/>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Joao A. Silva <jaa.silva@campus.fct.unl.pt>
 */
#ifndef CHORD_PS_H
#define CHORD_PS_H

#include "ns3/ipv4.h"
#include "ns3/hyrax-api.h"
#include "ns3/chord-ipv4.h"
#include "ns3/udp-socket-factory.h"
#include "ns3/chord-common.h"
#include "ns3/chord-message2.h"
#include "ns3/hyrax-request.h"
#include "ns3/chord-subscribe-object.h"

namespace ns3 {
namespace hyrax {

class ChordPS : public Application
{
public:
  static TypeId GetTypeId (void);
  ChordPS ();
  virtual ~ChordPS ();

  /**
   * 1. 
   * 
   * Failures: 
   */
  virtual void DoPublish (Ptr<ThymeObject> obj);

  /**
   * 1. 
   * 
   * Failures: 
   */
  void DoUnpublish (Ptr<ThymeObject> obj);

  /**
   * 1. Send CDOWNLOAD_REQ msg to owner
   * 2. Recv RSP (CDOWNLOAD_REP)
   * 3. Save object in passive replicas
   * 4. Notify application
   * 
   * Failures: OBJECT_NOT_FOUND, TIMEOUT
   */
  void DoDownload (Ptr<HyraxIdentifier> identifier, Ipv4Address owner);

  /**
   * 1. 
   * 
   * Failures: 
   */
  void DoSubscribe (std::string filter, Time startTs, Time endTs);

  /**
   * 1. Check for movement
   * 2. Check subscription ownership
   * 3. Check subscription validity
   * 
   * Failures: 
   */
  void Unsubscribe (Ptr<Subscription> subObj);

  Ptr<Subscription> GetSubscription (uint32_t subId);
  std::vector<uint32_t> GetSubscriptionIds ();

  void IfaceDown ();
  void IfaceUp ();

  void TraceRing ();

protected:
  void DoDispose ();
  void DoInitialize ();

private:
  void StartApplication ();
  void StopApplication ();
  void SetupRoutingLayer ();

  // ----- MESSAGES -----
  void ProcessPacket (Ptr<Socket> socket);
  void ProcessPublishReq (CMessage::CPublishReq msg, Ipv4Address src);
  void ProcessPublishRep (CMessage::CPublishRep msg);
  void ProcessUnpublishReq (CMessage::CUnpublishReq msg, Ipv4Address src);
  void ProcessUnpublishRep (CMessage::CUnpublishRep msg);
  void ProcessDownloadReq (CMessage::CDownloadReq msg, Ipv4Address src);
  void ProcessDownloadRep (CMessage::CDownloadRep msg);
  void ProcessSubscribeReq (CMessage::CSubscribeReq msg, Ipv4Address src);
  void ProcessSubscribeRep (CMessage::CSubscribeRep msg);
  void ProcessSubscribeNot (CMessage::CSubscribeNot msg);
  void ProcessUnsubscribeReq (CMessage::CUnsubscribeReq msg, Ipv4Address src);
  void ProcessUnsubscribeRep (CMessage::CUnsubscribeRep msg);
  void ProcessStateTransfer (CMessage::CStateTransfer msg);
  void ProcessJoinReq (Ipv4Address src);
  void ProcessJoinRep (Ipv4Address src);

  void Broadcast (CMessage msg);
  void Send (CMessage msg, Ipv4Address dst);

  // ----- CALLBACKS -----
  void HandleLookupSuccess (Ptr<ChordIdentifier> key, Ipv4Address addr, uint16_t port);
  void HandleLookupFailure (Ptr<ChordIdentifier> key);
  void HandleKeyOwnershipChanged (Ptr<ChordIdentifier> vNodeKey,
                                  Ptr<ChordIdentifier> predecessorKey,
                                  Ptr<ChordIdentifier> oldPredecessorKey,
                                  Ipv4Address predecessorAddr, uint16_t port);
  void HandleVNodeFailure (std::string name, Ptr<ChordIdentifier> key);
  void HandleVNodeJoinSucces (std::string name, Ptr<ChordIdentifier> key);
  void HandleTraceRing (std::string name, Ptr<ChordIdentifier> key);

  // ----- REQUESTS -----
  void StartDownloadObjectTimeoutHandler (Ptr<DownloadRequest> req);
  void HandleDownloadObjectTimeout (Ptr<DownloadRequest> req);

  // ----- OTHERS -----
  uint8_t* Hash (std::string key);
  Ptr<ChordIdentifier> CreateChordIdentifier (std::string key);
  void Join ();
  void JoinTimerExpired ();
  void InsertRequest (Ptr<ChordIdentifier> key, Ptr<Request> req);

  // ----- VARIABLES ------
  Ptr<Ipv4> m_ipv4;
  Ptr<UniformRandomVariable> m_rand;

  Ptr<ChordIpv4> m_routing;
  uint16_t m_dHashPort;
  Ptr<NetDevice> m_iface;
  Ptr<Socket> m_socket;

  std::string m_name;
  uint8_t* m_key;
  uint8_t m_joinRingRetries;
  bool m_joined;
  EventId m_joinTimer;
  Time m_joinTimeout;

  // < subscriptionId, subscriptionObj >
  //COwnSubscriptions m_mySubs; // indexed by subId
  // < node, < subscriptionId, subscriptionObj > >
  //CSubscriptions m_subscriptions; // indexed by subscriberId
  // < tag, < (objId, node), objMeta > >
  Tags m_tagSubs; // indexed by tag

  // < chordId, < reqs > >
  std::map<ChordIdentifier, std::vector<Ptr<Request> > > m_ongoingLookups;

  Time m_auditObjectsTimeout;
};

}
}

#endif /* CHORD_PS_H */
