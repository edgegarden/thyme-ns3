/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2015 NOVA LINCS DI-FCT-UNL <http://nova-lincs.di.fct.unl.pt/>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Joao A. Silva <jaa.silva@campus.fct.unl.pt>
 */
#include <ns3/thyme-experiment.h>

using namespace ns3;

int
main (int argc, char *argv[])
{
  ThymeExperiment::EnableLogs (false, false, false);

  uint32_t numNodes = NUM_NODES;
  double simTime = SIM_TIME;
  double stopAppsTime = STOP_APPS;
  std::string traceFile = TRACE_FILE;
  double maxX = MAX_X;
  double maxY = MAX_Y;

  CommandLine cmd;
  cmd.AddValue ("numNodes", "Number of nodes.", numNodes);
  cmd.AddValue ("simTime", "Total simulation time (s).", simTime);
  cmd.AddValue ("stopTime", "Time for stopping apps (s).", stopAppsTime);
  cmd.AddValue ("file", "File with traces to run.", traceFile);
  cmd.AddValue ("maxX", "Maximum X coordinate (m).", maxX);
  cmd.AddValue ("maxY", "Maximum Y coordinate (m).", maxY);
  cmd.Parse (argc, argv);

  // set max coordinates for grid manager
  Config::SetDefault ("ns3::hyrax::GridManager::SizeX", DoubleValue (maxX));
  Config::SetDefault ("ns3::hyrax::GridManager::SizeY", DoubleValue (maxY));

  std::cout << ">>> Creating " << numNodes << " nodes...\n";
  NodeContainer nodes = ThymeExperiment::CreateNodes (numNodes);
  ThymeExperiment::SetupFileMobilityModel (traceFile);
  NetDeviceContainer devices = ThymeExperiment::CreateDevices (nodes);

  ThymeExperiment::InstallStaticRouting (nodes, devices);
  Ptr<hyrax::Profiler> profiler = ThymeExperiment::ScheduleTasks (traceFile,
    nodes, stopAppsTime, false);

  ThymeExperiment::PopulateArpCache ();

  std::cout << ">>> Starting simulation for " << simTime << "s...\n";
  Simulator::Stop (Seconds (simTime));
  Simulator::Run ();

  profiler->PrintReport ();
  Simulator::Destroy ();

  return 0;
}

