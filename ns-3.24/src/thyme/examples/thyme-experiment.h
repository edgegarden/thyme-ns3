/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2015 NOVA LINCS DI-FCT-UNL <http://nova-lincs.di.fct.unl.pt/>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Joao A. Silva <jaa.silva@campus.fct.unl.pt>
 */
#ifndef THYME_EXPERIMENT_H
#define THYME_EXPERIMENT_H

#include <ns3/abort.h>
#include <ns3/aodv-helper.h>
#include <ns3/batman-helper.h>
#include <ns3/broadcast-helper.h>
#include <ns3/config.h>
#include <ns3/core-module.h>
#include <ns3/dsdv-helper.h>
#include <ns3/dsr-helper.h>
#include <ns3/dsr-main-helper.h>
#include <ns3/flow-monitor.h>
#include <ns3/flow-monitor-helper.h>
#include <ns3/internet-stack-helper.h>
#include <ns3/ipv4-address-helper.h>
#include <ns3/ipv4-flow-classifier.h>
#include <ns3/ipv4-interface-container.h>
#include <ns3/ipv4-static-routing-helper.h>
#include <ns3/log.h>
#include <ns3/mobility-module.h>
#include <ns3/node-container.h>
#include <ns3/node-list.h>
#include <ns3/nqos-wifi-mac-helper.h>
#include <ns3/olsr-helper.h>
#include <ns3/thyme-module.h>
#include <ns3/wifi-helper.h>
#include <ns3/yans-wifi-helper.h>

using namespace ns3;

#define NUM_NODES 36
#define START_APPS 30 // s
#define WARMUP 30 // s
#define COOLDOWN 60 // s
#define SIM_TIME (600 + (START_APPS) + (WARMUP) + (COOLDOWN)) // s
#define STOP_APPS (SIM_TIME - 1) // s
#define MAX_X 240 // m
#define MAX_Y 120 // m
#define TRACE_FILE "../scripts/traces/files/n36-euro2016-t1-p30-s2.5-c0-i0.txt"
#define ROUTING_PROTOCOL 2 // 0-AODV, 1-OLSR, 2-DSDV, 3-DSR, 4-BATMAN

class ThymeExperiment
{
public:

  static void
  EnableLogs (bool fineGrain, bool broadcast, bool print)
  {
    LogComponentEnable ("API", (LogLevel) (LOG_LEVEL_DEBUG | LOG_PREFIX_ALL));
    LogComponentEnable ("Thyme", (LogLevel) (LOG_LEVEL_INFO | LOG_PREFIX_ALL));
    LogComponentEnable ("RP", (LogLevel) (LOG_LEVEL_ALL | LOG_PREFIX_ALL));
    LogComponentEnable ("RT", (LogLevel) (LOG_LEVEL_INFO | LOG_PREFIX_ALL));
    LogComponentEnable ("Prof", (LogLevel) (LOG_LEVEL_ERROR | LOG_PREFIX_ALL));
    //LogComponentEnable ("Ns2MobilityHelper", (LogLevel) (LOG_LEVEL_WARN | LOG_PREFIX_ALL));

    LogComponentEnable ("Id", (LogLevel) (LOG_LEVEL_ERROR | LOG_PREFIX_ALL));
    LogComponentEnable ("Meta", (LogLevel) (LOG_LEVEL_ERROR | LOG_PREFIX_ALL));
    LogComponentEnable ("Obj", (LogLevel) (LOG_LEVEL_ERROR | LOG_PREFIX_ALL));
    LogComponentEnable ("Sub", (LogLevel) (LOG_LEVEL_ERROR | LOG_PREFIX_ALL));
    LogComponentEnable ("Req", (LogLevel) (LOG_LEVEL_ERROR | LOG_PREFIX_ALL));

    //LogComponentEnable ("Ipv4L3Protocol", (LogLevel) (LOG_LEVEL_ALL | LOG_PREFIX_ALL));
    //LogComponentEnable ("Ipv4Interface", (LogLevel) (LOG_LEVEL_ALL | LOG_PREFIX_ALL));
    //LogComponentEnable ("ArpL3Protocol", (LogLevel) (LOG_LEVEL_ALL | LOG_PREFIX_ALL));
    //LogComponentEnable ("ArpCache", (LogLevel) (LOG_LEVEL_ALL | LOG_PREFIX_ALL));

    if (broadcast)
     { // PL;SG
      LogComponentEnable ("Bcast", (LogLevel) (LOG_LEVEL_WARN | LOG_PREFIX_ALL));

      if (fineGrain)
       {
        LogComponentEnable ("Dpd", (LogLevel) (LOG_LEVEL_ALL | LOG_PREFIX_ALL));
        LogComponentEnable ("BSubObj", (LogLevel) (LOG_LEVEL_ALL | LOG_PREFIX_ALL));
       }
     }
    else
     { // DCS
      LogComponentEnable ("SubObj", (LogLevel) (LOG_LEVEL_ERROR | LOG_PREFIX_ALL));
      LogComponentEnable ("GM", (LogLevel) (LOG_LEVEL_WARN | LOG_PREFIX_ALL));
      LogComponentEnable ("LS", (LogLevel) (LOG_LEVEL_INFO | LOG_PREFIX_ALL));

      if (fineGrain)
       {
        LogComponentEnable ("NodeAddr", (LogLevel) (LOG_LEVEL_ALL | LOG_PREFIX_ALL));
       }
     }

    if (print) LogComponentPrintList ();
  }

  static NodeContainer
  CreateNodes (uint32_t numNodes)
  {
    NodeContainer nodes;
    nodes.Create (numNodes);

    for (int i = 0; i < numNodes; ++i)
     { // give names to nodes
      std::string str = "n";
      str += std::to_string (i);
      Names::Add (str, nodes.Get (i));
     }

    return nodes;
  }

  static void
  SetupListPosAllocConstPosMobilityModel (NodeContainer nodes,
                                          Ptr<ListPositionAllocator> initialAlloc)
  {
    MobilityHelper mobility;
    mobility.SetPositionAllocator (initialAlloc);

    mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
    mobility.Install (nodes);
  }

  static void
  SetupGridPosAllocConstPosMobilityModel (NodeContainer nodes, double dist,
                                          uint32_t gridWidth)
  {
    MobilityHelper mobility;
    mobility.SetPositionAllocator ("ns3::GridPositionAllocator",
                                   "MinX", DoubleValue (0.0),
                                   "MinY", DoubleValue (0.0),
                                   "DeltaX", DoubleValue (dist),
                                   "DeltaY", DoubleValue (dist),
                                   "GridWidth", UintegerValue (gridWidth),
                                   "LayoutType", StringValue ("RowFirst"));

    mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
    mobility.Install (nodes);
  }

  static Ptr<RandomRectanglePositionAllocator>
  SetupRandRectAllocator (double maxX, double maxY)
  {
    Ptr<RandomRectanglePositionAllocator> alloc =
      CreateObject<RandomRectanglePositionAllocator> ();
    std::string str = "ns3::UniformRandomVariable[Min=0.0|Max=";
    str += std::to_string (maxX);
    str += "]";
    alloc->SetAttribute ("X", StringValue (str));

    str = "ns3::UniformRandomVariable[Min=0.0|Max=";
    str += std::to_string (maxY);
    str += "]";
    alloc->SetAttribute ("Y", StringValue (str));

    return alloc;
  }

  static void
  SetupRandRectAllocConstPosMobilityModel (NodeContainer nodes, double maxX,
                                           double maxY)
  {
    MobilityHelper mobility;
    mobility.SetPositionAllocator (SetupRandRectAllocator (maxX, maxY));

    mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
    mobility.Install (nodes);
  }

  static void
  SetupFileMobilityModel (std::string file)
  {
    Ns2MobilityHelper ns2 = Ns2MobilityHelper (file);
    ns2.Install ();
  }

  static NetDeviceContainer
  CreateDevices (NodeContainer nodes, bool pcap = false, bool trace = false)
  {
    WifiHelper wifi; // defaults to WIFI_PHY_STANDARD_80211a
    wifi.SetStandard (WIFI_PHY_STANDARD_80211g);
    /**
     * WIFI_PHY_STANDARD_80211a        - 802.11a (5GHz)   - OfdmRateXXMbps 6 9 12 18 24 36 48 54
     * WIFI_PHY_STANDARD_80211b        - 802.11b (2.4GHz) - DsssRateXXMbps 1 2 5_5 11
     * WIFI_PHY_STANDARD_80211g        - 802.11g (2.4GHz) - ErpOfdmRateXXMbps 6 9 12 18 24 36 48 54
     * WIFI_PHY_STANDARD_80211n_2_4GHZ - 802.11n (2.4GHz) - HT OFDM PHY
     * WIFI_PHY_STANDARD_80211n_5GHZ   - 802.11n (5GHz)   - HT OFDM PHY
     * WIFI_PHY_STANDARD_80211ac       - 802.11ac (5GHz)  - VHT OFDM PHY
     */

    NqosWifiMacHelper wifiMac = NqosWifiMacHelper::Default ();
    wifiMac.SetType ("ns3::AdhocWifiMac");

    std::string phyMode ("ErpOfdmRate6Mbps");
    // defaults to ArfWifiManager (algorithm found in real devices: ConstantRateWifiManager)
    wifi.SetRemoteStationManager ("ns3::ConstantRateWifiManager",
                                  "DataMode", StringValue (phyMode),
                                  "ControlMode", StringValue (phyMode),
                                  "NonUnicastMode", StringValue (phyMode),
                                  "RtsCtsThreshold", UintegerValue (2200) // defaults to 65535
                                  //"FragmentationThreshold", UintegerValue (2200) // defaults to 2346
                                  );

    // defaults to NistErrorRateModel
    YansWifiPhyHelper wifiPhy = YansWifiPhyHelper::Default ();

    // defaults to ConstantSpeedPropagationDelayModel
    // defaults to LogDistancePropagationLossModel
    YansWifiChannelHelper wifiChannel = YansWifiChannelHelper::Default ();
    //wifiChannel.SetPropagationDelay ("ns3::ConstantSpeedPropagationDelayModel");
    //wifiChannel.AddPropagationLoss ("ns3::RangePropagationLossModel",
    //                                "MaxRange", DoubleValue (113.2));
    wifiPhy.SetChannel (wifiChannel.Create ());

    NetDeviceContainer devices = wifi.Install (wifiPhy, wifiMac, nodes);

    if (trace)
     {
      AsciiTraceHelper ascii;
      wifiPhy.EnableAsciiAll (ascii.CreateFileStream ("hyrax.tr"));
     }

    if (pcap) wifiPhy.EnablePcapAll ("hyrax", false);

    return devices;
  }

  static Ipv4InterfaceContainer
  InstallStaticRouting (NodeContainer nodes, NetDeviceContainer devices)
  {
    Ipv4StaticRoutingHelper staticRouting; // static routing w/ no routes
    InternetStackHelper stack;
    stack.SetRoutingHelper (staticRouting);
    stack.Install (nodes);

    Ipv4AddressHelper address; // install addresses
    address.SetBase ("10.0.0.0", "255.255.0.0");
    return address.Assign (devices);
  }

  static Ipv4InterfaceContainer
  InstallAodvRouting (NodeContainer nodes, NetDeviceContainer devices)
  {
    AodvHelper aodv;
    InternetStackHelper stack;
    stack.SetRoutingHelper (aodv);
    stack.Install (nodes);

    Ipv4AddressHelper address; // install addresses
    address.SetBase ("10.0.0.0", "255.255.0.0");
    return address.Assign (devices);
  }

  static Ipv4InterfaceContainer
  InstallOlsrRouting (NodeContainer nodes, NetDeviceContainer devices)
  {
    OlsrHelper olsr;
    InternetStackHelper stack;
    stack.SetRoutingHelper (olsr);
    stack.Install (nodes);

    Ipv4AddressHelper address; // install addresses
    address.SetBase ("10.0.0.0", "255.255.0.0");
    return address.Assign (devices);
  }

  static Ipv4InterfaceContainer
  InstallDsdvRouting (NodeContainer nodes, NetDeviceContainer devices)
  {
    DsdvHelper dsdv;
    InternetStackHelper stack;
    stack.SetRoutingHelper (dsdv);
    stack.Install (nodes);

    Ipv4AddressHelper address; // install addresses
    address.SetBase ("10.0.0.0", "255.255.0.0");
    return address.Assign (devices);
  }

  static Ipv4InterfaceContainer
  InstallDsrRouting (NodeContainer nodes, NetDeviceContainer devices)
  {
    InternetStackHelper stack;
    DsrMainHelper dsrMain;
    DsrHelper dsr;
    stack.Install (nodes);
    dsrMain.Install (dsr, nodes);

    Ipv4AddressHelper address; // install addresses
    address.SetBase ("10.0.0.0", "255.255.0.0");
    return address.Assign (devices);
  }

  static Ipv4InterfaceContainer
  InstallBatmanRouting (NodeContainer nodes, NetDeviceContainer devices)
  {
    BatmanHelper batman;
    InternetStackHelper stack;
    stack.SetRoutingHelper (batman);
    stack.Install (nodes);

    Ipv4AddressHelper address; // install addresses
    address.SetBase ("10.0.0.0", "255.255.0.0");
    return address.Assign (devices);
  }

  static void
  ConnectTraces (Ptr<hyrax::Profiler> profiler)
  {
    // PHY layer
    Config::Connect ("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Phy/PhyRxEnd",
                     MakeCallback (&hyrax::Profiler::OnPhyRxEnd, profiler));
    Config::Connect ("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Phy/PhyRxDrop",
                     MakeCallback (&hyrax::Profiler::OnPhyRxDrop, profiler));
    Config::Connect ("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Phy/PhyTxBegin",
                     MakeCallback (&hyrax::Profiler::OnPhyTxBegin, profiler));
    Config::Connect ("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Phy/PhyTxDrop",
                     MakeCallback (&hyrax::Profiler::OnPhyTxDrop, profiler));

    // MAC layer
    Config::Connect ("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Mac/MacRx",
                     MakeCallback (&hyrax::Profiler::OnMacRx, profiler));
    Config::Connect ("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Mac/MacRxDrop",
                     MakeCallback (&hyrax::Profiler::OnMacRxDrop, profiler));
    Config::Connect ("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Mac/MacTx",
                     MakeCallback (&hyrax::Profiler::OnMacTx, profiler));
    Config::Connect ("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Mac/MacTxDrop",
                     MakeCallback (&hyrax::Profiler::OnMacTxDrop, profiler));
    Config::Connect ("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/RemoteStationManager/MacTxDataFailed",
                     MakeCallback (&hyrax::Profiler::OnMacTxDataFailed, profiler));
    Config::Connect ("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/RemoteStationManager/MacTxFinalDataFailed",
                     MakeCallback (&hyrax::Profiler::OnMacTxFinalDataFailed, profiler));

    // IP layer
    Config::Connect ("/NodeList/*/$ns3::Ipv4L3Protocol/LocalDeliver",
                     MakeCallback (&hyrax::Profiler::OnIpIn, profiler));
    Config::Connect ("/NodeList/*/$ns3::Ipv4L3Protocol/SendOutgoing",
                     MakeCallback (&hyrax::Profiler::OnIpOut, profiler));
    Config::Connect ("/NodeList/*/$ns3::Ipv4L3Protocol/Drop",
                     MakeCallback (&hyrax::Profiler::OnIpDrop, profiler));
    Config::Connect ("/NodeList/*/$ns3::Ipv4L3Protocol/UnicastForward",
                     MakeCallback (&hyrax::Profiler::OnIpFwd, profiler));
  }

  static Ptr<hyrax::Profiler>
  InstallApplicationThyme (NodeContainer nodes, double start, double stop,
                           double warmup)
  {
    Ptr<hyrax::Profiler> profiler = Create<hyrax::Profiler> (nodes, false);
    ThymeHelper thyme (profiler);
    static const Ptr<UniformRandomVariable> rand =
      CreateObject<UniformRandomVariable> ();

    for (int i = 0; i < nodes.GetN (); ++i)
     {
      ApplicationContainer apps = thyme.Install (nodes.Get (i));
      apps.Start (Seconds (rand->GetValue (start, start + warmup - 2)));
      apps.Stop (Seconds (stop));
     }

    ConnectTraces (profiler);
    return profiler;
  }

  static Ptr<hyrax::Profiler>
  InstallApplicationBroadcast (NodeContainer nodes, double start, double stop,
                               double warmup)
  {
    Ptr<hyrax::Profiler> profiler = Create<hyrax::Profiler> (nodes, true);
    BroadcastHelper broadcast (profiler);
    static const Ptr<UniformRandomVariable> rand =
      CreateObject<UniformRandomVariable> ();

    for (int i = 0; i < nodes.GetN (); ++i)
     {
      ApplicationContainer apps = broadcast.Install (nodes.Get (i));
      apps.Start (Seconds (rand->GetValue (start, start + warmup - 2)));
      apps.Stop (Seconds (stop));
     }

    ConnectTraces (profiler);
    return profiler;
  }

  static void
  SetSubscriptionNotificationCallback (NodeContainer nodes)
  {
    for (int i = 0; i < nodes.GetN (); ++i)
     {
      Ptr<hyrax::ThymeApi> app =
        nodes.Get (i)->GetApplication (0)->GetObject<hyrax::ThymeApi> ();
      app->SetSubscriptionNotificationCallback (MakeCallback (&hyrax::ThymeApi::BenchmarkDownload, app));
     }
  }

  static void
  PopulateArpCache ()
  {
    Ptr<ArpCache> arp = CreateObject<ArpCache> ();
    for (NodeList::Iterator i = NodeList::Begin (); i != NodeList::End (); ++i)
     {
      Ptr<Ipv4L3Protocol> ip = (*i)->GetObject<Ipv4L3Protocol> ();
      NS_ASSERT (ip != 0);

      ObjectVectorValue interfaces;
      ip->GetAttribute ("InterfaceList", interfaces);

      for (ObjectVectorValue::Iterator j = interfaces.Begin ();
        j != interfaces.End (); j++)
       {
        Ptr<Ipv4Interface> ipIface = (*j).second->GetObject<Ipv4Interface> ();
        NS_ASSERT (ipIface != 0);

        Ptr<NetDevice> device = ipIface->GetDevice ();
        NS_ASSERT (device != 0);

        Mac48Address addr = Mac48Address::ConvertFrom (device->GetAddress ());

        for (uint32_t k = 0; k < ipIface->GetNAddresses (); k++)
         {
          Ipv4Address ipAddr = ipIface->GetAddress (k).GetLocal ();

          if (ipAddr == Ipv4Address::GetLoopback ()) continue;

          ArpCache::Entry * entry = arp->Add (ipAddr);
          entry->SetMacAddress (addr);
          entry->MarkPermanent ();
         }
       }
     }

    for (NodeList::Iterator i = NodeList::Begin (); i != NodeList::End (); ++i)
     {
      Ptr<Ipv4L3Protocol> ip = (*i)->GetObject<Ipv4L3Protocol> ();
      NS_ASSERT (ip != 0);

      ObjectVectorValue interfaces;
      ip->GetAttribute ("InterfaceList", interfaces);

      for (ObjectVectorValue::Iterator j = interfaces.Begin ();
        j != interfaces.End (); j++)
       {
        Ptr<Ipv4Interface> ipIface = (*j).second->GetObject<Ipv4Interface> ();
        ipIface->SetAttribute ("ArpCache", PointerValue (arp));
       }
     }
  }

  static void
  PrintFlowMonitorReport (Ptr<FlowMonitor> monitor)
  {
    monitor->CheckForLostPackets ();
    std::map<FlowId, FlowMonitor::FlowStats> stats = monitor->GetFlowStats ();
    uint64_t totalPacketsTx = 0, totalPacketsRx = 0, numberOfFlows = 0,
      lostPackets = 0;
    Time totalDelay = Seconds (0.0);

    for (auto i = stats.begin (); i != stats.end (); ++i)
     {
      numberOfFlows++;
      totalPacketsTx += i->second.txPackets;
      totalPacketsRx += i->second.rxPackets;
      lostPackets += i->second.lostPackets;
      totalDelay += i->second.delaySum;
     }

    std::cout << "\n\n"
      << "#### FLOW MONITOR REPORT ####\n"
      << "Number of flows:       " << numberOfFlows << "\n"
      << "Total # Packets Lost:  " << lostPackets << "\n"
      << "Total # Packets Tx:    " << totalPacketsTx << "\n"
      << "Total # Packets Rx:    " << totalPacketsRx << "\n"
      << "Avg Delay:             " << totalDelay.GetSeconds ()
      << "/" << totalPacketsRx
      << " = " << totalDelay.GetSeconds () / totalPacketsRx << "s\n\n";
  }

  static Ptr<hyrax::Profiler>
  ScheduleTasks (std::string traceFile, NodeContainer nodes, double stop,
                 bool bcast)
  {
    if (traceFile == "") NS_ABORT_MSG ("No file name");

    std::ifstream file;
    file.open (traceFile.c_str ());

    if (!file.is_open ()) NS_ABORT_MSG ("Unable to open file: " << traceFile);
    else std::cout << ">>> Reading file " << traceFile << "\n";

    uint32_t _pubs = 0;
    uint32_t _unpubs = 0;
    uint32_t _subs_p = 0;
    uint32_t _subs_f = 0;
    uint32_t _unsubs = 0;
    uint32_t _enters = 0;
    uint32_t _exits = 0;
    uint32_t _pauses = 0;
    uint32_t _resumes = 0;

    Ptr<hyrax::Profiler> profiler = Create<hyrax::Profiler> (nodes, bcast);

    BroadcastHelper broadcast;
    ThymeHelper thyme;
    if (bcast) broadcast = BroadcastHelper (profiler);
    else thyme = ThymeHelper (profiler);

    Time stopTime = Seconds (stop);

    std::string delimiter = "$|$";
    std::string line;
    while (!file.eof ())
     {
      std::getline (file, line, '\n');
      if (line == "") continue; // skip blank lines

      std::vector<std::string> tokens;
      size_t pos = 0;
      while ((pos = line.find (delimiter)) != std::string::npos)
       {
        tokens.push_back (line.substr (0, pos));
        line.erase (0, pos + delimiter.length ());
       }
      tokens.push_back (line);

      if (tokens.size () < 2) continue; // probably is a movement command

      auto it = tokens.begin ();
      std::string op = *it;

      ++it;
      std::istringstream sin (*it);
      double time;
      sin >> time;
      Time t = Seconds (time);

      ++it;
      sin = std::istringstream (*it);
      int nodeId;
      sin >> nodeId;

      if (op == "NODE")
       {
        ApplicationContainer app;
        if (bcast) app = broadcast.Install (nodes.Get (nodeId));
        else app = thyme.Install (nodes.Get (nodeId));

        app.Start (t);
        app.Stop (stopTime);
        ++_enters;
       }
      else if (op == "EXIT")
       {
        Ptr<hyrax::ThymeApi> app =
          nodes.Get (nodeId)->GetApplication (0)->GetObject<hyrax::ThymeApi> ();

        Simulator::Schedule (t, &Application::StopApplicationNow, app);
        ++_exits;
       }
      else if (op == "PAUSE")
       {
        Ptr<hyrax::ThymeApi> app =
          nodes.Get (nodeId)->GetApplication (0)->GetObject<hyrax::ThymeApi> ();

        Simulator::Schedule (t, &hyrax::ThymeApi::BenchmarkPause, app);
        ++_pauses;
       }
      else if (op == "RESUME")
       {
        Ptr<hyrax::ThymeApi> app =
          nodes.Get (nodeId)->GetApplication (0)->GetObject<hyrax::ThymeApi> ();

        Simulator::Schedule (t, &hyrax::ThymeApi::BenchmarkResume, app);
        ++_resumes;
       }
      else if (op == "PUB")
       {
        Ptr<hyrax::ThymeApi> app =
          nodes.Get (nodeId)->GetApplication (0)->GetObject<hyrax::ThymeApi> ();

        ++it;
        std::string key = *it;

        ++it;
        std::string value = *it;

        ++it;
        std::string summary = *it;

        ++it;
        std::string _tags = *it;
        std::set<std::string> tags;
        std::string delimiter = " ";
        size_t pos = 0;
        while ((pos = _tags.find (delimiter)) != std::string::npos)
         {
          tags.insert (_tags.substr (0, pos));
          _tags.erase (0, pos + delimiter.length ());
         }
        tags.insert (_tags);

        Simulator::Schedule (t, &hyrax::ThymeApi::BenchmarkPublish, app, key,
                             value, summary, tags);
        _pubs++;
       }
      else if (op == "SUB_P")
       {
        Ptr<hyrax::ThymeApi> app =
          nodes.Get (nodeId)->GetApplication (0)->GetObject<hyrax::ThymeApi> ();

        ++it;
        std::string tag = *it;
        Simulator::Schedule (t, &hyrax::ThymeApi::BenchmarkSubscribePast,
                             app, tag);
        _subs_p++;
       }
      else if (op == "SUB_F")
       {
        Ptr<hyrax::ThymeApi> app =
          nodes.Get (nodeId)->GetApplication (0)->GetObject<hyrax::ThymeApi> ();

        ++it;
        std::string tag = *it;
        Simulator::Schedule (t, &hyrax::ThymeApi::BenchmarkSubscribeFuture,
                             app, tag);
        _subs_f++;
       }
      else if (op == "UNPUB")
       {
        Ptr<hyrax::ThymeApi> app =
          nodes.Get (nodeId)->GetApplication (0)->GetObject<hyrax::ThymeApi> ();

        ++it;
        std::string key = *it;
        Simulator::Schedule (t, &hyrax::ThymeApi::BenchmarkUnpublish, app, key);
        _unpubs++;
       }
      else if (op == "UNSUB")
       {
        Ptr<hyrax::ThymeApi> app =
          nodes.Get (nodeId)->GetApplication (0)->GetObject<hyrax::ThymeApi> ();

        ++it;
        sin = std::istringstream (*it);
        int subId;
        sin >> subId;
        Simulator::Schedule (t, &hyrax::ThymeApi::BenchmarkUnsubscribe, app,
                             subId);
        _unsubs++;
       }
       else
       {
        std::cout << "Unknown command: " << op << "\n";
       }
     }

    SetSubscriptionNotificationCallback (nodes);

    ConnectTraces (profiler);

    std::cout << ">>>"
      << " Enters: " << _enters
      << " Exits: " << _exits
      << " Pauses: " << _pauses
      << " Resumes: " << _resumes
      << " Pubs: " << _pubs
      << " Unpubs: " << _unpubs
      << " Subs: " << (_subs_p + _subs_f)
      << " (P: " << _subs_p << " F: " << _subs_f << ")"
      << " Unsubs: " << _unsubs
      << "\n";

    return profiler;
  }
};

#endif /* THYME_EXPERIMENT_H */

