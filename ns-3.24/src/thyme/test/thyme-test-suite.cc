/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2015 NOVA LINCS DI-FCT-UNL <http://nova-lincs.di.fct.unl.pt/>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Joao A. Silva <jaa.silva@campus.fct.unl.pt>
 */
#include <ns3/test.h>
#include <ns3/thyme-common.h>
#include <ns3/thyme-identifier.h>
#include <ns3/thyme-object-metadata.h>
#include <ns3/thyme-subscription.h>

using namespace ns3;

class ObjectIdentifierTestCase : public TestCase
{
public:

  ObjectIdentifierTestCase () : TestCase ("Test ObjectIdentifier class")
  {
  }

  virtual
  ~ObjectIdentifierTestCase ()
  {
  }

private:

  virtual void
  DoRun (void)
  {
    Ptr<hyrax::ObjectIdentifier> id = Create<hyrax::ObjectIdentifier> ();

    NS_TEST_ASSERT_MSG_EQ (
      id->GetKeySize (),
      0,
      "id should have key size equal to 0");

    std::string a = "H3LL0";
    std::string b = "AH3LL0";

    Ptr<hyrax::ObjectIdentifier> id1 = Create<hyrax::ObjectIdentifier> ((uint8_t*) a.c_str (), a.size ());
    Ptr<hyrax::ObjectIdentifier> id2 = Create<hyrax::ObjectIdentifier> (id1);
    Ptr<hyrax::ObjectIdentifier> id3 = Create<hyrax::ObjectIdentifier> ((uint8_t*) b.c_str (), b.size ());

    NS_TEST_ASSERT_MSG_EQ (
      id1->IsEqual (id2),
      true,
      "id1 should be equal to id2");
    NS_TEST_ASSERT_MSG_EQ (
      (PEEK (id1) == PEEK (id2)),
      true,
      "id1 should be equal to id2");
    NS_TEST_ASSERT_MSG_EQ (
      id1->IsEqual (id3),
      false,
      "id1 should not be equal to id3");

    NS_TEST_ASSERT_MSG_EQ (
      id1->IsLess (id2),
      false,
      "id1 should not be less than id2");
    NS_TEST_ASSERT_MSG_EQ (
      id2->IsLess (id1),
      false,
      "id2 should not be less than id1");
    NS_TEST_ASSERT_MSG_EQ (
      id1->IsLess (id3),
      true,
      "id1 should be less than id3");
    NS_TEST_ASSERT_MSG_EQ (
      (PEEK (id1) < PEEK (id3)),
      true,
      "id1 should be less than id3");
    NS_TEST_ASSERT_MSG_EQ (
      id3->IsLess (id1),
      false,
      "id3 should not be less than id1");

    NS_TEST_ASSERT_MSG_EQ (
      id3->ToString (),
      b,
      "id3 key should be equal to string b");

    NS_TEST_ASSERT_MSG_EQ (
      id1->GetSerializedSize (),
      6,
      "GetSerializedSize () of id1 should return 6 bytes");
    NS_TEST_ASSERT_MSG_EQ (
      id3->GetSerializedSize (),
      7,
      "GetSerializedSize () of id3 should return 7 bytes");

    NS_TEST_ASSERT_MSG_EQ (
      id1->GetKeySize (),
      5,
      "GetKeySize () of id1 should return 5 bytes");
    NS_TEST_ASSERT_MSG_EQ (
      id3->GetKeySize (),
      6,
      "GetKeySize () of id3 should return 6 bytes");

    // @TODO test (de)serialization
    //    Buffer buf (id1->GetSerializedSize ());
    //    Buffer::Iterator i = buf.Begin ();
    //    id1->Serialize (i);
    //    Ptr<hyrax::ObjectIdentifier> id4 = Create<hyrax::ObjectIdentifier> ();
    //    id4->Deserialize (i);
    //    res = id4->IsEqual (id1);
    //    NS_TEST_ASSERT_MSG_EQ (res, true, "id4 should be equal to id1");
    //    res = id4->IsEqual (id2);
    //    NS_TEST_ASSERT_MSG_EQ (res, true, "id4 should be equal to id2");
    //    res = id4->IsEqual (id3);
    //    NS_TEST_ASSERT_MSG_EQ (res, false, "id4 should not be equal to id3");
  }
};

class ObjectMetadataTestCase : public TestCase
{
public:

  ObjectMetadataTestCase () : TestCase ("Test ObjectMetadata class")
  {
  }

  virtual
  ~ObjectMetadataTestCase ()
  {
  }

private:

  virtual void
  DoRun (void)
  {
    std::string a = "H3LL0";
    std::string b = "AH3LL0";

    Ptr<hyrax::ObjectIdentifier> id1 = Create<hyrax::ObjectIdentifier> ((uint8_t*) a.c_str (), a.size ());
    Ptr<hyrax::ObjectIdentifier> id2 = Create<hyrax::ObjectIdentifier> (id1);
    Ptr<hyrax::ObjectIdentifier> id3 = Create<hyrax::ObjectIdentifier> ((uint8_t*) b.c_str (), b.size ());

    Ptr<hyrax::ObjectMetadata> meta1 = Create<hyrax::ObjectMetadata> ();

    // @TODO implement
  }
};

class ThymeObjectTestCase : public TestCase
{
public:

  ThymeObjectTestCase () : TestCase ("Test ThymeObject class")
  {
  }

  virtual
  ~ThymeObjectTestCase ()
  {
  }

private:

  virtual void
  DoRun (void)
  {
    // @TODO implement
  }
};

class SubscriptionTestCase : public TestCase
{
public:

  SubscriptionTestCase () : TestCase ("Test Subscription class")
  {
  }

  virtual
  ~SubscriptionTestCase ()
  {
  }

private:

  virtual void
  DoRun (void)
  {
    // @TODO implement
  }
};

class ThymeTestSuite : public TestSuite
{
public:

  ThymeTestSuite () : TestSuite ("thyme", UNIT)
  { // TestDuration for TestCase can be QUICK, EXTENSIVE or TAKES_FOREVER
    AddTestCase (new ObjectIdentifierTestCase, TestCase::QUICK);
    AddTestCase (new ObjectMetadataTestCase, TestCase::QUICK);
    AddTestCase (new ThymeObjectTestCase, TestCase::QUICK);
    AddTestCase (new SubscriptionTestCase, TestCase::QUICK);
  } // TestSuite type can be ALL, BVT, UNIT, SYSTEM, EXAMPLE or PERFORMANCE
};

static ThymeTestSuite thymeTestSuite;

