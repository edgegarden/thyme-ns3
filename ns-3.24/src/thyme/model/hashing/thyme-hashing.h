/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2015 NOVA LINCS DI-FCT-UNL <http://nova-lincs.di.fct.unl.pt/>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Joao A. Silva <jaa.silva@campus.fct.unl.pt>
 */
#ifndef THYME_HASHING_H
#define THYME_HASHING_H

#include <ns3/simple-ref-count.h>
#include <ns3/thyme-grid-manager.h>

namespace ns3 {
namespace hyrax {

/**
 * \ingroup hyrax
 * 
 * \brief Encapsulates the hashing logic. It translates tags (i.e., strings)
 * into cell addresses (i.e., uint16_t).
 */
class Hashing : public SimpleRefCount<Hashing>
{
private:
  Ptr<GridManager> m_gridMan;

public:
  Hashing (Ptr<GridManager> gridMan);
  virtual ~Hashing ();

  uint16_t Hash (std::string key) const;
};

}
}

#endif /* THYME_HASHING_H */

