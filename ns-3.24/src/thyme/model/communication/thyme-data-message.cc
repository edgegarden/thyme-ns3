/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2015 NOVA LINCS DI-FCT-UNL <http://nova-lincs.di.fct.unl.pt/>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Joao A. Silva <jaa.silva@campus.fct.unl.pt>
 */
#include "thyme-data-message.h"
#include <ns3/address-utils.h>
#include <ns3/thyme-utils.h>

namespace ns3 {
namespace hyrax {

NS_OBJECT_ENSURE_REGISTERED (DataMessage);

const bool DataMessage::m_noRepLoop = NO_REP_LOOP_OPT;
const bool DataMessage::m_chooseHand = CHOOSE_HAND_OPT;

TypeId
DataMessage::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::hyrax::DataMessage")
    .SetParent<Header> ()
    .AddConstructor<DataMessage> ();

  return tid;
}

TypeId
DataMessage::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}

DataMessage::DataMessage () : m_msgType ((MessageType) 0),
  m_target ((MessageTarget) 0), m_src (0), m_inRec (false), m_lastHop (-1),
  m_lastGreedy (-1), m_firstPerHop (-1), m_hand (0), m_homeCell (-1),
  m_noRepHomeCell (-1), m_cbcast (true), m_ttl (0)
{
}

DataMessage::~DataMessage ()
{
  m_src = 0;
  m_dsts.clear ();
}

void
DataMessage::Serialize (Buffer::Iterator start) const
{
  const Buffer::Iterator i = start;
  start.WriteU8 ((uint8_t) m_msgType);

  start.WriteU8 ((uint8_t) m_target);

  m_src->Serialize (start);

  start.WriteHtonU16 (m_dsts.size ()); // max 64 KB dsts
  for (auto it = m_dsts.begin (); it != m_dsts.end (); ++it)
   {
    if (m_target == NODE) WriteTo (start, it->first);
    start.WriteHtonU16 (it->second);
   }

  if (m_target != CELL_BCAST)
   {
    start.WriteU8 ((uint8_t) m_inRec);
    if (m_inRec)
     {
      start.WriteHtonU16 (m_lastHop);
      start.WriteHtonU16 (m_lastGreedy);
      start.WriteHtonU16 (m_firstPerHop);

      if (m_chooseHand) start.WriteU8 (m_hand);
     }
   }
  else // m_target == CELL_BCAST
   {
    start.WriteHtonU16 (m_homeCell);
    if (m_noRepLoop) start.WriteHtonU16 (m_noRepHomeCell);
   }

  if (m_target == CELL) start.WriteU8 ((uint8_t) m_cbcast);

  if (m_target != CELL_BCAST) start.WriteU8 (m_ttl);

  switch (m_msgType)
  {
  case PUBLISH:
    m_message.pub.Serialize (start);
    break;
  case PUBLISH_RSP:
    m_message.pubRsp.Serialize (start);
    break;
  case ACTIVE_REP:
    m_message.actRep.Serialize (start);
    break;
  case DOWNLOAD:
    m_message.down.Serialize (start);
    break;
  case DOWNLOAD_RSP:
    m_message.downRsp.Serialize (start);
    break;
  case UNPUBLISH:
    m_message.unpub.Serialize (start);
    break;
  case UNPUBLISH_RSP:
    m_message.unpubRsp.Serialize (start);
    break;
  case UNPUBLISH_REP:
    m_message.unpubRep.Serialize (start);
    break;
  case UPDATE_LOC:
    break;
  case REGISTER_REP:
    m_message.regRep.Serialize (start);
    break;
  case SUBSCRIBE:
    m_message.sub.Serialize (start);
    break;
  case SUBSCRIBE_RSP:
    m_message.subRsp.Serialize (start);
    break;
  case SUBSCRIBE_NOT:
    m_message.subNot.Serialize (start);
    break;
  case UNSUBSCRIBE:
    m_message.unsub.Serialize (start);
    break;
  case UNSUBSCRIBE_RSP:
    m_message.unsubRsp.Serialize (start);
    break;
  case JOIN:
    break;
  case JOIN_RSP:
    m_message.joinRsp.Serialize (start);
    break;
  case NACK_MSG:
    m_message.nackMsg.Serialize (start);
    break;

  default:
    NS_ABORT_MSG ("Unknown type " << m_msgType);
  }

  uint32_t dist = 0;
  uint32_t serSize = 0;
  NS_ASSERT_MSG ((dist = start.GetDistanceFrom (i))
    == (serSize = GetSerializedSize ()), dist << " != " << serSize);
}

uint32_t
DataMessage::Deserialize (Buffer::Iterator start)
{
  const Buffer::Iterator i = start;
  m_msgType = (MessageType) start.ReadU8 ();

  m_target = (MessageTarget) start.ReadU8 ();

  m_src = Create<NodeAddress> ();
  m_src->Deserialize (start);

  const int num = start.ReadNtohU16 (); // max 64 KB dsts
  for (int i = 0; i < num; ++i)
   {
    Ipv4Address addr;
    if (m_target == NODE) ReadFrom (start, addr);
    uint16_t cell = start.ReadNtohU16 ();
    m_dsts.insert (PAIR (m_target == NODE ? addr : Ipv4Address (cell), cell));
   }

  if (m_target != CELL_BCAST)
   {
    m_inRec = (bool) start.ReadU8 ();
    if (m_inRec)
     {
      m_lastHop = start.ReadNtohU16 ();
      m_lastGreedy = start.ReadNtohU16 ();
      m_firstPerHop = start.ReadNtohU16 ();

      if (m_chooseHand) m_hand = start.ReadU8 ();
     }
   }
  else // m_target == CELL_BCAST
   {
    m_homeCell = start.ReadNtohU16 ();
    if (m_noRepLoop) m_noRepHomeCell = start.ReadNtohU16 ();
   }

  if (m_target == CELL) m_cbcast = (bool) start.ReadU8 ();

  if (m_target != CELL_BCAST) m_ttl = start.ReadU8 ();

  switch (m_msgType)
  {
  case PUBLISH:
    m_message.pub.Deserialize (start);
    break;
  case PUBLISH_RSP:
    m_message.pubRsp.Deserialize (start);
    break;
  case ACTIVE_REP:
    m_message.actRep.Deserialize (start);
    break;
  case DOWNLOAD:
    m_message.down.Deserialize (start);
    break;
  case DOWNLOAD_RSP:
    m_message.downRsp.Deserialize (start);
    break;
  case UNPUBLISH:
    m_message.unpub.Deserialize (start);
    break;
  case UNPUBLISH_RSP:
    m_message.unpubRsp.Deserialize (start);
    break;
  case UNPUBLISH_REP:
    m_message.unpubRep.Deserialize (start);
    break;
  case UPDATE_LOC:
    break;
  case REGISTER_REP:
    m_message.regRep.Deserialize (start);
    break;
  case SUBSCRIBE:
    m_message.sub.Deserialize (start);
    break;
  case SUBSCRIBE_RSP:
    m_message.subRsp.Deserialize (start);
    break;
  case SUBSCRIBE_NOT:
    m_message.subNot.Deserialize (start);
    break;
  case UNSUBSCRIBE:
    m_message.unsub.Deserialize (start);
    break;
  case UNSUBSCRIBE_RSP:
    m_message.unsubRsp.Deserialize (start);
    break;
  case JOIN:
    break;
  case JOIN_RSP:
    m_message.joinRsp.Deserialize (start);
    break;
  case NACK_MSG:
    m_message.nackMsg.Deserialize (start);
    break;

  default:
    NS_ABORT_MSG ("Unknown type " << m_msgType);
  }

  const uint32_t dist = start.GetDistanceFrom (i);
  uint32_t serSize = 0;
  NS_ASSERT_MSG (dist == (serSize = GetSerializedSize ()),
    dist << " != " << serSize);
  return dist;
}

uint32_t
DataMessage::GetSerializedSize (void) const
{
  uint32_t size = 2; // msgType and msgTarget

  size += m_src->GetSerializedSize (); // src nodeAddress

  size += 2; // list size
  size += m_dsts.size () * 2; // dsts cells
  size += m_target == NODE ? m_dsts.size () * 4 : 0; // dsts ips

  if (m_target != CELL_BCAST)
   {
    size += 1; // inRec
    if (m_inRec)
     {
      size += 6; // lastHop, lastGreedy, firstPerHop
      if (m_chooseHand) size += 1; // hand
     }
   }
  else // m_target == CELL_BCAST
   {
    size += 2; // homeCell
    if (m_noRepLoop) size += 2; // noRepHomeCell
   }

  if (m_target == CELL) size += 1; // cbcast

  if (m_target != CELL_BCAST) size += 1; // ttl

  switch (m_msgType)
  {
  case PUBLISH:
    size += m_message.pub.GetSerializedSize ();
    break;
  case PUBLISH_RSP:
    size += m_message.pubRsp.GetSerializedSize ();
    break;
  case ACTIVE_REP:
    size += m_message.actRep.GetSerializedSize ();
    break;
  case DOWNLOAD:
    size += m_message.down.GetSerializedSize ();
    break;
  case DOWNLOAD_RSP:
    size += m_message.downRsp.GetSerializedSize ();
    break;
  case UNPUBLISH:
    size += m_message.unpub.GetSerializedSize ();
    break;
  case UNPUBLISH_RSP:
    size += m_message.unpubRsp.GetSerializedSize ();
    break;
  case UNPUBLISH_REP:
    size += m_message.unpubRep.GetSerializedSize ();
    break;
  case UPDATE_LOC:
    break;
  case REGISTER_REP:
    size += m_message.regRep.GetSerializedSize ();
    break;
  case SUBSCRIBE:
    size += m_message.sub.GetSerializedSize ();
    break;
  case SUBSCRIBE_RSP:
    size += m_message.subRsp.GetSerializedSize ();
    break;
  case SUBSCRIBE_NOT:
    size += m_message.subNot.GetSerializedSize ();
    break;
  case UNSUBSCRIBE:
    size += m_message.unsub.GetSerializedSize ();
    break;
  case UNSUBSCRIBE_RSP:
    size += m_message.unsubRsp.GetSerializedSize ();
    break;
  case JOIN:
    break;
  case JOIN_RSP:
    size += m_message.joinRsp.GetSerializedSize ();
    break;
  case NACK_MSG:
    size += m_message.nackMsg.GetSerializedSize ();
    break;

  default:
    NS_ABORT_MSG ("Unknown type " << m_msgType);
  }

  return size;
}

void
DataMessage::Print (std::ostream &os) const
{
  os << "\n***Message Dump***\n";
  os << ":::Header:::\n"
    << "Type: " << Utils::GetMsgTypeString (m_msgType)
    << " (" << m_msgType << ")\n"
    << "Target: " << Utils::GetMsgTargetString (m_target)
    << " (" << m_target << ")\n"
    << "Src: ";
  m_src->Print (os);
  os << "\n";

  os << "Dsts (" << m_dsts.size () << "): ";
  for (auto it = m_dsts.begin (); it != m_dsts.end (); ++it)
   {
    os << "{";
    if (m_target == NODE) os << it->first << "-";
    os << it->second << "} ";
   }
  os << "\n";

  if (m_target != CELL_BCAST)
   {
    os << "InRecovery: " << m_inRec << "\n";
    if (m_inRec)
     {
      os << "LastHop: " << m_lastHop
        << " LastGreedy: " << m_lastGreedy
        << " FirstPerHop: " << m_firstPerHop;
      if (m_chooseHand) os << " Hand: " << (uint16_t) m_hand;
      os << "\n";
     }
   }
  else // m_target == CELL_BCAST
   {
    os << "HomeCell: " << m_homeCell;
    if (m_noRepLoop) os << " NoRepHomeCell: " << m_noRepHomeCell;
    os << "\n";
   }

  if (m_target == CELL) os << "Cbcast: " << m_cbcast << "\n";

  if (m_target != CELL_BCAST) os << "TTL: " << (uint16_t) m_ttl << "\n";

  os << ":::Payload:::\n";
  switch (m_msgType)
  {
  case PUBLISH:
    m_message.pub.Print (os);
    break;
  case PUBLISH_RSP:
    m_message.pubRsp.Print (os);
    break;
  case ACTIVE_REP:
    m_message.actRep.Print (os);
    break;
  case DOWNLOAD:
    m_message.down.Print (os);
    break;
  case DOWNLOAD_RSP:
    m_message.downRsp.Print (os);
    break;
  case UNPUBLISH:
    m_message.unpub.Print (os);
    break;
  case UNPUBLISH_RSP:
    m_message.unpubRsp.Print (os);
    break;
  case UNPUBLISH_REP:
    m_message.unpubRep.Print (os);
    break;
  case UPDATE_LOC:
    os << "::UpLoc::";
    break;
  case REGISTER_REP:
    m_message.regRep.Print (os);
    break;
  case SUBSCRIBE:
    m_message.sub.Print (os);
    break;
  case SUBSCRIBE_RSP:
    m_message.subRsp.Print (os);
    break;
  case SUBSCRIBE_NOT:
    m_message.subNot.Print (os);
    break;
  case UNSUBSCRIBE:
    m_message.unsub.Print (os);
    break;
  case UNSUBSCRIBE_RSP:
    m_message.unsubRsp.Print (os);
    break;
  case JOIN:
    os << "::Join::";
    break;
  case JOIN_RSP:
    m_message.joinRsp.Print (os);
    break;
  case NACK_MSG:
    m_message.nackMsg.Print (os);
    break;

  default:
    NS_ABORT_MSG ("Unknown type " << m_msgType);
  }
}

MessageType
DataMessage::GetMsgType () const
{
  return m_msgType;
}

MessageTarget
DataMessage::GetMsgTarget () const
{
  return m_target;
}

void
DataMessage::SetMsgTarget (MessageTarget target)
{
  m_target = target;
}

Ptr<NodeAddress>
DataMessage::GetSource () const
{
  return m_src;
}

void
DataMessage::SetSource (Ptr<NodeAddress> src)
{
  m_src = src;
}

Locations
DataMessage::GetDestinations () const
{
  return m_dsts;
}

void
DataMessage::AddDestination (uint16_t cell)
{
  m_dsts.insert (PAIR (Ipv4Address (cell), cell));
}

void
DataMessage::AddDestination (Ipv4Address addr, uint16_t cell)
{
  m_dsts.insert (PAIR (addr, cell));
}

void
DataMessage::SetDestinations (Locations dsts)
{
  m_dsts = dsts;
}

bool
DataMessage::IsDestination (Ipv4Address addr)
{
  return m_dsts.find (addr) != m_dsts.end ();
}

bool
DataMessage::IsDestination (uint16_t cell)
{
  return IsDestination (Ipv4Address (cell));
}

bool
DataMessage::RemoveDestination (Ipv4Address addr)
{
  m_dsts.erase (addr);
  return !m_dsts.empty ();
}

bool
DataMessage::RemoveDestination (uint16_t cell)
{
  return RemoveDestination (Ipv4Address (cell));
}

bool
DataMessage::RemoveDestinations (Locations dsts)
{
  for (auto it = dsts.begin (); it != dsts.end (); ++it)
   m_dsts.erase (it->first);

  return !m_dsts.empty ();
}

uint32_t
DataMessage::NumDestinations ()
{
  return m_dsts.size ();
}

void
DataMessage::ClearDestinations ()
{
  m_dsts.clear ();
}

bool
DataMessage::IsInRecovery () const
{
  return m_inRec;
}

void
DataMessage::SetGreedyMode ()
{
  m_inRec = false;
  m_lastHop = -1;
  m_firstPerHop = -1;
  m_lastGreedy = -1;
  m_hand = 0;
}

void
DataMessage::SetPerimeterMode (uint16_t cell)
{
  m_inRec = true;
  m_lastGreedy = cell;
}

uint16_t
DataMessage::GetLastHop () const
{
  return m_lastHop;
}

void
DataMessage::SetLastHop (uint16_t cell)
{
  m_lastHop = cell;
}

uint16_t
DataMessage::GetLastGreedy () const
{
  return m_lastGreedy;
}

uint16_t
DataMessage::GetFirstPerHop () const
{
  return m_firstPerHop;
}

void
DataMessage::SetFirstPerHop (uint16_t cell)
{
  m_firstPerHop = cell;
}

uint8_t
DataMessage::GetHand () const
{
  return m_hand;
}

void
DataMessage::SetHand (uint8_t hand)
{
  m_hand = hand;
}

uint16_t
DataMessage::GetHomeCell () const
{
  return m_homeCell;
}

void
DataMessage::SetHomeCell (uint16_t cell)
{
  m_homeCell = cell;
}

uint16_t
DataMessage::GetNoRepHomeCell () const
{
  return m_noRepHomeCell;
}

void
DataMessage::SetNoRepHomeCell (uint16_t cell)
{
  m_noRepHomeCell = cell;
}

bool
DataMessage::IsCellBcast () const
{
  return m_cbcast;
}

void
DataMessage::SetCellBcast (bool cbcast)
{
  m_cbcast = cbcast;
}

uint8_t
DataMessage::GetTtl () const
{
  return m_ttl;
}

bool
DataMessage::IsMaxTtl () const
{
  return m_ttl <= 0;
}

void
DataMessage::SetTtl (uint8_t ttl)
{
  m_ttl = ttl;
}

void
DataMessage::DecrementTtl ()
{
  m_ttl--;
}

// Publish --------------------------------------------------------------------

void
DataMessage::Publish::Serialize (Buffer::Iterator &start) const
{
  start.WriteHtonU32 (reqId);

  objMeta->Serialize (start);

  start.WriteU8 (tagIdx);

  start.WriteHtonU16 (cell);
}

uint32_t
DataMessage::Publish::Deserialize (Buffer::Iterator &start)
{
  reqId = start.ReadNtohU32 ();

  objMeta = Create<ObjectMetadata> ();
  objMeta->Deserialize (start);

  tagIdx = start.ReadU8 ();
  tag = objMeta->GetTag (tagIdx);

  cell = start.ReadNtohU16 ();

  return GetSerializedSize ();
}

uint32_t
DataMessage::Publish::GetSerializedSize () const
{
  uint32_t size = 4; // reqId

  size += objMeta->GetSerializedSize (); // objMeta

  size += 3; // tag index and cell

  return size;
}

void
DataMessage::Publish::Print (std::ostream &os) const
{
  os << "::Pub:: ReqId: " << reqId << " ";
  objMeta->Print (os);
  os << " Tag: " << tag << " Cell: " << cell;
}

DataMessage::Publish
DataMessage::GetPublish () const
{
  return m_message.pub;
}

void
DataMessage::SetPublish (uint32_t rId, Ptr<ObjectMetadata> m, std::string t,
  uint16_t c)
{
  if (m_msgType == 0) m_msgType = PUBLISH;
  else NS_ABORT_MSG_IF (m_msgType != PUBLISH, m_msgType);

  m_message.pub.reqId = rId;
  m_message.pub.objMeta = m;
  m_message.pub.tag = t;
  m_message.pub.tagIdx = m->GetTagIndex (t);
  m_message.pub.cell = c;
}

std::ostream&
operator<< (std::ostream &os, DataMessage::Publish const &x)
{
  x.Print (os);
  return os;
}

// PublishRsp -----------------------------------------------------------------

void
DataMessage::PublishRsp::Serialize (Buffer::Iterator &start) const
{
  start.WriteHtonU32 (reqId);

  start.WriteU8 (tagIdx);
}

uint32_t
DataMessage::PublishRsp::Deserialize (Buffer::Iterator &start)
{
  reqId = start.ReadNtohU32 ();

  tagIdx = start.ReadU8 ();

  return GetSerializedSize ();
}

uint32_t
DataMessage::PublishRsp::GetSerializedSize () const
{
  return 5; // reqId and tag index
}

void
DataMessage::PublishRsp::Print (std::ostream &os) const
{
  os << "::PubRsp:: ReqId: " << reqId << " TagIdx: " << (uint16_t) tagIdx;
}

DataMessage::PublishRsp
DataMessage::GetPublishRsp () const
{
  return m_message.pubRsp;
}

void
DataMessage::SetPublishRsp (DataMessage::Publish pub)
{
  if (m_msgType == 0) m_msgType = PUBLISH_RSP;
  else NS_ABORT_MSG_IF (m_msgType != PUBLISH_RSP, m_msgType);

  m_message.pubRsp.reqId = pub.reqId;
  m_message.pubRsp.tagIdx = pub.tagIdx;
}

std::ostream&
operator<< (std::ostream &os, DataMessage::PublishRsp const &x)
{
  x.Print (os);
  return os;
}

// ActiveRep ------------------------------------------------------------------

void
DataMessage::ActiveRep::Serialize (Buffer::Iterator &start) const
{
  obj->Serialize (start);
}

uint32_t
DataMessage::ActiveRep::Deserialize (Buffer::Iterator &start)
{
  obj = Create<ThymeObject> ();
  obj->Deserialize (start);

  return GetSerializedSize ();
}

uint32_t
DataMessage::ActiveRep::GetSerializedSize () const
{
  return obj->GetSerializedSize (); // obj
}

void
DataMessage::ActiveRep::Print (std::ostream &os) const
{
  os << "::ActRep:: ";
  obj->Print (os);
}

DataMessage::ActiveRep
DataMessage::GetActiveRep () const
{
  return m_message.actRep;
}

void
DataMessage::SetActiveRep (Ptr<ThymeObject> o)
{
  if (m_msgType == 0) m_msgType = ACTIVE_REP;
  else NS_ABORT_MSG_IF (m_msgType != ACTIVE_REP, m_msgType);

  m_message.actRep.obj = o;
}

std::ostream&
operator<< (std::ostream &os, DataMessage::ActiveRep const &x)
{
  x.Print (os);
  return os;
}

// Download -------------------------------------------------------------------

void
DataMessage::Download::Serialize (Buffer::Iterator &start) const
{
  start.WriteHtonU32 (reqId);

  objId->Serialize (start);

  WriteTo (start, owner);
}

uint32_t
DataMessage::Download::Deserialize (Buffer::Iterator &start)
{
  reqId = start.ReadNtohU32 ();

  objId = Create<ObjectIdentifier> ();
  objId->Deserialize (start);

  ReadFrom (start, owner);

  return GetSerializedSize ();
}

uint32_t
DataMessage::Download::GetSerializedSize () const
{
  uint32_t size = 4; // reqId

  size += objId->GetSerializedSize (); // objId

  size += 4; // owner ip

  return size;
}

void
DataMessage::Download::Print (std::ostream &os) const
{
  os << "::Down:: ReqId: " << reqId << " ";
  objId->Print (os);
  os << " Owner: " << owner;
}

DataMessage::Download
DataMessage::GetDownload () const
{
  return m_message.down;
}

void
DataMessage::SetDownload (uint32_t rId, Ptr<ObjectIdentifier> oId,
  Ipv4Address o)
{
  if (m_msgType == 0) m_msgType = DOWNLOAD;
  else NS_ABORT_MSG_IF (m_msgType != DOWNLOAD, m_msgType);

  m_message.down.reqId = rId;
  m_message.down.objId = oId;
  m_message.down.owner = o;
}

std::ostream&
operator<< (std::ostream &os, DataMessage::Download const &x)
{
  x.Print (os);
  return os;
}

// DownloadRsp ----------------------------------------------------------------

void
DataMessage::DownloadRsp::Serialize (Buffer::Iterator &start) const
{
  start.WriteHtonU32 (reqId);

  start.WriteU8 ((uint8_t) st);
  if (st == OBJECT_FOUND) obj->Serialize2 (start);
}

uint32_t
DataMessage::DownloadRsp::Deserialize (Buffer::Iterator &start)
{
  reqId = start.ReadNtohU32 ();

  st = (MessageStatus) start.ReadU8 ();
  if (st == OBJECT_FOUND)
   {
    obj = Create<ThymeObject> ();
    obj->Deserialize2 (start);
   }

  return GetSerializedSize ();
}

uint32_t
DataMessage::DownloadRsp::GetSerializedSize () const
{
  uint32_t size = 5; // reqId and msgStatus

  size += st == OBJECT_FOUND ? obj->GetSerializedSize2 () : 0; // obj

  return size;
}

void
DataMessage::DownloadRsp::Print (std::ostream &os) const
{
  os << "::DownRsp:: ReqId: " << reqId << " St: " << (uint16_t) st;
  if (st == OBJECT_FOUND)
   {
    os << " ";
    obj->Print (os);
   }
}

DataMessage::DownloadRsp
DataMessage::GetDownloadRsp () const
{
  return m_message.downRsp;
}

void
DataMessage::SetDownloadRsp (uint32_t rId, Ptr<ThymeObject> o,
  MessageStatus st)
{
  if (m_msgType == 0) m_msgType = DOWNLOAD_RSP;
  else NS_ABORT_MSG_IF (m_msgType != DOWNLOAD_RSP, m_msgType);

  m_message.downRsp.reqId = rId;
  m_message.downRsp.obj = o;
  m_message.downRsp.st = st;
}

std::ostream&
operator<< (std::ostream &os, DataMessage::DownloadRsp const &x)
{
  x.Print (os);
  return os;
}

// Unpublish ------------------------------------------------------------------

void
DataMessage::Unpublish::Serialize (Buffer::Iterator &start) const
{
  start.WriteHtonU32 (reqId);

  objId->Serialize (start);

  start.WriteU8 (tagIdx);
}

uint32_t
DataMessage::Unpublish::Deserialize (Buffer::Iterator &start)
{
  reqId = start.ReadNtohU32 ();

  objId = Create<ObjectIdentifier> ();
  objId->Deserialize (start);

  tagIdx = start.ReadU8 ();

  return GetSerializedSize ();
}

uint32_t
DataMessage::Unpublish::GetSerializedSize () const
{
  uint32_t size = 4; // reqId

  size += objId->GetSerializedSize (); // objId

  size += 1; // tag index

  return size;
}

void
DataMessage::Unpublish::Print (std::ostream &os) const
{
  os << "::Unpub:: ReqId: " << reqId << " ";
  objId->Print (os);
  os << " TagIdx: " << (uint16_t) tagIdx;
}

DataMessage::Unpublish
DataMessage::GetUnpublish () const
{
  return m_message.unpub;
}

void
DataMessage::SetUnpublish (uint32_t rId, Ptr<ObjectIdentifier> oId,
  uint8_t tIdx)
{
  if (m_msgType == 0) m_msgType = UNPUBLISH;
  else NS_ABORT_MSG_IF (m_msgType != UNPUBLISH, m_msgType);

  m_message.unpub.reqId = rId;
  m_message.unpub.objId = oId;
  m_message.unpub.tagIdx = tIdx;
}

std::ostream&
operator<< (std::ostream &os, DataMessage::Unpublish const &x)
{
  x.Print (os);
  return os;
}

// UnpublishRsp ---------------------------------------------------------------

void
DataMessage::UnpublishRsp::Serialize (Buffer::Iterator &start) const
{
  start.WriteHtonU32 (reqId);

  start.WriteU8 (tagIdx);
}

uint32_t
DataMessage::UnpublishRsp::Deserialize (Buffer::Iterator &start)
{
  reqId = start.ReadNtohU32 ();

  tagIdx = start.ReadU8 ();

  return GetSerializedSize ();
}

uint32_t
DataMessage::UnpublishRsp::GetSerializedSize () const
{
  return 5; // reqId and tag index
}

void
DataMessage::UnpublishRsp::Print (std::ostream &os) const
{
  os << "::UnpubRsp:: ReqId: " << reqId << " TagIdx: " << (uint16_t) tagIdx;
}

DataMessage::UnpublishRsp
DataMessage::GetUnpublishRsp () const
{
  return m_message.unpubRsp;
}

void
DataMessage::SetUnpublishRsp (DataMessage::Unpublish unpub)
{
  if (m_msgType == 0) m_msgType = UNPUBLISH_RSP;
  else NS_ABORT_MSG_IF (m_msgType != UNPUBLISH_RSP, m_msgType);

  m_message.unpubRsp.reqId = unpub.reqId;
  m_message.unpubRsp.tagIdx = unpub.tagIdx;
}

std::ostream&
operator<< (std::ostream &os, DataMessage::UnpublishRsp const &x)
{
  x.Print (os);
  return os;
}

// UnpublishRep ---------------------------------------------------------------

void
DataMessage::UnpublishRep::Serialize (Buffer::Iterator &start) const
{
  objId->Serialize (start);
}

uint32_t
DataMessage::UnpublishRep::Deserialize (Buffer::Iterator &start)
{
  objId = Create<ObjectIdentifier> ();
  objId->Deserialize (start);

  return GetSerializedSize ();
}

uint32_t
DataMessage::UnpublishRep::GetSerializedSize () const
{
  return objId->GetSerializedSize (); // objId
}

void
DataMessage::UnpublishRep::Print (std::ostream &os) const
{
  os << "::UnpubRep:: ";
  objId->Print (os);
}

DataMessage::UnpublishRep
DataMessage::GetUnpublishRep () const
{
  return m_message.unpubRep;
}

void
DataMessage::SetUnpublishRep (Ptr<ObjectIdentifier> oId)
{
  if (m_msgType == 0) m_msgType = UNPUBLISH_REP;
  else NS_ABORT_MSG_IF (m_msgType != UNPUBLISH_REP, m_msgType);

  m_message.unpubRep.objId = oId;
}

std::ostream&
operator<< (std::ostream &os, DataMessage::UnpublishRep const &x)
{
  x.Print (os);
  return os;
}

// UpdateLoc ------------------------------------------------------------------

void
DataMessage::SetUpdateLoc ()
{
  if (m_msgType == 0) m_msgType = UPDATE_LOC;
  else NS_ABORT_MSG_IF (m_msgType != UPDATE_LOC, m_msgType);
}

// RegisterRep ------------------------------------------------------------------

void
DataMessage::RegisterRep::Serialize (Buffer::Iterator &start) const
{
  objId->Serialize (start);

  WriteTo (start, owner);
}

uint32_t
DataMessage::RegisterRep::Deserialize (Buffer::Iterator &start)
{
  objId = Create<ObjectIdentifier> ();
  objId->Deserialize (start);

  ReadFrom (start, owner);

  return GetSerializedSize ();
}

uint32_t
DataMessage::RegisterRep::GetSerializedSize () const
{
  return objId->GetSerializedSize () + 4; // objId and owner ip
}

void
DataMessage::RegisterRep::Print (std::ostream &os) const
{
  os << "::RegRep:: ";
  objId->Print (os);
  os << " Owner: " << owner;
}

DataMessage::RegisterRep
DataMessage::GetRegisterRep () const
{
  return m_message.regRep;
}

void
DataMessage::SetRegisterRep (Ptr<ObjectIdentifier> oId, Ipv4Address o)
{
  if (m_msgType == 0) m_msgType = REGISTER_REP;
  else NS_ABORT_MSG_IF (m_msgType != REGISTER_REP, m_msgType);

  m_message.regRep.objId = oId;
  m_message.regRep.owner = o;
}

std::ostream&
operator<< (std::ostream &os, DataMessage::RegisterRep const &x)
{
  x.Print (os);
  return os;
}

// Subscribe ------------------------------------------------------------------

void
DataMessage::Subscribe::Serialize (Buffer::Iterator &start) const
{
  start.WriteHtonU32 (reqId);

  subObj->Serialize (start);

  start.WriteU8 (sendTagIdx);
}

uint32_t
DataMessage::Subscribe::Deserialize (Buffer::Iterator &start)
{
  reqId = start.ReadNtohU32 ();

  subObj = Create<SubscriptionObject> ();
  subObj->Deserialize (start);

  sendTagIdx = start.ReadU8 ();

  return GetSerializedSize ();
}

uint32_t
DataMessage::Subscribe::GetSerializedSize () const
{
  uint32_t size = subObj->GetSerializedSize ();

  size += 5; // reqId and sendTag index

  return size;
}

void
DataMessage::Subscribe::Print (std::ostream &os) const
{
  os << "::Sub:: ReqId: " << reqId << " ";
  subObj->Print (os);
  os << " SendTagIdx: " << (uint16_t) sendTagIdx;
}

DataMessage::Subscribe
DataMessage::GetSubscribe () const
{
  return m_message.sub;
}

void
DataMessage::SetSubscribe (uint32_t rId, Ptr<SubscriptionObject> sObj,
  uint8_t tIdx)
{
  if (m_msgType == 0) m_msgType = SUBSCRIBE;
  else NS_ABORT_MSG_IF (m_msgType != SUBSCRIBE, m_msgType);

  m_message.sub.reqId = rId;
  m_message.sub.subObj = sObj;
  m_message.sub.sendTagIdx = tIdx;
}

std::ostream&
operator<< (std::ostream &os, DataMessage::Subscribe const &x)
{
  x.Print (os);
  return os;
}

// SubscribeRsp ---------------------------------------------------------------

void
DataMessage::SubscribeRsp::Serialize (Buffer::Iterator &start) const
{
  start.WriteHtonU32 (reqId);

  start.WriteU8 (sendTagIdx);
}

uint32_t
DataMessage::SubscribeRsp::Deserialize (Buffer::Iterator &start)
{
  reqId = start.ReadNtohU32 ();

  sendTagIdx = start.ReadU8 ();

  return GetSerializedSize ();
}

uint32_t
DataMessage::SubscribeRsp::GetSerializedSize () const
{
  return 5; // reqId and sendTag index
}

void
DataMessage::SubscribeRsp::Print (std::ostream &os) const
{
  os << "::SubRsp:: ReqId: " << reqId
    << " SendTagIdx: " << (uint16_t) sendTagIdx;
}

DataMessage::SubscribeRsp
DataMessage::GetSubscribeRsp () const
{
  return m_message.subRsp;
}

void
DataMessage::SetSubscribeRsp (DataMessage::Subscribe sub)
{
  if (m_msgType == 0) m_msgType = SUBSCRIBE_RSP;
  else NS_ABORT_MSG_IF (m_msgType != SUBSCRIBE_RSP, m_msgType);

  m_message.subRsp.reqId = sub.reqId;
  m_message.subRsp.sendTagIdx = sub.sendTagIdx;
}

std::ostream&
operator<< (std::ostream &os, DataMessage::SubscribeRsp const &x)
{
  x.Print (os);
  return os;
}

// SubscribeNot ---------------------------------------------------------------

void
DataMessage::SubscribeNot::Serialize (Buffer::Iterator &start) const
{
  // subscription ids
  start.WriteHtonU16 (subIds.size ());
  for (auto it = subIds.begin (); it != subIds.end (); ++it)
   {
    WriteTo (start, it->first); // addr

    start.WriteHtonU16 (it->second.size ()); // max 64 KB subIds
    for (int i = 0; i < it->second.size (); ++i)
     start.WriteHtonU32 (it->second[i]); // subIds
   }

  // metadata
  start.WriteHtonU16 (objMeta.size ()); // max 64 KB objs
  for (int i = 0; i < objMeta.size (); ++i)
   objMeta[i]->Serialize (start); // metas

  // locations
  start.WriteHtonU16 (locs.size ()); // max 64 KB locs
  for (int i = 0; i < locs.size (); ++i)
   {
    start.WriteHtonU16 (locs[i].size ()); // max 64 KB locs
    for (auto it = locs[i].begin (); it != locs[i].end (); ++it)
     {
      WriteTo (start, it->first); // addr
      start.WriteHtonU16 (it->second); // cell
     }
   }
}

uint32_t
DataMessage::SubscribeNot::Deserialize (Buffer::Iterator &start)
{
  // subscription ids
  uint16_t size = start.ReadNtohU16 ();
  for (int i = 0; i < size; ++i)
   {
    Ipv4Address addr;
    ReadFrom (start, addr);

    uint16_t size2 = start.ReadNtohU16 (); // max 64 KB subIds
    std::vector<uint32_t> ids;
    for (int j = 0; j < size2; ++j)
     ids.push_back (start.ReadNtohU32 ());

    subIds.insert (PAIR (addr, ids));
   }

  // metadata
  size = start.ReadNtohU16 (); // max 64 KB objs
  for (int i = 0; i < size; ++i)
   {
    Ptr<ObjectMetadata> meta = Create<ObjectMetadata> ();
    meta->Deserialize (start);

    objMeta.push_back (meta);
   }

  // locations
  size = start.ReadNtohU16 (); // max 64 KB locs
  for (int i = 0; i < size; ++i)
   {
    uint16_t size2 = start.ReadNtohU16 (); // max 64 KB locs
    Locations aux;
    for (int j = 0; j < size2; ++j)
     {
      Ipv4Address addr;
      ReadFrom (start, addr);

      aux.insert (PAIR (addr, start.ReadNtohU16 ()));
     }

    locs.push_back (aux);
   }

  return GetSerializedSize ();
}

uint32_t
DataMessage::SubscribeNot::GetSerializedSize () const
{
  // subscription ids
  uint32_t size = 2; // max 64 KB subIds
  for (auto it = subIds.begin (); it != subIds.end (); ++it) // max 64 KB subIds
   size += 6 + it->second.size () * 4; // addr, list size and ids

  // metadata
  size += 2; // max 64 KB objs
  for (int i = 0; i < objMeta.size (); ++i)
   size += objMeta[i]->GetSerializedSize ();

  // locations
  size += 2; // max 64 KB locs
  for (int i = 0; i < locs.size (); ++i)
   {
    size += 2; // max 64 KB locs
    size += locs[i].size () * 6; // addr and cell
   }

  return size;
}

void
DataMessage::SubscribeNot::Print (std::ostream &os) const
{
  os << "::SubNot:: #SubIds: " << subIds.size ()
    << " #Metas: " << objMeta.size ()
    << " #Locs: " << locs.size ();
}

std::vector<uint32_t>
DataMessage::SubscribeNot::GetMySubscriptionIds (Ipv4Address addr) const
{
  std::vector<uint32_t> res;

  auto found = subIds.find (addr);
  if (found != subIds.end ()) res = found->second;

  return res;
}

DataMessage::SubscribeNot
DataMessage::GetSubscribeNot () const
{
  return m_message.subNot;
}

void
DataMessage::SetSubscribeNot (std::map<Ipv4Address, std::vector<uint32_t> > subIds,
  Ptr<ObjectMetadata> objMeta, Locations locs)
{
  if (m_msgType == 0) m_msgType = SUBSCRIBE_NOT;
  else NS_ABORT_MSG_IF (m_msgType != SUBSCRIBE_NOT, m_msgType);

  m_message.subNot.subIds = subIds;

  std::vector<Ptr<ObjectMetadata> > metas;
  metas.push_back (objMeta);
  m_message.subNot.objMeta = metas;

  std::vector<Locations> locations;
  locations.push_back (locs);
  m_message.subNot.locs = locations;
}

void
DataMessage::SetSubscribeNot (Ipv4Address addr, uint32_t subId,
  std::vector<Ptr<ObjectMetadata> > metas, std::vector<Locations> locs)
{
  if (m_msgType == 0) m_msgType = SUBSCRIBE_NOT;
  else NS_ABORT_MSG_IF (m_msgType != SUBSCRIBE_NOT, m_msgType);

  std::vector<uint32_t> aux;
  aux.push_back (subId);
  m_message.subNot.subIds.insert (PAIR (addr, aux));

  m_message.subNot.objMeta = metas;
  m_message.subNot.locs = locs;
}

std::ostream&
operator<< (std::ostream &os, DataMessage::SubscribeNot const &x)
{
  x.Print (os);
  return os;
}

// Unsubscribe ----------------------------------------------------------------

void
DataMessage::Unsubscribe::Serialize (Buffer::Iterator &start) const
{
  start.WriteHtonU32 (reqId);

  start.WriteHtonU32 (subId);

  start.WriteU8 (sendTagIdx);
}

uint32_t
DataMessage::Unsubscribe::Deserialize (Buffer::Iterator &start)
{
  reqId = start.ReadNtohU32 ();

  subId = start.ReadNtohU32 ();

  sendTagIdx = start.ReadU8 ();

  return GetSerializedSize ();
}

uint32_t
DataMessage::Unsubscribe::GetSerializedSize (void) const
{
  return 9; // reqId, subId and sendTag index
}

void
DataMessage::Unsubscribe::Print (std::ostream &os) const
{
  os << "::Unsub:: ReqId: " << reqId << " SubId: " << subId
    << " SendTagIdx: " << (uint16_t) sendTagIdx;
}

DataMessage::Unsubscribe
DataMessage::GetUnsubscribe () const
{
  return m_message.unsub;
}

void
DataMessage::SetUnsubscribe (uint32_t rId, uint32_t sId, uint8_t tIdx)
{
  if (m_msgType == 0) m_msgType = UNSUBSCRIBE;
  else NS_ABORT_MSG_IF (m_msgType != UNSUBSCRIBE, m_msgType);

  m_message.unsub.reqId = rId;
  m_message.unsub.subId = sId;
  m_message.unsub.sendTagIdx = tIdx;
}

std::ostream&
operator<< (std::ostream &os, DataMessage::Unsubscribe const &x)
{
  x.Print (os);
  return os;
}

// UnsubscribeRsp -------------------------------------------------------------

void
DataMessage::UnsubscribeRsp::Serialize (Buffer::Iterator &start) const
{
  start.WriteHtonU32 (reqId);

  start.WriteU8 (sendTagIdx);
}

uint32_t
DataMessage::UnsubscribeRsp::Deserialize (Buffer::Iterator &start)
{
  reqId = start.ReadNtohU32 ();

  sendTagIdx = start.ReadU8 ();

  return GetSerializedSize ();
}

uint32_t
DataMessage::UnsubscribeRsp::GetSerializedSize () const
{
  return 5; // reqId and sendTag index
}

void
DataMessage::UnsubscribeRsp::Print (std::ostream &os) const
{
  os << "::UnsubRsp:: ReqId: " << reqId
    << " SendTagIdx: " << (uint16_t) sendTagIdx;
}

DataMessage::UnsubscribeRsp
DataMessage::GetUnsubscribeRsp () const
{
  return m_message.unsubRsp;
}

void
DataMessage::SetUnsubscribeRsp (DataMessage::Unsubscribe unsub)
{
  if (m_msgType == 0) m_msgType = UNSUBSCRIBE_RSP;
  else NS_ABORT_MSG_IF (m_msgType != UNSUBSCRIBE_RSP, m_msgType);

  m_message.unsubRsp.reqId = unsub.reqId;
  m_message.unsubRsp.sendTagIdx = unsub.sendTagIdx;
}

std::ostream&
operator<< (std::ostream &os, DataMessage::UnsubscribeRsp const &x)
{
  x.Print (os);
  return os;
}

// Join -----------------------------------------------------------------------

void
DataMessage::SetJoin ()
{
  if (m_msgType == 0) m_msgType = JOIN;
  else NS_ABORT_MSG_IF (m_msgType != JOIN, m_msgType);
}

// JoinRsp --------------------------------------------------------------------

void
DataMessage::JoinRsp::Serialize (Buffer::Iterator &start) const
{
  // serialize locsByObjKey & objKeyCount
  start.WriteHtonU16 (locsByObjKey.size ()); // list size
  for (auto it : locsByObjKey)
   {
    // serialize ObjectKey
    it.first.first.Serialize (start); // objId
    WriteTo (start, it.first.second); // address

    // serialize Locations
    start.WriteHtonU16 (it.second.size ()); // list size
    for (auto loc : it.second)
     {
      WriteTo (start, loc.first); // address
      start.WriteHtonU16 (loc.second); // cell
     }

    // serialize objKeyCount
    auto found = objKeyCount.find (it.first);
    if (found == objKeyCount.end ()) start.WriteU8 (1);
    else start.WriteU8 (found->second);
   }
  // @TODO !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  if (locsByObjKey.size () != objKeyCount.size ())
   std::cout << locsByObjKey.size () << " " << objKeyCount.size () << " NO!\n";

  // serialize actReps
  start.WriteHtonU16 (actReps.size ()); // list size
  for (auto it : actReps)
   it.second->Serialize (start); // obj

  // serialize metasByTag
  start.WriteHtonU16 (metasByTag.size ()); // list size
  for (auto it : metasByTag)
   {
    Utils::SerializeString (it.first, start); // tag

    // serialize ObjectsMetadata
    start.WriteHtonU16 (it.second.size ()); // list size
    for (auto it2 : it.second)
     it2.second->Serialize (start); // metadata
   }

  // serialize subs
  start.WriteHtonU16 (subs.size ()); // list size
  for (auto it : subs)
   {
    // serialize OwnSubscriptions
    start.WriteHtonU16 (it.second.size ()); // list size
    for (auto it2 : it.second)
     it2.second->Serialize (start); // subObj
   }
}

uint32_t
DataMessage::JoinRsp::Deserialize (Buffer::Iterator &start)
{
  // deserialize locsByObjKey & objKeyCount
  uint16_t size = start.ReadNtohU16 (); // list size
  for (int i = 0; i < size; ++i)
   {
    // deserialize ObjectKey
    Ptr<ObjectIdentifier> objId = Create<ObjectIdentifier> ();
    objId->Deserialize (start); // objId
    Ipv4Address addr;
    ReadFrom (start, addr); // address
    ObjectKey objKey = OBJ_KEY (objId, addr);

    // deserialize Locations
    uint16_t locsSize = start.ReadNtohU16 (); // list size
    Locations locs;
    for (int j = 0; j < locsSize; ++j)
     {
      Ipv4Address ip;
      ReadFrom (start, ip); // address
      uint16_t cell = start.ReadNtohU16 (); // cell

      locs.insert (PAIR (ip, cell));
     }
    locsByObjKey.insert (PAIR (objKey, locs));

    // deserialize objKeyCount
    uint8_t count = start.ReadU8 ();
    objKeyCount.insert (PAIR (objKey, count));
   }

  // deserialize activeReps
  size = start.ReadNtohU16 ();
  for (int i = 0; i < size; ++i)
   {
    Ptr<ThymeObject> obj = Create<ThymeObject> ();
    obj->Deserialize (start); // obj

    ObjectKey objKey = OBJ_KEY (obj->GetObjectIdentifier (), obj->GetOwner ());
    actReps.insert (PAIR (objKey, obj));
   }

  // deserialize metasByTag
  size = start.ReadNtohU16 ();
  for (int i = 0; i < size; ++i)
   {
    std::string tag = Utils::DeserializeString (start); // tag

    // deserialize ObjectsMetadata
    uint16_t objsMetaSize = start.ReadNtohU16 ();
    ObjectsMetadata objsMeta;
    for (int j = 0; j < objsMetaSize; ++j)
     {
      Ptr<ObjectMetadata> objMeta = Create<ObjectMetadata> ();
      objMeta->Deserialize (start); // metadata

      ObjectKey objKey = OBJ_KEY (objMeta->GetObjectIdentifier (),
        objMeta->GetOwner ());
      objsMeta.insert (PAIR (objKey, objMeta));
     }

    metasByTag.insert (PAIR (tag, objsMeta));
   }

  // deserialize subscriptions
  size = start.ReadNtohU16 ();
  for (int i = 0; i < size; ++i)
   {
    // deserialize OwnSubscriptions
    uint16_t ownSubsSize = start.ReadNtohU16 ();
    MySubscriptions ownSubs;
    for (int j = 0; j < ownSubsSize; ++j)
     {
      Ptr<SubscriptionObject> subObj = Create<SubscriptionObject> ();
      subObj->Deserialize (start); // subObj

      ownSubs.insert (PAIR (subObj->GetSubscriptionId (), subObj));
     }

    Ipv4Address addr = ownSubs.begin ()->second->GetSubscriber ();
    subs.insert (PAIR (addr, ownSubs));
   }

  return GetSerializedSize ();
}

uint32_t
DataMessage::JoinRsp::GetSerializedSize (void) const
{
  // locsByObjKey & objKeyCount
  uint32_t size = 2; // list size
  for (auto it : locsByObjKey)
   {
    // ObjectKey
    size += it.first.first.GetSerializedSize () + 4; // objId and address

    // Locations
    size += 2; // list size
    size += it.second.size () * 6; // address and cell

    // objKeyCount
    size += 1; // count
   }

  // actReps
  size += 2; // list size
  for (auto it : actReps)
   size += it.second->GetSerializedSize (); // obj

  // metasByTag
  size += 2; // list size
  for (auto it : metasByTag)
   {
    size += Utils::GetSerializedStringSize (it.first); // tag

    // ObjectsMetadata
    size += 2; // list size
    for (auto it2 : it.second)
     size += it2.second->GetSerializedSize (); // metadata
   }

  // subs
  size += 2; // list size
  for (auto it : subs)
   {
    // OwnSubscriptions
    size += 2; // list size
    for (auto it2 : it.second)
     size += it2.second->GetSerializedSize (); // subObj
   }

  return size;
}

void
DataMessage::JoinRsp::Print (std::ostream &os) const
{
  os << "::JoinRsp:: Locs: " << locsByObjKey.size ()
    << " ActReps: " << actReps.size ()
    << " Tags: " << metasByTag.size () << " Subs: " << subs.size ()
    << " ObjCount: " << objKeyCount.size ();
}

DataMessage::JoinRsp
DataMessage::GetJoinRsp () const
{
  return m_message.joinRsp;
}

void
DataMessage::SetJoinRsp (ObjectsLocations locs, Publications actReps,
  TagIndex metas, Subscriptions subs, std::map<ObjectKey, uint8_t> objKeyCount)
{
  if (m_msgType == 0) m_msgType = JOIN_RSP;
  else NS_ABORT_MSG_IF (m_msgType != JOIN_RSP, m_msgType);

  m_message.joinRsp.locsByObjKey = locs;
  m_message.joinRsp.actReps = actReps;
  m_message.joinRsp.metasByTag = metas;
  m_message.joinRsp.subs = subs;
  m_message.joinRsp.objKeyCount = objKeyCount;
}

std::ostream&
operator<< (std::ostream &os, DataMessage::JoinRsp const &x)
{
  x.Print (os);
  return os;
}

// NackMsg --------------------------------------------------------------------

void
DataMessage::NackMsg::Serialize (Buffer::Iterator &start) const
{
  start.WriteHtonU64 (puid);

  start.WriteU8 (dsts.size ());
  for (Ipv4Address addr : dsts)
   WriteTo (start, addr);
}

uint32_t
DataMessage::NackMsg::Deserialize (Buffer::Iterator &start)
{
  puid = start.ReadNtohU64 ();

  const uint8_t size = start.ReadU8 ();
  for (int i = 0; i < size; ++i)
   dsts.push_back (Ipv4Address (start.ReadNtohU32 ()));

  return GetSerializedSize ();
}

uint32_t
DataMessage::NackMsg::GetSerializedSize () const
{
  uint32_t size = 9; // pcktId, and list size
  size += 4 * dsts.size ();

  return size;
}

void
DataMessage::NackMsg::Print (std::ostream &os) const
{
  os << "::NackMsg:: PcktId: " << puid << " #Addrs: " << dsts.size ();
}

DataMessage::NackMsg
DataMessage::GetNackMsg () const
{
  return m_message.nackMsg;
}

void
DataMessage::SetNackMsg (uint64_t pcktId, std::vector<Ipv4Address> nodes)
{
  if (m_msgType == 0) m_msgType = NACK_MSG;
  else NS_ABORT_MSG_IF (m_msgType != NACK_MSG, m_msgType);

  m_message.nackMsg.puid = pcktId;
  m_message.nackMsg.dsts = nodes;
}

std::ostream&
operator<< (std::ostream &os, DataMessage::NackMsg const &x)
{
  x.Print (os);
  return os;
}

}
}

