/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2015 NOVA LINCS DI-FCT-UNL <http://nova-lincs.di.fct.unl.pt/>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Joao A. Silva <jaa.silva@campus.fct.unl.pt>
 */
#ifndef THYME_HELLO_BEACON_H
#define THYME_HELLO_BEACON_H

#include <ns3/header.h>

namespace ns3 {
namespace hyrax {

/**
 * \ingroup hyrax
 * 
 * \brief Represents a HELLO message/beacon. It carries the node's cell 
 * address, and a movement flag (if true, the node is currently moving).
 */
class HelloBeacon : public Header
{
private:
  uint16_t m_cell;
  bool m_moving;

public:
  static TypeId GetTypeId (void);
  TypeId GetInstanceTypeId (void) const override;
  HelloBeacon (); // for deserialization **only**
  HelloBeacon (uint16_t cell, bool moving);

  uint16_t GetCell () const;
  bool IsMoving () const;

  void Serialize (Buffer::Iterator start) const override;
  uint32_t Deserialize (Buffer::Iterator start) override;
  uint32_t GetSerializedSize () const override;
  void Print (std::ostream &os) const override;
};

std::ostream& operator<< (std::ostream &os, HelloBeacon const &x);

}
}

#endif /* THYME_HELLO_BEACON_H */

