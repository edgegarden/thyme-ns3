/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2015 NOVA LINCS DI-FCT-UNL <http://nova-lincs.di.fct.unl.pt/>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Joao A. Silva <jaa.silva@campus.fct.unl.pt>
 */
#include "thyme-hello-beacon.h"

namespace ns3 {
namespace hyrax {

NS_OBJECT_ENSURE_REGISTERED (HelloBeacon);

TypeId
HelloBeacon::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::hyrax::HelloBeacon")
    .SetParent<Header> ()
    .AddConstructor<HelloBeacon> ();

  return tid;
}

TypeId
HelloBeacon::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}

HelloBeacon::HelloBeacon () : m_cell (-1), m_moving (false)
{
}

HelloBeacon::HelloBeacon (uint16_t cell, bool moving) : m_cell (cell),
  m_moving (moving)
{
}

uint16_t
HelloBeacon::GetCell () const
{
  return m_cell;
}

bool
HelloBeacon::IsMoving () const
{
  return m_moving;
}

void
HelloBeacon::Serialize (Buffer::Iterator start) const
{
  const Buffer::Iterator i = start;
  start.WriteHtonU16 (m_cell);
  start.WriteU8 ((uint8_t) m_moving);

  uint32_t dist = 0;
  uint32_t serSize = 0;
  NS_ASSERT_MSG ((dist = start.GetDistanceFrom (i))
    == (serSize = GetSerializedSize ()), dist << " != " << serSize);
}

uint32_t
HelloBeacon::Deserialize (Buffer::Iterator start)
{
  const Buffer::Iterator i = start;
  m_cell = start.ReadNtohU16 ();
  m_moving = (bool) start.ReadU8 ();

  const uint32_t dist = start.GetDistanceFrom (i);
  uint32_t serSize = 0;
  NS_ASSERT_MSG (dist == (serSize = GetSerializedSize ()),
    dist << " != " << serSize);
  return dist;
}

uint32_t
HelloBeacon::GetSerializedSize () const
{
  return 3; // cell and isMoving
}

void
HelloBeacon::Print (std::ostream &os) const
{
  os << "::Hello:: Cell: " << m_cell << " IsMoving: " << m_moving;
}

std::ostream&
operator<< (std::ostream &os, HelloBeacon const &x)
{
  x.Print (os);
  return os;
}

}
}

