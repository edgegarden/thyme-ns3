/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2015 NOVA LINCS DI-FCT-UNL <http://nova-lincs.di.fct.unl.pt/>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Joao A. Silva <jaa.silva@campus.fct.unl.pt>
 */
#ifndef THYME_DATA_MESSAGE_H
#define THYME_DATA_MESSAGE_H

#include <ns3/header.h>
#include <ns3/thyme-common.h>
#include <ns3/thyme-node-address.h>
#include <ns3/thyme-subscription-object.h>

namespace ns3 {
namespace hyrax {

class DataMessage : public Header
{
private:
  MessageType m_msgType;

  MessageTarget m_target;
  Ptr<NodeAddress> m_src;
  Locations m_dsts;

  bool m_inRec;
  uint16_t m_lastHop;
  uint16_t m_lastGreedy;
  uint16_t m_firstPerHop;
  uint8_t m_hand;

  uint16_t m_homeCell;
  uint16_t m_noRepHomeCell;

  bool m_cbcast;

  uint8_t m_ttl;

  static const bool m_noRepLoop;
  static const bool m_chooseHand;

public:
  static TypeId GetTypeId (void);
  TypeId GetInstanceTypeId (void) const override;
  DataMessage ();
  virtual ~DataMessage ();

  void Serialize (Buffer::Iterator start) const override;
  uint32_t Deserialize (Buffer::Iterator start) override;
  uint32_t GetSerializedSize () const override;
  void Print (std::ostream &os) const override;

  MessageType GetMsgType () const;

  MessageTarget GetMsgTarget () const;
  void SetMsgTarget (MessageTarget target);

  Ptr<NodeAddress> GetSource () const;
  void SetSource (Ptr<NodeAddress> src);

  Locations GetDestinations () const;
  void AddDestination (uint16_t cell);
  void AddDestination (Ipv4Address addr, uint16_t cell);
  void SetDestinations (Locations dsts);
  bool IsDestination (Ipv4Address addr);
  bool IsDestination (uint16_t cell);
  bool RemoveDestination (Ipv4Address addr); // returns "are there more dsts?"
  bool RemoveDestination (uint16_t cell); // returns "are there more dsts?"
  bool RemoveDestinations (Locations dsts); // returns "are there more dsts?"
  uint32_t NumDestinations ();
  void ClearDestinations ();

  bool IsInRecovery () const;
  void SetGreedyMode ();
  void SetPerimeterMode (uint16_t cell);
  uint16_t GetLastHop () const;
  void SetLastHop (uint16_t cell);
  uint16_t GetLastGreedy () const;
  uint16_t GetFirstPerHop () const;
  void SetFirstPerHop (uint16_t cell);

  uint8_t GetHand () const;
  void SetHand (uint8_t hand);

  uint16_t GetHomeCell () const;
  void SetHomeCell (uint16_t cell);
  uint16_t GetNoRepHomeCell () const;
  void SetNoRepHomeCell (uint16_t cell);

  bool IsCellBcast () const;
  void SetCellBcast (bool cbcast);

  uint8_t GetTtl () const;
  bool IsMaxTtl () const;
  void SetTtl (uint8_t ttl);
  void DecrementTtl ();

  // Publish ------------------------------------------------------------------

  struct Publish
  {
    uint32_t reqId;
    Ptr<ObjectMetadata> objMeta;
    std::string tag; // **not** serialized
    uint8_t tagIdx;
    uint16_t cell;

    void Serialize (Buffer::Iterator &start) const;
    uint32_t Deserialize (Buffer::Iterator &start);
    uint32_t GetSerializedSize () const;
    void Print (std::ostream &os) const;
  };

  Publish GetPublish () const;
  void SetPublish (uint32_t rId, Ptr<ObjectMetadata> m, std::string t, 
                   uint16_t c);

  // PublishRsp ---------------------------------------------------------------

  struct PublishRsp
  {
    uint32_t reqId;
    uint8_t tagIdx;

    void Serialize (Buffer::Iterator &start) const;
    uint32_t Deserialize (Buffer::Iterator &start);
    uint32_t GetSerializedSize () const;
    void Print (std::ostream &os) const;
  };

  PublishRsp GetPublishRsp () const;
  void SetPublishRsp (DataMessage::Publish pub);

  // ActiveRep ----------------------------------------------------------------

  struct ActiveRep
  {
    Ptr<ThymeObject> obj;

    void Serialize (Buffer::Iterator &start) const;
    uint32_t Deserialize (Buffer::Iterator &start);
    uint32_t GetSerializedSize () const;
    void Print (std::ostream &os) const;
  };

  ActiveRep GetActiveRep () const;
  void SetActiveRep (Ptr<ThymeObject> o);

  // Download -----------------------------------------------------------------

  struct Download
  {
    uint32_t reqId;
    Ptr<ObjectIdentifier> objId;
    Ipv4Address owner;

    void Serialize (Buffer::Iterator &start) const;
    uint32_t Deserialize (Buffer::Iterator &start);
    uint32_t GetSerializedSize () const;
    void Print (std::ostream &os) const;
  };

  Download GetDownload () const;
  void SetDownload (uint32_t rId, Ptr<ObjectIdentifier> oId, Ipv4Address o);

  // DownloadRsp --------------------------------------------------------------

  struct DownloadRsp
  {
    uint32_t reqId;
    MessageStatus st;
    Ptr<ThymeObject> obj;

    void Serialize (Buffer::Iterator &start) const;
    uint32_t Deserialize (Buffer::Iterator &start);
    uint32_t GetSerializedSize () const;
    void Print (std::ostream &os) const;
  };

  DownloadRsp GetDownloadRsp () const;
  void SetDownloadRsp (uint32_t rId, Ptr<ThymeObject> o, MessageStatus st);

  // Unpublish ----------------------------------------------------------------

  struct Unpublish
  {
    uint32_t reqId;
    Ptr<ObjectIdentifier> objId;
    uint8_t tagIdx;

    void Serialize (Buffer::Iterator &start) const;
    uint32_t Deserialize (Buffer::Iterator &start);
    uint32_t GetSerializedSize () const;
    void Print (std::ostream &os) const;
  };

  Unpublish GetUnpublish () const;
  void SetUnpublish (uint32_t rId, Ptr<ObjectIdentifier> oId, uint8_t tIdx);

  // UnpublishRsp -------------------------------------------------------------

  struct UnpublishRsp
  {
    uint32_t reqId;
    uint8_t tagIdx;

    void Serialize (Buffer::Iterator &start) const;
    uint32_t Deserialize (Buffer::Iterator &start);
    uint32_t GetSerializedSize () const;
    void Print (std::ostream &os) const;
  };

  UnpublishRsp GetUnpublishRsp () const;
  void SetUnpublishRsp (DataMessage::Unpublish unpub);

  // UnpublishRep -------------------------------------------------------------

  struct UnpublishRep
  {
    Ptr<ObjectIdentifier> objId;

    void Serialize (Buffer::Iterator &start) const;
    uint32_t Deserialize (Buffer::Iterator &start);
    uint32_t GetSerializedSize () const;
    void Print (std::ostream &os) const;
  };

  UnpublishRep GetUnpublishRep () const;
  void SetUnpublishRep (Ptr<ObjectIdentifier> oId);

  // UpdateLoc ----------------------------------------------------------------

  void SetUpdateLoc ();

  // RegisterRep --------------------------------------------------------------

  struct RegisterRep
  {
    Ptr<ObjectIdentifier> objId;
    Ipv4Address owner;

    void Serialize (Buffer::Iterator &start) const;
    uint32_t Deserialize (Buffer::Iterator &start);
    uint32_t GetSerializedSize () const;
    void Print (std::ostream &os) const;
  };

  RegisterRep GetRegisterRep () const;
  void SetRegisterRep (Ptr<ObjectIdentifier> oId, Ipv4Address o);

  // Subscribe ----------------------------------------------------------------

  struct Subscribe
  {
    uint32_t reqId;
    Ptr<SubscriptionObject> subObj;
    uint8_t sendTagIdx;

    void Serialize (Buffer::Iterator &start) const;
    uint32_t Deserialize (Buffer::Iterator &start);
    uint32_t GetSerializedSize () const;
    void Print (std::ostream &os) const;
  };

  Subscribe GetSubscribe () const;
  void SetSubscribe (uint32_t rId, Ptr<SubscriptionObject> sObj, uint8_t tIdx);

  // SubscribeRsp -------------------------------------------------------------

  struct SubscribeRsp
  {
    uint32_t reqId;
    uint8_t sendTagIdx;

    void Serialize (Buffer::Iterator &start) const;
    uint32_t Deserialize (Buffer::Iterator &start);
    uint32_t GetSerializedSize () const;
    void Print (std::ostream &os) const;
  };

  SubscribeRsp GetSubscribeRsp () const;
  void SetSubscribeRsp (DataMessage::Subscribe sub);

  // SubscribeNot -------------------------------------------------------------

  struct SubscribeNot
  {
    std::map<Ipv4Address, std::vector<uint32_t> > subIds;
    std::vector<Ptr<ObjectMetadata> > objMeta;
    std::vector<Locations> locs;

    void Serialize (Buffer::Iterator &start) const;
    uint32_t Deserialize (Buffer::Iterator &start);
    uint32_t GetSerializedSize () const;
    void Print (std::ostream &os) const;

    std::vector<uint32_t> GetMySubscriptionIds (Ipv4Address addr) const;
  };

  SubscribeNot GetSubscribeNot () const;
  void SetSubscribeNot (std::map<Ipv4Address, std::vector<uint32_t> > subIds,
                        Ptr<ObjectMetadata> objMeta, Locations locs);
  void SetSubscribeNot (Ipv4Address addr, uint32_t subId,
                        std::vector<Ptr<ObjectMetadata> > metas,
                        std::vector<Locations> locs);

  // Unsubscribe --------------------------------------------------------------

  struct Unsubscribe
  {
    uint32_t reqId;
    uint32_t subId;
    uint8_t sendTagIdx;

    void Serialize (Buffer::Iterator &start) const;
    uint32_t Deserialize (Buffer::Iterator &start);
    uint32_t GetSerializedSize () const;
    void Print (std::ostream &os) const;
  };

  Unsubscribe GetUnsubscribe () const;
  void SetUnsubscribe (uint32_t rId, uint32_t sId, uint8_t tIdx);

  // UnsubscribeRsp -----------------------------------------------------------

  struct UnsubscribeRsp
  {
    uint32_t reqId;
    uint8_t sendTagIdx;

    void Serialize (Buffer::Iterator &start) const;
    uint32_t Deserialize (Buffer::Iterator &start);
    uint32_t GetSerializedSize () const;
    void Print (std::ostream &os) const;
  };

  UnsubscribeRsp GetUnsubscribeRsp () const;
  void SetUnsubscribeRsp (DataMessage::Unsubscribe unsub);

  // Join ---------------------------------------------------------------------

  void SetJoin ();

  // JoinRsp ------------------------------------------------------------------

  struct JoinRsp
  {
    ObjectsLocations locsByObjKey;
    Publications actReps;
    TagIndex metasByTag;
    Subscriptions subs;
    std::map<ObjectKey, uint8_t> objKeyCount;

    void Serialize (Buffer::Iterator &start) const;
    uint32_t Deserialize (Buffer::Iterator &start);
    uint32_t GetSerializedSize (void) const;
    void Print (std::ostream &os) const;
  };

  JoinRsp GetJoinRsp () const;
  void SetJoinRsp (ObjectsLocations locs, Publications actReps, TagIndex metas,
                   Subscriptions subs, 
                   std::map<ObjectKey, uint8_t> objKeyCount);

  // NackMsg ------------------------------------------------------------------

  struct NackMsg
  {
    uint64_t puid;
    std::vector<Ipv4Address> dsts;

    void Serialize (Buffer::Iterator &start) const;
    uint32_t Deserialize (Buffer::Iterator &start);
    uint32_t GetSerializedSize () const;
    void Print (std::ostream &os) const;
  };

  NackMsg GetNackMsg () const;
  void SetNackMsg (uint64_t pcktId, std::vector<Ipv4Address> nodes);

private:

  struct
  {
    Publish pub;
    PublishRsp pubRsp;
    ActiveRep actRep;
    Download down;
    DownloadRsp downRsp;
    Unpublish unpub;
    UnpublishRsp unpubRsp;
    UnpublishRep unpubRep;
    RegisterRep regRep;
    Subscribe sub;
    SubscribeRsp subRsp;
    SubscribeNot subNot;
    Unsubscribe unsub;
    UnsubscribeRsp unsubRsp;
    JoinRsp joinRsp;
    NackMsg nackMsg;
  } m_message;
};

std::ostream& operator<< (std::ostream &os, DataMessage::Publish const &x);
std::ostream& operator<< (std::ostream &os, DataMessage::PublishRsp const &x);
std::ostream& operator<< (std::ostream &os, DataMessage::ActiveRep const &x);
std::ostream& operator<< (std::ostream &os, DataMessage::Download const &x);
std::ostream& operator<< (std::ostream &os, DataMessage::DownloadRsp const &x);
std::ostream& operator<< (std::ostream &os, DataMessage::Unpublish const &x);
std::ostream& operator<< (std::ostream &os, DataMessage::UnpublishRsp const &x);
std::ostream& operator<< (std::ostream &os, DataMessage::UnpublishRep const &x);
std::ostream& operator<< (std::ostream &os, DataMessage::RegisterRep const &x);
std::ostream& operator<< (std::ostream &os, DataMessage::Subscribe const &x);
std::ostream& operator<< (std::ostream &os, DataMessage::SubscribeRsp const &x);
std::ostream& operator<< (std::ostream &os, DataMessage::SubscribeNot const &x);
std::ostream& operator<< (std::ostream &os, DataMessage::Unsubscribe const &x);
std::ostream& operator<< (std::ostream &os, DataMessage::UnsubscribeRsp const &x);
std::ostream& operator<< (std::ostream &os, DataMessage::JoinRsp const &x);
std::ostream& operator<< (std::ostream &os, DataMessage::NackMsg const &x);

}
}

#endif /* THYME_DATA_MESSAGE_H */

