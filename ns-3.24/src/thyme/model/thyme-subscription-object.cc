/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2015 NOVA LINCS DI-FCT-UNL <http://nova-lincs.di.fct.unl.pt/>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Joao A. Silva <jaa.silva@campus.fct.unl.pt>
 */
#include "thyme-subscription-object.h"
#include <ns3/log.h>
#include <ns3/thyme-utils.h>

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("SubObj");

namespace hyrax {

Ptr<UniformRandomVariable> SubscriptionObject::m_rand =
  CreateObject<UniformRandomVariable> ();

SubscriptionObject::SubscriptionObject () : Subscription (), m_cell (-1),
  m_conjunction (), m_sendTags (), m_recvdObjs ()
{
  NS_LOG_FUNCTION (this);
}

SubscriptionObject::SubscriptionObject (uint32_t subId, Ipv4Address subscriber,
  Time start, Time end, std::string filter, uint16_t cell) :
  Subscription (subId, subscriber, start, end, filter), m_cell (cell),
  m_conjunction (), m_sendTags (), m_recvdObjs ()
{
  NS_LOG_FUNCTION (this << subId << subscriber << start << end << filter
    << cell);
  for (Conjunction conj : m_filter) // set all send tags
   m_sendTags.push_back (GetRandPositiveTag (conj));

  NS_LOG_LOGIC ("Subscription " << subId << " has " << m_sendTags.size ()
    << " conjunction(s)");
}

uint16_t
SubscriptionObject::GetSubscriberCell () const
{
  NS_LOG_FUNCTION (this);
  return m_cell;
}

void
SubscriptionObject::SetSubscriberCell (uint16_t cell)
{
  NS_LOG_FUNCTION (this << cell);
  m_cell = cell;
}

Conjunction
SubscriptionObject::GetConjunction () const
{
  NS_LOG_FUNCTION (this);
  return m_conjunction;
}

void
SubscriptionObject::SetConjunction (Conjunction conjunction)
{
  NS_ABORT_MSG_IF (conjunction.empty (), "One or more tags required.");
  NS_LOG_FUNCTION (this << conjunction.size ());
  m_conjunction = Conjunction (conjunction);
}

std::vector<std::string>
SubscriptionObject::GetSendTags () const
{
  NS_LOG_FUNCTION (this);
  return m_sendTags;
}

std::vector<std::string>
SubscriptionObject::GetPositiveTags () const
{ // can only be called after setting the respective conjunction
  NS_ABORT_MSG_IF (m_conjunction.empty (), "Conjunction needs to be set.");
  NS_LOG_FUNCTION (this);
  std::vector<std::string> posTags;
  for (Tag tag : m_conjunction)
   if (!tag.second) posTags.push_back (tag.first); // if tag is positive

  return posTags;
}

uint8_t
SubscriptionObject::GetSendTagIndex (std::string tag) const
{
  NS_LOG_FUNCTION (this << tag);
  uint8_t idx = 0;
  for (std::string t : m_sendTags)
   {
    if (t == tag) return idx;
    ++idx;
   }

  NS_ABORT_MSG ("Invalid tag " << tag);
  return -1;
}

std::string
SubscriptionObject::GetSendTag (uint8_t index) const
{
  NS_ABORT_MSG_IF (index >= m_sendTags.size (), "Invalid tag index "
    << (uint16_t) index << " (size: " << m_sendTags.size () << ")");
  NS_LOG_FUNCTION (this << index);
  return m_sendTags[index];
}

bool
SubscriptionObject::IsDuplicateObject (Ptr<ObjectMetadata> meta)
{
  NS_LOG_FUNCTION (this << meta);
  const ObjectKey objKey = OBJ_KEY (meta->GetObjectIdentifier (),
    meta->GetOwner ());
  const Time pubTs = meta->GetPublicationTimestamp ();

  auto found = m_recvdObjs.find (objKey);
  if (found != m_recvdObjs.end ()) // object was received in the past
   {
    if (pubTs > found->second) // newer object received
     {
      found->second = pubTs;
      return false;
     }

    NS_LOG_WARN ("Stale obj: " << meta->GetObjectIdentifier ()->ToString ()
      << " " << meta->GetOwner () << " - " << pubTs.GetSeconds ()
      << "s <= " << found->second.GetSeconds () << "s");
    return true; // receiving the same or older object
   }
  else // first time receiving this object
   {
    m_recvdObjs.insert (PAIR (objKey, pubTs));
    return false;
   }
}

void
SubscriptionObject::Serialize (Buffer::Iterator &start) const
{
  NS_LOG_FUNCTION (this);
  const Buffer::Iterator i = start;
  Subscription::Serialize (start);

  start.WriteHtonU16 (m_cell);

  Utils::SerializeConjunction (m_conjunction, start);

  uint32_t dist = 0;
  uint32_t serSize = 0;
  NS_ASSERT_MSG ((dist = start.GetDistanceFrom (i))
    == (serSize = GetSerializedSize ()), dist << " != " << serSize);
}

uint32_t
SubscriptionObject::Deserialize (Buffer::Iterator &start)
{
  NS_LOG_FUNCTION (this);
  const Buffer::Iterator i = start;
  Subscription::Deserialize (start);

  m_cell = start.ReadNtohU16 ();

  m_conjunction = Utils::DeserializeConjunction (start);

  const uint32_t dist = start.GetDistanceFrom (i);
  uint32_t serSize = 0;
  NS_ASSERT_MSG (dist == (serSize = GetSerializedSize ()),
    dist << " != " << serSize);
  return dist;
}

uint32_t
SubscriptionObject::GetSerializedSize () const
{
  NS_LOG_FUNCTION (this);
  uint32_t size = Subscription::GetSerializedSize (); // super class

  size += 2; // cell

  size += Utils::GetSerializedConjunctionSize (m_conjunction); // conjunction

  return size;
}

void
SubscriptionObject::Print (std::ostream &os) const
{
  Subscription::Print (os);
  os << " #Tags: " << m_conjunction.size () << " Cell: " << m_cell;
}

bool
SubscriptionObject::IsValidFilter (std::set<std::string> tags) const
{
  NS_LOG_FUNCTION (this << tags.size ());
  for (Tag tag : m_conjunction)
   {
    bool found = tags.find (tag.first) != tags.end ();
    bool isNeg = tag.second;
    if ((found && isNeg) || (!found && !isNeg))
     { // (found tag and tag is neg) OR (didn't find tag and tag is pos)
      return false;
     }
   }

  return true;
}

std::string
SubscriptionObject::GetRandPositiveTag (Conjunction conjunction) const
{
  NS_ABORT_MSG_IF (conjunction.empty (), "One or more tags required.");
  NS_LOG_FUNCTION (this);
  std::vector<std::string> posTags;
  for (Tag tag : conjunction)
   if (!tag.second) posTags.push_back (tag.first); // if tag is positive

  NS_ABORT_MSG_IF (posTags.empty (), "At least one positive tag required.");
  return posTags[m_rand->GetInteger (0, posTags.size () - 1)];
}

}
}

