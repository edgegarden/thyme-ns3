/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2015 NOVA LINCS DI-FCT-UNL <http://nova-lincs.di.fct.unl.pt/>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Joao A. Silva <jaa.silva@campus.fct.unl.pt>
 */
#ifndef THYME_SUBSCRIPTION_OBJECT_H
#define THYME_SUBSCRIPTION_OBJECT_H

#include <ns3/random-variable-stream.h>
#include <ns3/thyme-subscription.h>

namespace ns3 {
namespace hyrax {

class SubscriptionObject : public Subscription
{
private:
  uint16_t m_cell; // subscriber cell
  Conjunction m_conjunction; // individual AND clause (max 255 tags)

  std::vector<std::string> m_sendTags; // **not** serialized
  std::map<ObjectKey, Time> m_recvdObjs; // **not** serialized

  static Ptr<UniformRandomVariable> m_rand; // **not** serialized

public:
  SubscriptionObject (); // for deserialization **only**
  SubscriptionObject (uint32_t subId, Ipv4Address subscriber, Time start,
                      Time end, std::string filter, uint16_t cell);

  uint16_t GetSubscriberCell () const;
  void SetSubscriberCell (uint16_t cell);

  Conjunction GetConjunction () const;
  void SetConjunction (Conjunction conjunction);

  std::vector<std::string> GetSendTags () const;
  std::vector<std::string> GetPositiveTags () const;
  uint8_t GetSendTagIndex (std::string tag) const;
  std::string GetSendTag (uint8_t index) const;

  bool IsDuplicateObject (Ptr<ObjectMetadata> meta);

  void Serialize (Buffer::Iterator &start) const override;
  uint32_t Deserialize (Buffer::Iterator &start) override;
  uint32_t GetSerializedSize () const override;
  void Print (std::ostream &os) const override;

private:
  bool IsValidFilter (std::set<std::string> tags) const override;

  std::string GetRandPositiveTag (Conjunction conjunction) const;
};

}
}

#endif /* THYME_SUBSCRIPTION_OBJECT_H */

