/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2015 NOVA LINCS DI-FCT-UNL <http://nova-lincs.di.fct.unl.pt/>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Joao A. Silva <jaa.silva@campus.fct.unl.pt>
 */
#include "thyme-profiler.h"
#include <ns3/log.h>
#include <ns3/thyme-utils.h>

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("Prof");

namespace hyrax {

NS_OBJECT_ENSURE_REGISTERED (Profiler);

TypeId
Profiler::GetTypeId (void)
{
  NS_LOG_FUNCTION_NOARGS ();
  static TypeId tid = TypeId ("ns3::hyrax::Profiler")
    .SetParent<Object> ()
    .AddConstructor<Profiler> ();

  return tid;
}

Profiler::Profiler () : m_numNodes (0), m_bcast (false)
{
  NS_ABORT_MSG ("Not implemented!");
}

Profiler::Profiler (NodeContainer nodes, bool bcast) : m_nodes (nodes),
  m_numNodes (nodes.GetN ()), m_bcast (bcast)
{
  NS_LOG_FUNCTION (this << nodes.GetN () << bcast);
  m_phy_rx_bytes = std::vector<uint32_t> (m_numNodes, 0);
  m_phy_rx_pckts = std::vector<uint32_t> (m_numNodes, 0);
  m_phy_rx_drop_bytes = std::vector<uint32_t> (m_numNodes, 0);
  m_phy_rx_drop_pckts = std::vector<uint32_t> (m_numNodes, 0);
  m_phy_tx_bytes = std::vector<uint32_t> (m_numNodes, 0);
  m_phy_tx_pckts = std::vector<uint32_t> (m_numNodes, 0);
  m_phy_tx_drop_bytes = std::vector<uint32_t> (m_numNodes, 0);
  m_phy_tx_drop_pckts = std::vector<uint32_t> (m_numNodes, 0);

  m_mac_rx_bytes = std::vector<uint32_t> (m_numNodes, 0);
  m_mac_rx_pckts = std::vector<uint32_t> (m_numNodes, 0);
  m_mac_rx_drop_bytes = std::vector<uint32_t> (m_numNodes, 0);
  m_mac_rx_drop_pckts = std::vector<uint32_t> (m_numNodes, 0);
  m_mac_tx_bytes = std::vector<uint32_t> (m_numNodes, 0);
  m_mac_tx_pckts = std::vector<uint32_t> (m_numNodes, 0);
  m_mac_tx_drop_bytes = std::vector<uint32_t> (m_numNodes, 0);
  m_mac_tx_drop_pckts = std::vector<uint32_t> (m_numNodes, 0);
  m_mac_tx_failed = std::vector<uint32_t> (m_numNodes, 0);
  m_mac_tx_final_failed = std::vector<uint32_t> (m_numNodes, 0);

  m_ip_rx_bytes = std::vector<uint32_t> (m_numNodes, 0);
  m_ip_rx_pckts = std::vector<uint32_t> (m_numNodes, 0);
  m_ip_tx_bytes = std::vector<uint32_t> (m_numNodes, 0);
  m_ip_tx_pckts = std::vector<uint32_t> (m_numNodes, 0);
  m_ip_drop_bytes = std::vector<std::vector<uint32_t> > (m_numNodes, std::vector<uint32_t> (7, 0));
  m_ip_drop_pckts = std::vector<std::vector<uint32_t> > (m_numNodes, std::vector<uint32_t> (7, 0));
  m_ip_fwd_bytes = std::vector<uint32_t> (m_numNodes, 0);
  m_ip_fwd_pckts = std::vector<uint32_t> (m_numNodes, 0);

  m_ctl_tx_bytes = std::vector<uint32_t> (m_numNodes, 0);
  m_ctl_tx_pckts = std::vector<uint32_t> (m_numNodes, 0);
  m_ctl_rx_bytes = std::vector<uint32_t> (m_numNodes, 0);
  m_ctl_rx_pckts = std::vector<uint32_t> (m_numNodes, 0);

  m_app_tx_bytes = std::vector<std::vector<uint32_t> > (m_numNodes, std::vector<uint32_t> (NUM_OPS, 0));
  m_app_tx_pckts = std::vector<std::vector<uint32_t> > (m_numNodes, std::vector<uint32_t> (NUM_OPS, 0));
  m_app_bcast_bytes = std::vector<std::vector<uint32_t> > (m_numNodes, std::vector<uint32_t> (NUM_OPS, 0));
  m_app_bcast_pckts = std::vector<std::vector<uint32_t> > (m_numNodes, std::vector<uint32_t> (NUM_OPS, 0));
  m_app_rx_bytes = std::vector<std::vector<uint32_t> > (m_numNodes, std::vector<uint32_t> (NUM_OPS, 0));
  m_app_rx_pckts = std::vector<std::vector<uint32_t> > (m_numNodes, std::vector<uint32_t> (NUM_OPS, 0));
}

Profiler::~Profiler ()
{
  NS_LOG_FUNCTION (this);
}

void
Profiler::RegisterNode (Ipv4Address addr)
{
  NS_LOG_FUNCTION (this << addr);
  m_pubsByNode.insert ({addr, 0});
  m_actRepsByNode.insert ({addr, 0});
  m_unpubsByNode.insert ({addr, 0});
  m_subsByNode.insert ({addr, 0});
  m_unsubsByNode.insert ({addr, 0});
}

// PHY layer ------------------------------------------------------------------

void
Profiler::OnPhyRxEnd (std::string ctx, Ptr<const Packet> p)
{
  NS_LOG_FUNCTION (this << ctx << p->GetUid ());
  uint32_t id = GetIdFromContext (ctx);
  m_phy_rx_bytes[id] += p->GetSize ();
  m_phy_rx_pckts[id]++;
}

void
Profiler::OnPhyRxDrop (std::string ctx, Ptr<const Packet> p)
{
  NS_LOG_FUNCTION (this << ctx << p->GetUid ());
  uint32_t id = GetIdFromContext (ctx);
  m_phy_rx_drop_bytes[id] += p->GetSize ();
  m_phy_rx_drop_pckts[id]++;
}

void
Profiler::OnPhyTxBegin (std::string ctx, Ptr<const Packet> p)
{
  NS_LOG_FUNCTION (this << ctx << p->GetUid ());
  uint32_t id = GetIdFromContext (ctx);
  m_phy_tx_bytes[id] += p->GetSize ();
  m_phy_tx_pckts[id]++;
}

void
Profiler::OnPhyTxDrop (std::string ctx, Ptr<const Packet> p)
{
  NS_LOG_FUNCTION (this << ctx << p->GetUid ());
  uint32_t id = GetIdFromContext (ctx);
  m_phy_tx_drop_bytes[id] += p->GetSize ();
  m_phy_tx_drop_pckts[id]++;
}

// MAC layer ------------------------------------------------------------------

void
Profiler::OnMacRx (std::string ctx, Ptr<const Packet> p)
{
  NS_LOG_FUNCTION (this << ctx << p->GetUid ());
  uint32_t id = GetIdFromContext (ctx);
  m_mac_rx_bytes[id] += p->GetSize ();
  m_mac_rx_pckts[id]++;
}

void
Profiler::OnMacRxDrop (std::string ctx, Ptr<const Packet> p)
{
  NS_LOG_FUNCTION (this << ctx << p->GetUid ());
  uint32_t id = GetIdFromContext (ctx);
  m_mac_rx_drop_bytes[id] += p->GetSize ();
  m_mac_rx_drop_pckts[id]++;
}

void
Profiler::OnMacTx (std::string ctx, Ptr<const Packet> p)
{
  NS_LOG_FUNCTION (this << ctx << p->GetUid ());
  uint32_t id = GetIdFromContext (ctx);
  m_mac_tx_bytes[id] += p->GetSize ();
  m_mac_tx_pckts[id]++;
}

void
Profiler::OnMacTxDrop (std::string ctx, Ptr<const Packet> p)
{
  NS_LOG_FUNCTION (this << ctx << p->GetUid ());
  uint32_t id = GetIdFromContext (ctx);
  m_mac_tx_drop_bytes[id] += p->GetSize ();
  m_mac_tx_drop_pckts[id]++;
}

void
Profiler::OnMacTxDataFailed (std::string ctx, Mac48Address addr)
{
  NS_LOG_FUNCTION (this << ctx << addr);
  uint32_t id = GetIdFromContext (ctx);
  m_mac_tx_failed[id]++;
}

void
Profiler::OnMacTxFinalDataFailed (std::string ctx, Mac48Address addr)
{
  NS_LOG_FUNCTION (this << ctx << addr);
  uint32_t id = GetIdFromContext (ctx);
  m_mac_tx_final_failed[id]++;
}

// IP layer -------------------------------------------------------------------

void
Profiler::OnIpIn (std::string ctx, const Ipv4Header &h, Ptr<const Packet> p,
  uint32_t i)
{
  NS_LOG_FUNCTION (this << ctx << h << p->GetUid () << i);
  uint64_t size = h.GetSerializedSize () + p->GetSize ();
  uint32_t id = GetIdFromContext (ctx);
  m_ip_rx_bytes[id] += size;
  m_ip_rx_pckts[id]++;

  HandlePacketsHistogram (p->GetUid (), size);
}

void
Profiler::OnIpOut (std::string ctx, const Ipv4Header &h, Ptr<const Packet> p,
  uint32_t i)
{
  NS_LOG_FUNCTION (this << ctx << h << p->GetUid () << i);
  uint32_t id = GetIdFromContext (ctx);
  m_ip_tx_bytes[id] += h.GetSerializedSize () + p->GetSize ();
  m_ip_tx_pckts[id]++;
}

void
Profiler::OnIpDrop (std::string ctx, const Ipv4Header &h, Ptr<const Packet> p,
  Ipv4L3Protocol::DropReason r, Ptr<Ipv4> ip, uint32_t i)
{
  NS_LOG_FUNCTION (this << ctx << h << p->GetUid () << r << i);
  uint32_t id = GetIdFromContext (ctx);
  m_ip_drop_bytes[id][r] += h.GetSerializedSize () + p->GetSize ();
  m_ip_drop_pckts[id][r]++;
}

void
Profiler::OnIpFwd (std::string ctx, const Ipv4Header &h, Ptr<const Packet> p,
  uint32_t i)
{ // **not called by DCS. only by PL;SG**
  NS_LOG_FUNCTION (this << ctx << h << p->GetUid () << i);
  uint64_t size = h.GetSerializedSize () + p->GetSize ();
  uint32_t id = GetIdFromContext (ctx);
  m_ip_fwd_bytes[id] += size;
  m_ip_fwd_pckts[id]++;

  HandlePacketsHistogram (p->GetUid (), size);
}

// Routing layer --------------------------------------------------------------

void
Profiler::OnCtlTx (uint32_t id, uint32_t pcktSize)
{ // **not called by PL;SG. only by DCS**
  NS_LOG_FUNCTION (this << id << pcktSize);
  m_ctl_tx_bytes[id] += pcktSize;
  m_ctl_tx_pckts[id]++;
}

void
Profiler::OnCtlRx (uint32_t id, uint32_t pcktSize)
{ // **not called by PL;SG. only by DCS**
  NS_LOG_FUNCTION (this << id << pcktSize);
  m_ctl_rx_bytes[id] += pcktSize;
  m_ctl_rx_pckts[id]++;
}

void
Profiler::OnCtlFwd (uint32_t id, uint32_t pcktSize)
{ // **not called by PL;SG. only by DCS**
  NS_LOG_FUNCTION (this << id << pcktSize);
  m_ip_fwd_bytes[id] += pcktSize + 20; // IP header size = 20
  m_ip_fwd_pckts[id]++;

  // correct DCS calling OnIpOut and OnIpIn too many times for each packet
  m_ip_rx_bytes[id] -= pcktSize + 20;
  m_ip_rx_pckts[id]--;
  m_ip_tx_bytes[id] -= pcktSize + 20;
  m_ip_tx_pckts[id]--;
}

void
Profiler::OnRepeatingPath (uint16_t lastGreedy, uint16_t firstPerHop)
{ // **not called by PL;SG. only by DCS**
  NS_LOG_FUNCTION (this << lastGreedy << firstPerHop);
  std::pair<uint16_t, uint16_t> key = PAIR (lastGreedy, firstPerHop);
  auto found = m_ctl_rep_paths.find (key);
  if (found == m_ctl_rep_paths.end ()) m_ctl_rep_paths.insert (PAIR (key, 1));
  else found->second++;
}

void
Profiler::RegisterDownloadPacket (uint64_t puid)
{
  NS_LOG_FUNCTION (this << puid);
  m_downPckts.insert (puid);
}

void
Profiler::RegisterDownloadRspPacket (uint64_t puid)
{
  NS_LOG_FUNCTION (this << puid);
  m_downRspPckts.insert (puid);
}

// APP layer ------------------------------------------------------------------

void
Profiler::OnAppTx (uint32_t id, MessageType type, uint32_t pcktSize)
{
  NS_LOG_FUNCTION (this << id << type << pcktSize);
  Op op = GetOp (type);
  m_app_tx_bytes[id][op] += pcktSize;
  m_app_tx_pckts[id][op]++;
}

void
Profiler::OnAppBcast (uint32_t id, MessageType type, uint32_t pcktSize)
{
  NS_LOG_FUNCTION (this << id << type << pcktSize);
  Op op = GetOp (type);
  m_app_bcast_bytes[id][op] += pcktSize;
  m_app_bcast_pckts[id][op]++;
}

void
Profiler::OnAppRx (uint32_t id, MessageType type, uint32_t pcktSize)
{
  NS_LOG_FUNCTION (this << id << type << pcktSize);
  Op op = GetOp (type);
  m_app_rx_bytes[id][op] += pcktSize;
  m_app_rx_pckts[id][op]++;
}

// Publish op -----------------------------------------------------------------

void
Profiler::OnPublishStart (Ipv4Address addr, uint32_t reqId, Ptr<ThymeObject> obj)
{
  NS_LOG_FUNCTION (this << addr << reqId << obj);
  Key key{addr, reqId};
  PubRecord rec{Simulator::Now ().GetNanoSeconds (), obj};
  m_pubRecs.insert ({key, rec});
}

void
Profiler::OnPublishSuccess (Ipv4Address addr, uint32_t reqId, uint8_t retries)
{
  NS_LOG_FUNCTION (this << addr << reqId << retries);
  Key key{addr, reqId};
  auto rec = m_pubRecs.find (key);
  NS_ABORT_MSG_IF (rec == m_pubRecs.end (), "No pub rec found!");

  int64_t dur = Simulator::Now ().GetNanoSeconds () - rec->second.start;
  m_pub_dur.push_back (dur);

  m_pub_objSize.push_back (rec->second.obj->GetObjectSize ());
  m_pub_keySize.push_back (
    rec->second.obj->GetObjectIdentifier ()->GetKeySize ());
  uint16_t num_tags = rec->second.obj->GetTags ().size ();
  m_pub_numTags.push_back (num_tags);

  m_pub_retries.push_back (retries);

  auto found = m_pub_dursByTag.find (num_tags);
  if (found != m_pub_dursByTag.end ()) found->second.push_back (dur);
  else
   {
    std::vector<int64_t> aux;
    aux.push_back (dur);
    m_pub_dursByTag.insert ({num_tags, aux});
   }

  auto found2 = m_pub_dursByRetry.find (retries);
  if (found2 != m_pub_dursByRetry.end ()) found2->second.push_back (dur);
  else
   {
    std::vector<int64_t> aux;
    aux.push_back (dur);
    m_pub_dursByRetry.insert ({retries, aux});
   }

  InsertPublication (rec->second.obj);
  CheckAllSubscriptions (rec->second.obj);

  rec->second.obj = 0;
  m_pubRecs.erase (rec);
}

void
Profiler::OnPublishFailure (Ipv4Address addr, uint32_t reqId, MessageStatus st)
{
  NS_LOG_FUNCTION (this << addr << reqId << st);
  Key key{addr, reqId};
  auto rec = m_pubRecs.find (key);
  NS_ABORT_MSG_IF (rec == m_pubRecs.end (), "No pub rec found!");

  rec->second.obj = 0;
  m_pubRecs.erase (rec);

  std::vector<int64_t> aux; // publish ops only fail by TIMEOUT
  aux.push_back (Simulator::Now ().GetNanoSeconds ());
  m_pub_timeout.insert ({key, aux});

  auto ms = m_pub_failReason.find (st);
  if (ms != m_pub_failReason.end ()) ms->second++;
  else m_pub_failReason.insert ({st, 1});
}

void
Profiler::OnPublishReceived (Ipv4Address rcvr)
{
  NS_LOG_FUNCTION (this << rcvr);
  auto found = m_pubsByNode.find (rcvr);
  found->second++;
}

void
Profiler::OnPublishRspReceived (Ipv4Address addr, uint32_t reqId)
{ // delayed rsp received
  NS_LOG_FUNCTION (this << addr << reqId);
  Key key{addr, reqId};
  auto found = m_pub_timeout.find (key);
  if (found != m_pub_timeout.end ()) // key exists
   found->second.push_back (
    Simulator::Now ().GetNanoSeconds () - found->second[0]);
  else NS_LOG_ERROR ("Shouldn't happen! " << addr << " " << reqId);
}

void
Profiler::OnActiveReplicaReceived (Ipv4Address rcvr)
{
  NS_LOG_FUNCTION (this << rcvr);
  auto found = m_actRepsByNode.find (rcvr);
  found->second++;
}

// Unpublish op ---------------------------------------------------------------

void
Profiler::OnUnpublishStart (Ipv4Address addr, uint32_t reqId,
  Ptr<ThymeObject> obj)
{
  NS_LOG_FUNCTION (this << addr << reqId << obj);
  Key key{addr, reqId};
  UnpubRecord rec{Simulator::Now ().GetNanoSeconds (), obj};
  m_unpubRecs.insert ({key, rec});
}

void
Profiler::OnUnpublishSuccess (Ipv4Address addr, uint32_t reqId,
  uint8_t retries)
{
  NS_LOG_FUNCTION (this << addr << reqId << retries);
  Key key{addr, reqId};
  auto rec = m_unpubRecs.find (key);
  NS_ABORT_MSG_IF (rec == m_unpubRecs.end (), "No unpub rec found!");

  int64_t dur = Simulator::Now ().GetNanoSeconds () - rec->second.start;
  m_unpub_dur.push_back (dur);

  m_unpub_retries.push_back (retries);

  int16_t num_tags = rec->second.obj->GetTags ().size ();
  auto found = m_unpub_dursByTag.find (num_tags);
  if (found != m_unpub_dursByTag.end ()) found->second.push_back (dur);
  else
   {
    std::vector<int64_t> aux;
    aux.push_back (dur);
    m_unpub_dursByTag.insert ({num_tags, aux});
   }

  auto found2 = m_unpub_dursByRetry.find (retries);
  if (found2 != m_unpub_dursByRetry.end ()) found2->second.push_back (dur);
  else
   {
    std::vector<int64_t> aux;
    aux.push_back (dur);
    m_unpub_dursByRetry.insert ({retries, aux});
   }

  RemovePublication (rec->second.obj);

  rec->second.obj = 0;
  m_unpubRecs.erase (rec);
}

void
Profiler::OnUnpublishFailure (Ipv4Address addr, uint32_t reqId,
  MessageStatus st)
{
  NS_LOG_FUNCTION (this << addr << reqId << st);
  Key key{addr, reqId};
  auto rec = m_unpubRecs.find (key);
  NS_ABORT_MSG_IF (rec == m_unpubRecs.end (), "No unpub rec found!");

  rec->second.obj = 0;
  m_unpubRecs.erase (rec);

  std::vector<int64_t> aux; // unpublish ops only fail by TIMEOUT
  aux.push_back (Simulator::Now ().GetNanoSeconds ());
  m_unpub_timeout.insert ({key, aux});

  auto ms = m_unpub_failReason.find (st);
  if (ms != m_unpub_failReason.end ()) ms->second++;
  else m_unpub_failReason.insert ({st, 1});
}

void
Profiler::OnUnpublishReceived (Ipv4Address rcvr)
{
  NS_LOG_FUNCTION (this << rcvr);
  auto found = m_unpubsByNode.find (rcvr);
  found->second++;
}

void
Profiler::OnUnpublishRspReceived (Ipv4Address addr, uint32_t reqId)
{ // delayed rsp received
  NS_LOG_FUNCTION (this << addr << reqId);
  Key key{addr, reqId};
  auto found = m_unpub_timeout.find (key);
  if (found != m_unpub_timeout.end ()) // key exists
   found->second.push_back (
    Simulator::Now ().GetNanoSeconds () - found->second[0]);
}

// Download op ----------------------------------------------------------------

void
Profiler::OnDownloadStart (Ipv4Address addr, uint32_t reqId)
{
  NS_LOG_FUNCTION (this << addr << reqId);
  Key key{addr, reqId};
  DownRecord rec{Simulator::Now ().GetNanoSeconds ()};
  m_downRecs.insert ({key, rec});
}

void
Profiler::OnDownloadSuccess (Ipv4Address addr, uint32_t reqId,
  Ptr<ThymeObject> obj, uint8_t retries)
{
  NS_LOG_FUNCTION (this << addr << reqId << obj << retries);
  Key key{addr, reqId};
  auto rec = m_downRecs.find (key);
  NS_ABORT_MSG_IF (rec == m_downRecs.end (), "No down rec found!");

  int64_t dur = Simulator::Now ().GetNanoSeconds () - rec->second.start;
  m_down_dur.push_back (dur);

  m_down_objSize.push_back (obj->GetObjectSize ());

  m_down_retries.push_back (retries);

  auto found = m_down_dursByRetry.find (retries);
  if (found != m_down_dursByRetry.end ()) found->second.push_back (dur);
  else
   {
    std::vector<int64_t> aux;
    aux.push_back (dur);
    m_down_dursByRetry.insert ({retries, aux});
   }

  m_downRecs.erase (rec);
}

void
Profiler::OnDownloadFailure (Ipv4Address addr, uint32_t reqId,
  MessageStatus st)
{
  NS_LOG_FUNCTION (this << addr << reqId << st);
  Key key{addr, reqId};
  auto rec = m_downRecs.find (key);
  NS_ABORT_MSG_IF (rec == m_downRecs.end (), "No down rec found!");

  m_downRecs.erase (rec);

  if (st == TIMEOUT)
   {
    std::vector<int64_t> aux;
    aux.push_back (Simulator::Now ().GetNanoSeconds ());
    m_down_timeout.insert ({key, aux});
   }

  auto ms = m_down_failReason.find (st);
  if (ms != m_down_failReason.end ()) ms->second++;
  else m_down_failReason.insert ({st, 1});
}

void
Profiler::OnDownloadRspReceived (Ipv4Address addr, uint32_t reqId)
{ // delayed rsp received
  NS_LOG_FUNCTION (this << addr << reqId);
  Key key{addr, reqId};
  auto found = m_down_timeout.find (key);
  if (found != m_down_timeout.end ()) // key exists
   found->second.push_back (
    Simulator::Now ().GetNanoSeconds () - found->second[0]);
}

// Subscribe op ---------------------------------------------------------------

void
Profiler::OnSubscribeStart (Ipv4Address addr, uint32_t reqId,
  Ptr<Subscription> sub)
{
  NS_LOG_FUNCTION (this << addr << reqId << sub);
  Key key{addr, reqId};
  SubRecord rec{Simulator::Now ().GetNanoSeconds (), sub};
  m_subRecs.insert ({key, rec});

  if (m_bcast)
   {
    auto found = m_subsByNode.find (addr);
    found->second++;

    m_subs.push_back (sub); // insert subscription
    CheckAllPublications (sub);

    m_subTime.insert ({PAIR (addr, sub->GetSubscriptionId ()), rec.start});
   }
}

void
Profiler::OnSubscribeSuccess (Ipv4Address addr, uint32_t reqId,
  uint8_t retries)
{
  NS_LOG_FUNCTION (this << addr << reqId << retries);
  Key key{addr, reqId};
  auto rec = m_subRecs.find (key);
  NS_ABORT_MSG_IF (rec == m_subRecs.end (), "No sub rec found!");

  int64_t dur = Simulator::Now ().GetNanoSeconds () - rec->second.start;
  m_sub_dur.push_back (dur);

  m_sub_retries.push_back (retries);

  m_subs.push_back (rec->second.sub); // insert subscription

  int16_t num_tags = rec->second.sub->GetFilter ().size ();
  auto found = m_sub_dursByTag.find (num_tags);
  if (found != m_sub_dursByTag.end ()) found->second.push_back (dur);
  else
   {
    std::vector<int64_t> aux;
    aux.push_back (dur);
    m_sub_dursByTag.insert ({num_tags, aux});
   }

  auto found2 = m_sub_dursByRetry.find (retries);
  if (found2 != m_sub_dursByRetry.end ()) found2->second.push_back (dur);
  else
   {
    std::vector<int64_t> aux;
    aux.push_back (dur);
    m_sub_dursByRetry.insert ({retries, aux});
   }

  CheckAllPublications (rec->second.sub);
}

void
Profiler::OnSubscribeFailure (Ipv4Address addr, uint32_t reqId,
  MessageStatus st)
{
  NS_LOG_FUNCTION (this << addr << reqId << st);
  Key key{addr, reqId};
  auto rec = m_subRecs.find (key);
  NS_ABORT_MSG_IF (rec == m_subRecs.end (), "No sub rec found!");

  std::vector<int64_t> aux; // subscribe ops only fail by TIMEOUT
  aux.push_back (Simulator::Now ().GetNanoSeconds ());
  m_sub_timeout.insert ({key, aux});

  auto ms = m_sub_failReason.find (st);
  if (ms != m_sub_failReason.end ()) ms->second++;
  else m_sub_failReason.insert ({st, 1});
}

void
Profiler::OnSubscribeReceived (Ipv4Address rcvr, Ipv4Address addr,
  uint32_t reqId)
{
  NS_LOG_FUNCTION (this << rcvr << addr << reqId);
  auto found = m_subsByNode.find (rcvr);
  found->second++;

  Key key{addr, reqId};
  auto rec = m_subRecs.find (key);
  NS_ABORT_MSG_IF (rec == m_subRecs.end (), "No sub rec found!");

  if (m_bcast)
   {
    auto found = m_sub_dur_bcast.find (key);
    if (found != m_sub_dur_bcast.end ()) // subscription exists
     {
      found->second.push_back (
        Simulator::Now ().GetNanoSeconds () - rec->second.start);
     }
    else // new subscription. insert new entry
     {
      std::vector<int64_t> aux;
      aux.push_back (Simulator::Now ().GetNanoSeconds () - rec->second.start);
      m_sub_dur_bcast.insert ({key, aux});
     }
   }
  else
   m_subTime.insert ({PAIR (addr, rec->second.sub->GetSubscriptionId ()),
    rec->second.start});
}

void
Profiler::OnSubscribeRspReceived (Ipv4Address addr, uint32_t reqId)
{ // delayed rsp received
  NS_LOG_FUNCTION (this << addr << reqId);
  Key key{addr, reqId};
  auto found = m_sub_timeout.find (key);
  if (found != m_sub_timeout.end ()) // key exists
   found->second.push_back (
    Simulator::Now ().GetNanoSeconds () - found->second[0]);
}

// Unsubscribe op -------------------------------------------------------------

void
Profiler::OnUnsubscribeStart (Ipv4Address addr, uint32_t reqId,
  Ptr<Subscription> sub)
{
  NS_LOG_FUNCTION (this << addr << reqId << sub);
  Key key{addr, reqId};
  UnsubRecord rec{Simulator::Now ().GetNanoSeconds (), sub};
  m_unsubRecs.insert ({key, rec});

  if (m_bcast)
   {
    auto found = m_unsubsByNode.find (addr);
    found->second++;

    RemoveSubscription (addr, sub->GetSubscriptionId ());
   }
}

void
Profiler::OnUnsubscribeSuccess (Ipv4Address addr, uint32_t reqId,
  uint8_t retries)
{
  NS_LOG_FUNCTION (this << addr << reqId << retries);
  Key key{addr, reqId};
  auto rec = m_unsubRecs.find (key);
  NS_ABORT_MSG_IF (rec == m_unsubRecs.end (), "No unsub rec found!");

  int64_t dur = Simulator::Now ().GetNanoSeconds () - rec->second.start;
  m_unsub_dur.push_back (dur);

  m_unsub_retries.push_back (retries);

  int16_t num_tags = rec->second.sub->GetFilter ().size ();
  auto found = m_unsub_dursByTag.find (num_tags);
  if (found != m_unsub_dursByTag.end ()) found->second.push_back (dur);
  else
   {
    std::vector<int64_t> aux;
    aux.push_back (dur);
    m_unsub_dursByTag.insert ({num_tags, aux});
   }

  auto found2 = m_unsub_dursByRetry.find (retries);
  if (found2 != m_unsub_dursByRetry.end ()) found2->second.push_back (dur);
  else
   {
    std::vector<int64_t> aux;
    aux.push_back (dur);
    m_unsub_dursByRetry.insert ({retries, aux});
   }

  RemoveSubscription (addr, rec->second.sub->GetSubscriptionId ());

  rec->second.sub = 0;
  m_unsubRecs.erase (rec);
}

void
Profiler::OnUnsubscribeFailure (Ipv4Address addr, uint32_t reqId,
  MessageStatus st)
{
  NS_LOG_FUNCTION (this << addr << reqId << st);
  Key key{addr, reqId};
  auto rec = m_unsubRecs.find (key);
  NS_ABORT_MSG_IF (rec == m_unsubRecs.end (), "No unsub rec found!");

  rec->second.sub = 0;
  m_unsubRecs.erase (rec);

  std::vector<int64_t> aux; // unsubscribe ops only fail by TIMEOUT
  aux.push_back (Simulator::Now ().GetNanoSeconds ());
  m_unsub_timeout.insert ({key, aux});

  auto ms = m_unsub_failReason.find (st);
  if (ms != m_unsub_failReason.end ()) ms->second++;
  else m_unsub_failReason.insert ({st, 1});
}

void
Profiler::OnUnsubscribeReceived (Ipv4Address rcvr, Ipv4Address addr,
  uint32_t reqId)
{
  NS_LOG_FUNCTION (this << rcvr << addr << reqId);
  auto found = m_unsubsByNode.find (rcvr);
  found->second++;

  if (m_bcast)
   {
    Key key{addr, reqId};
    auto rec = m_unsubRecs.find (key);
    NS_ABORT_MSG_IF (rec == m_unsubRecs.end (), "No unsub rec found!");

    auto found = m_unsub_dur_bcast.find (key);
    if (found != m_unsub_dur_bcast.end ()) // subscription exists
     {
      found->second.push_back (
        Simulator::Now ().GetNanoSeconds () - rec->second.start);
     }
    else // new subscription. insert new entry
     {
      std::vector<int64_t> aux;
      aux.push_back (Simulator::Now ().GetNanoSeconds () - rec->second.start);
      m_unsub_dur_bcast.insert ({key, aux});
     }
   }
}

void
Profiler::OnUnsubscribeRspReceived (Ipv4Address addr, uint32_t reqId)
{ // delayed rsp received
  NS_LOG_FUNCTION (this << addr << reqId);
  Key key{addr, reqId};
  auto found = m_unsub_timeout.find (key);
  if (found != m_unsub_timeout.end ()) // key exists
   found->second.push_back (
    Simulator::Now ().GetNanoSeconds () - found->second[0]);
}

// Notifications --------------------------------------------------------------

void
Profiler::OnNotificationSent (Ipv4Address sender, Ipv4Address addr,
  uint32_t subId, Ptr<ObjectMetadata> meta)
{
  NS_LOG_FUNCTION (this << sender << addr << subId << meta);
  m_nots_sent++;

  Notif n{sender, addr, subId, meta, Simulator::Now ().GetNanoSeconds ()};
  m_nots.push_back (n);
}

void
Profiler::OnNotificationReceived (Ipv4Address sender, Ipv4Address addr,
  Ptr<Subscription> sub, Ptr<ObjectMetadata> meta, uint32_t nots)
{
  NS_LOG_FUNCTION (this << sender << addr << sub << meta << nots);
  m_nots_recvd++;

  for (auto notif = m_nots.begin (); notif != m_nots.end (); ++notif)
   {
    if (sender == (*notif).sender
        && addr == (*notif).recvr
        && sub->GetSubscriptionId () == (*notif).subId
        && (*notif).meta->IsEqual (meta))
     {
      auto found = m_subTime.find (PAIR (addr, sub->GetSubscriptionId ()));
      NS_ABORT_MSG_IF (found == m_subTime.end (), "No subTime found!");
      int64_t subTime = found->second;
      int64_t pubTime = (*notif).meta->GetPublicationTimestamp ()
        .GetNanoSeconds ();
      int64_t now = Simulator::Now ().GetNanoSeconds ();

      if (subTime > pubTime) // in the past
       m_notsTime.push_back ((now - subTime) / nots);
      else // in the present
       m_notsTime.push_back ((now - pubTime) / nots);

      m_nots.erase (notif);
      break;
     }
   }
}

void
Profiler::OnDuplicateNotificationReceived (Ipv4Address addr,
  Ptr<Subscription> sub, Ptr<ObjectMetadata> meta)
{
  NS_LOG_FUNCTION (this << addr << sub << meta);
  m_dup_nots_recvd++;
}

void
Profiler::OnNoIdNotification (uint32_t numMetas)
{
  NS_LOG_FUNCTION (this << numMetas);
  m_noid_nots += numMetas;
}

void
Profiler::OnNackSent (uint64_t puid, MessageType type,
  std::vector<Ipv4Address> nodes)
{
  NS_LOG_FUNCTION (this << puid << type << nodes.size ());
  auto p = m_nacks.find (puid);
  NS_ABORT_MSG_IF (p != m_nacks.end (), "Shouldn't have found packet!");
  m_nacks.insert ({puid, type});

  auto found = m_nacksSentPerOp.find (type);
  if (found == m_nacksSentPerOp.end ())
   m_nacksSentPerOp.insert ({type, 1});
  else
   found->second++;

  m_nackAddrs.push_back (nodes.size ());
}

void
Profiler::OnNackReceived (uint64_t puid)
{
  NS_LOG_FUNCTION (this << puid);
  auto p = m_nacks.find (puid);
  NS_ABORT_MSG_IF (p == m_nacks.end (), "No packet found!");
  MessageType type = p->second;

  auto found = m_nacksRecvdPerOp.find (type);
  if (found == m_nacksRecvdPerOp.end ())
   m_nacksRecvdPerOp.insert ({type, 1});
  else
   found->second++;
}

void
Profiler::OnTx (uint64_t puid, MessageType type, uint32_t size)
{
  NS_LOG_FUNCTION (this << puid << type);
  _a.insert ({puid, Simulator::Now ()});
  m_txs.insert ({puid, type});
  m_aaa.insert ({puid, size});
}

void
Profiler::OnTxOk (uint64_t puid)
{
  NS_LOG_FUNCTION (this << puid);
  auto found = _a.find (puid);
  NS_ABORT_MSG_IF (found == _a.end (), "No packet found! " << puid);

  Time now = Simulator::Now ();
  Time res = now - found->second;
  auto size = m_aaa.find (puid);
  std::cout << "AAAAAAAAAAAAAAAAAAAAAAAA " << res.GetMicroSeconds () << " " << size->second << "\n";

  found->second = now;

  m_txs.erase (puid);
}

void
Profiler::OnTxError (uint64_t puid)
{
  NS_LOG_FUNCTION (this << puid);
  auto found = m_txs.find (puid);
  //  if (found != m_txs.end ())
  //  std::cout << "2>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> " << puid << " " << found->second << "\n";
  //  else
  //   std::cout << "2SSSSSSSSSSSSSSSSSSSSSSSSSSSS " << puid << "\n";


  auto found2 = m_txs2.find (found->second);
  if (found2 == m_txs2.end ())
   {
    m_txs2.insert ({found->second, 1});
   }
  else
   {
    found2->second++;
   }
  m_txs.erase (puid);
}

// Report ---------------------------------------------------------------------

void
Profiler::PrintReport ()
{
  NS_LOG_FUNCTION (this);
  std::cout << "\n#$%&-----#$%&\n";

  PrintPhyData ();

  PrintMacData ();

  PrintIpData ();
  PrintHistogramData ();

  PrintCtrlData ();

  PrintAppRxData ();
  PrintAppTxData ();
  PrintAppBcastData ();

  PrintPublishData ();
  PrintUnpublishData ();
  PrintDownloadData ();
  PrintSubscribeData ();
  PrintUnsubscribeData ();
  PrintNotificationsData ();

  std::cout << "#$%&-----#$%&\n";

  std::cout << "\n############################################################\n";
  uint32_t phy_tx_bytes = 0, phy_tx_pckts = 0;
  for (int i = 0; i < m_numNodes; ++i)
   {
    phy_tx_bytes += m_phy_tx_bytes[i];
    phy_tx_pckts += m_phy_tx_pckts[i];
   }
  std::cout << "PHY layer: (bytes, pckts)\n"
    << "Tx: " << phy_tx_bytes << " " << phy_tx_pckts << "\n";

  uint32_t mac_tx_bytes = 0, mac_tx_pckts = 0,
    mac_tx_failed = 0, mac_tx_final_failed = 0;
  for (int i = 0; i < m_numNodes; ++i)
   {
    mac_tx_bytes += m_mac_tx_bytes[i];
    mac_tx_pckts += m_mac_tx_pckts[i];
    mac_tx_failed += m_mac_tx_failed[i];
    mac_tx_final_failed += m_mac_tx_final_failed[i];
   }
  std::cout << "\nMAC layer: (bytes, pckts)\n"
    << "Tx: " << mac_tx_bytes << " " << mac_tx_pckts << "\n"
    << "Tx Failed: " << mac_tx_final_failed << " (" << mac_tx_failed << ")\n";

  if (!m_bcast)
   {
    std::cout << "\nRep Paths: \n";
    for (auto it : m_ctl_rep_paths)
     std::cout << " " << it.first.first << " " << it.first.second
      << " - " << it.second << "\n";
   }

  std::cout << "\n";
  PrintPublishOperationReport ();
  std::cout << "\n";
  PrintUnpublishOperationReport ();
  std::cout << "\n";
  PrintDownloadOperationReport ();
  std::cout << "\n";
  PrintSubscribeOperationReport ();
  std::cout << "\n";
  PrintUnsubscribeOperationReport ();
  std::cout << "\n";
  PrintSubscriptionNotificationReport ();
}

// Auxiliary functions --------------------------------------------------------

void
Profiler::HandlePacketsHistogram (uint64_t puid, uint32_t size)
{
  NS_LOG_FUNCTION (this << puid << size);
  auto packet = m_hopsHistogram.find (puid);
  if (packet == m_hopsHistogram.end ()) // new packet
   m_hopsHistogram.insert ({puid, PAIR (1, size)}); // < numHops, packetSize >
  else // pre-existent packet
   packet->second.first++; // increment numHops
}

void
Profiler::InsertPublication (Ptr<ThymeObject> obj)
{
  NS_LOG_FUNCTION (this << obj);
  for (std::string tag : obj->GetTags ()) // insert object indexed by its tags
   {
    auto found = m_pubsByTag.find (tag);
    if (found != m_pubsByTag.end ()) // tag exists
     {
      found->second.push_back (obj->GetObjectMetadata ());
     }
    else // tag doesn't exist
     {
      std::vector<Ptr<ObjectMetadata> > aux;
      aux.push_back (obj->GetObjectMetadata ());
      m_pubsByTag.insert ({tag, aux});
     }
   }
}

void
Profiler::RemovePublication (Ptr<ThymeObject> obj)
{
  NS_LOG_FUNCTION (this << obj);
  for (std::string tag : obj->GetTags ()) // remove object indexed by its tags
   {
    auto found = m_pubsByTag.find (tag);
    if (found != m_pubsByTag.end ()) // tag exists
     {
      for (auto pub = found->second.begin (); pub != found->second.end (); ++pub)
       { // search for the corresponding metadata
        if (obj->GetObjectMetadata ()->IsEqual (*pub))
         {
          found->second.erase (pub);
          break;
         }
       }
     }
   }
}

void
Profiler::RemoveSubscription (Ipv4Address addr, uint32_t subId)
{
  NS_LOG_FUNCTION (this << addr << subId);
  for (auto sub = m_subs.begin (); sub != m_subs.end (); ++sub)
   {
    if ((*sub)->GetSubscriptionId () == subId && (*sub)->GetSubscriber () == addr)
     {
      m_subs.erase (sub);
      break;
     }
   }
}

void
Profiler::CheckAllSubscriptions (Ptr<ThymeObject> obj)
{
  NS_LOG_FUNCTION (this << obj);
  for (auto sub = m_subs.begin (); sub != m_subs.end ();)
   { // for each subscription check if it matches the published object
    if ((*sub)->GetSubscriber () == obj->GetOwner ())
     { // skip publisher own subscriptions
      ++sub;
      continue;
     }

    if ((*sub)->IsExpired ())
     { // if subscription is expired, erase it from list
      sub = m_subs.erase (sub);
     }
    else
     {
      if ((*sub)->IsMatch (obj->GetObjectMetadata ()))
       m_expected_nots++;

      ++sub;
     }
   }
}

void
Profiler::CheckAllPublications (Ptr<Subscription> sub)
{
  NS_LOG_FUNCTION (this << sub);
  if (!sub->IsStarted ()) return; // if not yet active...

  std::set<ObjectKey> keys;
  for (Conjunction conj : sub->GetFilter ()) // for each conjunction...
   {
    bool posTag = false;

    for (Tag t : conj) // for each literal...
     {
      if (!t.second) // if tag is positive...
       {
        posTag = true;
        const auto pubs = m_pubsByTag.find (t.first);

        if (pubs != m_pubsByTag.end ()) // if it is in the tags list...
         {
          for (auto pub : pubs->second)
           { // for each object published under that tag...
            if (sub->IsMatch (pub) &&
                keys.insert (OBJ_KEY (pub->GetObjectIdentifier (),
                pub->GetOwner ())).second)
             m_expected_nots++;
           }
         }
       }

      if (posTag) break; // go to next conjunction
     }
   }
}

Profiler::Op
Profiler::GetOp (MessageType type) const
{
  NS_LOG_FUNCTION (this << type);
  Op op;
  switch (type)
  {
  case PUBLISH:
  case PUBLISH_RSP:
    op = PUB;
    break;
  case DOWNLOAD:
  case DOWNLOAD_RSP:
    op = DOWN;
    break;
  case UNPUBLISH:
  case UNPUBLISH_RSP:
    op = UNPUB;
    break;
  case UNPUBLISH_REP:
  case ACTIVE_REP:
    op = ACT_REP;
    break;
  case UPDATE_LOC:
  case REGISTER_REP:
    op = PASS_REP;
    break;
  case SUBSCRIBE:
  case SUBSCRIBE_RSP:
    op = SUB;
    break;
  case SUBSCRIBE_NOT:
    op = NOT;
    break;
  case UNSUBSCRIBE:
  case UNSUBSCRIBE_RSP:
    op = UNSUB;
    break;
  case JOIN:
  case JOIN_RSP:
    op = CTRL;
    break;
  case NACK_MSG:
    op = NACK;
    break;
  default:
    NS_ABORT_MSG (type);
  }
  return op;
}

uint32_t
Profiler::GetIdFromContext (std::string ctx) const
{
  NS_LOG_FUNCTION (this << ctx);
  std::vector<std::string> v = Subscription::Split (ctx, '/');
  return std::stoul (v[2]);
}

double
Profiler::NsToMs (double val) const
{
  return val / 1000000.0;
}

void
Profiler::PrintPhyData () const
{
  NS_LOG_FUNCTION (this);
  for (int i = 0; i < m_numNodes; ++i) // {PHY-Rx-bytes	PHY-Rx-pckts} * {...}
   std::cout << m_phy_rx_bytes[i] << " " << m_phy_rx_pckts[i] << " ";
  std::cout << "\n";

  for (int i = 0; i < m_numNodes; ++i) // {PHY-Rx-drop-bytes	PHY-Rx-drop-pckts} * {...}
   std::cout << m_phy_rx_drop_bytes[i] << " " << m_phy_rx_drop_pckts[i] << " ";
  std::cout << "\n";

  for (int i = 0; i < m_numNodes; ++i) // {PHY-Tx-bytes	PHY-Tx-pckts} * {...}
   std::cout << m_phy_tx_bytes[i] << " " << m_phy_tx_pckts[i] << " ";
  std::cout << "\n";

  for (int i = 0; i < m_numNodes; ++i) // {PHY-Tx-drop-bytes	PHY-Tx-drop-pckts} * {...}
   std::cout << m_phy_tx_drop_bytes[i] << " " << m_phy_tx_drop_pckts[i] << " ";
  std::cout << "\n";
}

void
Profiler::PrintMacData () const
{
  NS_LOG_FUNCTION (this);
  for (int i = 0; i < m_numNodes; ++i) // {MAC-Rx-bytes	MAC-Rx-pckts} * {...}
   std::cout << m_mac_rx_bytes[i] << " " << m_mac_rx_pckts[i] << " ";
  std::cout << "\n";

  for (int i = 0; i < m_numNodes; ++i) // {MAC-Rx-drop-bytes	MAC-Rx-drop-pckts} * {...}
   std::cout << m_mac_rx_drop_bytes[i] << " " << m_mac_rx_drop_pckts[i] << " ";
  std::cout << "\n";

  for (int i = 0; i < m_numNodes; ++i) // {MAC-Tx-bytes	MAC-Tx-pckts} * {...}
   std::cout << m_mac_tx_bytes[i] << " " << m_mac_tx_pckts[i] << " ";
  std::cout << "\n";

  for (int i = 0; i < m_numNodes; ++i) // {MAC-Tx-drop-bytes	MAC-Tx-drop-pckts} * {...}
   std::cout << m_mac_tx_drop_bytes[i] << " " << m_mac_tx_drop_pckts[i] << " ";
  std::cout << "\n";

  for (int i = 0; i < m_numNodes; ++i) // {MAC-Tx-failed-pckts	MAC-Tx-maxfailed-pckts} * {...}
   std::cout << m_mac_tx_failed[i] << " " << m_mac_tx_final_failed[i] << " ";
  std::cout << "\n";
}

void
Profiler::PrintIpData () const
{
  NS_LOG_FUNCTION (this);
  for (int i = 0; i < m_numNodes; ++i) // {IP-Rx-bytes	IP-Rx-pckts} * {...}
   std::cout << m_ip_rx_bytes[i] << " " << m_ip_rx_pckts[i] << " ";
  std::cout << "\n";

  for (int i = 0; i < m_numNodes; ++i) // {IP-Tx-bytes	IP-Tx-pckts} * {...}
   std::cout << m_ip_tx_bytes[i] << " " << m_ip_tx_pckts[i] << " ";
  std::cout << "\n";

  for (int i = 0; i < m_numNodes; ++i) // {IP-Fwd-bytes IP-Fwd-pckts} * {...}
   std::cout << m_ip_fwd_bytes[i] << " " << m_ip_fwd_pckts[i] << " ";
  std::cout << "\n";

  for (int i = 0; i < m_numNodes; ++i) // {IP-Drop-bytes	IP-Drop-pckts} * {...} (TTL EXPIRED)
   std::cout << m_ip_drop_bytes[i][1] << " " << m_ip_drop_pckts[i][1] << " ";
  std::cout << "\n";

  for (int i = 0; i < m_numNodes; ++i) // {IP-Drop-bytes	IP-Drop-pckts} * {...} (NO ROUTE)
   std::cout << m_ip_drop_bytes[i][2] << " " << m_ip_drop_pckts[i][2] << " ";
  std::cout << "\n";

  for (int i = 0; i < m_numNodes; ++i) // {IP-Drop-bytes	IP-Drop-pckts} * {...} (BAD CHECKSUM)
   std::cout << m_ip_drop_bytes[i][3] << " " << m_ip_drop_pckts[i][3] << " ";
  std::cout << "\n";

  for (int i = 0; i < m_numNodes; ++i) // {IP-Drop-bytes	IP-Drop-pckts} * {...} (INTERFACE DOWN)
   std::cout << m_ip_drop_bytes[i][4] << " " << m_ip_drop_pckts[i][4] << " ";
  std::cout << "\n";

  for (int i = 0; i < m_numNodes; ++i) // {IP-Drop-bytes	IP-Drop-pckts} * {...} (ROUTE ERROR)
   std::cout << m_ip_drop_bytes[i][5] << " " << m_ip_drop_pckts[i][5] << " ";
  std::cout << "\n";

  for (int i = 0; i < m_numNodes; ++i) // {IP-Drop-bytes	IP-Drop-pckts} * {...} (FRAGMENT TIMEOUT)
   std::cout << m_ip_drop_bytes[i][6] << " " << m_ip_drop_pckts[i][6] << " ";
  std::cout << "\n";
}

void
Profiler::PrintHistogramData () const
{
  NS_LOG_FUNCTION (this);
  for (auto it : m_hopsHistogram) // {numHops pcktSize} * {...}
   if (m_downPckts.find (it.first) != m_downPckts.end ()) // is a DOWN packet
    std::cout << it.second.first << " " << it.second.second << " ";
  std::cout << "\n";

  for (auto it : m_hopsHistogram) // {numHops pcktSize} * {...}
   if (m_downRspPckts.find (it.first) != m_downRspPckts.end ()) // is a DOWN_RSP packet
    std::cout << it.second.first << " " << it.second.second << " ";
  std::cout << "\n";
}

void
Profiler::PrintCtrlData ()
{
  NS_LOG_FUNCTION (this);
  if (m_bcast)
   for (int i = 0; i < m_nodes.GetN (); ++i)
    { // get stats from routing protocol class
     Ptr<Ipv4RoutingProtocol> rp =
       m_nodes.Get (i)->GetObject<Ipv4RoutingProtocol> ();
     m_ctl_rx_pckts[i] = rp->m_ctl_rx_pckts;
     m_ctl_rx_bytes[i] = rp->m_ctl_rx_bytes;
     m_ctl_tx_pckts[i] = rp->m_ctl_tx_pckts;
     m_ctl_tx_bytes[i] = rp->m_ctl_tx_bytes;
    }

  for (int i = 0; i < m_numNodes; ++i) // {CTL-Rx-bytes	CTL-Rx-pckts} * {...}
   std::cout << m_ctl_rx_bytes[i] << " " << m_ctl_rx_pckts[i] << " ";
  std::cout << "\n";

  for (int i = 0; i < m_numNodes; ++i) // {CTL-Tx-bytes	CTL-Tx-pckts} * {...}
   std::cout << m_ctl_tx_bytes[i] << " " << m_ctl_tx_pckts[i] << " ";
  std::cout << "\n";

  for (auto it : m_ctl_rep_paths) // {lastGreedy firstPerHop reps} * {...}
   std::cout << it.first.first << " " << it.first.second << " " << it.second << " ";
  std::cout << "\n";
}

void
Profiler::PrintAppRxData () const
{
  NS_LOG_FUNCTION (this);
  for (int i = 0; i < m_numNodes; ++i) // {APP-Rx-bytes	APP-Rx-pckts} * {...} (PUBLISH)
   std::cout << m_app_rx_bytes[i][PUB] << " " << m_app_rx_pckts[i][PUB] << " ";
  std::cout << "\n";

  for (int i = 0; i < m_numNodes; ++i) // {APP-Rx-bytes	APP-Rx-pckts} * {...} (UNPUBLISH)
   std::cout << m_app_rx_bytes[i][UNPUB] << " " << m_app_rx_pckts[i][UNPUB] << " ";
  std::cout << "\n";

  for (int i = 0; i < m_numNodes; ++i) // {APP-Rx-bytes	APP-Rx-pckts} * {...} (DOWNLOAD)
   std::cout << m_app_rx_bytes[i][DOWN] << " " << m_app_rx_pckts[i][DOWN] << " ";
  std::cout << "\n";

  for (int i = 0; i < m_numNodes; ++i) // {APP-Rx-bytes	APP-Rx-pckts} * {...} (SUBSCRIBE)
   std::cout << m_app_rx_bytes[i][SUB] << " " << m_app_rx_pckts[i][SUB] << " ";
  std::cout << "\n";

  for (int i = 0; i < m_numNodes; ++i) // {APP-Rx-bytes	APP-Rx-pckts} * {...} (UNSUBSCRIBE)
   std::cout << m_app_rx_bytes[i][UNSUB] << " " << m_app_rx_pckts[i][UNSUB] << " ";
  std::cout << "\n";

  for (int i = 0; i < m_numNodes; ++i) // {APP-Rx-bytes	APP-Rx-pckts} * {...} (NOTIFICATION)
   std::cout << m_app_rx_bytes[i][NOT] << " " << m_app_rx_pckts[i][NOT] << " ";
  std::cout << "\n";

  for (int i = 0; i < m_numNodes; ++i) // {APP-Rx-bytes	APP-Rx-pckts} * {...} (CONTROL)
   std::cout << m_app_rx_bytes[i][CTRL] << " " << m_app_rx_pckts[i][CTRL] << " ";
  std::cout << "\n";

  for (int i = 0; i < m_numNodes; ++i) // {APP-Rx-bytes	APP-Rx-pckts} * {...} (ACTIVE REPLICATION)
   std::cout << m_app_rx_bytes[i][ACT_REP] << " " << m_app_rx_pckts[i][ACT_REP] << " ";
  std::cout << "\n";

  for (int i = 0; i < m_numNodes; ++i) // {APP-Rx-bytes	APP-Rx-pckts} * {...} (PASSIVE REPLICATION)
   std::cout << m_app_rx_bytes[i][PASS_REP] << " " << m_app_rx_pckts[i][PASS_REP] << " ";
  std::cout << "\n";

  for (int i = 0; i < m_numNodes; ++i) // {APP-Rx-bytes	APP-Rx-pckts} * {...} (NACK)
   std::cout << m_app_rx_bytes[i][NACK] << " " << m_app_rx_pckts[i][NACK] << " ";
  std::cout << "\n";
}

void
Profiler::PrintAppTxData () const
{
  NS_LOG_FUNCTION (this);
  for (int i = 0; i < m_numNodes; ++i) // {APP-Tx-bytes	APP-Tx-pckts} * {...} (PUBLISH)
   std::cout << m_app_tx_bytes[i][PUB] << " " << m_app_tx_pckts[i][PUB] << " ";
  std::cout << "\n";

  for (int i = 0; i < m_numNodes; ++i) // {APP-Tx-bytes	APP-Tx-pckts} * {...} (UNPUBLISH)
   std::cout << m_app_tx_bytes[i][UNPUB] << " " << m_app_tx_pckts[i][UNPUB] << " ";
  std::cout << "\n";

  for (int i = 0; i < m_numNodes; ++i) // {APP-Tx-bytes	APP-Tx-pckts} * {...} (DOWNLOAD)
   std::cout << m_app_tx_bytes[i][DOWN] << " " << m_app_tx_pckts[i][DOWN] << " ";
  std::cout << "\n";

  for (int i = 0; i < m_numNodes; ++i) // {APP-Tx-bytes	APP-Tx-pckts} * {...} (SUBSCRIBE)
   std::cout << m_app_tx_bytes[i][SUB] << " " << m_app_tx_pckts[i][SUB] << " ";
  std::cout << "\n";

  for (int i = 0; i < m_numNodes; ++i) // {APP-Tx-bytes	APP-Tx-pckts} * {...} (UNSUBSCRIBE)
   std::cout << m_app_tx_bytes[i][UNSUB] << " " << m_app_tx_pckts[i][UNSUB] << " ";
  std::cout << "\n";

  for (int i = 0; i < m_numNodes; ++i) // {APP-Tx-bytes	APP-Tx-pckts} * {...} (NOTIFICATION)
   std::cout << m_app_tx_bytes[i][NOT] << " " << m_app_tx_pckts[i][NOT] << " ";
  std::cout << "\n";

  for (int i = 0; i < m_numNodes; ++i) // {APP-Tx-bytes	APP-Tx-pckts} * {...} (CONTROL)
   std::cout << m_app_tx_bytes[i][CTRL] << " " << m_app_tx_pckts[i][CTRL] << " ";
  std::cout << "\n";

  for (int i = 0; i < m_numNodes; ++i) // {APP-Tx-bytes	APP-Tx-pckts} * {...} (ACTIVE REPLICATION)
   std::cout << m_app_tx_bytes[i][ACT_REP] << " " << m_app_tx_pckts[i][ACT_REP] << " ";
  std::cout << "\n";

  for (int i = 0; i < m_numNodes; ++i) // APP-Tx-bytes	APP-Tx-pckts} * {...} (PASSIVE REPLICATION)
   std::cout << m_app_tx_bytes[i][PASS_REP] << " " << m_app_tx_pckts[i][PASS_REP] << " ";
  std::cout << "\n";

  for (int i = 0; i < m_numNodes; ++i) // APP-Tx-bytes	APP-Tx-pckts} * {...} (NACK)
   std::cout << m_app_tx_bytes[i][NACK] << " " << m_app_tx_pckts[i][NACK] << " ";
  std::cout << "\n";
}

void
Profiler::PrintAppBcastData () const
{
  NS_LOG_FUNCTION (this);
  for (int i = 0; i < m_numNodes; ++i) // {APP-Bcast-bytes	APP-Bcast-pckts} * {...} (PUBLISH)
   std::cout << m_app_bcast_bytes[i][PUB] << " " << m_app_bcast_pckts[i][PUB] << " ";
  std::cout << "\n";

  for (int i = 0; i < m_numNodes; ++i) // {APP-Bcast-bytes	APP-Bcast-pckts} * {...} (UNPUBLISH)
   std::cout << m_app_bcast_bytes[i][UNPUB] << " " << m_app_bcast_pckts[i][UNPUB] << " ";
  std::cout << "\n";

  for (int i = 0; i < m_numNodes; ++i) // {APP-Bcast-bytes	APP-Bcast-pckts} * {...} (DOWNLOAD)
   std::cout << m_app_bcast_bytes[i][DOWN] << " " << m_app_bcast_pckts[i][DOWN] << " ";
  std::cout << "\n";

  for (int i = 0; i < m_numNodes; ++i) // {APP-Bcast-bytes	APP-Bcast-pckts} * {...} (SUBSCRIBE)
   std::cout << m_app_bcast_bytes[i][SUB] << " " << m_app_bcast_pckts[i][SUB] << " ";
  std::cout << "\n";

  for (int i = 0; i < m_numNodes; ++i) // {APP-Bcast-bytes	APP-Bcast-pckts} * {...} (UNSUBSCRIBE)
   std::cout << m_app_bcast_bytes[i][UNSUB] << " " << m_app_bcast_pckts[i][UNSUB] << " ";
  std::cout << "\n";

  for (int i = 0; i < m_numNodes; ++i) // {APP-Bcast-bytes	APP-Bcast-pckts} * {...} (NOTIFICATION)
   std::cout << m_app_bcast_bytes[i][NOT] << " " << m_app_bcast_pckts[i][NOT] << " ";
  std::cout << "\n";

  for (int i = 0; i < m_numNodes; ++i) // {APP-Bcast-bytes	APP-Bcast-pckts} * {...} (CONTROL)
   std::cout << m_app_bcast_bytes[i][CTRL] << " " << m_app_bcast_pckts[i][CTRL] << " ";
  std::cout << "\n";

  for (int i = 0; i < m_numNodes; ++i) // {APP-Bcast-bytes	APP-Bcast-pckts} * {...} (ACTIVE REPLICATION)
   std::cout << m_app_bcast_bytes[i][ACT_REP] << " " << m_app_bcast_pckts[i][ACT_REP] << " ";
  std::cout << "\n";

  for (int i = 0; i < m_numNodes; ++i) // {APP-Bcast-bytes	APP-Bcast-pckts} * {...} (PASSIVE REPLICATION)
   std::cout << m_app_bcast_bytes[i][PASS_REP] << " " << m_app_bcast_pckts[i][PASS_REP] << " ";
  std::cout << "\n";

  for (int i = 0; i < m_numNodes; ++i) // {APP-Bcast-bytes	APP-Bcast-pckts} * {...} (NACK)
   std::cout << m_app_bcast_bytes[i][NACK] << " " << m_app_bcast_pckts[i][NACK] << " ";
  std::cout << "\n";
}

void
Profiler::PrintPublishData () const
{
  NS_LOG_FUNCTION (this);
  double failedPubs = 0;
  for (auto it : m_pub_failReason) failedPubs += it.second;
  std::cout << m_pub_dur.size () << " " << failedPubs << "\n"; // PUB-succ-amount PUB-fail-amount

  for (auto it : m_pub_failReason) // {PUB-fail-reason	amount} * {...}
   std::cout << Utils::GetMsgStatusString (it.first) << " " << it.second << " ";
  std::cout << "\n";

  for (int i = 0; i < m_pub_dur.size (); ++i) // {PUB-dur} * {...}
   std::cout << m_pub_dur[i] << " ";
  std::cout << "\n";

  for (int i = 0; i < m_pub_objSize.size (); ++i) // {PUB-objSize} * {...}
   std::cout << m_pub_objSize[i] << " ";
  std::cout << "\n";

  for (int i = 0; i < m_pub_keySize.size (); ++i) // {PUB-keySize} * {...}
   std::cout << (uint16_t) m_pub_keySize[i] << " ";
  std::cout << "\n";

  for (int i = 0; i < m_pub_numTags.size (); ++i) // {PUB-numTags} * {...}
   std::cout << m_pub_numTags[i] << " ";
  std::cout << "\n";

  for (int i = 0; i < m_pub_retries.size (); ++i) // {PUB-retries} * {...}
   std::cout << (uint16_t) m_pub_retries[i] << " ";
  std::cout << "\n";

  for (auto it : m_pub_timeout) // {PUB-timeout} * {...}
   if (it.second.size () > 1) std::cout << it.second[it.second.size () - 1] << " ";
  std::cout << "\n";

  for (auto it : m_pubsByNode) // {PUB-objsPerNode} * {...}
   std::cout << it.second << " ";
  std::cout << "\n";

  for (auto it : m_actRepsByNode) // {PUB-objsPerNode} * {...}
   std::cout << it.second << " ";
  std::cout << "\n";

  std::cout << m_pub_dursByTag.size () << "\n"; // numTagDur
  for (auto it : m_pub_dursByTag) // {PUB-numTags} {dur} * {...}
   {
    std::cout << it.first << " ";
    for (int i = 0; i < it.second.size (); ++i)
     std::cout << it.second[i] << " ";
    std::cout << "\n";
   }

  std::cout << m_pub_dursByRetry.size () << "\n"; // numRetryDur
  for (auto it : m_pub_dursByRetry) // {PUB-numRetry} {dur} * {...}
   {
    std::cout << (uint16_t) it.first << " ";
    for (int i = 0; i < it.second.size (); ++i)
     std::cout << it.second[i] << " ";
    std::cout << "\n";
   }
}

void
Profiler::PrintUnpublishData () const
{
  NS_LOG_FUNCTION (this);
  uint32_t failedUnpubs = 0;
  for (auto it : m_unpub_failReason) failedUnpubs += it.second;
  std::cout << m_unpub_dur.size () << " " << failedUnpubs << "\n"; // UNPUB-succ-amount UNPUB-fail-amount

  for (auto it : m_unpub_failReason) // {UNPUB-fail-reason	amount} * {...}
   std::cout << Utils::GetMsgStatusString (it.first) << " " << it.second << " ";
  std::cout << "\n";

  for (int i = 0; i < m_unpub_dur.size (); ++i) // {UNPUB-dur} * {...}
   std::cout << m_unpub_dur[i] << " ";
  std::cout << "\n";

  for (int i = 0; i < m_unpub_retries.size (); ++i) // {UNPUB-retries} * {...}
   std::cout << (uint16_t) m_unpub_retries[i] << " ";
  std::cout << "\n";

  for (auto it : m_unpub_timeout) // {UNPUB-timeout} * {...}
   if (it.second.size () > 1) std::cout << it.second[it.second.size () - 1] << " ";
  std::cout << "\n";

  for (auto it : m_unpubsByNode) // {UNPUB-objsPerNode} * {...}
   std::cout << it.second << " ";
  std::cout << "\n";

  std::cout << m_unpub_dursByTag.size () << "\n"; // numTagDur
  for (auto it : m_unpub_dursByTag) // {UNPUB-numTags} {dur} * {...}
   {
    std::cout << it.first << " ";
    for (int i = 0; i < it.second.size (); ++i)
     std::cout << it.second[i] << " ";
    std::cout << "\n";
   }

  std::cout << m_unpub_dursByRetry.size () << "\n"; // numRetryDur
  for (auto it : m_unpub_dursByRetry) // {UNPUB-numRetry} {dur} * {...}
   {
    std::cout << (uint16_t) it.first << " ";
    for (int i = 0; i < it.second.size (); ++i)
     std::cout << it.second[i] << " ";
    std::cout << "\n";
   }
}

void
Profiler::PrintDownloadData () const
{
  NS_LOG_FUNCTION (this);
  uint32_t failedDowns = 0;
  for (auto it : m_down_failReason) failedDowns += it.second;
  std::cout << m_down_dur.size () << " " << failedDowns << "\n"; // DOWN-succ-amount DOWN-fail-amount

  for (auto it : m_down_failReason) // {DOWN-fail-reason	amount} * {...}
   std::cout << Utils::GetMsgStatusString (it.first) << " " << it.second << " ";
  std::cout << "\n";

  for (int i = 0; i < m_down_dur.size (); ++i) // {DOWN-dur} * {...}
   std::cout << m_down_dur[i] << " ";
  std::cout << "\n";

  for (int i = 0; i < m_down_objSize.size (); ++i) // {DOWN-objSize} * {...}
   std::cout << m_down_objSize[i] << " ";
  std::cout << "\n";

  for (int i = 0; i < m_down_retries.size (); ++i) // {DOWN-retries} * {...}
   std::cout << (uint16_t) m_down_retries[i] << " ";
  std::cout << "\n";

  for (auto it : m_down_timeout) // {DOWN-timeout} * {...}
   if (it.second.size () > 1) std::cout << it.second[it.second.size () - 1] << " ";
  std::cout << "\n";

  std::cout << m_down_dursByRetry.size () << "\n"; // numRetryDur
  for (auto it : m_down_dursByRetry) // {DOWN-numRetry} {dur} * {...}
   {
    std::cout << (uint16_t) it.first << " ";
    for (int i = 0; i < it.second.size (); ++i)
     std::cout << it.second[i] << " ";
    std::cout << "\n";
   }
}

void
Profiler::PrintSubscribeData () const
{
  NS_LOG_FUNCTION (this);
  uint32_t failedSubs = 0;
  for (auto it : m_sub_failReason) failedSubs += it.second;
  if (m_bcast) std::cout << m_sub_dur_bcast.size () << " " << failedSubs << "\n"; // SUB-succ-amount SUB-fail-amount
  else std::cout << m_sub_dur.size () << " " << failedSubs << "\n"; // SUB-succ-amount SUB-fail-amount

  for (auto it : m_sub_failReason) // {SUB-fail-reason	amount} * {...}
   std::cout << Utils::GetMsgStatusString (it.first) << " " << it.second << " ";
  std::cout << "\n";

  if (m_bcast)
   for (auto it : m_sub_dur_bcast) // {SUB-dur} * {...}
    std::cout << it.second[it.second.size () - 1] << " ";
  else
   for (int i = 0; i < m_sub_dur.size (); ++i) // {SUB-dur} * {...}
    std::cout << m_sub_dur[i] << " ";
  std::cout << "\n";

  for (int i = 0; i < m_sub_retries.size (); ++i) // {SUB-retries} * {...}
   std::cout << (uint16_t) m_sub_retries[i] << " ";
  std::cout << "\n";

  for (auto it : m_sub_timeout) // {SUB-timeout} * {...}
   if (it.second.size () > 1) std::cout << it.second[it.second.size () - 1] << " ";
  std::cout << "\n";

  for (auto it : m_subsByNode) // {SUB-subsPerNode} * {...}
   std::cout << it.second << " ";
  std::cout << "\n";

  std::cout << m_sub_dursByTag.size () << "\n"; // numTagDur
  for (auto it : m_sub_dursByTag) // {SUB-numTags} {dur} * {...}
   {
    std::cout << it.first << " ";
    for (int i = 0; i < it.second.size (); ++i)
     std::cout << it.second[i] << " ";
    std::cout << "\n";
   }

  std::cout << m_sub_dursByRetry.size () << "\n"; // numRetryDur
  for (auto it : m_sub_dursByRetry) // {SUB-numRetry} {dur} * {...}
   {
    std::cout << (uint16_t) it.first << " ";
    for (int i = 0; i < it.second.size (); ++i)
     std::cout << it.second[i] << " ";
    std::cout << "\n";
   }
}

void
Profiler::PrintUnsubscribeData () const
{
  NS_LOG_FUNCTION (this);
  uint32_t failedUnsubs = 0;
  for (auto it : m_unsub_failReason) failedUnsubs += it.second;
  if (m_bcast) std::cout << m_unsub_dur_bcast.size () << " " << failedUnsubs << "\n"; // UNSUB-succ-amount SUB-fail-amount
  else std::cout << m_unsub_dur.size () << " " << failedUnsubs << "\n"; // UNSUB-succ-amount SUB-fail-amount

  for (auto it : m_unsub_failReason) // {UNSUB-fail-reason	amount} * {...}
   std::cout << Utils::GetMsgStatusString (it.first) << " " << it.second << " ";
  std::cout << "\n";

  if (m_bcast)
   for (auto it : m_unsub_dur_bcast) // {UNSUB-dur} * {...}
    std::cout << it.second[it.second.size () - 1] << " ";
  else
   for (int i = 0; i < m_unsub_dur.size (); ++i) // {UNSUB-dur} * {...}
    std::cout << m_unsub_dur[i] << " ";
  std::cout << "\n";

  for (int i = 0; i < m_unsub_retries.size (); ++i) // {UNSUB-retries} * {...}
   std::cout << (uint16_t) m_unsub_retries[i] << " ";
  std::cout << "\n";

  for (auto it : m_unsub_timeout) // {UNSUB-timeout} * {...}
   if (it.second.size () > 1) std::cout << it.second[it.second.size () - 1] << " ";
  std::cout << "\n";

  for (auto it : m_unsubsByNode) // {UNSUB-subsPerNode} * {...}
   std::cout << it.second << " ";
  std::cout << "\n";

  std::cout << m_unsub_dursByTag.size () << "\n"; // numTagDur
  for (auto it : m_unsub_dursByTag) // {UNSUB-numTags} {dur} * {...}
   {
    std::cout << it.first << " ";
    for (int i = 0; i < it.second.size (); ++i)
     std::cout << it.second[i] << " ";
    std::cout << "\n";
   }

  std::cout << m_unsub_dursByRetry.size () << "\n"; // numRetryDur
  for (auto it : m_unsub_dursByRetry) // {UNSUB-numRetry} {dur} * {...}
   {
    std::cout << (uint16_t) it.first << " ";
    for (int i = 0; i < it.second.size (); ++i)
     std::cout << it.second[i] << " ";
    std::cout << "\n";
   }
}

void
Profiler::PrintNotificationsData () const
{
  NS_LOG_FUNCTION (this);
  // expected-nots nots-sent nots-recvd dup-nots-recvd noid-nots
  std::cout << m_expected_nots << " " << m_nots_sent << " " << m_nots_recvd
    << " " << m_dup_nots_recvd << " " << m_noid_nots << "\n";

  std::cout << m_notsTime.size () << " " << m_nots.size () << "\n"; // nots-time-amount nots-amount

  for (int i = 0; i < m_notsTime.size (); ++i) // {not-time} * {...}
   std::cout << m_notsTime[i] << " ";
  std::cout << "\n";
}

void
Profiler::PrintPublishOperationReport () const
{
  NS_LOG_FUNCTION (this);
  double avgDur = 0.0;
  double maxDur = 0.0;
  double minDur = 0.0;

  double avgObjSize = 0.0;
  double maxObjSize = 0.0;
  double minObjSize = 0.0;

  double avgKeySize = 0.0;
  double maxKeySize = 0.0;
  double minKeySize = 0.0;

  double avgNumTags = 0.0;
  double maxNumTags = 0.0;
  double minNumTags = 0.0;
  for (int i = 0; i < m_pub_dur.size (); ++i)
   {
    avgDur += m_pub_dur[i];
    maxDur = m_pub_dur[i] > maxDur ? m_pub_dur[i] : maxDur;
    minDur = (minDur == 0 || m_pub_dur[i] < minDur) ? m_pub_dur[i] : minDur;

    avgObjSize += m_pub_objSize[i];
    maxObjSize = m_pub_objSize[i] > maxObjSize ? m_pub_objSize[i] : maxObjSize;
    minObjSize = (minObjSize == 0 || m_pub_objSize[i] < minObjSize) ?
      m_pub_objSize[i] : minObjSize;

    avgKeySize += m_pub_keySize[i];
    maxKeySize = m_pub_keySize[i] > maxKeySize ? m_pub_keySize[i] : maxKeySize;
    minKeySize = (minKeySize == 0 || m_pub_keySize[i] < minKeySize) ?
      m_pub_keySize[i] : minKeySize;

    avgNumTags += m_pub_numTags[i];
    maxNumTags = m_pub_numTags[i] > maxNumTags ? m_pub_numTags[i] : maxNumTags;
    minNumTags = (minNumTags == 0 || m_pub_numTags[i] < minNumTags) ?
      m_pub_numTags[i] : minNumTags;
   }
  avgDur = avgDur / m_pub_dur.size ();
  avgObjSize = avgObjSize / m_pub_objSize.size ();
  avgKeySize = avgKeySize / m_pub_keySize.size ();
  avgNumTags = avgNumTags / m_pub_numTags.size ();

  double failedPubs = 0;
  for (auto it : m_pub_failReason) failedPubs += it.second;
  double totalPubs = m_pub_dur.size () + failedPubs;
  double successPercent = (m_pub_dur.size () * 100.0) / totalPubs;

  std::cout << "##### PUBLISH #####\n"
    << "Success: " << successPercent << "% (" << m_pub_dur.size () << ")\n"
    << "Failure: " << (100.0 - successPercent) << "% (" << failedPubs << ")\n";

  for (auto it : m_pub_failReason)
   std::cout << "  " << Utils::GetMsgStatusString (it.first)
   << " " << (it.second * 100.0) / totalPubs << "% (" << it.second << ")\n";

  std::cout << "Duration avg: " << NsToMs (avgDur) << " max: "
    << NsToMs (maxDur) << " min: " << NsToMs (minDur) << "\n"
    << "ObjSize avg: " << avgObjSize << " max: " << maxObjSize
    << " min: " << minObjSize << "\n"
    << "KeySize avg: " << avgKeySize << " max: " << maxKeySize
    << " min: " << minKeySize << "\n"
    << "NumTags avg: " << avgNumTags << " max: " << maxNumTags
    << " min: " << minNumTags << "\n";

  double avgRetries = 0.0;
  std::map<uint8_t, uint32_t> retries;
  for (auto it : m_pub_retries)
   {
    avgRetries += it;
    auto found = retries.find (it);
    if (found != retries.end ()) found->second++;
    else retries.insert ({it, 1});
   }
  avgRetries = avgRetries / m_pub_retries.size ();
  std::cout << "Retries avg: " << avgRetries << "\n";
  for (auto it : retries)
   std::cout << "  " << (uint16_t) it.first << " " << it.second << "\n";

  double avgTimeout = 0.0;
  double maxTimeout = 0.0;
  double minTimeout = 0.0;
  int its = 0;
  for (auto it : m_pub_timeout)
   {
    if (it.second.size () > 1)
     { // received timeouts
      its++;
      double time = it.second[it.second.size () - 1];
      avgTimeout += time;
      maxTimeout = time > maxTimeout ? time : maxTimeout;
      minTimeout = (minTimeout == 0 || time < minTimeout) ? time : minTimeout;
     }
   }
  avgTimeout = its == 0 ? 0 : avgTimeout / its;
  if (its > 0)
   std::cout << "Timeout avg: " << NsToMs (avgTimeout)
   << " max: " << NsToMs (maxTimeout)
   << " min: " << NsToMs (minTimeout) << "\n";

  double totalObjs = 0.0;
  double maxObjs = 0.0;
  double minObjs = 0.0;
  for (auto it : m_pubsByNode)
   {
    totalObjs += it.second;
    maxObjs = it.second > maxObjs ? it.second : maxObjs;
    minObjs = (minObjs == 0 || it.second < minObjs) ? it.second : minObjs;
   }
  double avgObjsPerNode = totalObjs / m_numNodes;
  std::cout << "ObjsPerNode avg: " << avgObjsPerNode
    << " total: " << totalObjs
    << " max: " << maxObjs
    << " min: " << minObjs << "\n";
}

void
Profiler::PrintUnpublishOperationReport () const
{
  NS_LOG_FUNCTION (this);
  double avgDur = 0.0;
  double maxDur = 0.0;
  double minDur = 0.0;
  for (int i = 0; i < m_unpub_dur.size (); ++i)
   {
    avgDur += m_unpub_dur[i];
    maxDur = m_unpub_dur[i] > maxDur ? m_unpub_dur[i] : maxDur;
    minDur = (minDur == 0 || m_unpub_dur[i] < minDur) ? m_unpub_dur[i] : minDur;
   }
  avgDur = avgDur / m_unpub_dur.size ();

  uint32_t failedUnpubs = 0;
  for (auto it : m_unpub_failReason) failedUnpubs += it.second;
  uint32_t totalUnpubs = m_unpub_dur.size () + failedUnpubs;
  double successPercent = (m_unpub_dur.size () * 100.0) / totalUnpubs;

  std::cout << "##### UNPUBLISH #####\n"
    << "Success: " << successPercent << "% (" << m_unpub_dur.size () << ")\n"
    << "Failure: " << (100.0 - successPercent) << "% (" << failedUnpubs << ")\n";

  for (auto it : m_unpub_failReason)
   std::cout << "  " << Utils::GetMsgStatusString (it.first)
   << " " << (it.second * 100.0) / totalUnpubs << "% (" << it.second << ")\n";

  std::cout << "Duration avg: " << NsToMs (avgDur)
    << " max: " << NsToMs (maxDur) << " min: " << NsToMs (minDur) << "\n";

  double avgRetries = 0.0;
  std::map<uint8_t, uint32_t> retries;
  for (auto it : m_unpub_retries)
   {
    avgRetries += it;
    auto found = retries.find (it);
    if (found != retries.end ()) found->second++;
    else retries.insert ({it, 1});
   }
  avgRetries = avgRetries / m_unpub_retries.size ();
  std::cout << "Retries avg: " << avgRetries << "\n";
  for (auto it : retries)
   std::cout << "  " << (uint16_t) it.first << " " << it.second << "\n";

  double avgTimeout = 0.0;
  double maxTimeout = 0.0;
  double minTimeout = 0.0;
  int its = 0;
  for (auto it : m_unpub_timeout)
   {
    if (it.second.size () > 1)
     { // received timeouts
      its++;
      double time = it.second[it.second.size () - 1];
      avgTimeout += time;
      maxTimeout = time > maxTimeout ? time : maxTimeout;
      minTimeout = (minTimeout == 0 || time < minTimeout) ? time : minTimeout;
     }
   }
  avgTimeout = its == 0 ? 0 : avgTimeout / its;
  if (its > 0)
   std::cout << "Timeout avg: " << NsToMs (avgTimeout)
   << " max: " << NsToMs (maxTimeout)
   << " min: " << NsToMs (minTimeout) << "\n";
}

void
Profiler::PrintDownloadOperationReport () const
{
  NS_LOG_FUNCTION (this);
  double avgDur = 0.0;
  double maxDur = 0.0;
  double minDur = 0.0;
  for (int i = 0; i < m_down_dur.size (); ++i)
   {
    avgDur += m_down_dur[i];
    maxDur = m_down_dur[i] > maxDur ? m_down_dur[i] : maxDur;
    minDur = (minDur == 0 || m_down_dur[i] < minDur) ? m_down_dur[i] : minDur;
   }
  avgDur = avgDur / m_down_dur.size ();

  uint32_t failedDowns = 0;
  for (auto it : m_down_failReason) failedDowns += it.second;
  uint32_t totalDowns = m_down_dur.size () + failedDowns;
  double successPercent = (m_down_dur.size () * 100.0) / totalDowns;
  double failurePercent = 100.0 - successPercent;

  std::cout << "##### DOWNLOAD #####\n"
    << "Success: " << successPercent << "% (" << m_down_dur.size () << ")\n"
    << "Failure: " << failurePercent << "% (" << failedDowns << ")\n";
  for (auto it : m_down_failReason)
   std::cout << "  " << Utils::GetMsgStatusString (it.first)
   << " " << (it.second * 100.0) / totalDowns << "% (" << it.second << ")\n";

  std::cout << "Duration avg: " << NsToMs (avgDur)
    << " max: " << NsToMs (maxDur) << " min: " << NsToMs (minDur) << "\n";

  double avgObjSize = 0.0;
  double maxObjSize = 0.0;
  double minObjSize = 0.0;
  for (int i = 0; i < m_down_objSize.size (); ++i)
   {
    avgObjSize += m_down_objSize[i];
    maxObjSize = m_down_objSize[i] > maxObjSize ? m_down_objSize[i] : maxObjSize;
    minObjSize = (minObjSize == 0 || m_down_objSize[i] < minObjSize) ?
      m_down_objSize[i] : minObjSize;
   }
  avgObjSize = avgObjSize / m_down_objSize.size ();
  std::cout << "ObjSize avg: " << avgObjSize << " max: " << maxObjSize
    << " min: " << minObjSize << "\n";

  double avgRetries = 0.0;
  std::map<uint8_t, uint32_t> retries;
  for (auto it : m_down_retries)
   {
    avgRetries += it;
    auto found = retries.find (it);
    if (found != retries.end ()) found->second++;
    else retries.insert ({it, 1});
   }
  avgRetries = avgRetries / m_down_retries.size ();
  std::cout << "Retries avg: " << avgRetries << "\n";
  for (auto it : retries)
   std::cout << "  " << (uint16_t) it.first << " " << it.second << "\n";

  double avgTimeout = 0.0;
  double maxTimeout = 0.0;
  double minTimeout = 0.0;
  int its = 0;
  for (auto it : m_down_timeout)
   {
    if (it.second.size () > 1)
     { // received timeouts
      its++;
      double time = it.second[it.second.size () - 1];
      avgTimeout += time;
      maxTimeout = time > maxTimeout ? time : maxTimeout;
      minTimeout = (minTimeout == 0 || time < minTimeout) ? time : minTimeout;
     }
   }
  avgTimeout = its == 0 ? 0 : avgTimeout / its;
  if (its > 0)
   std::cout << "Timeout avg: " << NsToMs (avgTimeout)
   << " max: " << NsToMs (maxTimeout)
   << " min: " << NsToMs (minTimeout) << "\n";
}

void
Profiler::PrintSubscribeOperationReport () const
{
  NS_LOG_FUNCTION (this);
  double avgDur = 0.0;
  double maxDur = 0.0;
  double minDur = 0.0;
  if (!m_bcast)
   {
    for (int i = 0; i < m_sub_dur.size (); ++i)
     {
      avgDur += m_sub_dur[i];
      maxDur = m_sub_dur[i] > maxDur ? m_sub_dur[i] : maxDur;
      minDur = (minDur == 0 || m_sub_dur[i] < minDur) ? m_sub_dur[i] : minDur;
     }
    avgDur = avgDur / m_sub_dur.size ();
   }
  else
   {
    for (auto it : m_sub_dur_bcast)
     {
      avgDur += it.second[it.second.size () - 1];
      maxDur = it.second[it.second.size () - 1] > maxDur ? it.second[it.second.size () - 1] : maxDur;
      minDur = (minDur == 0 || it.second[it.second.size () - 1] < minDur) ? it.second[it.second.size () - 1] : minDur;
     }
    avgDur = avgDur / m_sub_dur_bcast.size ();
   }

  uint32_t failedSubs = 0;
  for (auto it : m_sub_failReason) failedSubs += it.second;
  uint32_t totalSubs = (m_bcast ? m_sub_dur_bcast.size () : m_sub_dur.size ()) + failedSubs;
  double successPercent = ((m_bcast ? m_sub_dur_bcast.size () : m_sub_dur.size ()) * 100.0) / totalSubs;
  double failurePercent = 100.0 - successPercent;

  std::cout << "##### SUBSCRIBE #####\n"
    << "Success: " << successPercent << "% (" << (m_bcast ? m_sub_dur_bcast.size () : m_sub_dur.size ()) << ")\n"
    << "Failure: " << failurePercent << "% (" << failedSubs << ")\n";
  for (auto it = m_sub_failReason.begin (); it != m_sub_failReason.end (); ++it)
   std::cout << "  " << Utils::GetMsgStatusString (it->first)
   << " " << (it->second * 100.0) / totalSubs << "% (" << it->second << ")\n";
  std::cout << "Duration avg: " << NsToMs (avgDur)
    << " max: " << NsToMs (maxDur) << " min: " << NsToMs (minDur) << "\n";

  double avgRetries = 0.0;
  std::map<uint8_t, uint32_t> retries;
  for (auto it : m_sub_retries)
   {
    avgRetries += it;
    auto found = retries.find (it);
    if (found != retries.end ()) found->second++;
    else retries.insert ({it, 1});
   }
  avgRetries = avgRetries / m_sub_retries.size ();
  std::cout << "Retries avg: " << avgRetries << "\n";
  for (auto it : retries)
   std::cout << "  " << (uint16_t) it.first << " " << it.second << "\n";

  double avgTimeout = 0.0;
  double maxTimeout = 0.0;
  double minTimeout = 0.0;
  int its = 0;
  for (auto it : m_sub_timeout)
   {
    if (it.second.size () > 1)
     { // received timeouts
      its++;
      double time = it.second[it.second.size () - 1];
      avgTimeout += time;
      maxTimeout = time > maxTimeout ? time : maxTimeout;
      minTimeout = (minTimeout == 0 || time < minTimeout) ? time : minTimeout;
     }
   }
  avgTimeout = its == 0 ? 0 : avgTimeout / its;
  if (its > 0)
   std::cout << "Timeout avg: " << NsToMs (avgTimeout)
   << " max: " << NsToMs (maxTimeout)
   << " min: " << NsToMs (minTimeout) << "\n";

  double _totalSubs = 0.0;
  double maxSubs = 0.0;
  double minSubs = 0.0;
  for (auto it : m_subsByNode)
   {
    _totalSubs += it.second;
    maxSubs = it.second > maxSubs ? it.second : maxSubs;
    minSubs = (minSubs == 0 || it.second < minSubs) ? it.second : minSubs;
   }
  double avgSubsPerNode = _totalSubs / m_numNodes;
  std::cout << "SubsPerNode avg: " << avgSubsPerNode
    << " total: " << _totalSubs
    << " max: " << maxSubs
    << " min: " << minSubs << "\n";
}

void
Profiler::PrintUnsubscribeOperationReport () const
{
  NS_LOG_FUNCTION (this);
  double avgDur = 0.0;
  double maxDur = 0.0;
  double minDur = 0.0;
  if (!m_bcast)
   {
    for (int i = 0; i < m_unsub_dur.size (); ++i)
     {
      avgDur += m_unsub_dur[i];
      maxDur = m_unsub_dur[i] > maxDur ? m_unsub_dur[i] : maxDur;
      minDur = (minDur == 0 || m_unsub_dur[i] < minDur) ? m_unsub_dur[i] : minDur;
     }
    avgDur = avgDur / m_unsub_dur.size ();
   }
  else
   {
    for (auto it : m_unsub_dur_bcast)
     {
      avgDur += it.second[it.second.size () - 1];
      maxDur = it.second[it.second.size () - 1] > maxDur ? it.second[it.second.size () - 1] : maxDur;
      minDur = (minDur == 0 || it.second[it.second.size () - 1] < minDur) ? it.second[it.second.size () - 1] : minDur;
     }
    avgDur = avgDur / m_unsub_dur_bcast.size ();
   }

  uint32_t failedUnsubs = 0;
  for (auto it : m_unsub_failReason) failedUnsubs += it.second;
  uint32_t totalUnsubs = (m_bcast ? m_unsub_dur_bcast.size () : m_unsub_dur.size ()) + failedUnsubs;
  double successPercent = ((m_bcast ? m_unsub_dur_bcast.size () : m_unsub_dur.size ()) * 100.0) / totalUnsubs;
  double failurePercent = 100.0 - successPercent;

  std::cout << "##### UNSUBSCRIBE #####\n"
    << "Success: " << successPercent << "% (" << (m_bcast ? m_unsub_dur_bcast.size () : m_unsub_dur.size ()) << ")\n"
    << "Failure: " << failurePercent << "% (" << failedUnsubs << ")\n";
  for (auto it = m_unsub_failReason.begin (); it != m_unsub_failReason.end (); ++it)
   std::cout << "  " << Utils::GetMsgStatusString (it->first)
   << " " << (it->second * 100.0) / totalUnsubs << "% (" << it->second << ")\n";
  std::cout << "Duration avg: " << NsToMs (avgDur)
    << " max: " << NsToMs (maxDur) << " min: " << NsToMs (minDur) << "\n";

  double avgRetries = 0.0;
  std::map<uint8_t, uint32_t> retries;
  for (auto it : m_unsub_retries)
   {
    avgRetries += it;
    auto found = retries.find (it);
    if (found != retries.end ()) found->second++;
    else retries.insert ({it, 1});
   }
  avgRetries = avgRetries / m_unsub_retries.size ();
  std::cout << "Retries avg: " << avgRetries << "\n";
  for (auto it : retries)
   std::cout << "  " << (uint16_t) it.first << " " << it.second << "\n";

  double avgTimeout = 0.0;
  double maxTimeout = 0.0;
  double minTimeout = 0.0;
  int its = 0;
  for (auto it : m_unsub_timeout)
   {
    if (it.second.size () > 1)
     { // received timeouts
      its++;
      double time = it.second[it.second.size () - 1];
      avgTimeout += time;
      maxTimeout = time > maxTimeout ? time : maxTimeout;
      minTimeout = (minTimeout == 0 || time < minTimeout) ? time : minTimeout;
     }
   }
  avgTimeout = its == 0 ? 0 : avgTimeout / its;
  if (its > 0)
   std::cout << "Timeout avg: " << NsToMs (avgTimeout)
   << " max: " << NsToMs (maxTimeout)
   << " min: " << NsToMs (minTimeout) << "\n";

  double _totalUnsubs = 0.0;
  double maxUnsubs = 0.0;
  double minUnsubs = 0.0;
  for (auto it : m_unsubsByNode)
   {
    _totalUnsubs += it.second;
    maxUnsubs = it.second > maxUnsubs ? it.second : maxUnsubs;
    minUnsubs = (minUnsubs == 0 || it.second < minUnsubs) ? it.second : minUnsubs;
   }
  double avgUnsubsPerNode = _totalUnsubs / m_numNodes;
  std::cout << "UnsubsPerNode avg: " << avgUnsubsPerNode
    << " total: " << _totalUnsubs
    << " max: " << maxUnsubs
    << " min: " << minUnsubs << "\n";
}

void
Profiler::PrintSubscriptionNotificationReport () const
{
  NS_LOG_FUNCTION (this);
  std::cout << "##### NOTIFICATION #####\n"
    << "Expected: " << m_expected_nots << " Sent: " << m_nots_sent
    << " Recvd: " << m_nots_recvd << "\n"
    << "Dups: " << m_dup_nots_recvd << " NoId: " << m_noid_nots << "\n";

  std::cout << m_notsTime.size () << " " << m_nots.size () << "\n";

  double avgDur = 0.0;
  double maxDur = 0.0;
  double minDur = 0.0;
  for (int i = 0; i < m_notsTime.size (); ++i)
   {
    avgDur += m_notsTime[i];
    maxDur = m_notsTime[i] > maxDur ? m_notsTime[i] : maxDur;
    minDur = (minDur == 0 || m_notsTime[i] < minDur) ? m_notsTime[i] : minDur;
   }
  avgDur = avgDur / m_notsTime.size ();
  std::cout << "Duration avg: " << NsToMs (avgDur)
    << " max: " << NsToMs (maxDur) << " min: " << NsToMs (minDur) << "\n";
}

}
}

