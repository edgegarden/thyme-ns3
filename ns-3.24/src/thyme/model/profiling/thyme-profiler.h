/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2015 NOVA LINCS DI-FCT-UNL <http://nova-lincs.di.fct.unl.pt/>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Joao A. Silva <jaa.silva@campus.fct.unl.pt>
 */
#ifndef THYME_PROFILER_H
#define THYME_PROFILER_H

#include <ns3/ipv4-l3-protocol.h>
#include <ns3/node-container.h>
#include <ns3/thyme-common.h>
#include <ns3/thyme-request.h>

namespace ns3 {
namespace hyrax {

class Profiler : public Object
{
private:
  NodeContainer m_nodes;
  uint32_t m_numNodes;
  bool m_bcast;

  std::vector<uint32_t> m_phy_rx_bytes;
  std::vector<uint32_t> m_phy_rx_pckts;
  std::vector<uint32_t> m_phy_rx_drop_bytes;
  std::vector<uint32_t> m_phy_rx_drop_pckts;
  std::vector<uint32_t> m_phy_tx_bytes;
  std::vector<uint32_t> m_phy_tx_pckts;
  std::vector<uint32_t> m_phy_tx_drop_bytes;
  std::vector<uint32_t> m_phy_tx_drop_pckts;

  std::vector<uint32_t> m_mac_rx_bytes;
  std::vector<uint32_t> m_mac_rx_pckts;
  std::vector<uint32_t> m_mac_rx_drop_bytes;
  std::vector<uint32_t> m_mac_rx_drop_pckts;
  std::vector<uint32_t> m_mac_tx_bytes;
  std::vector<uint32_t> m_mac_tx_pckts;
  std::vector<uint32_t> m_mac_tx_drop_bytes;
  std::vector<uint32_t> m_mac_tx_drop_pckts;
  std::vector<uint32_t> m_mac_tx_failed;
  std::vector<uint32_t> m_mac_tx_final_failed;

  std::vector<uint32_t> m_ip_rx_bytes;
  std::vector<uint32_t> m_ip_rx_pckts;
  std::vector<uint32_t> m_ip_tx_bytes;
  std::vector<uint32_t> m_ip_tx_pckts;
  std::vector<std::vector<uint32_t> > m_ip_drop_bytes; // idx 0 is not used
  std::vector<std::vector<uint32_t> > m_ip_drop_pckts; // idx 0 is not used
  std::vector<uint32_t> m_ip_fwd_bytes;
  std::vector<uint32_t> m_ip_fwd_pckts;

  std::vector<uint32_t> m_ctl_tx_bytes;
  std::vector<uint32_t> m_ctl_tx_pckts;
  std::vector<uint32_t> m_ctl_rx_bytes;
  std::vector<uint32_t> m_ctl_rx_pckts;
  // < (lastGreedy, firstPerHop), numReps >
  std::map<std::pair<uint16_t, uint16_t>, uint64_t> m_ctl_rep_paths;

  // < packetId, < numHops, packetSize > >
  std::map<uint64_t, std::pair<uint32_t, uint32_t> > m_hopsHistogram;
  // < packetId >
  std::set<uint64_t> m_downPckts;
  // < packetId >
  std::set<uint64_t> m_downRspPckts;

  static const int NUM_OPS = 10;
  std::vector<std::vector<uint32_t> > m_app_tx_bytes;
  std::vector<std::vector<uint32_t> > m_app_tx_pckts;
  std::vector<std::vector<uint32_t> > m_app_bcast_bytes;
  std::vector<std::vector<uint32_t> > m_app_bcast_pckts;
  std::vector<std::vector<uint32_t> > m_app_rx_bytes;
  std::vector<std::vector<uint32_t> > m_app_rx_pckts;

  typedef std::pair<Ipv4Address, uint32_t> Key; // < nodeAddr, reqId >

  struct PubRecord
  {
    int64_t start; // nanoseconds
    Ptr<ThymeObject> obj;
  };

  std::map<Key, PubRecord> m_pubRecs; // holds ongoing publish ops
  std::map<Ipv4Address, uint32_t> m_pubsByNode; // < nodeAddr, numPubs >
  std::vector<int64_t> m_pub_dur; // nanoseconds
  std::vector<uint32_t> m_pub_objSize; // bytes
  std::vector<uint8_t> m_pub_keySize; // bytes
  std::vector<uint16_t> m_pub_numTags;
  std::vector<uint8_t> m_pub_retries;
  std::map<uint16_t, std::vector<int64_t> > m_pub_dursByTag; // nanoseconds
  std::map<uint8_t, std::vector<int64_t> > m_pub_dursByRetry; // nanoseconds
  // < tag, < objMeta > >
  std::map<std::string, std::vector<Ptr<ObjectMetadata> > > m_pubsByTag;
  std::map<Key, std::vector<int64_t> > m_pub_timeout; // nanoseconds
  std::map<MessageStatus, uint32_t> m_pub_failReason;
  std::map<Ipv4Address, uint32_t> m_actRepsByNode; // < nodeAddr, numReps >

  struct UnpubRecord
  {
    int64_t start; // nanoseconds
    Ptr<ThymeObject> obj;
  };

  std::map<Key, UnpubRecord> m_unpubRecs; // holds ongoing unpublish ops
  std::map<Ipv4Address, uint64_t> m_unpubsByNode; // < nodeAddr, numUnpubs >
  std::vector<int64_t> m_unpub_dur; // nanoseconds
  std::vector<uint8_t> m_unpub_retries;
  std::map<uint16_t, std::vector<int64_t> > m_unpub_dursByTag; // nanoseconds
  std::map<uint8_t, std::vector<int64_t> > m_unpub_dursByRetry; // nanoseconds
  std::map<Key, std::vector<int64_t> > m_unpub_timeout; // nanoseconds
  std::map<MessageStatus, uint32_t> m_unpub_failReason;

  struct DownRecord
  {
    int64_t start; // nanoseconds
  };

  std::map<Key, DownRecord> m_downRecs; // holds ongoing download ops
  std::vector<int64_t> m_down_dur; // nanoseconds
  std::vector<uint32_t> m_down_objSize; // bytes
  std::vector<uint8_t> m_down_retries;
  std::map<uint8_t, std::vector<int64_t> > m_down_dursByRetry; // nanoseconds
  std::map<Key, std::vector<int64_t> > m_down_timeout; // nanoseconds
  std::map<MessageStatus, uint32_t> m_down_failReason;

  struct SubRecord
  {
    int64_t start; // nanoseconds
    Ptr<Subscription> sub;
  };

  std::map<Key, SubRecord> m_subRecs; // holds ongoing subscribe ops
  std::map<Ipv4Address, uint64_t> m_subsByNode; // < nodeAddr, numSubs >
  std::vector<int64_t> m_sub_dur; // nanoseconds
  std::map<Key, std::vector<int64_t> > m_sub_dur_bcast; // nanoseconds
  std::vector<uint8_t> m_sub_retries;
  std::map<uint16_t, std::vector<int64_t> > m_sub_dursByTag; // nanoseconds
  std::map<uint8_t, std::vector<int64_t> > m_sub_dursByRetry; // nanoseconds
  std::map<Key, std::vector<int64_t> > m_sub_timeout; // nanoseconds
  std::map<MessageStatus, uint32_t> m_sub_failReason;
  std::vector<Ptr<Subscription> > m_subs;
  std::map<Key, int64_t> m_subTime; // < (nodeAddr, subId), time >

  struct UnsubRecord
  {
    int64_t start; // nanoseconds
    Ptr<Subscription> sub;
  };

  std::map<Key, UnsubRecord> m_unsubRecs; // holds ongoing unsubscribe ops
  std::vector<int64_t> m_unsub_dur; // nanoseconds
  std::map<Key, std::vector<int64_t> > m_unsub_dur_bcast; // nanoseconds
  std::vector<uint8_t> m_unsub_retries;
  std::map<Ipv4Address, uint64_t> m_unsubsByNode; // < nodeAddr, numUnsubs >
  std::map<uint16_t, std::vector<int64_t> > m_unsub_dursByTag; // nanoseconds
  std::map<uint8_t, std::vector<int64_t> > m_unsub_dursByRetry; // nanoseconds
  std::map<Key, std::vector<int64_t> > m_unsub_timeout; // nanoseconds
  std::map<MessageStatus, uint32_t> m_unsub_failReason;

  struct Notif
  {
    Ipv4Address sender;
    Ipv4Address recvr;
    uint32_t subId;
    Ptr<ObjectMetadata> meta;
    int64_t start; // nanoseconds
  };

  std::vector<Notif> m_nots;
  std::vector<int64_t> m_notsTime;

  uint64_t m_nots_sent = 0;
  uint64_t m_nots_recvd = 0;
  uint64_t m_dup_nots_recvd = 0;
  uint64_t m_expected_nots = 0;
  uint64_t m_noid_nots = 0;

  std::map<uint64_t, MessageType> m_nacks;
  std::map<MessageType, uint32_t> m_nacksSentPerOp;
  std::map<MessageType, uint32_t> m_nacksRecvdPerOp;
  std::vector<uint32_t> m_nackAddrs;

  std::map<uint64_t, MessageType> m_txs;
  std::map<MessageType, uint32_t> m_txs2;
  std::map<uint64_t, Time> _a;
  std::map<uint64_t, uint32_t> m_aaa;

  enum Op
  {
    PUB,
    UNPUB,
    DOWN,
    SUB,
    UNSUB,
    NOT,
    CTRL,
    ACT_REP,
    PASS_REP,
    NACK,
  };

public:
  static TypeId GetTypeId (void);
  Profiler ();
  Profiler (NodeContainer nodes, bool bcast);
  virtual ~Profiler ();

  void RegisterNode (Ipv4Address addr);

  /**
   * The trace source fired when a packet ends the reception process from
   * the medium.
   */
  void OnPhyRxEnd (std::string ctx, Ptr<const Packet> p);
  /**
   * The trace source fired when the phy layer drops a packet it has received.
   */
  void OnPhyRxDrop (std::string ctx, Ptr<const Packet> p);
  /**
   * The trace source fired when a packet begins the transmission process on
   * the medium.
   */
  void OnPhyTxBegin (std::string ctx, Ptr<const Packet> p);
  /**
   * The trace source fired when the phy layer drops a packet as it tries
   * to transmit it.
   */
  void OnPhyTxDrop (std::string ctx, Ptr<const Packet> p);

  /**
   * The trace source fired for packets successfully received by the device
   * immediately before being forwarded up to higher layers (at the L2/L3
   * transition).  This is a non-promiscuous trace.
   */
  void OnMacRx (std::string ctx, Ptr<const Packet> p);
  /**
   * The trace source fired when packets coming into the "top" of the device
   * are dropped at the MAC layer during reception.
   */
  void OnMacRxDrop (std::string ctx, Ptr<const Packet> p);
  /**
   * The trace source fired when packets come into the "top" of the device
   * at the L3/L2 transition, before being queued for transmission.
   */
  void OnMacTx (std::string ctx, Ptr<const Packet> p);
  /**
   * The trace source fired when packets coming into the "top" of the device
   * are dropped at the MAC layer during transmission.
   */
  void OnMacTxDrop (std::string ctx, Ptr<const Packet> p);
  /**
   * The trace source fired when the transmission of a single data packet has 
   * failed
   */
  void OnMacTxDataFailed (std::string ctx, Mac48Address addr);
  /**
   * The trace source fired when the transmission of a data packet has
   * exceeded the maximum number of attempts
   */
  void OnMacTxFinalDataFailed (std::string ctx, Mac48Address addr);

  // Trace of locally delivered packets
  void OnIpIn (std::string ctx, const Ipv4Header &h, Ptr<const Packet> p,
               uint32_t i);
  // Trace of sent packets
  void OnIpOut (std::string ctx, const Ipv4Header &h, Ptr<const Packet> p,
                uint32_t i);
  // Trace of dropped packets
  void OnIpDrop (std::string ctx, const Ipv4Header &h, Ptr<const Packet> p,
                 Ipv4L3Protocol::DropReason r, Ptr<Ipv4> ip, uint32_t i);
  // Trace of unicast forwarded packets
  void OnIpFwd (std::string ctx, const Ipv4Header &h, Ptr<const Packet> p,
                uint32_t i); // **not called by DCS. only by PL;SG**

  // **not called by PL;SG. only by DCS**
  void OnCtlTx (uint32_t id, uint32_t pcktSize);
  // **not called by PL;SG. only by DCS**
  void OnCtlRx (uint32_t id, uint32_t pcktSize);
  // **not called by PL;SG. only by DCS**
  void OnCtlFwd (uint32_t id, uint32_t pcktSize);
  // **not called by PL;SG. only by DCS**
  void OnRepeatingPath (uint16_t lastGreedy, uint16_t firstPerHop);

  void RegisterDownloadPacket (uint64_t puid);
  void RegisterDownloadRspPacket (uint64_t puid);

  void OnAppTx (uint32_t id, MessageType type, uint32_t pcktSize);
  void OnAppBcast (uint32_t id, MessageType type, uint32_t pcktSize);
  void OnAppRx (uint32_t id, MessageType type, uint32_t pcktSize);

  void OnPublishStart (Ipv4Address addr, uint32_t reqId, Ptr<ThymeObject> obj);
  void OnPublishSuccess (Ipv4Address addr, uint32_t reqId, uint8_t retries);
  void OnPublishFailure (Ipv4Address addr, uint32_t reqId, MessageStatus st);
  void OnPublishReceived (Ipv4Address rcvr);
  void OnPublishRspReceived (Ipv4Address addr, uint32_t reqId);
  void OnActiveReplicaReceived (Ipv4Address rcvr);

  void OnUnpublishStart (Ipv4Address addr, uint32_t reqId,
                         Ptr<ThymeObject> obj);
  void OnUnpublishSuccess (Ipv4Address addr, uint32_t reqId, uint8_t retries);
  void OnUnpublishFailure (Ipv4Address addr, uint32_t reqId, MessageStatus st);
  void OnUnpublishReceived (Ipv4Address rcvr);
  void OnUnpublishRspReceived (Ipv4Address addr, uint32_t reqId);

  void OnDownloadStart (Ipv4Address addr, uint32_t reqId);
  void OnDownloadSuccess (Ipv4Address addr, uint32_t reqId,
                          Ptr<ThymeObject> obj, uint8_t retries);
  void OnDownloadFailure (Ipv4Address addr, uint32_t reqId, MessageStatus st);
  void OnDownloadRspReceived (Ipv4Address addr, uint32_t reqId);

  void OnSubscribeStart (Ipv4Address addr, uint32_t reqId,
                         Ptr<Subscription> sub);
  void OnSubscribeSuccess (Ipv4Address addr, uint32_t reqId, uint8_t retries);
  void OnSubscribeFailure (Ipv4Address addr, uint32_t reqId, MessageStatus st);
  void OnSubscribeReceived (Ipv4Address rcvr, Ipv4Address addr,
                            uint32_t reqId);
  void OnSubscribeRspReceived (Ipv4Address addr, uint32_t reqId);

  void OnUnsubscribeStart (Ipv4Address addr, uint32_t reqId,
                           Ptr<Subscription> sub);
  void OnUnsubscribeSuccess (Ipv4Address addr, uint32_t reqId,
                             uint8_t retries);
  void OnUnsubscribeFailure (Ipv4Address addr, uint32_t reqId,
                             MessageStatus st);
  void OnUnsubscribeReceived (Ipv4Address rcvr, Ipv4Address addr,
                              uint32_t reqId);
  void OnUnsubscribeRspReceived (Ipv4Address addr, uint32_t reqId);

  void OnNotificationSent (Ipv4Address sender, Ipv4Address addr,
                           uint32_t subId, Ptr<ObjectMetadata> meta);
  void OnNotificationReceived (Ipv4Address sender, Ipv4Address addr,
                               Ptr<Subscription> sub, Ptr<ObjectMetadata> meta,
                               uint32_t nots);
  void OnDuplicateNotificationReceived (Ipv4Address addr,
                                        Ptr<Subscription> sub,
                                        Ptr<ObjectMetadata> meta);
  void OnNoIdNotification (uint32_t numMetas);

  void OnNackSent (uint64_t puid, MessageType type, std::vector<Ipv4Address> nodes);
  void OnNackReceived (uint64_t puid);

  void OnTx (uint64_t puid, MessageType type, uint32_t size);
  void OnTxOk (uint64_t puid);
  void OnTxError (uint64_t puid);

  void PrintReport ();

private:
  void HandlePacketsHistogram (uint64_t puid, uint32_t size);
  void InsertPublication (Ptr<ThymeObject> obj);
  void RemovePublication (Ptr<ThymeObject> obj);
  void RemoveSubscription (Ipv4Address addr, uint32_t subId);

  void CheckAllSubscriptions (Ptr<ThymeObject> obj);
  void CheckAllPublications (Ptr<Subscription> sub);

  Op GetOp (MessageType type) const;
  uint32_t GetIdFromContext (std::string ctx) const;
  double NsToMs (double val) const;

  void PrintPhyData () const;
  void PrintMacData () const;
  void PrintIpData () const;
  void PrintHistogramData () const;
  void PrintCtrlData ();
  void PrintAppRxData () const;
  void PrintAppTxData () const;
  void PrintAppBcastData () const;
  void PrintPublishData () const;
  void PrintUnpublishData () const;
  void PrintDownloadData () const;
  void PrintSubscribeData () const;
  void PrintUnsubscribeData () const;
  void PrintNotificationsData () const;

  void PrintPublishOperationReport () const;
  void PrintUnpublishOperationReport () const;
  void PrintDownloadOperationReport () const;
  void PrintSubscribeOperationReport () const;
  void PrintUnsubscribeOperationReport () const;
  void PrintSubscriptionNotificationReport () const;
};

}
}

#endif /* THYME_PROFILER_H */

