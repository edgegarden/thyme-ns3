/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2015 NOVA LINCS DI-FCT-UNL <http://nova-lincs.di.fct.unl.pt/>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Joao A. Silva <jaa.silva@campus.fct.unl.pt>
 */
#include "thyme.h"
#include "thyme-subscription-object.h"
#include <ns3/abort.h>
#include <ns3/boolean.h>
#include <ns3/double.h>
#include <ns3/log.h>
#include <ns3/thyme-utils.h>
#include <ns3/uinteger.h>

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("Thyme");

namespace hyrax {

NS_OBJECT_ENSURE_REGISTERED (Thyme);

TypeId
Thyme::GetTypeId (void)
{
  NS_LOG_FUNCTION_NOARGS ();
  static TypeId tid = TypeId ("ns3::hyrax::Thyme")
    .SetParent<ThymeApi> ()
    .AddConstructor<Thyme> ()

    .AddAttribute ("Handoff",
    "Handoff timeout interval (ms).",
    TimeValue (MilliSeconds (HANDOFF_INT)),
    MakeTimeAccessor (&Thyme::m_handoffInt),
    MakeTimeChecker ())

    .AddAttribute ("MaxNeighTtl",
    "Maximum msgs TTL when asking your neighbor.",
    UintegerValue (MAX_NEIGH_TTL),
    MakeUintegerAccessor (&Thyme::m_maxNeighborTtl),
    MakeUintegerChecker<uint32_t> ())

    .AddAttribute ("DynTimeout",
    "Calculate dynamic timeout flag.",
    BooleanValue (DYN_TIMEOUT),
    MakeBooleanAccessor (&Thyme::m_dynTimeout),
    MakeBooleanChecker ())

    .AddAttribute ("RouteLatency",
    "Transmission latency from cell to cell (ms).",
    UintegerValue (ROUTE_LATENCY),
    MakeUintegerAccessor (&Thyme::m_routeLatency),
    MakeUintegerChecker<uint16_t> ())

    .AddAttribute ("TimeoutFactor",
    "Timeout multiplication factor.",
    DoubleValue (TIMEOUT_FACTOR),
    MakeDoubleAccessor (&Thyme::m_timeoutFactor),
    MakeDoubleChecker<double> ())

    .AddAttribute ("OpTimeoutFactor",
    "Operations timeout multiplication factor.",
    DoubleValue (OP_TIMEOUT_FACTOR),
    MakeDoubleAccessor (&Thyme::m_opTimeoutFactor),
    MakeDoubleChecker<double> ())

    .AddAttribute ("NackOpt",
    "Send NACKs for NODE msgs.",
    BooleanValue (NACK_OPT),
    MakeBooleanAccessor (&Thyme::m_nackOpt),
    MakeBooleanChecker ());

  return tid;
}

Thyme::Thyme () : ThymeApi ()
{
  NS_LOG_FUNCTION (this);
}

Thyme::~Thyme ()
{
  NS_LOG_FUNCTION (this);
}

Ptr<const RoutingProtocol>
Thyme::GetRoutingProtocol () const
{
  NS_LOG_FUNCTION (this);
  return m_routing;
}

Ptr<const GridManager>
Thyme::GetGridManager () const
{
  NS_LOG_FUNCTION (this);
  return m_gridMan;
}

void
Thyme::DoPublish (Ptr<ThymeObject> obj)
{
  NS_LOG_FUNCTION (this << obj);
  // create request
  uint16_t cell = m_routing->GetMyCell ();
  Ptr<PublishRequest> req = CreatePublishRequest (obj, cell);
  uint32_t reqId = req->GetRequestId ();
  m_profiler->OnPublishStart (m_addr, reqId, obj);

  // save publication locally
  Ptr<ObjectIdentifier> objId = obj->GetObjectIdentifier ();
  ObjectIdentifier id = PEEK (objId);
  m_myPubs.insert (PAIR (id, obj)); // index object by objId

  // publish object in tag broker cells - index object metadata by tags
  DataMessage msg;
  Ptr<ObjectMetadata> objMeta = obj->GetObjectMetadata ();
  std::vector<uint16_t> dsts;
  for (std::string tag : obj->GetTags ())
   { // for each tag in object metadata, send msg to hash(tag)
    msg.SetPublish (reqId, objMeta, tag, cell);
    uint16_t dst = m_hash->Hash (tag);
    dsts.push_back (dst);
    m_routing->SendToCell (msg, dst);
   }

  Time timeout = m_dynTimeout ? CalculateRequestTimeout (cell, dsts) :
    m_pubReqTimeout;
  req->SetTimeout (timeout);
  StartPublishRequestTimeout (req, timeout); // start timer
}

void
Thyme::DoUnpublish (Ptr<ThymeObject> obj)
{
  NS_LOG_FUNCTION (this << obj);
  // check if publication is ongoing
  if (IsPublicationOngoing (obj->GetObjectIdentifier ()))
   NotifyUnpublishFailure (obj->GetObjectIdentifier (), INVALID_ARG);

  // create request
  Ptr<UnpublishRequest> req = CreateUnpublishRequest (obj);
  uint32_t reqId = req->GetRequestId ();
  m_profiler->OnUnpublishStart (m_addr, reqId, obj);

  // unpublish object in tag broker cells
  Ptr<ObjectIdentifier> objId = obj->GetObjectIdentifier ();
  uint8_t idx = 0;
  DataMessage msg;
  std::vector<uint16_t> dsts;
  for (std::string tag : obj->GetTags ())
   { // for each tag in object metadata, send msg to hash(tag)
    msg.SetUnpublish (reqId, objId, idx++);
    uint16_t dst = m_hash->Hash (tag);
    dsts.push_back (dst);
    m_routing->SendToCell (msg, dst);
   }

  Time timeout = m_dynTimeout ?
    CalculateRequestTimeout (m_routing->GetMyCell (), dsts) :
    m_unpubReqTimeout;
  req->SetTimeout (timeout);
  StartUnpublishRequestTimeout (req, timeout); // start timer
}

void
Thyme::DoDownload (Ptr<ObjectMetadata> objMeta, Locations locs)
{
  NS_LOG_FUNCTION (this << objMeta << locs.size ());
  /*if (m_routing->IsMoving ())
   { // node is moving (or waiting for stabilization timeout)
    Down d{objMeta, locs};
    m_waitingDowns.push_back (d);
    return; // re-schedule operation
   }*/

  Ptr<ObjectIdentifier> objId = objMeta->GetObjectIdentifier ();
  Ipv4Address owner = objMeta->GetOwner ();

  // check active replicas
  ObjectKey objKey = OBJ_KEY (objId, owner);
  auto found = m_activeReps.find (objKey);
  if (found != m_activeReps.end ())
   { // objKey exists
    m_profiler->OnDownloadStart (m_addr, -1);
    NotifyDownloadSuccess (found->second);
    m_profiler->OnDownloadSuccess (m_addr, -1, found->second, 1);
    return;
   }

  // create request
  Ptr<DownloadRequest> req = CreateDownloadRequest (objMeta, locs);
  m_profiler->OnDownloadStart (m_addr, req->GetRequestId ());

  // try download object from closest node/cell
  TryDownload (locs, req);
}

void
Thyme::DoSubscribe (std::string filter, Time startTs, Time endTs)
{
  NS_LOG_FUNCTION (this << filter << startTs << endTs);
  // create subscription object
  uint32_t subId = GetNextSubscriptionId ();
  uint16_t myCell = m_routing->GetMyCell ();
  Ptr<SubscriptionObject> subObj =
    Create<SubscriptionObject> (subId, m_addr, startTs, endTs, filter, myCell);

  // create request
  Ptr<SubscribeRequest> req = CreateSubscribeRequest (subObj);
  req->SetTags (subObj->GetSendTags ()); // set send tags in the request
  uint32_t reqId = req->GetRequestId ();
  m_profiler->OnSubscribeStart (m_addr, reqId, subObj);

  // save subscription locally
  m_mySubs.insert (PAIR (subId, subObj));

  // subscribe in tag broker cells - index subObj by send tags
  DataMessage msg;
  std::vector<std::string> sendTags = subObj->GetSendTags ();
  Filter conjunctions = subObj->GetFilter ();
  std::vector<uint16_t> dsts;
  for (int i = 0; i < conjunctions.size (); ++i)
   { // for each conjunction, send to hash(tag)
    subObj->SetConjunction (conjunctions[i]);
    msg.SetSubscribe (reqId, subObj, i);
    uint16_t dst = m_hash->Hash (sendTags[i]);
    dsts.push_back (dst);
    m_routing->SendToCell (msg, dst);
   }

  Time timeout = m_dynTimeout ? CalculateRequestTimeout (myCell, dsts) :
    m_subReqTimeout;
  req->SetTimeout (timeout);
  StartSubscribeRequestTimeout (req, timeout); // start timer
}

void
Thyme::DoUnsubscribe (Ptr<Subscription> subObj)
{
  NS_LOG_FUNCTION (this << subObj);
  // create request
  Ptr<SubscriptionObject> so =
    dynamic_cast<SubscriptionObject*> (PeekPointer (subObj));
  Ptr<UnsubscribeRequest> req = CreateUnsubscribeRequest (so);
  uint32_t reqId = req->GetRequestId ();
  uint32_t subId = so->GetSubscriptionId ();
  req->SetTags (so->GetSendTags ());
  m_profiler->OnUnsubscribeStart (m_addr, reqId, so);

  DataMessage unsub;
  std::vector<std::string> sendTags = so->GetSendTags ();
  std::vector<uint16_t> dsts;
  for (int i = 0; i < sendTags.size (); ++i)
   { // for each send tag
    unsub.SetUnsubscribe (reqId, subId, i);
    uint16_t dst = m_hash->Hash (sendTags[i]);
    dsts.push_back (dst);
    m_routing->SendToCell (unsub, dst);
   }

  Time timeout = m_dynTimeout ?
    CalculateRequestTimeout (m_routing->GetMyCell (), dsts) :
    m_unsubReqTimeout;
  req->SetTimeout (timeout);
  StartUnsubscribeRequestTimeout (req, timeout);
}

void
Thyme::DoDispose ()
{
  NS_LOG_FUNCTION (this);
  ThymeApi::DoDispose ();
}

void
Thyme::DoInitialize ()
{
  NS_LOG_FUNCTION (this);
  ThymeApi::DoInitialize ();
}

void
Thyme::StartApplication ()
{
  NS_LOG_FUNCTION (this);
  // create location sensor
  NS_LOG_INFO ("Create location sensor");
  m_loc = CreateObject<LocationSensor> ();

  // create grid manager
  NS_LOG_INFO ("Create grid manager");
  m_gridMan = CreateObject<GridManager> ();
  m_gridMan->Start (m_loc);

  // create hashing
  NS_LOG_INFO ("Create hashing");
  m_hash = Create<Hashing> (m_gridMan);

  // create routing protocol
  NS_LOG_INFO ("Create routing protocol");
  m_routing = CreateObject<RoutingProtocol> ();
  m_routing->SetForwardUpCallback (MakeCallback (&Thyme::ProcessMsg, this));
  m_routing->SetNodeMovedCallback (MakeCallback (&Thyme::NodeMoved, this));
  m_routing->SetNodeStabilizedCallback (MakeCallback (&Thyme::NodeStabilized, this));
  m_routing->SetSameCellBeaconCallback (MakeCallback (&Thyme::Join, this));
  m_routing->Start (m_node, m_gridMan, m_loc, m_profiler);
  m_addr = m_routing->GetMyAddress ();
  m_profiler->RegisterNode (m_addr);

  // start handoff timer
  NS_LOG_INFO ("Start hand-off " << m_handoffInt.GetSeconds () << "s");
  m_handoff = Simulator::Schedule (m_handoffInt, &Thyme::HandoffExpired, this);
}

void
Thyme::StopApplication ()
{
  NS_LOG_FUNCTION (this);
  m_routing->Dispose ();
  m_handoff.Cancel ();
  ThymeApi::StopApplication ();
}

void
Thyme::ProcessMsg (DataMessage msg, bool isCoord, uint64_t pcktId)
{
  MessageType type = msg.GetMsgType ();
  NS_LOG_FUNCTION (this << &msg << Utils::GetMsgTypeString (type) << isCoord
    << pcktId);
  switch (type)
  {
  case PUBLISH:
    ProcessPublish (msg.GetPublish (), msg.GetSource (), isCoord);
    break;
  case PUBLISH_RSP:
    ProcessPublishRsp (msg.GetPublishRsp ());
    break;
  case ACTIVE_REP:
    ProcessActiveRep (msg.GetActiveRep ());
    break;
  case UNPUBLISH:
    ProcessUnpublish (msg.GetUnpublish (), msg.GetSource (), isCoord);
    break;
  case UNPUBLISH_RSP:
    ProcessUnpublishRsp (msg.GetUnpublishRsp ());
    break;
  case UNPUBLISH_REP:
    ProcessUnpublishRep (msg.GetUnpublishRep (), msg.GetSource ());
    break;
  case DOWNLOAD:
    m_profiler->RegisterDownloadPacket (pcktId);
    ProcessDownload (msg, msg.GetSource ());
    break;
  case DOWNLOAD_RSP:
    m_profiler->RegisterDownloadRspPacket (pcktId);
    ProcessDownloadRsp (msg.GetDownloadRsp ());
    break;
  case UPDATE_LOC:
    ProcessUpdateLoc (msg.GetSource ());
    break;
  case REGISTER_REP:
    ProcessRegisterRep (msg.GetRegisterRep (), msg.GetSource ());
    break;
  case SUBSCRIBE:
    ProcessSubscribe (msg.GetSubscribe (), msg.GetSource (), isCoord);
    break;
  case SUBSCRIBE_RSP:
    ProcessSubscribeRsp (msg.GetSubscribeRsp ());
    break;
  case SUBSCRIBE_NOT:
    ProcessSubscribeNot (msg.GetSubscribeNot (), msg.GetSource ());
    break;
  case UNSUBSCRIBE:
    ProcessUnsubscribe (msg.GetUnsubscribe (), msg.GetSource (), isCoord);
    break;
  case UNSUBSCRIBE_RSP:
    ProcessUnsubscribeRsp (msg.GetUnsubscribeRsp ());
    break;
  case JOIN:
    ProcessJoin (msg.GetSource ());
    break;
  case JOIN_RSP:
    ProcessJoinRsp (msg.GetJoinRsp ());
    break;
  case NACK_MSG:
    ProcessNackMsg (msg.GetNackMsg ());
    break;

  default:
    NS_ABORT_MSG ("Unknown type " << type);
  }
}

void
Thyme::ProcessPublish (DataMessage::Publish pub, Ptr<NodeAddress> sender,
  bool isCoord)
{
  NS_LOG_FUNCTION (this << pub << sender << isCoord);
  m_profiler->OnPublishReceived (m_addr);

  // index object metadata by tag
  Ptr<ObjectMetadata> objMeta = pub.objMeta;
  bool isNewTag = InsertMetaByTag (pub.tag, objMeta);

  // index object metadata by objKey
  bool isFirstTag = isNewTag ? InsertMetaByObjKey (objMeta) : false;

  // index locations by objKey
  if (isFirstTag)
   {
    Locations locs;
    locs.insert (PAIR (objMeta->GetOwner (), pub.cell)); // primary copy
    locs.insert (PAIR (Ipv4Address::GetBroadcast (), pub.cell)); // act. rep.
    InsertLocsByObjKey (objMeta, locs);
   }

  // send response message
  if (isCoord && pub.reqId != -1)
   { // only the cbcast coordinator responds back to requester
    DataMessage rsp;
    rsp.SetPublishRsp (pub);
    uint64_t puid = m_routing->SendToNode (rsp, sender);

    if (m_nackOpt)
     {
      Locations dsts;
      dsts.insert ({sender->GetAddress (), sender->GetCell ()});
      Msg x{rsp, dsts};
      m_msgs.insert ({puid, x});
      Simulator::Schedule (Seconds (60), &Thyme::RemoveMsg, this, puid);
     }

    // only the cbcast coordinator sends notifications to subscribers
    // send notifications for ("present") publication
    if (isFirstTag) // still not processed
     HandleSubscriptions (objMeta);
   }
}

bool
Thyme::InsertMetaByTag (std::string tag, Ptr<ObjectMetadata> objMeta)
{
  NS_LOG_FUNCTION (this << tag << objMeta);
  ObjectKey objKey = OBJ_KEY (objMeta->GetObjectIdentifier (),
    objMeta->GetOwner ());

  auto it = m_metasByTag.find (tag);
  if (it != m_metasByTag.end ())
   { // tag exists
    return it->second.insert (PAIR (objKey, objMeta)).second;
   }
  else
   { // tag doesn't exist
    ObjectsMetadata objsMeta;
    objsMeta.insert (PAIR (objKey, objMeta)); // insert new entry

    return m_metasByTag.insert (PAIR (tag, objsMeta)).second;
   }
}

bool
Thyme::InsertMetaByObjKey (Ptr<ObjectMetadata> objMeta)
{
  NS_LOG_FUNCTION (this << objMeta);
  ObjectKey objKey = OBJ_KEY (objMeta->GetObjectIdentifier (),
    objMeta->GetOwner ());

  bool inserted = m_metasByObjKey.insert (PAIR (objKey, objMeta)).second;
  if (inserted)
   { // first time insert
    m_objKeyCount.insert (PAIR (objKey, 1)); // insert new entry
   }
  else
   { // was already inserted before
    auto found = m_objKeyCount.find (objKey);
    if (found == m_objKeyCount.end ()) NS_LOG_ERROR ("No objKey found!");
    found->second++; // increment counter
   }

  return inserted;
}

void
Thyme::InsertLocsByObjKey (Ptr<ObjectMetadata> objMeta, Locations locs)
{
  NS_LOG_FUNCTION (this << objMeta << locs.size ());
  ObjectKey objKey = OBJ_KEY (objMeta->GetObjectIdentifier (),
    objMeta->GetOwner ());
  m_locsByObjKey.insert (PAIR (objKey, locs));
}

void
Thyme::ProcessPublishRsp (DataMessage::PublishRsp pubRsp)
{
  NS_LOG_FUNCTION (this << pubRsp);
  Ptr<PublishRequest> req = GetPublishRequest (pubRsp.reqId);
  if (req != 0)
   {
    std::string tag = req->GetObject ()->GetTag (pubRsp.tagIdx);
    req->ReceivedRspForTag (tag);
    if (req->IsFinished ()) HandleFinishedPublishRequest (req);
   }
  else // delayed rsp received
   m_profiler->OnPublishRspReceived (m_addr, pubRsp.reqId);
}

void
Thyme::HandleFinishedPublishRequest (Ptr<PublishRequest> req)
{
  NS_LOG_FUNCTION (this << req);
  uint32_t reqId = req->GetRequestId ();
  Ptr<ThymeObject> obj = req->GetObject ();
  uint16_t cell = req->GetCell ();
  uint8_t retries = req->GetRetries ();
  RemoveRequest (reqId); // cancels timeout (calls DoDispose)

  MakeActiveReplicas (obj, cell);

  NotifyPublishSuccess (obj);
  m_profiler->OnPublishSuccess (m_addr, reqId, retries);
}

void
Thyme::MakeActiveReplicas (Ptr<ThymeObject> obj, uint16_t cell)
{ // active replication (ACTIVE_REP)
  NS_LOG_FUNCTION (this << obj);
  ObjectIdentifier id = PEEK (obj->GetObjectIdentifier ());
  m_pubCells.insert (PAIR (id, cell));

  DataMessage msg;
  msg.SetActiveRep (obj);
  if (cell == m_routing->GetMyCell ()) // bcast msg to own cell
   m_routing->BroadcastInCell (msg);
  else // send to cell where it was published
   m_routing->SendToCell (msg, cell);
}

void
Thyme::ProcessActiveRep (DataMessage::ActiveRep actRep)
{
  NS_LOG_FUNCTION (this << actRep);
  Ptr<ThymeObject> obj = actRep.obj;
  ObjectKey objKey = OBJ_KEY (obj->GetObjectIdentifier (), obj->GetOwner ());

  m_activeReps.insert (PAIR (objKey, obj));
  m_profiler->OnActiveReplicaReceived (m_addr);
}

bool
Thyme::IsPublicationOngoing (Ptr<ObjectIdentifier> objId) const
{
  NS_LOG_FUNCTION (this << objId);
  ObjectIdentifier id = PEEK (objId);
  auto found = m_myPubs.find (id);
  bool inMyPubs = found != m_myPubs.end ();

  auto found2 = m_pubCells.find (id);
  bool activeRepDone = found2 != m_pubCells.end ();

  // in my pubs, but not in active replicas yet
  return inMyPubs && !activeRepDone;
}

void
Thyme::ProcessUnpublish (DataMessage::Unpublish unpub, Ptr<NodeAddress> sender,
  bool isCoord)
{
  NS_LOG_FUNCTION (this << unpub << sender->GetAddress () << isCoord);
  m_profiler->OnUnpublishReceived (m_addr);

  Ipv4Address owner = sender->GetAddress ();
  ObjectKey objKey = OBJ_KEY (unpub.objId, owner);
  const auto found = m_metasByObjKey.find (objKey);
  if (found != m_metasByObjKey.end ())
   { // only done by nodes that have the object metadata
    // can happen if nodes in the cell don't receive the PUB msg of this obj
    const std::string tag = found->second->GetTag (unpub.tagIdx);

    // remove object metadata indexed by tag
    bool isNewTag = RemoveMetaByTag (tag, unpub.objId, owner);
    // remove object metadata indexed by objKey
    bool isLastTag = isNewTag ? RemoveMetaByObjKey (unpub.objId, owner) :
      false;
    // remove locations indexed by objKey
    if (isLastTag) RemoveLocsByObjKey (unpub.objId, owner);
   }

  // send response message
  if (isCoord && unpub.reqId != -1)
   { // only the cbcast coordinator responds back to requester
    DataMessage rsp;
    rsp.SetUnpublishRsp (unpub);
    uint64_t puid = m_routing->SendToNode (rsp, sender);

    if (m_nackOpt)
     {
      Locations dsts;
      dsts.insert ({sender->GetAddress (), sender->GetCell ()});
      Msg x{rsp, dsts};
      m_msgs.insert ({puid, x});
      Simulator::Schedule (Seconds (60), &Thyme::RemoveMsg, this, puid);
     }
   }
}

bool
Thyme::RemoveMetaByTag (std::string tag, Ptr<ObjectIdentifier> objId,
  Ipv4Address owner)
{
  NS_LOG_FUNCTION (this << tag << objId << owner);
  ObjectKey objKey = OBJ_KEY (objId, owner);
  uint32_t res = 0;
  auto it = m_metasByTag.find (tag);
  if (it != m_metasByTag.end ())
   { // tag exists
    res = it->second.erase (objKey);

    if (it->second.empty ()) m_metasByTag.erase (it); // erase empty tag
   }

  return res > 0;
}

bool
Thyme::RemoveMetaByObjKey (Ptr<ObjectIdentifier> objId, Ipv4Address owner)
{
  NS_LOG_FUNCTION (this << objId << owner);
  ObjectKey objKey = OBJ_KEY (objId, owner);
  auto found = m_objKeyCount.find (objKey);
  if (found == m_objKeyCount.end ()) NS_LOG_ERROR ("No objKey found!");
  found->second--;

  bool isLastTag = found->second == 0;
  if (isLastTag)
   {
    m_metasByObjKey.erase (objKey);
    m_objKeyCount.erase (objKey);
   }

  return isLastTag;
}

void
Thyme::RemoveLocsByObjKey (Ptr<ObjectIdentifier> objId, Ipv4Address owner)
{
  NS_LOG_FUNCTION (this << objId << owner);
  m_locsByObjKey.erase (OBJ_KEY (objId, owner));
}

void
Thyme::ProcessUnpublishRsp (DataMessage::UnpublishRsp unpubRsp)
{
  NS_LOG_FUNCTION (this << unpubRsp);
  Ptr<UnpublishRequest> req = GetUnpublishRequest (unpubRsp.reqId);
  if (req != 0)
   {
    std::string tag = req->GetObject ()->GetTag (unpubRsp.tagIdx);
    req->ReceivedRspForTag (tag);
    if (req->IsFinished ()) HandleFinishedUnpublishRequest (req);
   }
  else // delayed rsp received
   m_profiler->OnUnpublishRspReceived (m_addr, unpubRsp.reqId);
}

void
Thyme::HandleFinishedUnpublishRequest (Ptr<UnpublishRequest> req)
{
  NS_LOG_FUNCTION (this << req);
  uint32_t reqId = req->GetRequestId ();
  Ptr<ThymeObject> obj = req->GetObject ();
  uint8_t retries = req->GetRetries ();
  RemoveRequest (reqId); // cancels timeout (calls DoDispose)

  Ptr<ObjectIdentifier> objId = obj->GetObjectIdentifier ();
  RemoveActiveReplicas (objId);

  m_myPubs.erase (PEEK (objId)); // delete publication locally

  NotifyUnpublishSuccess (obj);
  m_profiler->OnUnpublishSuccess (m_addr, reqId, retries);
}

void
Thyme::RemoveActiveReplicas (Ptr<ObjectIdentifier> objId)
{
  NS_LOG_FUNCTION (this << objId);
  auto it = m_pubCells.find (PEEK (objId));
  if (it != m_pubCells.end ())
   {
    uint32_t cell = it->second;
    m_pubCells.erase (PEEK (objId)); // delete object location
    // m_pubCells.erase (it); -> this causes free(): invalid pointer ?!?!?!?!

    DataMessage msg; // unpublish active replicas (UNPUBLISH_REP)
    msg.SetUnpublishRep (objId);
    if (cell == m_routing->GetMyCell ()) // bcast msg to own cell
     m_routing->BroadcastInCell (msg);
    else // send to cell where it was published
     m_routing->SendToCell (msg, cell);
   }
  else
   NS_LOG_ERROR ("No active replicas for " << objId->ToString ());
}

void
Thyme::ProcessUnpublishRep (DataMessage::UnpublishRep unpubRep,
  Ptr<NodeAddress> sender)
{
  NS_LOG_FUNCTION (this << unpubRep << sender);
  m_activeReps.erase (OBJ_KEY (unpubRep.objId, sender->GetAddress ()));
}

void
Thyme::TryDownload (Locations objLocs, Ptr<DownloadRequest> req)
{
  NS_LOG_FUNCTION (this << objLocs.size () << req->GetRequestId ());
  Location node = m_routing->GetClosestReplicaToMe (objLocs);
  req->RemoveLocation (node.first);

  DataMessage msg; // request object from chosen node
  msg.SetDownload (req->GetRequestId (), req->GetObjectIdentifier (),
    req->GetOwner ());
  if (node.first == Ipv4Address::GetBroadcast ())
   { // active replica. send to cell w/o cbcast
    msg.SetCellBcast (false);
    m_routing->SendToCell (msg, node.second);
   }
  else // send to node
   m_routing->SendToNode (msg, node.first, node.second);

  //std::vector<uint16_t> dsts;
  //dsts.push_back (node.second);
  Time timeout = //m_dynTimeout ?
    //CalculateRequestTimeout (m_routing->GetMyCell (), dsts) :
    m_downReqTimeout;
  req->SetTimeout (timeout);
  StartDownloadRequestTimeout (req, timeout);
}

void
Thyme::ProcessDownload (DataMessage msg, Ptr<NodeAddress> sender)
{
  NS_LOG_FUNCTION (this << msg.GetDownload () << sender);
  DataMessage::Download down = msg.GetDownload ();
  ObjectIdentifier id = PEEK (down.objId);
  Ipv4Address owner = down.owner;

  Ptr<ThymeObject> obj = 0;
  if (owner == m_addr)
   { // I am the publisher of this object
    auto found = m_myPubs.find (id);
    if (found != m_myPubs.end ()) // I published this object
     obj = found->second;
   }

  ObjectKey objKey = PAIR (id, owner);
  if (obj == 0)
   { // check active replicas
    auto found = m_activeReps.find (objKey);
    if (found != m_activeReps.end ()) // objKey exists
     obj = found->second;
   }

  if (obj == 0)
   { // check passive replicas
    auto found = m_passReps.find (objKey);
    if (found != m_passReps.end ()) // objKey exists
     obj = found->second;
   }

  bool sendToNeighbor = false;
  if (obj == 0 && msg.GetMsgTarget () == CELL && msg.GetTtl () > 0)
   { // ask your neighbor mechanism
    if (msg.GetTtl () > m_maxNeighborTtl) // first node to receive this msg
     msg.SetTtl (m_maxNeighborTtl);

    msg.SetCellBcast (false);
    msg.SetTtl (msg.GetTtl () - 1);
    sendToNeighbor = m_routing->SendToNeighbor (msg, sender->GetAddress ());
   }

  if (!sendToNeighbor)
   {
    DataMessage rsp;
    rsp.SetDownloadRsp (down.reqId, obj, obj == 0 ? OBJECT_NOT_FOUND :
      OBJECT_FOUND);
    uint64_t puid = m_routing->SendToNode (rsp, sender);

    if (m_nackOpt)
     {
      Locations dsts;
      dsts.insert ({sender->GetAddress (), sender->GetCell ()});
      Msg x{rsp, dsts};
      m_msgs.insert ({puid, x});
      Simulator::Schedule (Seconds (60), &Thyme::RemoveMsg, this, puid);
     }
   }
}

void
Thyme::ProcessDownloadRsp (DataMessage::DownloadRsp downRsp)
{
  NS_LOG_FUNCTION (this << downRsp);
  uint32_t reqId = downRsp.reqId;
  Ptr<DownloadRequest> req = GetDownloadRequest (reqId);
  if (req != 0)
   {
    req->CancelTimeout ();

    if (downRsp.st == OBJECT_FOUND)
     { // object found. notify upper layer
      Ptr<ThymeObject> obj = downRsp.obj;
      obj->SetObjectMetadata (req->GetObjectMetadata ()); // reconstruct object

      uint8_t retries = req->GetRetries ();
      RemoveRequest (reqId); // cancels timeout (calls DoDispose)

      MakePassiveReplica (obj); // make this node a passive replica of this obj

      NotifyDownloadSuccess (obj);
      m_profiler->OnDownloadSuccess (m_addr, reqId, obj, retries);
     }
    else
     { // object not found. try another location
      Locations objLocs = req->GetLocations ();
      if (objLocs.size () > 0 && !req->HasReachedMaxRetries ())
       { // if I have more locations to try
        req->IncrementRetry ();
        TryDownload (objLocs, req);
       }
      else
       { // no more locations to try. download unsuccessful
        Ptr<ObjectIdentifier> objId = req->GetObjectIdentifier ();
        Ipv4Address owner = req->GetOwner ();
        RemoveRequest (reqId);

        NotifyDownloadFailure (objId, owner, OBJECT_NOT_FOUND);
        m_profiler->OnDownloadFailure (m_addr, reqId, OBJECT_NOT_FOUND);
       }
     }
   }
  else // delayed rsp received
   m_profiler->OnDownloadRspReceived (m_addr, reqId);
}

void
Thyme::MakePassiveReplica (Ptr<ThymeObject> obj)
{
  NS_LOG_FUNCTION (this << obj);
  Ptr<ObjectIdentifier> objId = obj->GetObjectIdentifier ();
  ObjectKey objKey = OBJ_KEY (objId, obj->GetOwner ());
  auto found = m_passReps.find (objKey);
  if (found != m_passReps.end ()) found = m_passReps.erase (found);
  m_passReps.insert (found, PAIR (objKey, obj));

  // aggregate tags/dsts
  std::set<uint16_t> cells;
  for (std::string tag : obj->GetTags ()) // for each tag
   cells.insert (m_hash->Hash (tag)); // send to hash(tag)

  Locations dsts;
  for (uint16_t cell : cells)
   dsts.insert (PAIR (Ipv4Address (cell), cell));

  // send to (multiple) cell(s)
  DataMessage msg;
  msg.SetRegisterRep (objId, obj->GetOwner ());
  m_routing->SendToCells (msg, dsts);
}

void
Thyme::ProcessUpdateLoc (Ptr<NodeAddress> sender)
{
  NS_LOG_FUNCTION (this << sender);
  Ipv4Address node = sender->GetAddress ();
  uint16_t cell = sender->GetCell ();

  // update subscriptions
  auto found = m_subscriptions.find (node);
  if (found != m_subscriptions.end ())
   { // subscriber exists
    for (auto sub : found->second)
     {
      Ptr<SubscriptionObject> subObj =
        dynamic_cast<SubscriptionObject*> (PeekPointer (sub.second));
      subObj->SetSubscriberCell (cell); // update location
     }
   }

  // update objects locations
  for (auto loc : m_locsByObjKey)
   {
    auto found = loc.second.find (node);
    if (found != loc.second.end ())
     { // node has replica
      found->second = cell;
     }
   }

  // check waiting NACKs
  auto found2 = m_waitingNacks.find (node);
  if (found2 != m_waitingNacks.end ())
   { // waiting node
    found2->second.timeout.Cancel (); // cancel timeout

    // re-send subNot (wait for NACK again???)
    m_routing->SendToNode (found2->second.msg, node, cell);

    //    if (m_nackOpt) @TODO!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    //     {
    //      Locations dsts;
    //      dsts.insert ({sender->GetAddress (), sender->GetCell ()});
    //      Msg x{rsp, dsts};
    //      m_msgs.insert ({puid, x});
    //      Simulator::Schedule (Seconds (60), &Thyme::RemoveMsg, this, puid);
    //     }
   }
}

void
Thyme::ProcessRegisterRep (DataMessage::RegisterRep regRep,
  Ptr<NodeAddress> sender)
{
  NS_LOG_FUNCTION (this << regRep << sender);
  ObjectKey objKey = OBJ_KEY (regRep.objId, regRep.owner);
  uint16_t cell = sender->GetCell ();

  auto locs = m_locsByObjKey.find (objKey);
  if (locs != m_locsByObjKey.end ())
   { // objKey exists
    auto node = locs->second.find (sender->GetAddress ());
    if (node != locs->second.end ()) // node exists
     node->second = cell; // update location
    else // new node
     locs->second.insert (node, PAIR (sender->GetAddress (), cell));
   }
  else
   { // objKey does not exist (it might happen...)
    Locations aux;
    aux.insert (PAIR (sender->GetAddress (), cell));

    m_locsByObjKey.insert (locs, PAIR (objKey, aux));
   }
}

void
Thyme::ProcessSubscribe (DataMessage::Subscribe sub, Ptr<NodeAddress> sender,
  bool isCoord)
{
  NS_LOG_FUNCTION (this << sub << sender << isCoord);
  Ptr<SubscriptionObject> subObj = sub.subObj;
  Ipv4Address subscriber = subObj->GetSubscriber ();
  uint32_t subId = subObj->GetSubscriptionId ();
  m_profiler->OnSubscribeReceived (m_addr, subscriber, sub.reqId);

  // save subscription
  auto found = m_subscriptions.find (subscriber);
  bool isNewSub = false;
  if (found != m_subscriptions.end ())
   { // subscriber exists
    isNewSub = found->second.insert (PAIR (subId, subObj)).second;
   }
  else
   { // subscriber does not exist
    MySubscriptions aux;
    aux.insert (PAIR (subId, subObj));

    isNewSub = m_subscriptions.insert (PAIR (subscriber, aux)).second;
   }

  if (isCoord && sub.reqId != -1)
   { // only the cbcast coordinator responds back to requester
    DataMessage rsp;
    rsp.SetSubscribeRsp (sub);
    uint64_t puid = m_routing->SendToNode (rsp, sender);

    if (m_nackOpt)
     {
      Locations dsts;
      dsts.insert ({sender->GetAddress (), sender->GetCell ()});
      Msg x{rsp, dsts};
      m_msgs.insert ({puid, x});
      Simulator::Schedule (Seconds (60), &Thyme::RemoveMsg, this, puid);
     }

    // only the cbcast coordinator sends notifications to subscribers
    if (isNewSub)
     HandleSubscriptions (subObj);
   }
}

void
Thyme::ProcessSubscribeRsp (DataMessage::SubscribeRsp subRsp)
{
  NS_LOG_FUNCTION (this << subRsp);
  uint32_t reqId = subRsp.reqId;
  Ptr<SubscribeRequest> req = GetSubscribeRequest (reqId);
  if (req != 0)
   {
    Ptr<SubscriptionObject> subObj =
      dynamic_cast<SubscriptionObject*> (PeekPointer (req->GetSubscription ()));
    std::string tag = subObj->GetSendTag (subRsp.sendTagIdx);
    req->ReceivedRspForTag (tag);
    if (req->IsFinished ())
     {
      uint8_t retries = req->GetRetries ();
      RemoveRequest (reqId); // cancels timeout (calls DoDispose)

      NotifySubscribeSuccess (subObj);
      m_profiler->OnSubscribeSuccess (m_addr, reqId, retries);
     }
   }
  else // delayed rsp received
   m_profiler->OnSubscribeRspReceived (m_addr, reqId);
}

void
Thyme::ProcessSubscribeNot (DataMessage::SubscribeNot subNot,
  Ptr<NodeAddress> sender)
{
  NS_LOG_FUNCTION (this << subNot);
  std::vector<uint32_t> subIds = subNot.GetMySubscriptionIds (m_addr);
  for (int i = 0; i < subIds.size (); ++i)
   {
    auto found = m_mySubs.find (subIds[i]);
    if (found != m_mySubs.end ())
     { // subscription exists
      std::vector<Ptr<ObjectMetadata> > metas = subNot.objMeta;
      for (int j = 0; j < metas.size (); ++j)
       {
        Ptr<SubscriptionObject> subObj =
          dynamic_cast<SubscriptionObject*> (PeekPointer (found->second));
        if (!subObj->IsDuplicateObject (metas[j]))
         { // if newer object
          NotifySubscriptionNotification (subObj, metas[j], subNot.locs[j]);
          m_profiler->OnNotificationReceived (sender->GetAddress (), m_addr,
            subObj, metas[j], metas.size ());
         }
        else
         m_profiler->OnDuplicateNotificationReceived (m_addr, subObj, metas[j]);
       }
     }
    else // subscription does not exist
     m_profiler->OnNoIdNotification (subNot.objMeta.size ());
   }
}

void
Thyme::ProcessUnsubscribe (DataMessage::Unsubscribe unsub,
  Ptr<NodeAddress> sender, bool isCoord)
{
  NS_LOG_FUNCTION (this << unsub << sender << isCoord);
  m_profiler->OnUnsubscribeReceived (m_addr, sender->GetAddress (),
    unsub.reqId);

  auto found = m_subscriptions.find (sender->GetAddress ());
  if (found != m_subscriptions.end ())
   { // subscriber exists
    found->second.erase (unsub.subId);

    if (found->second.empty ()) m_subscriptions.erase (found);
   }

  if (isCoord && unsub.reqId != -1)
   { // only the cbcast coordinator responds back to requester
    DataMessage rsp;
    rsp.SetUnsubscribeRsp (unsub);
    uint64_t puid = m_routing->SendToNode (rsp, sender);

    if (m_nackOpt)
     {
      Locations dsts;
      dsts.insert ({sender->GetAddress (), sender->GetCell ()});
      Msg x{rsp, dsts};
      m_msgs.insert ({puid, x});
      Simulator::Schedule (Seconds (60), &Thyme::RemoveMsg, this, puid);
     }
   }
}

void
Thyme::ProcessUnsubscribeRsp (DataMessage::UnsubscribeRsp unsubRsp)
{
  NS_LOG_FUNCTION (this << unsubRsp);
  uint32_t reqId = unsubRsp.reqId;
  Ptr<UnsubscribeRequest> req = GetUnsubscribeRequest (reqId);
  if (req != 0)
   {
    Ptr<SubscriptionObject> subObj =
      dynamic_cast<SubscriptionObject*> (PeekPointer (req->GetSubscription ()));
    std::string tag = subObj->GetSendTag (unsubRsp.sendTagIdx);
    req->ReceivedRspForTag (tag);
    if (req->IsFinished ())
     {
      uint8_t retries = req->GetRetries ();
      RemoveRequest (reqId); // cancels timeout (calls DoDispose)

      // delete subscription locally
      m_mySubs.erase (subObj->GetSubscriptionId ());

      NotifyUnsubscribeSuccess (subObj);
      m_profiler->OnUnsubscribeSuccess (m_addr, reqId, retries);
     }
   }
  else // delayed rsp received
   m_profiler->OnUnsubscribeRspReceived (m_addr, reqId);
}

void
Thyme::ProcessJoin (Ptr<NodeAddress> dst)
{
  NS_LOG_FUNCTION (this << dst);
  DataMessage msg;
  msg.SetJoinRsp (m_locsByObjKey, m_activeReps, m_metasByTag, m_subscriptions,
    m_objKeyCount);
  m_routing->SendToNode (msg, dst);
}

void
Thyme::ProcessJoinRsp (DataMessage::JoinRsp joinRsp)
{
  if (m_joined) return;
  NS_LOG_FUNCTION (this << joinRsp);
  m_join.Cancel ();
  m_joined = true;

  m_locsByObjKey = joinRsp.locsByObjKey;
  m_activeReps = joinRsp.actReps;
  m_metasByTag = joinRsp.metasByTag;
  m_metasByObjKey.clear ();

  for (auto tag : m_metasByTag)
   for (auto m : tag.second)
    m_metasByObjKey.insert (PAIR (m.first, m.second));

  m_subscriptions = joinRsp.subs;
  m_objKeyCount = joinRsp.objKeyCount;

  HandoffExpired ();
}

void
Thyme::ProcessNackMsg (DataMessage::NackMsg nack)
{
  NS_LOG_FUNCTION (this << nack);
  uint64_t p = nack.puid;
  m_profiler->OnNackReceived (p);
  std::vector<Ipv4Address> dsts = nack.dsts;
  auto found = m_msgs.find (p);
  NS_LOG_WARN ("Recv NACK for pckt " << p << " " << dsts.size () << " " << *dsts.begin ());
  if (found != m_msgs.end ())
   { // packet found
    for (Ipv4Address addr : dsts)
     {
      EventId event = Simulator::Schedule (Seconds (120), &Thyme::TimeoutNack, this, addr);
      Info x{found->second.msg, event};
      m_waitingNacks.insert ({addr, x});
     }
   }
}

void
Thyme::TimeoutNack (Ipv4Address addr)
{
  NS_LOG_FUNCTION (this << addr);
  // assume node is dead. delete every subscription from this node
  m_waitingNacks.erase (addr);
  //m_subscriptions.erase (addr); @TODO !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
}

void
Thyme::HandlePublishRequestTimeout (Ptr<PublishRequest> req)
{
  NS_LOG_FUNCTION (this << req->GetRequestId () << req->GetState ()
    << (uint16_t) req->GetRetries ()
    << req->GetObject ()->GetObjectIdentifier ()->ToString ());
  uint32_t reqId = req->GetRequestId ();
  if (req->HasReachedMaxRetries ())
   { // notify failure
    Ptr<ThymeObject> obj = req->GetObject ();

    // cleanup halfway publish
    m_myPubs.erase (PEEK (obj->GetObjectIdentifier ()));
    DataMessage msg;
    std::set<std::string> remaining = req->GetRemainingTags ();
    for (std::string tag : obj->GetTags ())
     { // make diff between metadata tags and missing tags
      if (remaining.find (tag) == remaining.end ())
       { // if tag not in missing tags
        msg.SetUnpublish (-1, obj->GetObjectIdentifier (), -1);
        m_routing->SendToCell (msg, m_hash->Hash (tag));
       } // send UNPUB msgs to those cells (fire and forget)
     }

    NotifyPublishFailure (obj, TIMEOUT);
    m_profiler->OnPublishFailure (m_addr, reqId, TIMEOUT);
    RemoveRequest (reqId);
   }
  else
   { // retry missing tags
    req->IncrementRetry ();

    DataMessage msg;
    Ptr<ObjectMetadata> objMeta = req->GetObject ()->GetObjectMetadata ();
    uint16_t cell = req->GetCell ();
    for (std::string tag : req->GetRemainingTags ())
     { // for each tag, send msg to hash(tag)
      msg.SetPublish (reqId, objMeta, tag, cell);
      m_routing->SendToCell (msg, m_hash->Hash (tag));
     }

    if (m_dynTimeout) req->SetTimeout (req->GetTimeout () * m_opTimeoutFactor);
    StartPublishRequestTimeout (req, req->GetTimeout ()); // restart timer
   }
}

void
Thyme::HandleUnpublishRequestTimeout (Ptr<UnpublishRequest> req)
{
  NS_LOG_FUNCTION (this << req->GetRequestId () << req->GetState ()
    << (uint16_t) req->GetRetries ()
    << req->GetObject ()->GetObjectIdentifier ()->ToString ());
  if (req->HasReachedMaxRetries ())
   { // notify failure
    Ptr<ThymeObject> obj = req->GetObject ();
    uint32_t reqId = req->GetRequestId ();

    // clean halfway unpublish
    auto pubCell = m_pubCells.find (PEEK (obj->GetObjectIdentifier ()));
    if (pubCell == m_pubCells.end ()) NS_LOG_ERROR ("No pubCell found!");
    DataMessage msg;
    std::set<std::string> remaining = req->GetRemainingTags ();
    for (std::string tag : obj->GetTags ())
     { // make diff between metadata tags and missing tags
      if (remaining.find (tag) == remaining.end ())
       { // if tag not in missing tags
        msg.SetPublish (-1, obj->GetObjectMetadata (), tag, pubCell->second);
        m_routing->SendToCell (msg, m_hash->Hash (tag));
       } // send PUB msgs to those cells (fire and forget)
     }

    NotifyUnpublishFailure (obj->GetObjectIdentifier (), TIMEOUT);
    m_profiler->OnUnpublishFailure (m_addr, reqId, TIMEOUT);
    RemoveRequest (req->GetRequestId ());
   }
  else
   { // retry missing tags
    req->IncrementRetry ();

    DataMessage msg;
    uint32_t reqId = req->GetRequestId ();
    Ptr<ThymeObject> obj = req->GetObject ();
    Ptr<ObjectIdentifier> objId = obj->GetObjectIdentifier ();
    for (std::string tag : req->GetRemainingTags ())
     { // for each missing tag, resend msg to hash(tag)
      msg.SetUnpublish (reqId, objId, obj->GetTagIndex (tag));
      m_routing->SendToCell (msg, m_hash->Hash (tag));
     }

    if (m_dynTimeout) req->SetTimeout (req->GetTimeout () * m_opTimeoutFactor);
    StartUnpublishRequestTimeout (req, req->GetTimeout ()); // restart timer
   }
}

void
Thyme::HandleDownloadRequestTimeout (Ptr<DownloadRequest> req)
{
  NS_LOG_FUNCTION (this << req->GetRequestId ()
    << (uint16_t) req->GetRetries ()
    << req->GetObjectIdentifier ()->ToString ());
  Locations objLocs = req->GetLocations ();
  if (objLocs.size () > 0 && !req->HasReachedMaxRetries ())
   { // if I have more locations to try
    req->IncrementRetry ();
    TryDownload (objLocs, req);
   }
  else
   { // notify failure
    Ptr<ObjectIdentifier> objId = req->GetObjectIdentifier ();
    Ipv4Address owner = req->GetOwner ();
    uint32_t reqId = req->GetRequestId ();
    RemoveRequest (reqId);

    NotifyDownloadFailure (objId, owner, TIMEOUT);
    m_profiler->OnDownloadFailure (m_addr, reqId, TIMEOUT);
   }
}

void
Thyme::HandleSubscribeRequestTimeout (Ptr<SubscribeRequest> req)
{
  NS_LOG_FUNCTION (this << req->GetRequestId () << req->GetState ()
    << (uint16_t) req->GetRetries ()
    << req->GetSubscription ()->GetSubscriptionId ());
  uint32_t reqId = req->GetRequestId ();
  Ptr<SubscriptionObject> subObj =
    dynamic_cast<SubscriptionObject*> (PeekPointer (req->GetSubscription ()));

  if (req->HasReachedMaxRetries ())
   {
    // cleanup halfway subscribe
    m_mySubs.erase (subObj->GetSubscriptionId ());
    DataMessage msg;
    std::set<std::string> remaining = req->GetRemainingTags ();
    for (std::string tag : subObj->GetSendTags ())
     { // make diff between send tags and missing tags
      if (remaining.find (tag) == remaining.end ())
       { // if tag not in missing tags
        msg.SetUnsubscribe (-1, subObj->GetSubscriptionId (), -1);
        m_routing->SendToCell (msg, m_hash->Hash (tag));
       } // send UNSUB msgs to those cells (fire and forget)
     }

    NotifySubscribeFailure (subObj, TIMEOUT);
    m_profiler->OnSubscribeFailure (m_addr, reqId, TIMEOUT);
    RemoveRequest (reqId);
   }
  else
   { // retry
    req->IncrementRetry ();

    Filter conjunctions = subObj->GetFilter ();
    std::set<std::string> remaining = req->GetRemainingTags ();
    DataMessage msg;
    int sendTagIdx = 0;
    for (std::string tag : subObj->GetSendTags ())
     { // for each conjunction, send to hash(tag)
      if (remaining.find (tag) != remaining.end ())
       { // if tag is remaining
        subObj->SetConjunction (conjunctions[sendTagIdx]);
        msg.SetSubscribe (reqId, subObj, sendTagIdx);
        m_routing->SendToCell (msg, m_hash->Hash (tag));
       }

      sendTagIdx++;
     }

    if (m_dynTimeout) req->SetTimeout (req->GetTimeout () * m_opTimeoutFactor);
    StartSubscribeRequestTimeout (req, req->GetTimeout ()); // restart timer
   }
}

void
Thyme::HandleUnsubscribeRequestTimeout (Ptr<UnsubscribeRequest> req)
{
  NS_LOG_FUNCTION (this << req->GetRequestId () << req->GetState ()
    << (uint16_t) req->GetRetries ()
    << req->GetSubscription ()->GetSubscriptionId ());
  uint32_t reqId = req->GetRequestId ();
  Ptr<SubscriptionObject> subObj =
    dynamic_cast<SubscriptionObject*> (PeekPointer (req->GetSubscription ()));

  if (req->HasReachedMaxRetries ())
   {
    // clean halfway unsubscribe
    Filter conjunctions = subObj->GetFilter ();
    std::set<std::string> remaining = req->GetRemainingTags ();
    DataMessage msg;
    int sendTagIdx = 0;
    for (std::string tag : subObj->GetSendTags ())
     { // make diff between send tags and missing tags
      if (remaining.find (tag) == remaining.end ())
       { // if tag not in missing tags
        subObj->SetConjunction (conjunctions[sendTagIdx]);
        msg.SetSubscribe (-1, subObj, sendTagIdx);
        m_routing->SendToCell (msg, m_hash->Hash (tag));
       }

      sendTagIdx++;
     }

    NotifyUnsubscribeFailure (subObj, TIMEOUT);
    m_profiler->OnUnsubscribeFailure (m_addr, reqId, TIMEOUT);
    RemoveRequest (reqId);
   }
  else
   { // retry
    req->IncrementRetry ();

    DataMessage msg;
    for (std::string tag : req->GetRemainingTags ())
     { // for each conjunction, send to hash(tag)
      msg.SetUnsubscribe (reqId, subObj->GetSubscriptionId (),
        subObj->GetSendTagIndex (tag));
      m_routing->SendToCell (msg, m_hash->Hash (tag));
     }

    if (m_dynTimeout) req->SetTimeout (req->GetTimeout () * m_opTimeoutFactor);
    StartUnsubscribeRequestTimeout (req, req->GetTimeout ()); // restart timer
   }
}

void
Thyme::HandleSubscriptions (Ptr<ObjectMetadata> objMeta)
{ // new publication. check against all subscriptions
  NS_LOG_FUNCTION (this << objMeta);
  const std::vector<Ptr<SubscriptionObject> > toNotify =
    CheckAllSubscriptions (objMeta);
  if (toNotify.size () > 0) SendSubscriptionNotifications (toNotify, objMeta);
}

std::vector<Ptr<SubscriptionObject> >
Thyme::CheckAllSubscriptions (Ptr<ObjectMetadata> objMeta)
{ // published object. check subscriptions that match publication
  NS_LOG_FUNCTION (this << objMeta);
  std::vector<Ptr<SubscriptionObject> > res;
  // pass through every subscription and check tags and time
  for (auto subs = m_subscriptions.begin (); subs != m_subscriptions.end ();)
   { // iterate over subscribers
    if (subs->first == objMeta->GetOwner ())
     { // ignore publisher own subscriptions
      ++subs; // do not notify publisher about her own publications
      continue; // she knows!
     }

    for (auto nSubs = subs->second.begin (); nSubs != subs->second.end ();)
     { // iterate over subscriptions of each subscriber
      if (nSubs->second->IsExpired ())
       { // check subscription time validity first
        nSubs = subs->second.erase (nSubs); // if expired, erase subscription
       }
      else
       { // if subscription is valid, check match with publication
        if (nSubs->second->IsMatch (objMeta))
         res.push_back (dynamic_cast<SubscriptionObject*> (PeekPointer (nSubs->second)));

        ++nSubs;
       }
     }

    // erase empty subscriber
    subs = subs->second.empty () ? m_subscriptions.erase (subs) : ++subs;
   }

  return res;
}

void
Thyme::SendSubscriptionNotifications (std::vector<Ptr<SubscriptionObject> > subs,
  Ptr<ObjectMetadata> objMeta)
{
  NS_LOG_FUNCTION (this << subs.size () << objMeta);
  std::map<Ipv4Address, std::vector<uint32_t> > subIds;
  Locations dsts;
  for (int i = 0; i < subs.size (); ++i)
   {
    Ipv4Address subscriber = subs[i]->GetSubscriber ();
    uint32_t subId = subs[i]->GetSubscriptionId ();
    m_profiler->OnNotificationSent (m_addr, subscriber, subId, objMeta);

    auto found = subIds.find (subscriber);
    if (found == subIds.end ())
     { // new subscriber
      std::vector<uint32_t> aux;
      aux.push_back (subId);
      subIds.insert (PAIR (subscriber, aux));
     }
    else
     found->second.push_back (subId);

    if (dsts.find (subscriber) == dsts.end ()) // new subscriber. add to dsts
     dsts.insert (PAIR (subscriber, subs[i]->GetSubscriberCell ()));
   }

  ObjectKey objKey = OBJ_KEY (objMeta->GetObjectIdentifier (),
    objMeta->GetOwner ());
  auto found = m_locsByObjKey.find (objKey);
  if (found == m_locsByObjKey.end ()) NS_LOG_ERROR ("No locs found!");
  Locations locs = found->second;

  DataMessage subNot;
  subNot.SetSubscribeNot (subIds, objMeta, locs);
  std::vector<uint64_t> pckts = m_routing->SendToNodes (subNot, dsts);

  if (m_nackOpt)
   {
    Msg x{subNot, dsts};
    for (uint64_t puid : pckts)
     {
      m_msgs.insert ({puid, x});
      Simulator::Schedule (Seconds (60), &Thyme::RemoveMsg, this, puid);
     }
   }
}

void
Thyme::HandleSubscriptions (Ptr<SubscriptionObject> subObj)
{ // new subscription. check against all previous publications (if need be)
  NS_LOG_FUNCTION (this << subObj);
  const std::vector<Ptr<ObjectMetadata> > toNotify =
    CheckAllPublications (subObj);
  if (toNotify.size () > 0) SendSubscriptionNotifications (subObj, toNotify);
}

std::vector<Ptr<ObjectMetadata> >
Thyme::CheckAllPublications (Ptr<SubscriptionObject> subObj)
{ // new subscription. check past publications that match subscription
  NS_LOG_FUNCTION (this << subObj);
  std::vector<Ptr<ObjectMetadata> > objsMeta;
  if (!subObj->IsStarted ()) return objsMeta; // if not yet started...

  std::set<ObjectKey> keys;
  for (std::string tag : subObj->GetPositiveTags ())
   { // for each positive tag in this subscription
    const auto itTags = m_metasByTag.find (tag);

    if (itTags != m_metasByTag.end ())
     { // if it is in the tags list
      for (auto it : itTags->second)
       { // for each object published under that tag
        Ptr<ObjectMetadata> meta = it.second;

        if (subObj->IsMatch (meta))
         { // check if it matches the subscription
          if (keys.insert (OBJ_KEY (meta->GetObjectIdentifier (),
              meta->GetOwner ())).second)
           { // to avoid duplicates
            objsMeta.push_back (meta);
           }
         }
       }
     }
   }

  return objsMeta;
}

void
Thyme::SendSubscriptionNotifications (Ptr<SubscriptionObject> sub,
  std::vector<Ptr<ObjectMetadata> > objsMeta)
{
  NS_LOG_FUNCTION (this << sub << objsMeta.size ());
  Ipv4Address subscriber = sub->GetSubscriber ();
  uint32_t subId = sub->GetSubscriptionId ();
  DataMessage subNot;

  std::vector<Locations> locations;
  for (auto meta : objsMeta)
   { // collect corresponding locations
    m_profiler->OnNotificationSent (m_addr, subscriber, subId, meta);
    ObjectKey objKey = OBJ_KEY (meta->GetObjectIdentifier (),
      meta->GetOwner ());
    auto found = m_locsByObjKey.find (objKey);
    if (found == m_locsByObjKey.end ()) NS_LOG_ERROR ("No locs found!");
    locations.push_back (found->second);
   }

  subNot.SetSubscribeNot (subscriber, subId, objsMeta, locations);
  uint64_t puid = m_routing->SendToNode (subNot, subscriber,
    sub->GetSubscriberCell ());

  if (m_nackOpt)
   {
    Locations dsts;
    dsts.insert ({subscriber, sub->GetSubscriberCell ()});
    Msg x{subNot, dsts};
    m_msgs.insert ({puid, x});
    Simulator::Schedule (Seconds (60), &Thyme::RemoveMsg, this, puid);
   }
}

void
Thyme::RemoveMsg (uint64_t puid)
{
  NS_LOG_FUNCTION (this << puid);
  m_msgs.erase (puid);
}

void
Thyme::NodeMoved (uint16_t newCell, uint16_t oldCell)
{ // moved to a new cell
  NS_LOG_FUNCTION (this << newCell << oldCell);
  UpdateLocation ();
}

void
Thyme::NodeStabilized (uint16_t newCell, uint16_t oldCell)
{
  NS_LOG_FUNCTION (this << newCell << oldCell);
  if (newCell != oldCell) // check if node is in a new cell
   {
    UpdateLocation (); // update my registered cell

    // wait for joining the network
    m_joined = false;
    m_joinSent = false;
    m_joinRetries = 0;
    m_handoff.Cancel ();
    m_join.Cancel ();

    NS_LOG_INFO ("Start hand-off " << m_handoffInt.GetSeconds () << "s");
    m_handoff = Simulator::Schedule (m_handoffInt, &Thyme::HandoffExpired, this);
   }
  else
   HandoffExpired ();
}

void
Thyme::UpdateLocation ()
{
  NS_LOG_FUNCTION (this);
  // update my registered cell
  std::set<std::string> tags;
  std::set<std::string> t = GetPublicationsTags (); // my publications
  tags.insert (t.begin (), t.end ());
  t = GetPassiveReplicasTags (); // my passive replicas
  tags.insert (t.begin (), t.end ());
  t = GetSubscriptionsTags (); // my subscriptions
  tags.insert (t.begin (), t.end ());

  DataMessage upLoc;
  upLoc.SetUpdateLoc ();

  // aggregate tags/dsts
  std::set<uint16_t> cells;
  for (std::string tag : tags) // for each tag
   cells.insert (m_hash->Hash (tag)); // send to hash(tag)

  Locations dsts;
  for (uint16_t cell : cells)
   dsts.insert (PAIR (Ipv4Address (cell), cell));

  m_routing->SendToCells (upLoc, dsts);
}

std::set<std::string>
Thyme::GetPublicationsTags ()
{
  NS_LOG_FUNCTION (this);
  std::set<std::string> tags;
  for (auto pub : m_myPubs)
   {
    std::set<std::string> objTags = pub.second->GetTags ();
    tags.insert (objTags.begin (), objTags.end ());
   }

  return tags;
}

std::set<std::string>
Thyme::GetPassiveReplicasTags ()
{
  NS_LOG_FUNCTION (this);
  std::set<std::string> tags;
  for (auto rep : m_passReps)
   {
    std::set<std::string> objTags = rep.second->GetTags ();
    tags.insert (objTags.begin (), objTags.end ());
   }

  return tags;
}

std::set<std::string>
Thyme::GetSubscriptionsTags ()
{
  NS_LOG_FUNCTION (this);
  std::set<std::string> tags;
  for (auto sub : m_mySubs)
   {
    Ptr<SubscriptionObject> subObj =
      dynamic_cast<SubscriptionObject*> (PeekPointer (sub.second));
    std::vector<std::string> subTags = subObj->GetSendTags ();
    tags.insert (subTags.begin (), subTags.end ());
   }

  return tags;
}

void
Thyme::Join (Ipv4Address dst)
{
  if (m_joined || m_joinSent) return;
  NS_LOG_FUNCTION (this << dst);
  m_handoff.Cancel (); // cancel hand-off timer
  m_joinSent = true;

  DataMessage msg;
  msg.SetJoin ();
  m_routing->SendToNode (msg, dst, m_routing->GetMyCell ());

  m_join = Simulator::Schedule (m_joinTimeout, &Thyme::JoinExpired, this);
  ++m_joinRetries;
}

void
Thyme::JoinExpired ()
{
  NS_LOG_FUNCTION (this);
  if (m_joinRetries == m_joinMaxRetries)
   { // assume I'm alone in my cell
    HandoffExpired ();
    return;
   }

  m_joinSent = false;

  NS_LOG_INFO ("Start hand-off " << m_handoffInt.GetSeconds () << "s");
  m_handoff = Simulator::Schedule (m_handoffInt, &Thyme::HandoffExpired, this);
}

void
Thyme::HandoffExpired ()
{ // assume I'm alone in my cell
  NS_LOG_FUNCTION (this);
  m_joined = true;
  m_joinSent = true;

  m_routing->StartBeaconing ();

  if (m_routing->StabilizedWhileDown ()) UpdateLocation ();

  /*if (!m_waitingDowns.empty ())
   {
    for (auto it = m_waitingDowns.begin (); it != m_waitingDowns.end ();)
     {
      DoDownload (it->objMeta, it->locs);
      it = m_waitingDowns.erase (it);
     }
   }*/
}

void
Thyme::IfDown ()
{
  NS_LOG_FUNCTION (this);
  m_routing->IfDown ();

  m_joined = false;
  m_joinSent = false;
  m_joinRetries = 0;
  m_handoff.Cancel ();
  m_join.Cancel ();
  m_routing->StopBeaconing ();
  RemoveAllRequests ();
}

void
Thyme::IfUp ()
{
  NS_LOG_FUNCTION (this);
  m_routing->IfUp ();

  NS_LOG_INFO ("Start hand-off " << m_handoffInt.GetSeconds () << "s");
  m_handoff = Simulator::Schedule (m_handoffInt, &Thyme::HandoffExpired, this);
}

bool
Thyme::IsIfUp ()
{
  NS_LOG_FUNCTION (this);
  return m_routing->IsIfaceUp ();
}

Time
Thyme::CalculateRequestTimeout (uint16_t src, std::vector<uint16_t> dsts) const
{
  NS_LOG_FUNCTION (this << src << dsts.size ());
  uint32_t maxDist = 0;
  for (uint16_t dst : dsts)
   {
    uint32_t dist = m_gridMan->CalculateManhattanDistance (src, dst);
    NS_LOG_INFO ("Manhattan dist (" << src << "-" << dst << "): " << dist);
    if (dist > maxDist) maxDist = dist;
   }

  uint32_t lat = maxDist * m_routeLatency;
  Time res =
    MilliSeconds (m_rand->GetValue (2, 20) + (lat * m_timeoutFactor));
  NS_LOG_DEBUG ("Timeout " << res.GetMilliSeconds () << "ms");
  return res;
}

}
}

