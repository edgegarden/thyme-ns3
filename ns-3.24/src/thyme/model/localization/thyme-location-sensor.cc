/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2015 NOVA LINCS DI-FCT-UNL <http://nova-lincs.di.fct.unl.pt/>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Joao A. Silva <jaa.silva@campus.fct.unl.pt>
 */
#include "thyme-location-sensor.h"
#include <ns3/log.h>

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("LS");

namespace hyrax {

NS_OBJECT_ENSURE_REGISTERED (LocationSensor);

TypeId
LocationSensor::GetTypeId (void)
{
  NS_LOG_FUNCTION_NOARGS ();
  static TypeId tid = TypeId ("ns3::hyrax::LocationSensor")
    .SetParent<Object> ()
    .AddConstructor<LocationSensor> ()

    .AddAttribute ("PosInt",
    "Position check interval (ms).",
    TimeValue (MilliSeconds (POS_INT)),
    MakeTimeAccessor (&LocationSensor::m_posInt),
    MakeTimeChecker ())

    .AddAttribute ("StabilizeInt",
    "Position stabilization interval (ms).",
    TimeValue (MilliSeconds (STABILIZE_INT)),
    MakeTimeAccessor (&LocationSensor::m_stabilizeInt),
    MakeTimeChecker ());

  return tid;
}

LocationSensor::LocationSensor () : m_nodeId (-1), m_model (0),
  m_posTimer (Timer::CANCEL_ON_DESTROY), m_posInt (0),
  m_stabilizeTimer (Timer::CANCEL_ON_DESTROY), m_stabilizeInt (0),
  m_moving (false), m_pos (-1, -1, -1), m_velocity (-1, -1, -1),
  m_heading (WRONG), m_lastUpdate (0), m_stabilizing (false)
{
  NS_LOG_FUNCTION (this);
  m_posTimer.SetFunction (&LocationSensor::PosTimerExpired, this);
  m_stabilizeTimer.SetFunction (&LocationSensor::StabilizeTimerExpired, this);
}

LocationSensor::~LocationSensor ()
{
  NS_LOG_FUNCTION (this);
  m_model = 0;

  m_posTimer.Cancel ();
  m_stabilizeTimer.Cancel ();
}

void
LocationSensor::Start (Ptr<Node> node)
{
  NS_LOG_FUNCTION (this << node);
  m_nodeId = node->GetId ();
  m_model = node->GetObject<MobilityModel> ();
  m_model->TraceConnectWithoutContext ("CourseChange",
    MakeCallback (&LocationSensor::CourseChanged, this));

  m_posTimer.SetDelay (m_posInt);
  m_stabilizeTimer.SetDelay (m_stabilizeInt);

  m_pos = DoGetPosition ();
  m_velocity = DoGetVelocity ();
  m_lastUpdate = Simulator::Now ();
}

Time
LocationSensor::GetPositionInterval () const
{
  NS_LOG_FUNCTION (this);
  return m_posInt;
}

void
LocationSensor::SetPositionInterval (Time posInt)
{
  NS_LOG_FUNCTION (this << posInt);
  m_posInt = posInt;
  m_posTimer.SetDelay (m_posInt);
}

Time
LocationSensor::GetStabilizeInterval () const
{
  NS_LOG_FUNCTION (this);
  return m_stabilizeInt;
}

void
LocationSensor::SetStabilizeInterval (Time stabInt)
{
  NS_LOG_FUNCTION (this << stabInt);
  m_stabilizeInt = stabInt;
  m_stabilizeTimer.SetDelay (m_stabilizeInt);
}

bool
LocationSensor::IsMoving () const
{
  NS_LOG_FUNCTION (this);
  return m_moving;
}

Vector
LocationSensor::GetPosition () const
{
  NS_LOG_FUNCTION (this);
  return m_pos;
}

Vector
LocationSensor::GetVelocity () const
{
  NS_LOG_FUNCTION (this);
  return m_velocity;
}

CardinalPos
LocationSensor::GetHeading () const
{
  NS_LOG_FUNCTION (this);
  return m_heading;
}

Time
LocationSensor::GetLastUpdate () const
{
  NS_LOG_FUNCTION (this);
  return m_lastUpdate;
}

double
LocationSensor::GetSpeed () const
{ // returns the magnitude of the current velocity vector
  NS_LOG_FUNCTION (this);
  return sqrt (m_velocity.x * m_velocity.x + m_velocity.y * m_velocity.y);
}

bool
LocationSensor::IsStabilizing () const
{
  NS_LOG_FUNCTION (this);
  return m_stabilizing;
}

Time
LocationSensor::GetStabilizingDelayLeft () const
{
  NS_LOG_FUNCTION (this);
  return m_stabilizeTimer.GetDelayLeft ();
}

void
LocationSensor::SetMovedCallback (Callback<void, Vector, Vector, CardinalPos, Time> movedFn)
{
  NS_LOG_FUNCTION (this);
  m_movedFn = movedFn;
}

void
LocationSensor::SetStabilizedCallback (Callback<void, Vector, Vector, CardinalPos, Time> stabilizedFn)
{
  NS_LOG_FUNCTION (this);
  m_stabilizedFn = stabilizedFn;
}

void
LocationSensor::PosTimerExpired ()
{
  NS_LOG_FUNCTION (this);
  m_lastUpdate += m_posInt; // avoids calling Simulator::Now () every time
  const Vector newPos = DoGetPosition ();

  if (newPos.x != m_pos.x || newPos.y != m_pos.y)
   { // if my position is different from last time
    NS_LOG_LOGIC ("Node " << m_nodeId << " is moving " << newPos);
    m_velocity = DoGetVelocity ();
    m_heading = CalculateHeading (m_pos, newPos);
    m_pos = newPos;

    if (!m_movedFn.IsNull ())
     m_movedFn (m_pos, m_velocity, m_heading, m_lastUpdate);
   }

  m_posTimer.Schedule (); // reschedule timer
}

void
LocationSensor::StabilizeTimerExpired ()
{ // node stopped and stabilized
  NS_LOG_FUNCTION (this);
  m_stabilizing = false;
  NS_LOG_INFO ("Node " << m_nodeId << " is stabilized " << m_pos);

  if (!m_stabilizedFn.IsNull ())
   m_stabilizedFn (m_pos, m_velocity, m_heading, m_lastUpdate);
}

void
LocationSensor::CourseChanged (Ptr<const MobilityModel> mob)
{
  NS_LOG_FUNCTION (this << mob);
  m_velocity = DoGetVelocity (); // check current velocity
  double speed = GetSpeed ();

  if (speed == 0 && m_moving) // speed is zero and I was moving
   { // I stopped moving
    m_moving = false;

    m_posTimer.Cancel (); // stop checking for movement

    m_pos = DoGetPosition ();
    NS_LOG_INFO ("Node " << m_nodeId << " stopped moving " << m_pos);
    m_lastUpdate = Simulator::Now ();
    m_heading = WRONG;

    m_stabilizeTimer.Schedule (); // start stabilization
    m_stabilizing = true;
   }
  else if (speed != 0 && !m_moving) // speed is not zero and I was not moving
   { // I started moving
    m_moving = true;

    m_stabilizeTimer.Cancel (); // if I moved after stabilization started
    m_stabilizing = false; // cancel stabilization process

    NS_LOG_INFO ("Node " << m_nodeId << " started moving " << m_pos);
    m_lastUpdate = Simulator::Now () - m_posInt;
    PosTimerExpired (); // start timer
   }
}

Vector
LocationSensor::DoGetPosition () const
{
  NS_LOG_FUNCTION (this);
  return m_model->GetPosition ();
}

Vector
LocationSensor::DoGetVelocity () const
{
  NS_LOG_FUNCTION (this);
  return m_model->GetVelocity ();
}

CardinalPos
LocationSensor::CalculateHeading (Vector oldPos, Vector newPos) const
{
  NS_LOG_FUNCTION (this << oldPos << newPos);
  double radians = atan2 (newPos.y - oldPos.y, newPos.x - oldPos.x);
  if (radians < 0.0) radians += 2 * M_PI;

  const double degrees = radians * 360.0 / (2.0 * M_PI); // convert to degrees

  // E = 0, NE = 1, N = 2, NW = 3, W = 4, SW = 5, S = 6, SE = 7
  return (CardinalPos) (int(round (degrees / 45.0)) % 8);
}

}
}

