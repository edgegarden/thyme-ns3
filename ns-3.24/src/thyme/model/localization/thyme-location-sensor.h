/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2015 NOVA LINCS DI-FCT-UNL <http://nova-lincs.di.fct.unl.pt/>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Joao A. Silva <jaa.silva@campus.fct.unl.pt>
 */
#ifndef THYME_LOCATION_SENSOR_H
#define THYME_LOCATION_SENSOR_H

#include <ns3/mobility-model.h>
#include <ns3/node.h>
#include <ns3/thyme-common.h>
#include <ns3/timer.h>

namespace ns3 {
namespace hyrax {

#define POS_INT 400 // milliseconds
#define STABILIZE_INT (5 * (POS_INT)) // milliseconds (5 * 400 = 2000)

/**
 * \ingroup hyrax
 * 
 * \brief Location sensor checks the node's position at a fixed frequency,
 * and triggers the corresponding callbacks. The moved callback is called
 * **every** m_posInt time, if the node is currently moving. The stabilized
 * callback is only called **once** after the node has stopped moving and
 * m_stabilizeInt time has passed (without the node moving).
 */
class LocationSensor : public Object
{
private:
  uint32_t m_nodeId;
  Ptr<MobilityModel> m_model;

  Timer m_posTimer; // position checker timer
  Time m_posInt; // interval for checking node's position

  Timer m_stabilizeTimer; // stabilization checker timer
  Time m_stabilizeInt; // waiting time for node to be considered stable

  bool m_moving;
  Vector m_pos;
  Vector m_velocity;
  CardinalPos m_heading;
  Time m_lastUpdate;
  bool m_stabilizing;

  Callback<void, Vector, Vector, CardinalPos, Time> m_movedFn;
  Callback<void, Vector, Vector, CardinalPos, Time> m_stabilizedFn;

public:
  static TypeId GetTypeId (void);
  LocationSensor ();
  virtual ~LocationSensor ();
  void Start (Ptr<Node> node);

  Time GetPositionInterval () const;
  void SetPositionInterval (Time posInt);
  Time GetStabilizeInterval () const;
  void SetStabilizeInterval (Time stabInt);

  bool IsMoving () const;
  Vector GetPosition () const;
  Vector GetVelocity () const;
  CardinalPos GetHeading () const;
  Time GetLastUpdate () const;
  double GetSpeed () const;

  bool IsStabilizing () const;
  Time GetStabilizingDelayLeft () const;

  void SetMovedCallback (Callback<void, Vector, Vector, CardinalPos, Time> movedFn);
  void SetStabilizedCallback (Callback<void, Vector, Vector, CardinalPos, Time> stabilizedFn);

private:
  void PosTimerExpired ();
  void StabilizeTimerExpired ();

  void CourseChanged (Ptr<const MobilityModel> mob);

  Vector DoGetPosition () const;
  Vector DoGetVelocity () const;
  CardinalPos CalculateHeading (Vector oldPos, Vector newPos) const;
};

}
}

#endif /* THYME_LOCATION_SENSOR_H */

