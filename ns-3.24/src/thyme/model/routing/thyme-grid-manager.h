/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2015 NOVA LINCS DI-FCT-UNL <http://nova-lincs.di.fct.unl.pt/>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Joao A. Silva <jaa.silva@campus.fct.unl.pt>
 */
#ifndef THYME_GRID_MANAGER_H
#define THYME_GRID_MANAGER_H

#include <ns3/thyme-location-sensor.h>

namespace ns3 {
namespace hyrax {

#define CELL_SIZE_X 40
#define CELL_SIZE_Y 40
#define SIZE_X 240 // for 36 nodes (avg. 2 nodes per cell)
#define SIZE_Y 120

/**
 * \ingroup hyrax
 * 
 * \brief Manages the grid logic, encapsulating all the cells management, i.e.,
 * all the information regarding cell addresses comes from here. It also
 * uses the LocationSensor to be able to return the node's current cell
 * address (in "real time").
 */
class GridManager : public Object
{
private:
  double m_cellSizeX, m_cellSizeY;
  double m_sizeX, m_sizeY;
  uint16_t m_maxCellsX, m_maxCellsY;
  int m_numCells;
  int m_arrayPos[8]; // E, NE, N, NW, W, SW, S, SE

  Ptr<LocationSensor> m_loc;

public:
  static TypeId GetTypeId (void);
  GridManager ();
  virtual ~GridManager ();
  void Start (Ptr<LocationSensor> loc);

  uint16_t GetCell () const; // get my current cell (using the LocationSensor)
  uint16_t GetCell (Vector pos) const;
  Vector GetCellCenter (uint16_t cell) const;
  uint16_t GetTotalNumCells () const;

  std::vector<uint16_t> GetAdjacentCells (uint16_t cell) const;

  int GetSouthCell (uint16_t cell) const;
  int GetWestCell (uint16_t cell) const;

  uint32_t CalculateManhattanDistance (uint16_t a, uint16_t b) const;
};

}
}

#endif /* THYME_GRID_MANAGER_H */

