/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2015 NOVA LINCS DI-FCT-UNL <http://nova-lincs.di.fct.unl.pt/>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Joao A. Silva <jaa.silva@campus.fct.unl.pt>
 */
#include "thyme-routing-table.h"
#include <ns3/double.h>
#include <ns3/log.h>
#include <ns3/uinteger.h>

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("RT");

namespace hyrax {

NS_OBJECT_ENSURE_REGISTERED (RoutingTable);

TypeId
RoutingTable::GetTypeId (void)
{
  NS_LOG_FUNCTION_NOARGS ();
  static TypeId tid = TypeId ("ns3::hyrax::RoutingTable")
    .SetParent<Object> ()
    .AddConstructor<RoutingTable> ()

    .AddAttribute ("MovingEntryTtl",
    "Moving entry TTL (s).",
    TimeValue (Seconds (MOVING_ENTRY_TTL)),
    MakeTimeAccessor (&RoutingTable::m_movingEntryTtl),
    MakeTimeChecker ())

    .AddAttribute ("StationaryEntryTtl",
    "Stationary entry TTL (s).",
    TimeValue (Seconds (STATIONARY_ENTRY_TTL)),
    MakeTimeAccessor (&RoutingTable::m_stationaryEntryTtl),
    MakeTimeChecker ())

    .AddAttribute ("PopulationThreshold",
    "Cell population threshold.",
    UintegerValue (POP_THRESHOLD),
    MakeUintegerAccessor (&RoutingTable::m_popThreshold),
    MakeUintegerChecker<uint32_t> ());

  return tid;
}

RoutingTable::RoutingTable () : m_gridMan (0), m_loc (0), m_rand (0),
  m_popThreshold (-1)
{
  NS_LOG_FUNCTION (this);
}

RoutingTable::~RoutingTable ()
{
  NS_LOG_FUNCTION (this);
  m_gridMan = 0;
  m_loc = 0;
  m_rand = 0;

  m_oneHopNeighbors.clear ();
  m_twoHopNeighbors.clear ();
  m_movingNeighbors.clear ();

  m_neighborCells.clear ();
}

void
RoutingTable::Start (Ptr<GridManager> gridMan, Ptr<LocationSensor> loc)
{
  NS_LOG_FUNCTION (this << gridMan);
  m_gridMan = gridMan;
  m_loc = loc;
  m_rand = CreateObject<UniformRandomVariable> ();

  RecalculateNeighborCells (m_gridMan->GetCell ());
}

bool
RoutingTable::IsCellEmpty (uint16_t cell) const
{ // it only works for my surrounding cells
  NS_LOG_FUNCTION (this << cell);
  return GetNumNodesInOneHopNeighborCell (cell) < m_popThreshold;
}

bool
RoutingTable::IsMyNeighbor (uint16_t cell) const
{
  NS_LOG_FUNCTION (this << cell);
  if (cell == m_gridMan->GetCell ()) return true; // is in my cell

  for (uint16_t neighborCell : m_neighborCells)
   if (neighborCell == cell) return true; // in one of the 8 surrounding cells 

  return false;
}

void
RoutingTable::UpdateNeighbor (Ipv4Address addr, uint16_t cell, bool moving)
{
  NS_LOG_FUNCTION (this << addr << cell << moving);
  if (IsMyNeighbor (cell))
   { // if node is in one of my neighboring cells
    if (moving) UpdateMovingNeighbor (addr, cell); // moving neighbor
    else UpdateOneHopNeighbor (addr, cell);
   }
  else
   { // node is a 2-hop neighbor. ignore moving 2-hop neighbors!
    if (!moving) UpdateTwoHopNeighbor (addr, cell);
   }
}

Locations
RoutingTable::NeighborsExist (Locations dsts)
{
  NS_LOG_FUNCTION (this << dsts.size ());
  Locations directNeighbors;

  Locations aux = OneHopNeighborsExist (dsts);
  if (!aux.empty ()) directNeighbors.insert (aux.begin (), aux.end ());

  aux = MovingNeighborsExist (dsts);
  if (!aux.empty ()) directNeighbors.insert (aux.begin (), aux.end ());

  return directNeighbors;
}

void
RoutingTable::UpdateOneHopNeighbor (Ipv4Address addr, uint16_t cell)
{
  NS_LOG_FUNCTION (this << addr << cell);
  auto it = m_oneHopNeighbors.find (addr);
  if (it != m_oneHopNeighbors.end ())
   { // already exist in table. update values
    NS_LOG_LOGIC ("Update 1-hop neighbor: " << addr << " - " << cell);
    it->second.cell = cell;
    it->second.expiration = Simulator::Now () + CalculateEntryTtl (false);
   }
  else
   { // insert new entry
    NS_LOG_LOGIC ("New 1-hop neighbor: " << addr << " - " << cell);
    Hello h (cell, Simulator::Now () + CalculateEntryTtl (false));
    m_oneHopNeighbors.insert (PAIR (addr, h));

    DeleteMovingNeighbor (addr);
    DeleteTwoHopNeighbor (addr);
   }
}

bool
RoutingTable::OneHopNeighborExists (Ipv4Address addr)
{
  NS_LOG_FUNCTION (this << addr);
  auto it = m_oneHopNeighbors.find (addr);
  if (it != m_oneHopNeighbors.end ())
   { // node exists
    if (IsHelloExpired (it->second))
     { // if entry is expired, delete it
      m_oneHopNeighbors.erase (it);
      return false;
     }

    return true;
   }

  return false;
}

Locations
RoutingTable::OneHopNeighborsExist (Locations dsts)
{ // returns the direct neighbors that actually exist
  NS_LOG_FUNCTION (this << dsts.size ());
  Locations directNeighbors;
  for (auto loc : dsts)
   if (OneHopNeighborExists (loc.first)) directNeighbors.insert (loc);

  return directNeighbors;
}

uint32_t
RoutingTable::GetTotalNumOneHopNeighbors () const
{
  NS_LOG_FUNCTION (this);
  return m_oneHopNeighbors.size ();
}

void
RoutingTable::DeleteOneHopNeighbor (Ipv4Address addr)
{
  NS_LOG_FUNCTION (this << addr);
  m_oneHopNeighbors.erase (addr);
}

void
RoutingTable::UpdateTwoHopNeighbor (Ipv4Address addr, uint16_t cell)
{
  NS_LOG_FUNCTION (this << addr << cell);
  auto it = m_twoHopNeighbors.find (addr);
  if (it != m_twoHopNeighbors.end ())
   { // already exist in table. update values
    NS_LOG_LOGIC ("Update 2-hop neighbor: " << addr << " - " << cell);
    it->second.cell = cell;
    it->second.expiration = Simulator::Now () + CalculateEntryTtl (false);
   }
  else
   { // insert new entry
    NS_LOG_LOGIC ("New 2-hop neighbor: " << addr << " - " << cell);
    Hello h (cell, Simulator::Now () + CalculateEntryTtl (false));
    m_twoHopNeighbors.insert (PAIR (addr, h));

    DeleteMovingNeighbor (addr);
    DeleteOneHopNeighbor (addr);
   }
}

bool
RoutingTable::TwoHopNeighborExists (Ipv4Address addr)
{
  NS_LOG_FUNCTION (this << addr);
  auto it = m_twoHopNeighbors.find (addr);
  if (it != m_twoHopNeighbors.end ())
   { // node exists
    if (IsHelloExpired (it->second))
     { // if entry is expired, delete it
      m_twoHopNeighbors.erase (it);
      return false;
     }

    return true;
   }

  return false;
}

Locations
RoutingTable::TwoHopNeighborsExist (Locations dsts)
{ // returns the direct neighbors that actually exist
  NS_LOG_FUNCTION (this << dsts.size ());
  Locations directNeighbors;
  for (auto loc : dsts)
   if (TwoHopNeighborExists (loc.first)) directNeighbors.insert (loc);

  return directNeighbors;
}

uint32_t
RoutingTable::GetTotalNumTwoHopNeighbors () const
{
  NS_LOG_FUNCTION (this);
  return m_twoHopNeighbors.size ();
}

void
RoutingTable::DeleteTwoHopNeighbor (Ipv4Address addr)
{
  NS_LOG_FUNCTION (this << addr);
  m_twoHopNeighbors.erase (addr);
}

void
RoutingTable::UpdateMovingNeighbor (Ipv4Address addr, uint16_t cell)
{
  NS_LOG_FUNCTION (this << addr << cell);
  auto it = m_movingNeighbors.find (addr);
  if (it != m_movingNeighbors.end ())
   { // already exist in table. update values
    NS_LOG_LOGIC ("Update moving neighbor: " << addr << " - " << cell);
    it->second.cell = cell;
    it->second.expiration = Simulator::Now () + CalculateEntryTtl (true);
   }
  else
   { // insert new entry
    NS_LOG_LOGIC ("New moving neighbor: " << addr << " - " << cell);
    Hello h (cell, Simulator::Now () + CalculateEntryTtl (true));
    m_movingNeighbors.insert (PAIR (addr, h));

    DeleteOneHopNeighbor (addr);
    DeleteTwoHopNeighbor (addr);
   }
}

bool
RoutingTable::MovingNeighborExists (Ipv4Address addr)
{
  NS_LOG_FUNCTION (this << addr);
  auto it = m_movingNeighbors.find (addr);
  if (it != m_movingNeighbors.end ())
   { // node exists
    if (IsHelloExpired (it->second))
     { // if entry is expired, delete it
      m_movingNeighbors.erase (it);
      return false;
     }

    return true;
   }

  return false;
}

Locations
RoutingTable::MovingNeighborsExist (Locations dsts)
{ // returns the direct neighbors that actually exist
  NS_LOG_FUNCTION (this << dsts.size ());
  Locations directNeighbors;
  for (auto loc : dsts)
   if (MovingNeighborExists (loc.first)) directNeighbors.insert (loc);

  return directNeighbors;
}

uint32_t
RoutingTable::GetTotalNumMovingNeighbors () const
{
  NS_LOG_FUNCTION (this);
  return m_movingNeighbors.size ();
}

void
RoutingTable::DeleteMovingNeighbor (Ipv4Address addr)
{
  NS_LOG_FUNCTION (this << addr);
  m_movingNeighbors.erase (addr);
}

void
RoutingTable::RecalculateNeighborCells (uint16_t cell)
{
  NS_LOG_FUNCTION (this << cell);
  m_neighborCells = m_gridMan->GetAdjacentCells (cell);

  // clean old neighbors
  for (auto it = m_oneHopNeighbors.begin (); it != m_oneHopNeighbors.end ();)
   it = !IsMyNeighbor (it->second.cell) ? m_oneHopNeighbors.erase (it) : ++it;

  for (auto it = m_movingNeighbors.begin (); it != m_movingNeighbors.end ();)
   it = !IsMyNeighbor (it->second.cell) ? m_movingNeighbors.erase (it) : ++it;

  m_twoHopNeighbors.clear ();
}

std::vector<std::pair<uint16_t, Vector> > // < cell, cellCenter >
RoutingTable::GetPopulatedOneHopNeighborCellCenters () const
{
  NS_LOG_FUNCTION (this);
  std::vector<std::pair<uint16_t, Vector> > res;
  for (uint16_t cell : m_neighborCells)
   if (!IsCellEmpty (cell))
    res.push_back (PAIR (cell, m_gridMan->GetCellCenter (cell)));

  return res;
}

uint32_t
RoutingTable::GetNumNodesInOneHopNeighborCell (uint16_t cell) const
{
  NS_LOG_FUNCTION (this << cell);
  uint32_t res = 0;
  for (auto neighbor : m_oneHopNeighbors)
   if (neighbor.second.cell == cell) res++;

  return res;
}

std::vector<Ipv4Address>
RoutingTable::GetNodesInOneHopNeighborCell (uint16_t cell) const
{
  NS_LOG_FUNCTION (this << cell);
  std::vector<Ipv4Address> res;
  for (auto neighbor : m_oneHopNeighbors)
   if (neighbor.second.cell == cell) res.push_back (neighbor.first);

  return res;
}

Ipv4Address
RoutingTable::GetRandNodeFromOneHopNeighborCell (uint16_t cell) const
{
  NS_LOG_FUNCTION (this << cell);
  std::vector<Ipv4Address> nodes = GetNodesInOneHopNeighborCell (cell);
  NS_LOG_INFO ("One-hop cell " << cell << " has " << nodes.size ()
    << " node(s)");

  if (nodes.empty ()) return Ipv4Address::GetAny ();

  uint32_t rand = m_rand->GetInteger (0, nodes.size () - 1);
  NS_LOG_LOGIC ("Chosen node " << nodes[rand]);
  return nodes[rand]; // select random node in cell
}

/*
 * If dstCell is empty, priority is given to the lowest x followed by the
 * lowest y. This means that in the following configuration:
 * ------
 * --Wd--
 * ---S--
 * ------
 * W is the home cell, if it is populated, otherwise it is S, if it is
 * populated. Furthermore, only S and W may know that they are the home cells.
 * In the remaining cases it is necessary to go around the home perimeter.
 */
bool
RoutingTable::IsAdjacentHomeCell (uint16_t dstCell)
{
  NS_LOG_FUNCTION (this << dstCell);
  PurgeOneHopNeighbors ();

  uint16_t myCell = m_gridMan->GetCell ();
  if (IsMyNeighbor (dstCell))
   { // dst cell is my direct neighbor ...
    NS_LOG_LOGIC ("I (" << myCell << ") am direct neighbor of " << dstCell);
    if (IsCellEmpty (dstCell))
     { // ... and dst cell is considered empty
      NS_LOG_LOGIC ("Direct neighbor (" << dstCell << ") is considered empty");

      int west = m_gridMan->GetWestCell (dstCell);
      if (west == myCell && !IsMyCellEmpty ())
       { // I'm the cell west (left) of dst (and I'm not considered empty)
        NS_LOG_INFO ("I (" << myCell << ") am (west) home cell of "
          << dstCell);
        return true; // I'm home cell!
       }

      int south = m_gridMan->GetSouthCell (dstCell);
      if (south == myCell && (west == -1 || IsCellEmpty (west)))
       { // I'm the cell south of dst (and west cell doesn't exist or is considered empty)
        NS_LOG_INFO ("I (" << myCell << ") am (south) home cell of "
          << dstCell);
        return true; // I'm home cell!
       }
     }
   }

  return false;
}

std::vector<std::pair<uint16_t, Vector> > // < cell, cellCenter >
RoutingTable::GetPopulatedTwoHopNeighborCellCenters ()
{
  NS_LOG_FUNCTION (this);
  std::set<uint16_t> aux; // get cells and filter duplicates
  for (auto neighbor : m_twoHopNeighbors)
   aux.insert (neighbor.second.cell);

  std::vector<std::pair<uint16_t, Vector> > res;
  for (uint16_t cell : aux)
   res.push_back (PAIR (cell, m_gridMan->GetCellCenter (cell)));

  return res;
}

std::vector<Ipv4Address>
RoutingTable::GetNodesInTwoHopNeighborCell (uint16_t cell) const
{
  NS_LOG_FUNCTION (this << cell);
  std::vector<Ipv4Address> res;
  for (auto neighbor : m_twoHopNeighbors)
   if (neighbor.second.cell == cell) res.push_back (neighbor.first);

  return res;
}

Ipv4Address
RoutingTable::GetRandNodeFromTwoHopNeighborCell (uint16_t cell) const
{
  NS_LOG_FUNCTION (this << cell);
  std::vector<Ipv4Address> nodes = GetNodesInTwoHopNeighborCell (cell);
  NS_LOG_INFO ("Two-hop cell " << cell << " has " << nodes.size ()
    << " node(s)");

  if (nodes.empty ()) return Ipv4Address::GetAny ();

  uint32_t rand = m_rand->GetInteger (0, nodes.size () - 1);
  NS_LOG_LOGIC ("Chosen node " << nodes[rand]);
  return nodes[rand]; // select random node in cell
}

int
RoutingTable::GetBestOneHopNeighbor (uint16_t dstCell)
{
  NS_LOG_FUNCTION (this << dstCell);
  PurgeOneHopNeighbors (); // clears outdated neighbors from map
  auto ncc = GetPopulatedOneHopNeighborCellCenters ();
  uint16_t myCell = m_gridMan->GetCell ();
  NS_LOG_INFO ("I (" << myCell << ") have " << ncc.size ()
    << " pop. 1-hop neighb. cell(s)");

  if (ncc.empty ())
   { // no populated 1-hop neighboring cells
    NS_LOG_DEBUG ("No pop. 1-hop neighb. cells. No forwarding possible");
    return -1; // no forwarding possible
   }

  return GetBestCell (myCell, dstCell, ncc);
}

int
RoutingTable::GetBestTwoHopNeighbor (uint16_t dstCell)
{
  NS_LOG_FUNCTION (this << dstCell);
  PurgeTwoHopNeighbors (); // clears outdated neighbors from map
  auto ncc = GetPopulatedTwoHopNeighborCellCenters ();
  uint16_t myCell = m_gridMan->GetCell ();
  NS_LOG_INFO ("I (" << myCell << ") have " << ncc.size ()
    << " pop. 2-hop neighb. cell(s)");

  if (ncc.empty ())
   { // no populated 2-hop neighboring cells
    NS_LOG_DEBUG ("No pop. 2-hop neighb. cells. No forwarding possible");
    return -1; // no forwarding possible
   }

  return GetBestCell (myCell, dstCell, ncc);
}

int
RoutingTable::GetBestNeighborThan (uint16_t cell, uint16_t dstCell)
{
  NS_LOG_FUNCTION (this << cell << dstCell);
  PurgeOneHopNeighbors (); // clears outdated neighbors from map
  auto ncc = GetPopulatedOneHopNeighborCellCenters ();
  uint16_t myCell = m_gridMan->GetCell ();
  NS_LOG_INFO ("I (" << myCell << ") have " << ncc.size ()
    << " pop. 1-hop neighb. cell(s)");

  if (ncc.empty ())
   { // no populated 1-hop neighboring cells
    NS_LOG_DEBUG ("No pop. 1-hop neighb. cells. No forwarding possible");
    PurgeTwoHopNeighbors (); // clears outdated neighbors from map
    ncc = GetPopulatedTwoHopNeighborCellCenters ();
    NS_LOG_INFO ("I (" << myCell << ") have " << ncc.size ()
      << " pop. 2-hop neighb. cell(s)");

    if (ncc.empty ())
     { // no populated 2-hop neighboring cells
      NS_LOG_DEBUG ("No pop. 2-hop neighb. cells. No forwarding possible");
      return -1; // no forwarding possible
     }
   }

  return GetBestCell (cell, dstCell, ncc);
}

std::pair<int, int>
RoutingTable::GetBestAngle (uint16_t prevCell, bool firstTime, uint16_t dstCell)
{
  // if (firstTime) PERI-INIT-FORWARD(p), Brad Karp's PhD thesis, p. 49, Harvard U.
  // else RIGHT-HAND-FORWARD(p, n_in), Brad Karp's PhD thesis, p. 26, Harvard U.
  NS_LOG_FUNCTION (this << prevCell << firstTime << dstCell);
  uint16_t myCell = m_gridMan->GetCell ();
  PurgeOneHopNeighbors (); // clears outdated neighbors from map
  auto ncc = GetPopulatedOneHopNeighborCellCenters ();
  NS_LOG_INFO ("I (" << myCell << ") have " << ncc.size ()
    << " pop. 1-hop neighb. cell(s)");

  if (ncc.empty ())
   { // no populated 1-hop neighboring cells
    NS_LOG_DEBUG ("No pop. 1-hop neighb. cells. No forwarding possible");
    return PAIR (-1, -1); // no forwarding possible
   }

  int bestMinCell = -1; // a_min
  int bestMaxCell = -1;
  Vector prevCenter = m_gridMan->GetCellCenter (prevCell);
  Vector myCenter = m_gridMan->GetCellCenter (myCell);
  double inBearing = Norm (atan2 (myCenter.y - prevCenter.y,
    myCenter.x - prevCenter.x)); // b_in
  NS_LOG_LOGIC ("My angle to previous cell: " << RadToDeg (inBearing));

  // in PERI-INIT-FORWARD(p), prev is the dst node
  double deltaMin = 3 * M_PI; // delta_min
  double deltaMax = 0;
  double currBearing; // b_a
  double currDelta; // delta_b

  bool prevCellSkipped = false;
  for (auto neighbor : ncc) // table N
   {
    uint16_t currCell = neighbor.first;
    if (!firstTime && currCell == prevCell)
     { // pass prev node
      prevCellSkipped = true;
      NS_LOG_LOGIC ("Skip previous cell " << currCell);
      continue;
     }

    Vector curr = neighbor.second; // l
    currBearing = Norm (atan2 (myCenter.y - curr.y, myCenter.x - curr.x));
    currDelta = Norm (currBearing - inBearing);
    NS_LOG_LOGIC ("Cell " << currCell << " angle: " << RadToDeg (currBearing)
      << " (delta: " << RadToDeg (currDelta) << ")");

    if (currDelta < deltaMin)
     {
      bestMinCell = currCell;
      deltaMin = currDelta;
     }

    if (currDelta > deltaMax)
     {
      bestMaxCell = currCell;
      deltaMax = currDelta;
     }
   }

  /* - Not first time in perimeter mode
   * - Previous cell is (still) my (direct) neighbor and was skipped
   * - There is no other cell for perimeter forward
   * - Thus, choose previous cell as best cell
   * 
   * E-mails exchanged with Brad Karp:
   * The full version of RIGHT-HAND-FORWARD, which works on all faces, 
   * including degenerate (linear) ones, doesn't discard the ingress edge
   * from consideration. Rather, it considers all edges, but only forwards 
   * on the ingress edge when no other edge is present at that node.
   */
  if (prevCellSkipped && bestMinCell == -1) bestMinCell = prevCell;
  if (prevCellSkipped && bestMaxCell == -1) bestMaxCell = prevCell;

  NS_LOG_DEBUG ("Recovery best min cell: " << bestMinCell
    << " delta: " << RadToDeg (deltaMin));
  NS_LOG_DEBUG ("Recovery best max cell: " << bestMaxCell
    << " delta: " << RadToDeg (deltaMax));
  return PAIR (bestMinCell, bestMaxCell);
}

void
RoutingTable::ProcessTxError (Ipv4Address addr)
{
  NS_LOG_FUNCTION (this << addr);
  uint32_t del = m_oneHopNeighbors.erase (addr);
  del += m_twoHopNeighbors.erase (addr);
  del += m_movingNeighbors.erase (addr);

  if (del > 0)
   NS_LOG_WARN ("L2 feedback: purged node " << addr << " from routing table");
}

Time
RoutingTable::CalculateEntryTtl (bool moving) const
{
  NS_LOG_FUNCTION (this << moving);
  if (moving || m_loc->GetSpeed () > 0)
   { // if the node if moving or if I am moving
    return m_movingEntryTtl;
   } // else

  return m_stationaryEntryTtl;
}

bool
RoutingTable::IsMyCellEmpty () const
{
  NS_LOG_FUNCTION (this);
  // I have to count with myself (I'm not in my own routing table)
  return (GetNumNodesInOneHopNeighborCell (m_gridMan->GetCell ()) + 1)
    < m_popThreshold;
}

int
RoutingTable::GetBestCell (uint16_t myCell, uint16_t dstCell,
  std::vector<std::pair<uint16_t, Vector> > ncc)
{ // GREEDY-FORWARD(p), Brad Karp's PhD thesis, p. 15, Harvard U.
  NS_LOG_FUNCTION (this << dstCell << ncc.size ());
  int bestCell = myCell; // n_best

  Vector dstCenter = m_gridMan->GetCellCenter (dstCell);
  Vector myCenter = m_gridMan->GetCellCenter (myCell);
  double bestDist = CalculateDistance (myCenter, dstCenter); // d_best
  NS_LOG_LOGIC ("My dist to destination: " << bestDist);

  double currDist; // d

  for (auto neighbor : ncc) // table N
   {
    currDist = CalculateDistance (neighbor.second, dstCenter);
    uint16_t currCell = neighbor.first;
    NS_LOG_LOGIC ("Cell " << currCell << " dist: " << currDist);

    if (currDist <= bestDist || dstCell == currCell)
     { // is closer to dst. save best neighbor and distance
      if (currDist == bestDist)
       { // if distances are the same...
        Vector curr = m_gridMan->GetCellCenter (currCell);
        Vector best = m_gridMan->GetCellCenter (bestCell);
        if (curr.x < best.x || (curr.x == best.x && curr.y < best.y))
         { // ...break the tie. prefer lowest x followed by lowest y
          bestCell = currCell;
          bestDist = currDist;
         }
       }
      else
       { // choose the best
        bestCell = currCell;
        bestDist = currDist;
       }

      if (dstCell == currCell)
       { // current cell is dst cell. stop searching
        NS_LOG_LOGIC ("Current cell " << currCell << " is destination");
        break;
       }
     }
   }

  NS_LOG_DEBUG ("Greedy best cell: " << bestCell << " dist: " << bestDist);
  return bestCell == myCell ? -1 : bestCell;
}

double
RoutingTable::Norm (double x) const
{ // normalizes to [0, 2PI]
  x = fmod (x, (2 * M_PI));
  if (x < 0) x += (2 * M_PI);
  return x;
}

double
RoutingTable::RadToDeg (double rads)
{
  return rads * 180.0 / M_PI;
}

void
RoutingTable::PurgeOneHopNeighbors ()
{
  NS_LOG_FUNCTION (this);
  uint32_t size = m_oneHopNeighbors.size ();
  for (auto it = m_oneHopNeighbors.begin (); it != m_oneHopNeighbors.end ();)
   it = IsHelloExpired (it->second) ? m_oneHopNeighbors.erase (it) : ++it;

  NS_LOG_INFO ("Purged " << (size - m_oneHopNeighbors.size ())
    << " 1-hop neighbor(s)");
}

void
RoutingTable::PurgeTwoHopNeighbors ()
{
  NS_LOG_FUNCTION (this);
  uint32_t size = m_twoHopNeighbors.size ();
  for (auto it = m_twoHopNeighbors.begin (); it != m_twoHopNeighbors.end ();)
   it = IsHelloExpired (it->second) ? m_twoHopNeighbors.erase (it) : ++it;

  NS_LOG_INFO ("Purged " << (size - m_twoHopNeighbors.size ())
    << " 2-hop neighbor(s)");
}

void
RoutingTable::PurgeMovingNeighbors ()
{
  NS_LOG_FUNCTION (this);
  uint32_t size = m_movingNeighbors.size ();
  for (auto it = m_movingNeighbors.begin (); it != m_movingNeighbors.end ();)
   it = IsHelloExpired (it->second) ? m_movingNeighbors.erase (it) : ++it;

  NS_LOG_INFO ("Purged " << (size - m_movingNeighbors.size ())
    << " moving neighbor(s)");
}

bool
RoutingTable::IsHelloExpired (Hello h) const
{
  NS_LOG_FUNCTION (this << h.cell << h.expiration);
  return h.expiration < Simulator::Now ();
}

}
}

