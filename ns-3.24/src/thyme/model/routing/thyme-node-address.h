/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2015 NOVA LINCS DI-FCT-UNL <http://nova-lincs.di.fct.unl.pt/>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Joao A. Silva <jaa.silva@campus.fct.unl.pt>
 */
#ifndef THYME_NODE_ADDRESS_H
#define THYME_NODE_ADDRESS_H

#include <ns3/buffer.h>
#include <ns3/ipv4-address.h>

namespace ns3 {
namespace hyrax {

/**
 * \ingroup hyrax
 * 
 * \brief Represents a node's address in Thyme's version 2 DCS. It is
 * comprised by the node's IP address and the node's current cell.
 */
class NodeAddress : public SimpleRefCount<NodeAddress>
{
private:
  Ipv4Address m_addr;
  uint16_t m_cell;

public:
  NodeAddress (); // for deserialization **only**
  NodeAddress (Ipv4Address addr, uint16_t cell);

  Ipv4Address GetAddress () const;
  uint16_t GetCell () const;
  void SetCell (uint16_t cell);

  void Serialize (Buffer::Iterator &start) const;
  uint32_t Deserialize (Buffer::Iterator &start);
  uint32_t GetSerializedSize () const;
  void Print (std::ostream &os) const;
};

std::ostream& operator<< (std::ostream &os, const NodeAddress &node);
std::ostream& operator<< (std::ostream &os, Ptr<const NodeAddress> &node);
bool operator!= (const NodeAddress &nodeL, const NodeAddress &nodeR);
bool operator== (const NodeAddress &nodeL, const NodeAddress &nodeR);

}
}

#endif /* THYME_NODE_ADDRESS_H */

