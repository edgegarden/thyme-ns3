/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2015 NOVA LINCS DI-FCT-UNL <http://nova-lincs.di.fct.unl.pt/>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Joao A. Silva <jaa.silva@campus.fct.unl.pt>
 */
#ifndef THYME_ROUTING_H
#define THYME_ROUTING_H

#include "thyme-grid-manager.h"
#include "thyme-routing-table.h"
#include <queue>
#include <ns3/wifi-mac-queue.h>
#include <ns3/arp-cache.h>
#include <ns3/ipv4.h>
#include <ns3/thyme-common.h>
#include <ns3/thyme-data-message.h>
#include <ns3/thyme-profiler.h>
#include <ns3/wifi-mac-header.h>

namespace ns3 {
namespace hyrax {

#define CONTROL_PORT 0xff
#define DATA_PORT 0xfff
#define HELLO_INT 800 // milliseconds
#define JITTER_INT 0.5 // 50% of hello_int
#define MAX_MSG_TTL 70 // max overall network message TTL
#define C2DA_OPT true // true - routing destinations aggregation
#define L2_FEEDBACK_OPT true // true - turn layer 2 feedback on
#define PIGGYBACK_OPT true // true - enable piggybacked cell in data msgs
#define EAGER_GREEDY_RET_OPT false // true - try to return to greedy eagerly
#define NACK_OPT true // true - send nacks for subNot msgs

/**
 * Simplifications:
 * - only ipv4
 * - each Ipv4 iface has only one Ipv4 address
 * - first iface (that's not the loopback) is ad-hoc
 */

class RoutingProtocol : public Object
{
private:
  Ptr<Node> m_node; // Node
  uint32_t m_nodeId;
  Ptr<Ipv4> m_ipv4; // Ipv4
  Ptr<UniformRandomVariable> m_rand; // Pseudo-random number generator
  Ptr<GridManager> m_gridMan;
  Ptr<LocationSensor> m_loc; // Location sensor
  Ptr<RoutingTable> m_rTable; // Neighbors (routing) table
  Ptr<ArpCache> m_arp;
  Ptr<Profiler> m_profiler;

  std::set<uint16_t> m_noLoopHomeCell;

  Ptr<NetDevice> m_iface;
  Ipv4Address m_addr;
  Ptr<Socket> m_ctrlSocket;
  uint32_t m_ctrlPort; // UDP port for control traffic
  InetSocketAddress m_bcastCtrlAddr;
  Ptr<Socket> m_dataSocket;
  uint32_t m_dataPort; // UDP port for data traffic
  InetSocketAddress m_bcastDataAddr;

  Timer m_helloTimer; // Hello beacons timer
  // **Brad Karp's PhD thesis, p. 15, Harvard U.**
  // Jitter uniformly distributed between [0.5B; 1.5B]; B = m_helloInterval
  // Desynchronizing factor for hello beacons (half of the hello interval) 0.5B
  double m_helloJitter;
  Time m_helloInt; // Interval between hello beacons

  uint16_t m_currCell;
  uint16_t m_prevCell;
  uint16_t m_prevStableCell;
  Ptr<NodeAddress> m_nodeAddr;
  bool m_moving;

  Callback<void, DataMessage, bool, uint64_t> m_forwardUpFn;
  Callback<void, uint16_t, uint16_t> m_nodeMovedFn;
  Callback<void, uint16_t, uint16_t> m_nodeStabilizedFn;
  Callback<void, Ipv4Address> m_sameCellBeaconFn;

  bool m_destAggr;
  bool m_l2Feedback;
  bool m_piggybackInfo;
  bool m_noRepLoop;
  bool m_chooseHand;
  bool m_eagerGreedyRet;
  bool m_nackOpt;

  uint8_t m_maxTtl;

  bool m_ipv4Fwd;

  bool m_stabilizedWhileDown;

  struct NextHop
  {
    FwdMode mode;
    uint16_t nextHop;
    bool repeating;
    uint8_t hand;
    bool changed;

    NextHop (FwdMode m, uint16_t n, bool r, uint8_t h, bool c) : mode (m),
      nextHop (n), repeating (r), hand (h), changed (c)
    {
    }
  };

  Ptr<WifiMacQueue> m_queue;

public:
  static TypeId GetTypeId (void);
  RoutingProtocol ();
  virtual ~RoutingProtocol ();
  void Start (Ptr<Node> node, Ptr<GridManager> gridMan, Ptr<LocationSensor> loc, Ptr<Profiler> profiler);

  uint16_t GetMyCell () const;
  Ipv4Address GetMyAddress () const;
  Ptr<const RoutingTable> GetRoutingTable () const;
  Location GetClosestReplicaToMe (Locations locs) const;
  bool IsMoving () const;
  void StartBeaconing ();
  void StopBeaconing ();

  void SendToCell (DataMessage msg, uint16_t cell);
  void SendToCells (DataMessage msg, Locations dsts);
  uint64_t SendToNode (DataMessage msg, Ptr<NodeAddress> dst);
  uint64_t SendToNode (DataMessage msg, Ipv4Address addr, uint16_t cell);
  std::vector<uint64_t> SendToNodes (DataMessage msg, Locations dsts);
  void BroadcastInCell (DataMessage msg);
  bool SendToNeighbor (DataMessage msg, Ipv4Address lastNeighbor);

  void SetForwardUpCallback (Callback<void, DataMessage, bool, uint64_t> forwardUpFn);
  void SetNodeMovedCallback (Callback<void, uint16_t, uint16_t> nodeMovedFn);
  void SetNodeStabilizedCallback (Callback<void, uint16_t, uint16_t> nodeStabilizedFn);
  void SetSameCellBeaconCallback (Callback<void, Ipv4Address> sameCellBeaconFn);

  void IfDown ();
  void IfUp ();
  bool IsIfaceUp ();

  bool IsMyCellEmpty () const;
  bool StabilizedWhileDown () const;

protected:
  virtual void DoDispose ();

private:
  void AssignInterface ();
  void CreateSockets ();

  void ProcessCtrlPacket (Ptr<Socket> socket);
  void HelloTimerExpired ();
  void SendHelloMsg ();
  Time GetRandHelloInterval () const;
  void ResetHelloTimer ();

  bool ProcessPromiscPacket (Ptr<NetDevice> dev, Ptr<const Packet> p, uint16_t proto,
                             const Address &sender, const Address &recvr, NetDevice::PacketType pcktType);

  void ProcessDataPacket (Ptr<Socket> socket);

  void ForwardUp (DataMessage msg, bool isCoord, uint64_t pcktId);
  void DeliverToOwnCell (DataMessage msg, Ptr<Packet> packet);

  void ForwardMsg (DataMessage msg, Ptr<Packet> packet);
  Locations ForwardToDirectNeighbors (DataMessage msg, Ptr<Packet> packet);

  NextHop CheckForwardDestination (uint16_t dstCell, uint16_t lastGreedy, bool inRec, MessageTarget msgTarget,
                                   uint16_t lastHop, uint16_t firstPerHop, uint8_t hand);
  bool IsStillInPerimeterMode (uint16_t currCell, uint16_t lastGreedy, uint16_t dstCell) const;
  std::pair<uint8_t, uint16_t> GetBestPerimeterHand (uint16_t dstCell, uint16_t minCell, uint16_t maxCell) const;

  void OneHopUnicast (DataMessage msg, Ipv4Address addr, Ptr<Packet> packet);
  void CellBroadcast (DataMessage msg, Ptr<Packet> packet);

  bool IsRepeatingPath (uint16_t myCell, uint16_t lastGreedy, uint16_t nextHopCell, uint16_t firstPerHop) const;

  void NodeMoved (Vector pos, Vector vel, CardinalPos head, Time update);
  void NodeStabilized (Vector pos, Vector vel, CardinalPos head, Time update);

  void ProcessTxErrHeader (WifiMacHeader const &h, uint64_t puid);
  void ProcessTxOkHeader (WifiMacHeader const &h, uint64_t puid);

  void ProcessArpCacheDrop (Ptr<const Packet> p);
  void ProcessArpProtocolDrop (Ptr<const Packet> p);
};

}
}

#endif /* THYME_ROUTING_H */

