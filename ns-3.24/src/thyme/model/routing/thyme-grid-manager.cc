/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2015 NOVA LINCS DI-FCT-UNL <http://nova-lincs.di.fct.unl.pt/>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Joao A. Silva <jaa.silva@campus.fct.unl.pt>
 */
#include "thyme-grid-manager.h"
#include <ns3/double.h>
#include <ns3/log.h>

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("GM");

namespace hyrax {

NS_OBJECT_ENSURE_REGISTERED (GridManager);

TypeId
GridManager::GetTypeId (void)
{
  NS_LOG_FUNCTION_NOARGS ();
  static TypeId tid = TypeId ("ns3::hyrax::GridManager")
    .SetParent<Object> ()
    .AddConstructor<GridManager> ()

    .AddAttribute ("CellSizeX",
    "Size of each cell in the x axis (m).",
    DoubleValue (CELL_SIZE_X),
    MakeDoubleAccessor (&GridManager::m_cellSizeX),
    MakeDoubleChecker<double> ())

    .AddAttribute ("CellSizeY",
    "Size of each cell in the y axis (m).",
    DoubleValue (CELL_SIZE_Y),
    MakeDoubleAccessor (&GridManager::m_cellSizeY),
    MakeDoubleChecker<double> ())

    .AddAttribute ("SizeX",
    "Total size of the area in the x axis (m).",
    DoubleValue (SIZE_X),
    MakeDoubleAccessor (&GridManager::m_sizeX),
    MakeDoubleChecker<double> ())

    .AddAttribute ("SizeY",
    "Total size of the area in the y axis (m).",
    DoubleValue (SIZE_Y),
    MakeDoubleAccessor (&GridManager::m_sizeY),
    MakeDoubleChecker<double> ());

  return tid;
}

GridManager::GridManager () : m_cellSizeX (-1), m_cellSizeY (-1), m_sizeX (-1),
  m_sizeY (-1), m_maxCellsX (-1), m_maxCellsY (-1), m_numCells (-1), m_loc (0)
{
  NS_LOG_FUNCTION (this);
}

GridManager::~GridManager ()
{
  NS_LOG_FUNCTION (this);
  m_loc = 0;
}

void
GridManager::Start (Ptr<LocationSensor> loc)
{
  NS_LOG_FUNCTION (this);
  m_maxCellsX = ceil (m_sizeX / m_cellSizeX);
  m_maxCellsY = ceil (m_sizeY / m_cellSizeY);
  m_numCells = m_maxCellsX * m_maxCellsY;

  m_arrayPos[EAST] = 1;
  m_arrayPos[NORTHEAST] = m_maxCellsX + 1;
  m_arrayPos[NORTH] = m_maxCellsX;
  m_arrayPos[NORTHWEST] = m_maxCellsX - 1;
  m_arrayPos[WEST] = -1;
  m_arrayPos[SOUTHWEST] = -m_maxCellsX - 1;
  m_arrayPos[SOUTH] = -m_maxCellsX;
  m_arrayPos[SOUTHEAST] = -m_maxCellsX + 1;

  m_loc = loc;
}

uint16_t
GridManager::GetCell () const
{
  NS_LOG_FUNCTION (this);
  return GetCell (m_loc->GetPosition ());
}

uint16_t
GridManager::GetCell (Vector pos) const
{
  NS_LOG_FUNCTION (this << pos);
  uint16_t cell = m_maxCellsX * floor (pos.y / m_cellSizeY)
    + floor (pos.x / m_cellSizeX);

  NS_LOG_LOGIC ("Position " << pos << " is in cell " << cell);
  return cell;
}

Vector
GridManager::GetCellCenter (uint16_t cell) const
{
  NS_LOG_FUNCTION (this << cell);
  const double x = m_cellSizeX * (cell % m_maxCellsX + 0.5);
  const double y = m_cellSizeY * (floor (cell / m_maxCellsX) + 0.5);

  NS_LOG_LOGIC ("Center of cell " << cell << ": " << x << "," << y);
  return Vector (x, y, 0);
}

uint16_t
GridManager::GetTotalNumCells () const
{
  NS_LOG_FUNCTION (this);
  return m_numCells;
}

std::vector<uint16_t>
GridManager::GetAdjacentCells (uint16_t cell) const
{
  NS_LOG_FUNCTION (this << cell);
  std::vector<uint16_t> neighbors;

  for (int i = 0; i < 8; ++i)
   {
    int curr = cell + m_arrayPos[i];
    int horizDiff = (curr % m_maxCellsX) - (cell % m_maxCellsX);
    if ((curr >= 0 && curr < m_numCells) && // controls NORTH and SOUTH
        (horizDiff >= -1 && horizDiff <= 1)) // controls EAST and WEST
     neighbors.push_back (curr);
   }

  NS_LOG_LOGIC ("Cell " << cell << " has " << neighbors.size ()
    << " adjacent cells");
  return neighbors;
}

int
GridManager::GetSouthCell (uint16_t cell) const
{
  NS_LOG_FUNCTION (this << cell);
  int other = cell + m_arrayPos[SOUTH];
  return other >= 0 ? other : -1;
}

int
GridManager::GetWestCell (uint16_t cell) const
{
  NS_LOG_FUNCTION (this << cell);
  int other = cell + m_arrayPos[WEST];
  return (other % m_maxCellsX) == (cell % m_maxCellsX - 1) ? other : -1;
}

uint32_t
GridManager::CalculateManhattanDistance (uint16_t a, uint16_t b) const
{
  NS_LOG_FUNCTION (this << a << b);
  uint32_t horizDiff = abs ((a % m_maxCellsX) - (b % m_maxCellsX));
  uint32_t vertDiff = abs (floor (a / m_maxCellsX) - floor (b / m_maxCellsX));
  return horizDiff + vertDiff;
}

}
}

