/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2015 NOVA LINCS DI-FCT-UNL <http://nova-lincs.di.fct.unl.pt/>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Joao A. Silva <jaa.silva@campus.fct.unl.pt>
 */
#include "thyme-routing.h"
#include <ns3/pointer.h>
#include <ns3/dca-txop.h>
#include <ns3/abort.h>
#include <ns3/boolean.h>
#include <ns3/double.h>
#include <ns3/ipv4-header.h>
#include <ns3/ipv4-interface.h>
#include <ns3/ipv4-l3-protocol.h>
#include <ns3/log.h>
#include <ns3/thyme-hello-beacon.h>
#include <ns3/thyme-utils.h>
#include <ns3/udp-header.h>
#include <ns3/udp-l4-protocol.h>
#include <ns3/udp-socket-factory.h>
#include <ns3/wifi-mac.h>
#include <ns3/wifi-net-device.h>

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("RP");

namespace hyrax {

NS_OBJECT_ENSURE_REGISTERED (RoutingProtocol);

TypeId
RoutingProtocol::GetTypeId (void)
{
  NS_LOG_FUNCTION_NOARGS ();
  static TypeId tid = TypeId ("ns3::hyrax::RoutingProtocol")
    .SetParent<Object> ()
    .AddConstructor<RoutingProtocol> ()

    .AddAttribute ("ControlPort",
    "Control messages port number.",
    UintegerValue (CONTROL_PORT),
    MakeUintegerAccessor (&RoutingProtocol::m_ctrlPort),
    MakeUintegerChecker<uint32_t> ())

    .AddAttribute ("DataPort",
    "Data messages port number.",
    UintegerValue (DATA_PORT),
    MakeUintegerAccessor (&RoutingProtocol::m_dataPort),
    MakeUintegerChecker<uint32_t> ())

    .AddAttribute ("HelloInt",
    "HELLO beacons transmission interval (seconds).",
    TimeValue (MilliSeconds (HELLO_INT)),
    MakeTimeAccessor (&RoutingProtocol::m_helloInt),
    MakeTimeChecker ())

    .AddAttribute ("HelloJitter",
    "Jitter desync interval for HELLO beacons (seconds).",
    DoubleValue (JITTER_INT),
    MakeDoubleAccessor (&RoutingProtocol::m_helloJitter),
    MakeDoubleChecker<double> ())

    .AddAttribute ("RoutingOpt", "Message destinations aggregation flag.",
    BooleanValue (C2DA_OPT),
    MakeBooleanAccessor (&RoutingProtocol::m_destAggr),
    MakeBooleanChecker ())

    .AddAttribute ("L2FeedbackOpt", "Layer 2 feedback flag.",
    BooleanValue (L2_FEEDBACK_OPT),
    MakeBooleanAccessor (&RoutingProtocol::m_l2Feedback),
    MakeBooleanChecker ())

    .AddAttribute ("PiggybackOpt", "Use piggybacked cell in data msgs flag.",
    BooleanValue (PIGGYBACK_OPT),
    MakeBooleanAccessor (&RoutingProtocol::m_piggybackInfo),
    MakeBooleanChecker ())

    .AddAttribute ("NoRepLoopOpt", "Use no repeating loops flag.",
    BooleanValue (NO_REP_LOOP_OPT),
    MakeBooleanAccessor (&RoutingProtocol::m_noRepLoop),
    MakeBooleanChecker ())

    .AddAttribute ("ChooseHandOpt", "Use choose peri hand flag.",
    BooleanValue (CHOOSE_HAND_OPT),
    MakeBooleanAccessor (&RoutingProtocol::m_chooseHand),
    MakeBooleanChecker ())

    .AddAttribute ("EagerGreedyRetOpt", "Use eager greedy return flag.",
    BooleanValue (EAGER_GREEDY_RET_OPT),
    MakeBooleanAccessor (&RoutingProtocol::m_eagerGreedyRet),
    MakeBooleanChecker ())

    .AddAttribute ("NackOpt", "Send NACKs for msgs sent to nodes.",
    BooleanValue (NACK_OPT),
    MakeBooleanAccessor (&RoutingProtocol::m_nackOpt),
    MakeBooleanChecker ())

    .AddAttribute ("MaxMsgTtl", "Maximum msgs TTL.",
    UintegerValue (MAX_MSG_TTL),
    MakeUintegerAccessor (&RoutingProtocol::m_maxTtl),
    MakeUintegerChecker<uint8_t> ());

  return tid;
}

RoutingProtocol::RoutingProtocol () : m_node (0), m_nodeId (-1), m_ipv4 (0),
  m_rand (0), m_gridMan (0), m_loc (0), m_rTable (0), m_arp (0), m_profiler (0),
  m_iface (0), m_addr (), m_ctrlSocket (0), m_ctrlPort (-1), m_bcastCtrlAddr (-1),
  m_dataSocket (0), m_dataPort (-1), m_bcastDataAddr (-1),
  m_helloTimer (Timer::CANCEL_ON_DESTROY), m_helloJitter (-1), m_helloInt (0),
  m_currCell (-1), m_prevCell (-1), m_prevStableCell (-1), m_nodeAddr (0),
  m_moving (false),
  m_destAggr (true), m_l2Feedback (true), m_piggybackInfo (true),
  m_noRepLoop (true), m_chooseHand (true), m_eagerGreedyRet (false),
  m_nackOpt (true), m_maxTtl (-1), m_ipv4Fwd (false), m_stabilizedWhileDown (false)
{
  NS_LOG_FUNCTION (this);
}

RoutingProtocol::~RoutingProtocol ()
{
  NS_LOG_FUNCTION (this);
}

void
RoutingProtocol::Start (Ptr<Node> node, Ptr<GridManager> gridMan,
  Ptr<LocationSensor> loc, Ptr<Profiler> profiler)
{
  NS_LOG_FUNCTION (this << node);
  m_node = node;
  m_nodeId = m_node->GetId ();
  m_ipv4 = m_node->GetObject<Ipv4> ();
  m_rand = CreateObject<UniformRandomVariable> ();
  m_gridMan = gridMan;
  m_loc = loc;
  m_profiler = profiler;

  // set location sensor callbacks
  NS_LOG_INFO ("Set location sensor callbacks");
  m_loc->SetMovedCallback (MakeCallback (&RoutingProtocol::NodeMoved, this));
  m_loc->SetStabilizedCallback (MakeCallback (&RoutingProtocol::NodeStabilized, this));
  m_loc->Start (m_node);
  // can get valid position and cellId from here on

  // create routing table
  NS_LOG_INFO ("Create routing table");
  m_rTable = CreateObject<RoutingTable> ();
  m_rTable->Start (m_gridMan, m_loc);

  m_currCell = m_gridMan->GetCell ();
  m_prevStableCell = m_currCell;
  NS_LOG_INFO ("Node " << m_nodeId << " in cell " << m_currCell);

  // set hello beacon timer function
  NS_LOG_INFO ("Set hello timer function");
  m_helloTimer.SetFunction (&RoutingProtocol::HelloTimerExpired, this);

  // setup routing layer
  NS_LOG_INFO ("Setup routing layer");
  AssignInterface ();
  CreateSockets ();
  // can send/recv msgs from here on

  // extract and set ARP cache
  NS_LOG_INFO ("Set ARP cache");
  Ptr<Ipv4L3Protocol> ip = m_node->GetObject<Ipv4L3Protocol> ();
  Ptr<Ipv4Interface> ipIface =
    ip->GetInterface (ip->GetInterfaceForDevice (m_iface));
  m_arp = ipIface->GetArpCache ();
  m_arp->TraceConnectWithoutContext ("Drop",
    MakeCallback (&RoutingProtocol::ProcessArpCacheDrop, this));
}

uint16_t
RoutingProtocol::GetMyCell () const
{
  NS_LOG_FUNCTION (this);
  return m_currCell;
}

Ipv4Address
RoutingProtocol::GetMyAddress () const
{
  NS_LOG_FUNCTION (this);
  return m_addr;
}

Ptr<const RoutingTable>
RoutingProtocol::GetRoutingTable () const
{
  NS_LOG_FUNCTION (this);
  return m_rTable;
}

Location
RoutingProtocol::GetClosestReplicaToMe (Locations locs) const
{
  NS_LOG_FUNCTION (this << locs.size ());
  double bestDist = -1;
  Location best;
  Vector myCellCenter = m_gridMan->GetCellCenter (m_currCell);
  NS_LOG_LOGIC ("My cell: " << m_currCell << " (" << myCellCenter << ")");

  for (auto loc : locs)
   {
    Vector cell = m_gridMan->GetCellCenter (loc.second);
    double dist = CalculateDistance (myCellCenter, cell);
    NS_LOG_LOGIC ("Location " << loc.first << " cell " << loc.second
      << " (" << cell << ") dist to loc: " << dist);

    if (dist <= bestDist || bestDist == -1)
     {
      if (dist == bestDist)
       { // if distances are the same, load balance among nodes
        if (m_rand->GetInteger (0, 99) < 50) // toss coin to decide
         {
          bestDist = dist;
          best = loc;
         }
       }
      else
       {
        bestDist = dist;
        best = loc;
       }
     }
   }

  NS_LOG_LOGIC ("Best location: " << best.first << " cell: " << best.second);
  return best;
}

bool
RoutingProtocol::IsMoving () const
{
  NS_LOG_FUNCTION (this);
  return m_moving;
}

void
RoutingProtocol::StartBeaconing ()
{
  NS_LOG_FUNCTION (this);
  NS_LOG_INFO ("Beaconing started");
  HelloTimerExpired ();
}

void
RoutingProtocol::StopBeaconing ()
{
  NS_LOG_FUNCTION (this);
  NS_LOG_INFO ("Beaconing stopped");
  m_helloTimer.Cancel ();
}

void
RoutingProtocol::SendToCell (DataMessage msg, uint16_t cell)
{
  NS_LOG_FUNCTION (this << &msg << cell);
  msg.SetMsgTarget (CELL);
  msg.SetSource (m_nodeAddr);
  msg.AddDestination (cell);
  msg.SetLastHop (m_currCell);
  msg.SetGreedyMode ();
  msg.SetTtl (m_maxTtl);

  if (cell == m_currCell)
   { // optimize for sending to own cell
    DeliverToOwnCell (msg, Create<Packet>());
   }
  else
   {
    m_profiler->OnAppTx (m_nodeId, msg.GetMsgType (), msg.GetSerializedSize ());
    m_ipv4Fwd = false;
    ForwardMsg (msg, Create<Packet>());
   }
}

void
RoutingProtocol::SendToCells (DataMessage msg, Locations dsts)
{
  NS_LOG_FUNCTION (this << &msg << dsts.size ());
  if (!m_destAggr)
   { // if destinations aggregation is off...
    for (auto dst : dsts)
     SendToCell (msg, dst.second); // send to each destination separately
   }
  else
   {
    msg.SetMsgTarget (CELL);
    msg.SetSource (m_nodeAddr);
    msg.SetDestinations (dsts);
    msg.SetLastHop (m_currCell);
    msg.SetGreedyMode ();
    msg.SetTtl (m_maxTtl);

    if (msg.IsDestination (m_currCell))
     { // optimize for sending to own cell
      DeliverToOwnCell (msg, Create<Packet>());

      if (msg.RemoveDestination (m_currCell))
       { // remove own cell from destinations and send
        m_profiler->OnAppTx (m_nodeId, msg.GetMsgType (), msg.GetSerializedSize ());
        m_ipv4Fwd = false;
        ForwardMsg (msg, Create<Packet>());
       }
     }
    else
     {
      m_profiler->OnAppTx (m_nodeId, msg.GetMsgType (), msg.GetSerializedSize ());
      m_ipv4Fwd = false;
      ForwardMsg (msg, Create<Packet>());
     }
   }
}

uint64_t
RoutingProtocol::SendToNode (DataMessage msg, Ptr<NodeAddress> dst)
{
  return SendToNode (msg, dst->GetAddress (), dst->GetCell ());
}

uint64_t
RoutingProtocol::SendToNode (DataMessage msg, Ipv4Address addr, uint16_t cell)
{
  NS_LOG_FUNCTION (this << &msg << addr << cell);
  msg.SetMsgTarget (NODE);
  msg.SetSource (m_nodeAddr);
  msg.AddDestination (addr, cell);
  msg.SetLastHop (m_currCell);
  msg.SetGreedyMode ();
  msg.SetTtl (m_maxTtl);

  if (addr == m_addr)
   { // optimize for sending to self
    ForwardUp (msg, false, -1);

    return -1;
   }
  else
   {
    m_profiler->OnAppTx (m_nodeId, msg.GetMsgType (), msg.GetSerializedSize ());
    m_ipv4Fwd = false;
    Ptr<Packet> p = Create<Packet>();
    uint64_t puid = p->GetUid ();
    ForwardMsg (msg, p);

    return puid;
   }
}

std::vector<uint64_t>
RoutingProtocol::SendToNodes (DataMessage msg, Locations dsts)
{
  NS_LOG_FUNCTION (this << &msg << dsts.size ());
  if (!m_destAggr)
   { // if destinations aggregation is off...
    std::vector<uint64_t> aux;
    for (auto dst : dsts)
     { // send to each destination separately
      uint64_t res = SendToNode (msg, dst.first, dst.second);
      if (res != -1) aux.push_back (res);
     }

    return aux;
   }
  else
   {
    msg.SetMsgTarget (NODE);
    msg.SetSource (m_nodeAddr);
    msg.SetDestinations (dsts);
    msg.SetLastHop (m_currCell);
    msg.SetGreedyMode ();
    msg.SetTtl (m_maxTtl);

    if (msg.IsDestination (m_addr))
     { // optimize for sending to self
      ForwardUp (msg, false, -1);

      if (msg.RemoveDestination (m_addr))
       { // remove self from destinations and send
        m_profiler->OnAppTx (m_nodeId, msg.GetMsgType (), msg.GetSerializedSize ());
        m_ipv4Fwd = false;
        Ptr<Packet> p = Create<Packet>();
        std::vector<uint64_t> aux;
        aux.push_back (p->GetUid ());
        ForwardMsg (msg, p);

        return aux;
       }

      return std::vector<uint64_t> ();
     }
    else
     {
      m_profiler->OnAppTx (m_nodeId, msg.GetMsgType (), msg.GetSerializedSize ());
      m_ipv4Fwd = false;
      Ptr<Packet> p = Create<Packet>();
      std::vector<uint64_t> aux;
      aux.push_back (p->GetUid ());
      ForwardMsg (msg, p);

      return aux;
     }
   }
}

void
RoutingProtocol::BroadcastInCell (DataMessage msg)
{
  NS_LOG_FUNCTION (this << &msg);
  msg.SetSource (m_nodeAddr);
  msg.AddDestination (m_currCell);
  msg.SetMsgTarget (CELL_BCAST);
  DeliverToOwnCell (msg, Create<Packet>());
}

bool
RoutingProtocol::SendToNeighbor (DataMessage msg, Ipv4Address lastNeighbor)
{
  NS_LOG_FUNCTION (this << &msg);
  msg.SetLastHop (m_currCell);
  msg.SetGreedyMode ();

  uint32_t numNodes = m_rTable->GetNumNodesInOneHopNeighborCell (m_currCell);
  if (numNodes > 0)
   { // if I have neighbor to send the msg to...
    Ipv4Address neighbor =
      m_rTable->GetRandNodeFromOneHopNeighborCell (m_currCell);

    if (neighbor == lastNeighbor)
     { // I'm repeating the same neighbor
      if (numNodes == 1)
       { // I don't have any more neighbors to send the msg to...
        NS_LOG_INFO ("No neighbors to send the msg to");
        return false;
       }
      else
       { // I have more neighbor to choose from
        while (neighbor == lastNeighbor)
         { // try to get a different neighbor...
          neighbor = m_rTable->GetRandNodeFromOneHopNeighborCell (m_currCell);
         }
       }
     }

    m_profiler->OnAppTx (m_nodeId, msg.GetMsgType (), msg.GetSerializedSize ());
    NS_LOG_INFO ("Send to neighbor " << neighbor);
    OneHopUnicast (msg, neighbor, Create<Packet>());

    return true;
   }

  NS_LOG_INFO ("No neighbors to send the msg to");
  return false;
}

void
RoutingProtocol::SetForwardUpCallback (Callback<void, DataMessage, bool, uint64_t> forwardUpFn)
{
  m_forwardUpFn = forwardUpFn;
}

void
RoutingProtocol::SetNodeMovedCallback (Callback<void, uint16_t, uint16_t> nodeMovedFn)
{
  m_nodeMovedFn = nodeMovedFn;
}

void
RoutingProtocol::SetNodeStabilizedCallback (Callback<void, uint16_t, uint16_t> nodeStabilizedFn)
{
  m_nodeStabilizedFn = nodeStabilizedFn;
}

void
RoutingProtocol::SetSameCellBeaconCallback (Callback<void, Ipv4Address> sameCellBeaconFn)
{
  m_sameCellBeaconFn = sameCellBeaconFn;
}

void
RoutingProtocol::IfDown ()
{
  NS_LOG_FUNCTION (this);
  m_ipv4->SetDown (m_ipv4->GetInterfaceForDevice (m_iface));
}

void
RoutingProtocol::IfUp ()
{
  NS_LOG_FUNCTION (this);
  m_ipv4->SetUp (m_ipv4->GetInterfaceForDevice (m_iface));
}

bool
RoutingProtocol::IsIfaceUp ()
{
  NS_LOG_FUNCTION (this);
  return m_ipv4->IsUp (m_ipv4->GetInterfaceForDevice (m_iface));
}

bool
RoutingProtocol::IsMyCellEmpty () const
{
  NS_LOG_FUNCTION (this);
  return m_rTable->IsCellEmpty (m_currCell);
}

bool
RoutingProtocol::StabilizedWhileDown () const
{
  NS_LOG_FUNCTION (this);
  return m_stabilizedWhileDown;
}

void
RoutingProtocol::DoDispose ()
{
  NS_LOG_FUNCTION (this);
  m_helloTimer.Cancel ();

  m_ctrlSocket->Close ();
  m_dataSocket->Close ();

  // de-register promiscReceive callback
  m_iface->SetPromiscReceiveCallback (MakeNullCallback<bool,
    Ptr<NetDevice>, Ptr<const Packet>, uint16_t, const Address &,
    const Address &, NetDevice::PacketType > ());

  m_loc->SetMovedCallback (MakeNullCallback<void, Vector, Vector, CardinalPos, Time> ());
  m_loc->SetStabilizedCallback (MakeNullCallback<void, Vector, Vector, CardinalPos, Time> ());

  m_node = 0;
  m_ipv4 = 0;
  m_rand = 0;
  m_gridMan = 0;
  m_loc = 0;
  m_rTable = 0;
  m_arp = 0;
  m_profiler = 0;
  m_iface = 0;
  m_ctrlSocket = 0;
  m_dataSocket = 0;
  m_nodeAddr = 0;
}

void
RoutingProtocol::AssignInterface ()
{
  NS_LOG_FUNCTION (this);
  uint32_t devs = m_node->GetNDevices (); // get interfaces
  NS_LOG_INFO (devs << " device(s) found");

  for (int i = 0; i < devs; ++i)
   {
    Ptr<NetDevice> dev = m_node->GetDevice (i);
    NS_LOG_INFO ("Dev " << i << " " << dev->GetAddress () << " " <<
      dev->GetInstanceTypeId ().GetName () << " " <<
      m_ipv4->GetAddress (m_ipv4->GetInterfaceForDevice (dev), 0).GetLocal () <<
      " " << m_ipv4->GetInterfaceForDevice (dev));

    if (dev->GetInstanceTypeId ().GetName ().compare ("ns3::LoopbackNetDevice") != 0)
     { // if not the loopback iface (first one is considered the routing iface)
      m_iface = dev;
      m_addr = m_ipv4->GetAddress (m_ipv4->GetInterfaceForDevice (dev), 0).GetLocal ();
      m_nodeAddr = Create<NodeAddress> (m_addr, m_currCell);
      NS_LOG_INFO ("Iface: " << m_iface->GetAddress ());
      break;
     }
   }

  NS_ABORT_MSG_IF (m_iface == 0 || !m_ipv4->IsUp (m_ipv4->GetInterfaceForDevice (m_iface)),
    "Iface not assigned or not up");

  if (m_piggybackInfo) // set promiscuous mode on and set callback
   m_iface->SetPromiscReceiveCallback (MakeCallback (&RoutingProtocol::ProcessPromiscPacket, this));

  if (m_l2Feedback)
   { // allow neighbor table to use layer 2 feedback if possible
    Ptr<WifiNetDevice> wifi = m_iface->GetObject<WifiNetDevice> ();
    if (wifi != 0)
     {
      Ptr<WifiMac> mac = wifi->GetMac ();
      if (mac != 0)
       {
        mac->TraceConnectWithoutContext ("TxErrHeader",
          MakeCallback (&RoutingProtocol::ProcessTxErrHeader, this));
        mac->TraceConnectWithoutContext ("TxOkHeader",
          MakeCallback (&RoutingProtocol::ProcessTxOkHeader, this));
        NS_LOG_INFO ("Layer 2 feedback installed on iface");

        PointerValue ptr;
        mac->GetAttribute ("DcaTxop", ptr);
        Ptr<DcaTxop> dca = ptr.Get<DcaTxop> ();
        NS_ASSERT (dca != NULL);

        m_queue = dca->GetQueue ();
        NS_ASSERT (m_queue != NULL);

       }
     }
   }
}

void
RoutingProtocol::CreateSockets ()
{
  NS_LOG_FUNCTION (this);
  // control socket
  m_ctrlSocket = Socket::CreateSocket (m_node, UdpSocketFactory::GetTypeId ());
  NS_ABORT_MSG_IF (m_ctrlSocket == 0, "Control socket not created");
  m_ctrlSocket->SetAllowBroadcast (true);
  m_ctrlSocket->SetRecvCallback (MakeCallback (&RoutingProtocol::ProcessCtrlPacket, this));
  m_ctrlSocket->SetIpTtl (1); // one hop comm. only
  Ipv4Address bindAddr = Ipv4Address::GetAny ();
  int bind = m_ctrlSocket->Bind (InetSocketAddress (bindAddr, m_ctrlPort));
  NS_ABORT_MSG_IF (bind == -1, "Control socket not bound to addr");
  m_ctrlSocket->BindToNetDevice (m_iface);
  NS_LOG_INFO ("Bind control socket to " << bindAddr << ":" << m_ctrlPort);
  m_bcastCtrlAddr = InetSocketAddress (Ipv4Address::GetBroadcast (), m_ctrlPort);

  // data socket
  m_dataSocket = Socket::CreateSocket (m_node, UdpSocketFactory::GetTypeId ());
  NS_ABORT_MSG_IF (m_dataSocket == 0, "Data socket not created");
  m_dataSocket->SetAllowBroadcast (true);
  m_dataSocket->SetRecvCallback (MakeCallback (&RoutingProtocol::ProcessDataPacket, this));
  m_dataSocket->SetIpTtl (1); // one hop comm. only
  bind = m_dataSocket->Bind (InetSocketAddress (bindAddr, m_dataPort));
  NS_ABORT_MSG_IF (bind == -1, "Data socket not bound to addr");
  m_dataSocket->BindToNetDevice (m_iface);
  NS_LOG_INFO ("Bind data socket to " << bindAddr << ":" << m_dataPort);
  m_bcastDataAddr = InetSocketAddress (Ipv4Address::GetBroadcast (), m_dataPort);
}

void
RoutingProtocol::ProcessCtrlPacket (Ptr<Socket> socket)
{
  NS_LOG_FUNCTION (this << socket);
  Address src;
  Ptr<Packet> packet = socket->RecvFrom (src);
  Ipv4Address sender = InetSocketAddress::ConvertFrom (src).GetIpv4 ();
  m_profiler->OnCtlRx (m_nodeId, packet->GetSize ());

  HelloBeacon hello;
  packet->RemoveHeader (hello);
  NS_LOG_LOGIC ("Recv CTRL pckt " << packet->GetUid ()
    << " from " << sender << " - " << hello.GetCell ()
    << " " << hello.IsMoving ());

  uint16_t cell = hello.GetCell ();
  bool moving = hello.IsMoving ();
  m_rTable->UpdateNeighbor (sender, cell, moving);

  if (cell == m_currCell) m_sameCellBeaconFn (sender);
}

void
RoutingProtocol::HelloTimerExpired ()
{
  NS_LOG_FUNCTION (this);
  SendHelloMsg ();
  ResetHelloTimer ();
}

void
RoutingProtocol::SendHelloMsg ()
{
  NS_LOG_FUNCTION (this);
  Ptr<Packet> packet = Create<Packet> ();
  // if moving in own cell, send zero speed (still a router node)
  HelloBeacon beacon (m_currCell, m_moving);
  packet->AddHeader (beacon);
  int sent = m_ctrlSocket->SendTo (packet, 0, m_bcastCtrlAddr);
  if (sent == -1)
   {
    NS_LOG_WARN ("Error " << m_ctrlSocket->GetErrno () << " while bcast HELLO "
      << packet->GetUid () << " " << packet->GetSize ());
    return;
   }

  NS_LOG_LOGIC ("Bcast CTRL pckt " << packet->GetUid ()
    << " - " << beacon.GetCell () << " " << beacon.IsMoving ());
  m_profiler->OnCtlTx (m_nodeId, packet->GetSize ());
}

Time
RoutingProtocol::GetRandHelloInterval () const
{ // [0.5B, 1.5B]
  return MilliSeconds (m_helloInt.GetMilliSeconds ()
    + m_rand->GetValue (-(m_helloJitter * m_helloInt.GetMilliSeconds ()),
    (m_helloJitter * m_helloInt.GetMilliSeconds ())));
}

void
RoutingProtocol::ResetHelloTimer ()
{
  NS_LOG_FUNCTION (this);
  m_helloTimer.Cancel ();
  Time t = GetRandHelloInterval ();
  m_helloTimer.Schedule (t); // reschedule timer
  NS_LOG_LOGIC ("Beaconing rescheduled: " << t.GetMilliSeconds () << "ms");
}

bool
RoutingProtocol::ProcessPromiscPacket (Ptr<NetDevice> dev, Ptr<const Packet> p,
  uint16_t proto, const Address &sender, const Address &recvr,
  NetDevice::PacketType pcktType)
{
  if (pcktType != NetDevice::PACKET_OTHERHOST || proto != 2048)
   { // 2048 is ethertype of Ipv4 traffic (2054 is for ARP)
    return false;
   }

  // make copy to be able to remove headers freely
  Ptr<Packet> pckt = p->Copy ();
  Ipv4Header ip;
  pckt->RemoveHeader (ip); // remove ip header

  if (ip.GetProtocol () == UdpL4Protocol::PROT_NUMBER
      && ip.GetFragmentOffset () == 0)
   { // is UDP packet and is first fragment
    UdpHeader udp;
    pckt->RemoveHeader (udp); // remove udp header

    if (udp.GetDestinationPort () == m_dataPort)
     { // this is a data message (pckt for data socket port)
      //NS_LOG_FUNCTION (this << dev << p << p->GetUid () << proto << sender 
      //  << recvr << pcktType);
      HelloBeacon beacon;
      pckt->RemoveHeader (beacon);
      m_rTable->UpdateNeighbor (ip.GetSource (), beacon.GetCell (), beacon.IsMoving ());
     }
   }

  return false;
}

void
RoutingProtocol::ProcessDataPacket (Ptr<Socket> socket)
{
  NS_LOG_FUNCTION (this << socket);
  Address src;
  Ptr<Packet> packet = socket->RecvFrom (src);
  Ipv4Address sender = InetSocketAddress::ConvertFrom (src).GetIpv4 ();

  if (m_piggybackInfo)
   { // remove piggyback information
    HelloBeacon beacon;
    packet->RemoveHeader (beacon);
    m_rTable->UpdateNeighbor (sender, beacon.GetCell (), beacon.IsMoving ());
   }

  DataMessage msg;
  packet->RemoveHeader (msg);
  NS_LOG_LOGIC ("Recv DATA pckt " << packet->GetUid ()
    << " " << Utils::GetMsgTypeString (msg.GetMsgType ()) << " from " << sender);

  MessageTarget msgTarget = msg.GetMsgTarget ();
  switch (msgTarget)
  {
  case NODE:
  { // message addressed to specific node(s)
    if (msg.IsDestination (m_addr)) // I am a destination
     { // deliver message to self
      NS_LOG_LOGIC ("Msg addressed to me " << m_addr);
      ForwardUp (msg, false, packet->GetUid ());

      if (msg.RemoveDestination (m_addr))
       { // (after removing my addr from dsts) there are more destinations
        NS_LOG_LOGIC ("There are more destinations. Keep forwarding it.");
        break; // keep forwarding it
       }

      return; // there are no more destinations
     }

    NS_LOG_LOGIC ("Msg not addressed to me. Forward it.");
    break; // keep forwarding it
  }
  case CELL:
  { // message addressed to cell(s)
    if (msg.IsDestination (m_currCell))
     { // message addressed to my cell
      NS_LOG_LOGIC ("Msg addressed to my cell " << m_currCell);
      DeliverToOwnCell (msg, packet);

      if (msg.RemoveDestination (m_currCell))
       { // (after removing my cell from dsts) there are more destinations
        NS_LOG_LOGIC ("There are more destinations. Keep forwarding it.");
        break; // keep forwarding it
       }

      return; // there are no more destinations
     }

    NS_LOG_LOGIC ("Msg not addressed to my cell. Forward it.");
    break; // keep forwarding it
  }
  case CELL_BCAST:
  { // message broadcast in my cell or close to me
    if (msg.IsDestination (m_currCell))
     { // message addressed to my cell
      NS_LOG_LOGIC ("Msg cell bcast to my cell");
      ForwardUp (msg, false, packet->GetUid ()); // deliver message to self
      return;
     }
    else if (msg.GetHomeCell () == m_currCell)
     { // message addressed to my cell - I'm home cell of msg
      NS_LOG_LOGIC ("Msg cell bcast to my cell - home cell");
      if (m_noRepLoop && msg.GetNoRepHomeCell () != (uint16_t) - 1)
       m_noLoopHomeCell.insert (msg.GetNoRepHomeCell ());

      ForwardUp (msg, false, packet->GetUid ()); // deliver message to self
      return;
     }

    NS_LOG_LOGIC ("Msg cell bcast not for my cell. Ignore");
    return; // if not for my cell, ignore message
  }
  default:
  {
    NS_ABORT_MSG ("Unknown target " << msgTarget);
  }
  }

  if (msg.GetTtl () > 0)
   msg.SetTtl (msg.GetTtl () - 1);
  else
   { // drop packet
    NS_LOG_WARN ("Drop pckt " << packet->GetUid () << " "
      << Utils::GetMsgTypeString (msg.GetMsgType ()) << ". Max TTL");
    return;
   }

  m_ipv4Fwd = true;
  ForwardMsg (msg, packet); // otherwise, forward msg
}

void
RoutingProtocol::ForwardUp (DataMessage msg, bool isCoord, uint64_t pcktId)
{ // deliver msg to upper layer
  NS_LOG_FUNCTION (this << msg << isCoord << pcktId);
  m_profiler->OnAppRx (m_nodeId, msg.GetMsgType (), msg.GetSerializedSize ());
  m_forwardUpFn (msg, isCoord, pcktId);
}

void
RoutingProtocol::DeliverToOwnCell (DataMessage msg, Ptr<Packet> packet)
{ // deliver msg to self, and do a cell broadcast (if need be)
  NS_LOG_FUNCTION (this << &msg << packet);
  Simulator::ScheduleNow (&RoutingProtocol::ForwardUp, this, msg, true,
    packet->GetUid ()); // deliver message to self

  if (msg.IsCellBcast ()
      && m_rTable->GetNumNodesInOneHopNeighborCell (m_currCell) > 0)
   { // if cell bcast flag is on (by default, it is on)
    m_profiler->OnAppBcast (m_nodeId, msg.GetMsgType (),
      msg.GetSerializedSize ());
    CellBroadcast (msg, packet); // broadcast to other nodes in my cell
   }
}

void
RoutingProtocol::ForwardMsg (DataMessage msg, Ptr<Packet> packet)
{
  NS_LOG_FUNCTION (this << &msg << packet);
  if (m_rTable->GetTotalNumOneHopNeighbors () == 0
      && m_rTable->GetTotalNumTwoHopNeighbors () == 0)
   { // I have no neighbors. Nowhere to forward msg to. Drop it...
    NS_LOG_DEBUG ("NO NEIGHBORS pckt " << packet->GetUid () << " "
      << Utils::GetMsgTypeString (msg.GetMsgType ()));
    return;
   }

  // here, msg target will never be CELL_BCAST, only NODE or CELL
  MessageTarget msgTarget = msg.GetMsgTarget ();
  NS_ABORT_MSG_IF (msgTarget == CELL_BCAST, "Wrong message target");
  Locations dsts = msg.GetDestinations ();

  if (msgTarget == NODE) // message addressed to specific node(s)
   dsts = ForwardToDirectNeighbors (msg, packet);

  if (dsts.size () == 0) return;
  NS_LOG_DEBUG ("Keep forwarding packet " << packet->GetUid () << " to " << dsts.size () << " destination(s)");

  std::map<uint16_t, Locations> greedy; // < nextHop, dsts >
  std::map<uint16_t, Locations> peri; // < nextHop, dsts >

  std::vector<Ipv4Address> dropAddrs;

  uint8_t hand = msg.GetHand ();
  for (auto it = dsts.begin (); it != dsts.end (); ++it)
   {
    NextHop res = CheckForwardDestination (it->second, msg.GetLastGreedy (),
      msg.IsInRecovery (), msg.GetMsgTarget (), msg.GetLastHop (),
      msg.GetFirstPerHop (), hand);

    if (hand == 0) hand = res.hand;

    switch (res.mode)
    {
    case HOME_CELL:
    {
      msg.ClearDestinations ();
      msg.AddDestination (it->second);
      msg.SetHomeCell (m_currCell);

      if (m_noRepLoop)
       {
        msg.SetNoRepHomeCell (res.repeating ? it->second : -1);
        if (res.repeating) m_noLoopHomeCell.insert (it->second);
       }

      DeliverToOwnCell (msg, packet);
      break;
    }
    case GREEDY:
    {
      auto g = greedy.find (res.nextHop);
      if (g != greedy.end ())
       { // existent nextHop
        g->second.insert (*it);
       }
      else
       { // new nextHop
        Locations x;
        x.insert (*it);
        greedy.insert (PAIR (res.nextHop, x));
       }
      break;
    }
    case PERIMETER:
    {
      auto p = peri.find (res.nextHop);
      if (p != peri.end ())
       { // existent nextHop
        p->second.insert (*it);
       }
      else
       { // new nextHop
        Locations x;
        x.insert (*it);
        peri.insert (PAIR (res.nextHop, x));
       }
      break;
    }
    case DROP:
    {
      if (m_nackOpt && msg.GetMsgType () != NACK_MSG
          && msg.GetMsgType () != DOWNLOAD && msg.GetMsgType () != DOWNLOAD_RSP
          && msg.GetMsgTarget () == NODE)
       dropAddrs.push_back (it->first);
      break;
    }
    default:
    {
      NS_LOG_WARN ("Drop destination " << it->first);
    }
    }
   }

  if (m_nackOpt && dropAddrs.size () > 0)
   {
    m_profiler->OnNackSent (packet->GetUid (), msg.GetMsgType (), dropAddrs);
    DataMessage nack;
    nack.SetNackMsg (packet->GetUid (), dropAddrs);
    NS_LOG_WARN ("Send NACK for pckt " << packet->GetUid () << " to " << msg.GetSource ()->GetAddress ());
    SendToNode (nack, msg.GetSource ());
   }

  msg.SetHand (hand);
  msg.SetLastHop (m_currCell);
  bool inRec = msg.IsInRecovery ();

  ///////// process perimeter forward destinations
  if (!inRec)
   { // if received msg in greedy, I have to switch to perimeter
    msg.SetPerimeterMode (m_currCell);
   }

  for (auto it = peri.begin (); it != peri.end (); ++it)
   {
    msg.ClearDestinations ();
    msg.SetDestinations (it->second);

    if (!inRec)
     { // if received msg in greedy, I have to set firstPerHop
      msg.SetFirstPerHop (it->first);
     }

    Ipv4Address addr = m_rTable->GetRandNodeFromOneHopNeighborCell (it->first); // send to random node in cell
    if (m_ipv4Fwd) m_profiler->OnCtlFwd (m_nodeId, msg.GetSerializedSize ());
    OneHopUnicast (msg, addr, packet);
   }

  ///////// process greedy forward destinations
  msg.SetGreedyMode ();

  for (auto it = greedy.begin (); it != greedy.end (); ++it)
   {
    msg.ClearDestinations ();
    msg.SetDestinations (it->second);

    // send to random node in cell
    Ipv4Address addr = m_rTable->GetRandNodeFromOneHopNeighborCell (it->first);
    if (addr == Ipv4Address::GetAny ()) // check if nextHop is 2-hop neighbor
     addr = m_rTable->GetRandNodeFromTwoHopNeighborCell (it->first);

    if (m_ipv4Fwd) m_profiler->OnCtlFwd (m_nodeId, msg.GetSerializedSize ());
    OneHopUnicast (msg, addr, packet);
   }
}

Locations
RoutingProtocol::ForwardToDirectNeighbors (DataMessage msg, Ptr<Packet> packet)
{ // message addressed to specific node(s)
  NS_LOG_FUNCTION (this << &msg << packet);
  Locations directNeighbors = m_rTable->NeighborsExist (msg.GetDestinations ());
  msg.RemoveDestinations (directNeighbors);
  Locations dsts = msg.GetDestinations (); // remaining dsts to forward to

  if (!directNeighbors.empty ())
   NS_LOG_DEBUG ("Forward packet " << packet->GetUid ()
    << " " << Utils::GetMsgTypeString (msg.GetMsgType ()) << " to "
    << directNeighbors.size () << " direct neighbor(s)");

  for (auto it = directNeighbors.begin (); it != directNeighbors.end (); ++it)
   { // send msg to direct neighbors
    msg.ClearDestinations ();
    msg.AddDestination (it->first, it->second);
    if (m_ipv4Fwd) m_profiler->OnCtlFwd (m_nodeId, msg.GetSerializedSize ());
    OneHopUnicast (msg, it->first, packet);
   }

  std::vector<Ipv4Address> addrs;
  for (auto it = dsts.begin (); it != dsts.end ();)
   { // check nodes that should be in my cell
    if (it->second == m_currCell)
     {
      /* msg addressed to a node in my cell and that node is not in my
       * routing table. probably moved out of this cell meanwhile (or crashed)
       */
      addrs.push_back (it->first);
      NS_LOG_WARN ("NO DIRECT NEIGHBOR " << it->first
        << " pckt " << packet->GetUid () << " "
        << Utils::GetMsgTypeString (msg.GetMsgType ()) << ". Drop it");
      it = dsts.erase (it);
     }
    else ++it;
   }

  if (m_nackOpt && msg.GetMsgType () != NACK_MSG
      && msg.GetMsgType () != DOWNLOAD && msg.GetMsgType () != DOWNLOAD_RSP
      && addrs.size () > 0)
   { // NACKs
    m_profiler->OnNackSent (packet->GetUid (), msg.GetMsgType (), addrs);
    DataMessage nack;
    nack.SetNackMsg (packet->GetUid (), addrs);
    NS_LOG_WARN ("Send NACK for pckt " << packet->GetUid () << " to " << msg.GetSource ()->GetAddress ());
    SendToNode (nack, msg.GetSource ());
   }

  return dsts;
}

RoutingProtocol::NextHop
RoutingProtocol::CheckForwardDestination (uint16_t dstCell, uint16_t lastGreedy,
  bool inRec, MessageTarget msgTarget, uint16_t lastHop, uint16_t firstPerHop, uint8_t hand)
{
  NS_LOG_FUNCTION (this << dstCell << lastGreedy << inRec << msgTarget
    << lastHop << firstPerHop << (uint16_t) hand);
  bool changed = false;
  if (m_noRepLoop && msgTarget == CELL && m_noLoopHomeCell.find (dstCell) != m_noLoopHomeCell.end ())
   return NextHop (HOME_CELL, 0, true, 0, changed);

  if (msgTarget == CELL && m_rTable->IsAdjacentHomeCell (dstCell))
   { // check adjacent home cell
    NS_LOG_DEBUG ("Msg addressed to cell " << dstCell << " but I'm (adjacent) home cell " << m_currCell);
    return NextHop (HOME_CELL, 0, false, 0, changed);
   }

  ///////// check if still in perimeter mode
  bool firstTimePeri = true;
  if (inRec)
   { // in perimeter mode
    if (IsStillInPerimeterMode (m_currCell, lastGreedy, dstCell))
     { // this is not the first hop in perimeter mode
      firstTimePeri = false;
     }
    else
     { // switch back to greedy mode
      inRec = false;
      changed = true;
     }
   }

  ///////// try greedy mode
  if (!inRec)
   { // in greedy mode
    int nextHop = m_rTable->GetBestOneHopNeighbor (dstCell);
    if (nextHop != -1)
     { // found populated neighboring cell for greedy forward
      NS_LOG_DEBUG ("GREEDY FWD (dst: " << dstCell
        << ") from " << m_currCell << " to " << nextHop << " (1-hop)");
      return NextHop (GREEDY, (uint16_t) nextHop, false, 0, changed);
     }
    else
     { // try 2-hop neighbors
      nextHop = m_rTable->GetBestTwoHopNeighbor (dstCell);
      if (nextHop != -1)
       { // found populated 2-hop neighboring cell for greedy forward
        NS_LOG_DEBUG ("GREEDY FWD (dst: " << dstCell
          << ") from " << m_currCell << " to " << nextHop << " (2-hop)");
        return NextHop (GREEDY, (uint16_t) nextHop, false, 0, changed);
       }

      // must switch to perimeter mode
      NS_LOG_DEBUG ("NO GREEDY FWD to dst " << dstCell
        << ". Try PERI mode");
      inRec = true;
     }
   }

  ///////// try perimeter mode
  if (inRec)
   { // in perimeter mode
    if (m_eagerGreedyRet)
     {
      int aux = m_rTable->GetBestNeighborThan (lastGreedy, dstCell);
      if (aux != -1)
       {
        NS_LOG_DEBUG ("GREEDY FWD (dst: " << dstCell
          << ") from " << m_currCell << " to " << aux << " (eager 1-hop)");
        return NextHop (GREEDY, (uint16_t) aux, false, 0, changed);
       }
     }

    uint16_t prevCell = firstTimePeri ? dstCell : lastHop;
    std::pair<int, int> next = m_rTable->GetBestAngle (prevCell, firstTimePeri, dstCell);

    if (next.first == -1)
     { // no route to host
      NS_LOG_DEBUG ("NO ROUTE TO HOST. Dead end! (dst " << dstCell
        << "). I'm cell " << m_currCell);

      if (msgTarget == CELL) // this becomes home cell for this msg...
       return NextHop (HOME_CELL, 0, false, 0, changed); // this happens if I have no neighbors!
     }
    else
     { // found populated neighboring cell for perimeter forward
      uint16_t nextHop;
      uint8_t resHand;

      if (m_chooseHand)
       {
        if (hand == 0)
         {
          std::pair<uint8_t, uint16_t> res = GetBestPerimeterHand (dstCell, next.first, next.second);
          nextHop = res.second;
          resHand = res.first;
         }
        else if (hand == 1) // min CW
         {
          nextHop = next.first;
          resHand = 1;
         }
        else // max CCW
         {
          nextHop = next.second;
          resHand = 2;
         }
       }
      else
       { // default is min CW
        nextHop = next.first;
        resHand = 1;
       }

      if (!firstTimePeri && IsRepeatingPath (m_currCell, lastGreedy, nextHop, firstPerHop))
       { // repeating path. home perimeter circulated. I'm home cell!
        NS_LOG_WARN ("REPEATING PATH " << m_currCell << "-" << nextHop
          << " (dst: " << dstCell << ", target: "
          << Utils::GetMsgTargetString (msgTarget) << ")");
        if (msgTarget == CELL)
         {
          m_profiler->OnRepeatingPath (lastGreedy, firstPerHop);
          return NextHop (HOME_CELL, 0, true, 0, changed);
         }

        return NextHop (DROP, 0, false, 0, changed); // drop dst
       }

      NS_LOG_DEBUG ("PERI FWD (dst: " << dstCell
        << ") from " << m_currCell << " to " << nextHop);
      return NextHop (PERIMETER, nextHop, false, resHand, changed);
     }
   }

  return NextHop (DROP, 0, false, 0, changed); // drop dst
}

bool
RoutingProtocol::IsStillInPerimeterMode (uint16_t currCell, uint16_t lastGreedy, uint16_t dstCell) const
{ // check if still in perimeter mode
  NS_LOG_FUNCTION (this << currCell << lastGreedy << dstCell);
  Vector dstCellCenter = m_gridMan->GetCellCenter (dstCell);
  Vector lastGreedyPos = m_gridMan->GetCellCenter (lastGreedy);
  Vector currCellCenter = m_gridMan->GetCellCenter (currCell);

  double currDist = CalculateDistance (currCellCenter, dstCellCenter);
  double lastGreedyDist = CalculateDistance (lastGreedyPos, dstCellCenter);

  bool res = currDist < lastGreedyDist // if I am closer to dst than lastGreedy
    || (currDist == lastGreedyDist // in case of a tie in distance
        && (currCellCenter.x < lastGreedyPos.x // prefer lower x coord
            || (currCellCenter.x == lastGreedyPos.x // followed by lower y coord
                && currCellCenter.y < lastGreedyPos.y)));

  NS_LOG_DEBUG ("Switch to greedy? curr dist: " << currDist
    << " lastGreedy dist: " << lastGreedyDist << (res ? " YES!" : " NO!"));
  return !res;
}

std::pair<uint8_t, uint16_t>
RoutingProtocol::GetBestPerimeterHand (uint16_t dstCell, uint16_t minCell, uint16_t maxCell) const
{
  NS_LOG_FUNCTION (this << dstCell << minCell << maxCell);
  Vector dstCellCenter = m_gridMan->GetCellCenter (dstCell);
  Vector minCellCenter = m_gridMan->GetCellCenter (minCell);
  Vector maxCellCenter = m_gridMan->GetCellCenter (maxCell);

  double toMin = CalculateDistance (minCellCenter, dstCellCenter);
  double toMax = CalculateDistance (maxCellCenter, dstCellCenter);

  if (toMin < toMax) return PAIR (1, minCell);
  else if (toMax < toMin) return PAIR (2, maxCell);
  else return PAIR (1, minCell);
  // in case of a tie, use clock-wise perimeter routing
}

void
RoutingProtocol::OneHopUnicast (DataMessage msg, Ipv4Address addr, Ptr<Packet> packet)
{
  NS_LOG_FUNCTION (this << &msg << addr << packet);
  packet->RemoveAllPacketTags (); // to prevent errors
  packet->RemoveAllByteTags ();
  packet->RemoveAtStart (packet->GetSize ());

  packet->AddHeader (msg);

  if (m_piggybackInfo)
   {
    HelloBeacon beacon (m_currCell, m_moving);
    packet->AddHeader (beacon);
    ResetHelloTimer ();
   }

  int sent = m_dataSocket->SendTo (packet, 0, InetSocketAddress (addr, m_dataPort));
  if (sent == -1)
   {
    NS_LOG_WARN ("Error " << m_dataSocket->GetErrno () << " while send "
      << Utils::GetMsgTypeString (msg.GetMsgType ()) << " "
      << packet->GetUid () << " " << packet->GetSize () << " to " << addr);
    return;
   }
  NS_LOG_DEBUG ("Unicast pckt " << Utils::GetMsgTypeString (msg.GetMsgType ())
    << " " << packet->GetUid () << " " << packet->GetSize () << " to " << addr);
  //m_profiler->OnTx (packet->GetUid (), msg.GetMsgType (), packet->GetSize ());
}

void
RoutingProtocol::CellBroadcast (DataMessage msg, Ptr<Packet> packet)
{
  NS_LOG_FUNCTION (this << &msg << packet);
  packet->RemoveAllPacketTags (); // to prevent errors
  packet->RemoveAllByteTags ();
  packet->RemoveAtStart (packet->GetSize ());

  msg.SetMsgTarget (CELL_BCAST); // change target to cell_bcast
  packet->AddHeader (msg);

  if (m_piggybackInfo)
   {
    HelloBeacon beacon (m_currCell, m_moving);
    packet->AddHeader (beacon);
    ResetHelloTimer ();
   }

  int sent = m_dataSocket->SendTo (packet, 0, m_bcastDataAddr);
  if (sent == -1)
   {
    NS_LOG_WARN ("Error " << m_dataSocket->GetErrno () << " while cbcast "
      << Utils::GetMsgTypeString (msg.GetMsgType ())
      << " " << packet->GetUid () << " " << packet->GetSize ());
    return;
   }

  if (msg.GetHomeCell () == (uint16_t) - 1)
   {
    NS_LOG_DEBUG ("CellBcast pckt " << Utils::GetMsgTypeString (msg.GetMsgType ())
      << " " << packet->GetUid ()
      << " to cell " << msg.GetDestinations ().begin ()->second);
   }
  else
   { // home cell defined
    NS_LOG_DEBUG ("CellBcast pckt " << Utils::GetMsgTypeString (msg.GetMsgType ())
      << " " << packet->GetUid ()
      << " to cell " << msg.GetHomeCell () << " (home cell)");
   }
}

bool
RoutingProtocol::IsRepeatingPath (uint16_t myCell, uint16_t lastGreedy,
  uint16_t nextHopCell, uint16_t firstPerHop) const
{
  NS_LOG_FUNCTION (this << myCell << lastGreedy << nextHopCell << firstPerHop);
  return myCell == lastGreedy && nextHopCell == firstPerHop;
}

void
RoutingProtocol::NodeMoved (Vector pos, Vector vel, CardinalPos head, Time update)
{
  NS_LOG_FUNCTION (this << pos << vel << head << update);
  uint16_t newCell = m_gridMan->GetCell (pos);

  if (newCell != m_currCell)
   { // node moved to a new cell
    m_prevCell = m_currCell;
    m_currCell = newCell;
    m_nodeAddr->SetCell (m_currCell);

    if (!m_moving)
     { // node started moving now
      m_moving = true;
      m_prevStableCell = m_prevCell;
      m_stabilizedWhileDown = false;
     }

    // force routing table to recalculate adjacent neighbor cells
    // and remove old neighbors from tables accordingly
    m_rTable->RecalculateNeighborCells (m_currCell);

    // force hello beacon every time a node changes cell
    bool isIfUp = IsIfaceUp ();
    if (isIfUp) HelloTimerExpired ();

    NS_LOG_WARN ("Node " << m_nodeId << " moved from " << m_prevCell
      << " to " << m_currCell);
    if (isIfUp) m_nodeMovedFn (m_currCell, m_prevCell);
   }
}

void
RoutingProtocol::NodeStabilized (Vector pos, Vector vel, CardinalPos head, Time update)
{
  NS_LOG_FUNCTION (this << pos << vel << head << update);
  uint16_t newCell = m_gridMan->GetCell (pos);

  if (newCell != m_prevStableCell)
   { // only if I changed cell while moving
    if (!IsIfaceUp ()) m_stabilizedWhileDown = true;
    m_moving = false;
    m_currCell = newCell;
    m_nodeAddr->SetCell (m_currCell);
    m_prevCell = -1;

    // force hello beacon before timer timeout
    bool isIfUp = IsIfaceUp ();
    if (isIfUp) HelloTimerExpired ();

    if (isIfUp) m_nodeStabilizedFn (m_currCell, m_prevStableCell);
    m_prevStableCell = m_currCell;

    m_rTable->RecalculateNeighborCells (m_currCell);

    NS_LOG_WARN ("Node " << m_nodeId << " stabilized in cell " << m_currCell);
   }
}

void
RoutingProtocol::ProcessTxErrHeader (WifiMacHeader const &h, uint64_t puid)
{
  Mac48Address to = h.GetAddr1 (); // packet was sent to this addr
  Mac48Address from = h.GetAddr2 (); // packet was sent from this addr
  NS_LOG_FUNCTION (this << h << from << to << puid);
  std::list<ArpCache::Entry*> list = m_arp->LookupInverse (to);
  if (list.size () == 0) return;

  NS_LOG_INFO ("MAC addr " << to << " has " << list.size () << " IP addrs");
  Ipv4Address addr = (*list.begin ())->GetIpv4Address (); // only for the first addr
  m_rTable->ProcessTxError (addr); // L2 feedback

  //m_profiler->OnTxError (puid);
}

void
RoutingProtocol::ProcessTxOkHeader (WifiMacHeader const &h, uint64_t puid)
{
  // @TODO !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  //m_profiler->OnTxOk (puid);
}

void
RoutingProtocol::ProcessArpCacheDrop (Ptr<const Packet> p)
{ // reason: wait reply timeout max retries exceeded (msg packet)
  NS_LOG_FUNCTION (this << p->GetUid ());
  Ptr<Packet> pckt = p->Copy ();
  Ipv4Header ip;
  pckt->RemoveHeader (ip); // remove ip header

  Ipv4Address dst = ip.GetDestination ();
  NS_LOG_DEBUG (dst);
  // @TODO !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
}

void
RoutingProtocol::ProcessArpProtocolDrop (Ptr<const Packet> p)
{
  NS_LOG_FUNCTION (this << p->GetUid ());
  if (p->GetSize () > 0)
   { // is not an ARP msg
    Ptr<Packet> pckt = p->Copy ();
    Ipv4Header ip;
    uint32_t size = pckt->RemoveHeader (ip);

    // reason 1: received arp reply for non-waiting entry (arp packet)
    // reason 2: received arp reply for unknown entry (arp packet)

    if (size > 0) // ip header successfully removed from packet
     {
      // reason 3: sending msg packet for (not expired) dead address
      // reason 4: wait reply pending queue overflow (msg packet)

      Ipv4Address dst = ip.GetDestination ();
      NS_LOG_DEBUG (dst);
      // @TODO !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     }
   }
}

}
}

