/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2015 NOVA LINCS DI-FCT-UNL <http://nova-lincs.di.fct.unl.pt/>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Joao A. Silva <jaa.silva@campus.fct.unl.pt>
 */
#ifndef THYME_ROUTING_TABLE_H
#define THYME_ROUTING_TABLE_H

#include "thyme-grid-manager.h"
#include <map>
#include <ns3/random-variable-stream.h>

namespace ns3 {
namespace hyrax {

// min number of nodes for a cell to be considered populated
#define POP_THRESHOLD 1 
#define MOVING_ENTRY_TTL 1.8 // seconds
#define STATIONARY_ENTRY_TTL 3.6 // seconds

class RoutingTable : public Object
{
private:
  Ptr<GridManager> m_gridMan;
  Ptr<LocationSensor> m_loc;
  Ptr<UniformRandomVariable> m_rand;

  struct Hello
  {
    uint16_t cell;
    Time expiration;

    Hello (uint16_t c, Time exp) : cell (c), expiration (exp)
    {
    }
  };

  Time m_movingEntryTtl;
  Time m_stationaryEntryTtl;

  std::map<Ipv4Address, Hello> m_oneHopNeighbors;
  std::map<Ipv4Address, Hello> m_twoHopNeighbors;
  std::map<Ipv4Address, Hello> m_movingNeighbors;

  uint32_t m_popThreshold;
  std::vector<uint16_t> m_neighborCells;

public:
  static TypeId GetTypeId (void);
  RoutingTable ();
  virtual ~RoutingTable ();
  void Start (Ptr<GridManager> gridMan, Ptr<LocationSensor> loc);

  bool IsCellEmpty (uint16_t cell) const; // only works for my surrounding cells
  bool IsMyNeighbor (uint16_t cell) const;

  void UpdateNeighbor (Ipv4Address addr, uint16_t cell, bool moving);
  Locations NeighborsExist (Locations dsts);

  void UpdateOneHopNeighbor (Ipv4Address addr, uint16_t cell);
  bool OneHopNeighborExists (Ipv4Address addr);
  Locations OneHopNeighborsExist (Locations dsts);
  uint32_t GetTotalNumOneHopNeighbors () const;
  void DeleteOneHopNeighbor (Ipv4Address addr);

  void UpdateTwoHopNeighbor (Ipv4Address addr, uint16_t cell);
  bool TwoHopNeighborExists (Ipv4Address addr);
  Locations TwoHopNeighborsExist (Locations dsts);
  uint32_t GetTotalNumTwoHopNeighbors () const;
  void DeleteTwoHopNeighbor (Ipv4Address addr);

  void UpdateMovingNeighbor (Ipv4Address addr, uint16_t cell);
  bool MovingNeighborExists (Ipv4Address addr);
  Locations MovingNeighborsExist (Locations dsts);
  uint32_t GetTotalNumMovingNeighbors () const;
  void DeleteMovingNeighbor (Ipv4Address addr);

  void RecalculateNeighborCells (uint16_t cell);
  std::vector<std::pair<uint16_t, Vector> > GetPopulatedOneHopNeighborCellCenters () const;
  uint32_t GetNumNodesInOneHopNeighborCell (uint16_t cell) const;
  std::vector<Ipv4Address> GetNodesInOneHopNeighborCell (uint16_t cell) const;
  Ipv4Address GetRandNodeFromOneHopNeighborCell (uint16_t cell) const;
  bool IsAdjacentHomeCell (uint16_t dstCell);

  std::vector<std::pair<uint16_t, Vector> > GetPopulatedTwoHopNeighborCellCenters ();
  std::vector<Ipv4Address> GetNodesInTwoHopNeighborCell (uint16_t cell) const;
  Ipv4Address GetRandNodeFromTwoHopNeighborCell (uint16_t cell) const;

  int GetBestOneHopNeighbor (uint16_t dstCell);
  int GetBestTwoHopNeighbor (uint16_t dstCell);
  int GetBestNeighborThan (uint16_t cell, uint16_t dstCell);

  /**
   * Right hand forward according to GPSR protocol
   * RIGHT-HAND-FORWARD(p, n_in), Brad Karp's PhD thesis, p. 26, Harvard U.
   * PERI-INIT-FORWARD(p), Brad Karp's PhD thesis, p. 49, Harvard U.
   *
   * Calls Purge () in the beginning of the function
   */
  std::pair<int, int> GetBestAngle (uint16_t prev, bool firstTime,
                                    uint16_t dstCell);

  void ProcessTxError (Ipv4Address addr);

private:
  Time CalculateEntryTtl (bool moving) const;

  bool IsMyCellEmpty () const;

  /**
   * Greedy forward according to GPSR protocol
   * GREEDY-FORWARD(p), Brad Karp's PhD thesis, p. 15, Harvard U.
   *
   * Calls Purge () in the beginning of the function
   */
  int GetBestCell (uint16_t myCell, uint16_t dstCell,
                   std::vector<std::pair<uint16_t, Vector> > ncc);

  double Norm (double x) const;
  double RadToDeg (double rads);

  void PurgeOneHopNeighbors ();
  void PurgeTwoHopNeighbors ();
  void PurgeMovingNeighbors ();

  bool IsHelloExpired (Hello h) const;
};

}
}

#endif /* THYME_ROUTING_TABLE_H */

