/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2015 NOVA LINCS DI-FCT-UNL <http://nova-lincs.di.fct.unl.pt/>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Joao A. Silva <jaa.silva@campus.fct.unl.pt>
 */
#include "thyme-node-address.h"
#include <ns3/address-utils.h>
#include <ns3/log.h>

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("NodeAddr");

namespace hyrax {

NodeAddress::NodeAddress () : m_addr (), m_cell (-1)
{
  NS_LOG_FUNCTION (this);
}

NodeAddress::NodeAddress (Ipv4Address addr, uint16_t cell) : m_addr (addr),
  m_cell (cell)
{
  NS_LOG_FUNCTION (this << addr << cell);
}

Ipv4Address
NodeAddress::GetAddress () const
{
  NS_LOG_FUNCTION (this);
  return m_addr;
}

uint16_t
NodeAddress::GetCell () const
{
  NS_LOG_FUNCTION (this);
  return m_cell;
}

void
NodeAddress::SetCell (uint16_t cell)
{
  NS_LOG_FUNCTION (this << cell);
  m_cell = cell;
}

void
NodeAddress::Serialize (Buffer::Iterator &start) const
{
  NS_LOG_FUNCTION (this);
  const Buffer::Iterator i = start;
  WriteTo (start, m_addr);
  start.WriteHtonU16 (m_cell);

  uint32_t dist = 0;
  uint32_t serSize = 0;
  NS_ASSERT_MSG ((dist = start.GetDistanceFrom (i))
    == (serSize = GetSerializedSize ()), dist << " != " << serSize);
}

uint32_t
NodeAddress::Deserialize (Buffer::Iterator &start)
{
  NS_LOG_FUNCTION (this);
  const Buffer::Iterator i = start;
  ReadFrom (start, m_addr);
  m_cell = start.ReadNtohU16 ();

  const uint32_t dist = start.GetDistanceFrom (i);
  uint32_t serSize = 0;
  NS_ASSERT_MSG (dist == (serSize = GetSerializedSize ()),
    dist << " != " << serSize);
  return dist;
}

uint32_t
NodeAddress::GetSerializedSize () const
{
  NS_LOG_FUNCTION (this);
  return 6; // addr and cell
}

void
NodeAddress::Print (std::ostream &os) const
{
  os << "{" << m_addr << "-" << m_cell << "}";
}

std::ostream&
operator<< (std::ostream &os, const NodeAddress &node)
{
  node.Print (os);
  return os;
}

std::ostream&
operator<< (std::ostream &os, Ptr<const NodeAddress> &node)
{
  node->Print (os);
  return os;
}

bool
operator!= (const NodeAddress &nodeL, const NodeAddress &nodeR)
{
  Ptr<NodeAddress> nodeAddrL = const_cast<NodeAddress *> (&nodeL);
  Ptr<NodeAddress> nodeAddrR = const_cast<NodeAddress *> (&nodeR);
  return nodeAddrL->GetAddress () != nodeAddrR->GetAddress ();
}

bool
operator== (const NodeAddress &nodeL, const NodeAddress &nodeR)
{
  Ptr<NodeAddress> nodeAddrL = const_cast<NodeAddress *> (&nodeL);
  Ptr<NodeAddress> nodeAddrR = const_cast<NodeAddress *> (&nodeR);
  return nodeAddrL->GetAddress () == nodeAddrR->GetAddress ();
}

}
}

