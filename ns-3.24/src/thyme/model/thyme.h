/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2015 NOVA LINCS DI-FCT-UNL <http://nova-lincs.di.fct.unl.pt/>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Joao A. Silva <jaa.silva@campus.fct.unl.pt>
 */
#ifndef THYME_H
#define THYME_H

#include <ns3/thyme-api.h>
#include <ns3/thyme-common.h>
#include <ns3/thyme-grid-manager.h>
#include <ns3/thyme-hashing.h>
#include <ns3/thyme-routing.h>

namespace ns3 {
namespace hyrax {

#define HANDOFF_INT 2400 // milliseconds
#define MAX_NEIGH_TTL 3 // max TTL when using Ask Your Neighbor mechanism
#define DYN_TIMEOUT true // true - calc timeout using manhattan distance
#define ROUTE_LATENCY 30 // milliseconds
#define TIMEOUT_FACTOR 6.5
#define OP_TIMEOUT_FACTOR 1.5

class Thyme : public ThymeApi
{
private:
  Ptr<LocationSensor> m_loc;
  Ptr<GridManager> m_gridMan;
  Ptr<Hashing> m_hash;
  Ptr<RoutingProtocol> m_routing;

  // < (objId, node), < node, cell > >
  ObjectsLocations m_locsByObjKey; // indexed by objKey

  // < (objId, node), obj > active replicas
  Publications m_activeReps; // indexed by objKey
  // < objId, cell > cells where my objects have been published (active rep)
  std::map<ObjectIdentifier, uint32_t> m_pubCells; // indexed by objId

  // < (objId, node), objMeta > 
  ObjectsMetadata m_metasByObjKey; // indexed by objKey
  // < tag, < (objId, node), objMeta > >
  TagIndex m_metasByTag; // indexed by tag
  // < (objId, node), uint >
  std::map<ObjectKey, uint8_t> m_objKeyCount; // indexed by objKey

  bool m_joinSent = false;
  EventId m_handoff;
  Time m_handoffInt;

  uint32_t m_maxNeighborTtl;

  bool m_dynTimeout;
  uint16_t m_routeLatency;
  double m_timeoutFactor;
  double m_opTimeoutFactor;

  bool m_nackOpt;

  struct Msg
  {
    DataMessage msg;
    Locations dsts;
  };

  std::map<uint64_t, Msg> m_msgs;

  struct Info
  {
    DataMessage msg;
    EventId timeout;
  };

  std::map<Ipv4Address, Info> m_waitingNacks;

  struct Down
  {
    Ptr<ObjectMetadata> objMeta;
    Locations locs;
  };

  std::vector<Down> m_waitingDowns;

public:
  static TypeId GetTypeId (void);
  Thyme ();
  virtual ~Thyme ();

  Ptr<const RoutingProtocol> GetRoutingProtocol () const;
  Ptr<const GridManager> GetGridManager () const;

protected:
  /**
   * 1. Create request
   * 2. Save publication locally
   * 3. Send (for each tag) PUBLISH msg to hash(tag)
   * 4. Recv RSPs (PUBLISH_RSP)
   * 5. Set active replicas
   * 6. Notify application
   * 
   * Failures: TIMEOUT
   */
  void DoPublish (Ptr<ThymeObject> obj);

  /**
   * 1. Check if publication is ongoing
   * 2. Create request
   * 3. Send (for each tag) UNPUBLISH msg to hash(tag)
   * 4. Recv RSPs (UNPUBLISH_RSP)
   * 5. Remove active replicas
   * 6. Delete publication locally
   * 7. Notify application
   * 
   * Failures: TIMEOUT, INVALID_ARG
   */
  void DoUnpublish (Ptr<ThymeObject> obj);

  /**
   * 1. Check active replicas
   * 2. Create request
   * 3. Send DOWNLOAD msg to closest node/cell
   * 4. Recv RSP (DOWNLOAD_RSP)
   * 5. Save passive replica
   * 6. Notify application
   * 
   * Failures: OBJECT_NOT_FOUND, TIMEOUT
   */
  void DoDownload (Ptr<ObjectMetadata> objMeta, Locations locs);

  /**
   * 1. Create subscription object
   * 2. Create request
   * 3. Save subscription locally
   * 4. Send (for each conjunction) SUBSCRIBE msg to hash(tag)
   * 5. Recv RSPs (SUBSCRIBE_RSP)
   * 6. Notify application
   * 
   * Failures: TIMEOUT
   */
  void DoSubscribe (std::string filter, Time startTs, Time endTs);

  /**
   * 1. Create request
   * 3. Send (for each conjunction) UNSUBSCRIBE msg to hash(tag)
   * 4. Recv RSPs (UNSUBSCRIBE_RSP)
   * 5. Delete subscription locally
   * 6. Notify application
   * 
   * Failures: TIMEOUT
   */
  void DoUnsubscribe (Ptr<Subscription> subObj);

  void DoDispose ();
  void DoInitialize ();

private:
  void StartApplication ();
  void StopApplication ();

  void ProcessMsg (DataMessage msg, bool isCoord, uint64_t pcktId);

  void ProcessPublish (DataMessage::Publish pub, Ptr<NodeAddress> sender,
                       bool isCoord);
  bool InsertMetaByTag (std::string tag, Ptr<ObjectMetadata> objMeta);
  bool InsertMetaByObjKey (Ptr<ObjectMetadata> objMeta);
  void InsertLocsByObjKey (Ptr<ObjectMetadata> objMeta, Locations locs);
  void ProcessPublishRsp (DataMessage::PublishRsp pubRsp);
  void HandleFinishedPublishRequest (Ptr<PublishRequest> req);
  void MakeActiveReplicas (Ptr<ThymeObject> obj, uint16_t cell);
  void ProcessActiveRep (DataMessage::ActiveRep actRep);

  bool IsPublicationOngoing (Ptr<ObjectIdentifier> objId) const;
  void ProcessUnpublish (DataMessage::Unpublish unpub, Ptr<NodeAddress> sender,
                         bool isCoord);
  bool RemoveMetaByTag (std::string tag, Ptr<ObjectIdentifier> objId,
                        Ipv4Address owner);
  bool RemoveMetaByObjKey (Ptr<ObjectIdentifier> objId, Ipv4Address owner);
  void RemoveLocsByObjKey (Ptr<ObjectIdentifier> objId, Ipv4Address owner);
  void ProcessUnpublishRsp (DataMessage::UnpublishRsp unpubRsp);
  void HandleFinishedUnpublishRequest (Ptr<UnpublishRequest> req);
  void RemoveActiveReplicas (Ptr<ObjectIdentifier> objId);
  void ProcessUnpublishRep (DataMessage::UnpublishRep unpubRep,
                            Ptr<NodeAddress> sender);

  void TryDownload (Locations objLocs, Ptr<DownloadRequest> req);
  void ProcessDownload (DataMessage msg, Ptr<NodeAddress> sender);
  void ProcessDownloadRsp (DataMessage::DownloadRsp downRsp);
  void MakePassiveReplica (Ptr<ThymeObject> obj);

  void ProcessUpdateLoc (Ptr<NodeAddress> sender);
  void ProcessRegisterRep (DataMessage::RegisterRep regRep,
                           Ptr<NodeAddress> sender);

  void ProcessSubscribe (DataMessage::Subscribe sub, Ptr<NodeAddress> sender,
                         bool isCoord);
  void ProcessSubscribeRsp (DataMessage::SubscribeRsp subRsp);
  void ProcessSubscribeNot (DataMessage::SubscribeNot subNot,
                            Ptr<NodeAddress> sender);

  void ProcessUnsubscribe (DataMessage::Unsubscribe unsub,
                           Ptr<NodeAddress> sender, bool isCoord);
  void ProcessUnsubscribeRsp (DataMessage::UnsubscribeRsp unsubRsp);

  void ProcessJoin (Ptr<NodeAddress> dst);
  void ProcessJoinRsp (DataMessage::JoinRsp joinRsp);

  void ProcessNackMsg (DataMessage::NackMsg nackMsg);
  void TimeoutNack (Ipv4Address addr);

  void HandlePublishRequestTimeout (Ptr<PublishRequest> req);
  void HandleUnpublishRequestTimeout (Ptr<UnpublishRequest> req);
  void HandleDownloadRequestTimeout (Ptr<DownloadRequest> req);
  void HandleSubscribeRequestTimeout (Ptr<SubscribeRequest> req);
  void HandleUnsubscribeRequestTimeout (Ptr<UnsubscribeRequest> req);

  void HandleSubscriptions (Ptr<ObjectMetadata> objMeta);
  std::vector<Ptr<SubscriptionObject> > CheckAllSubscriptions (Ptr<ObjectMetadata> objMeta);
  void SendSubscriptionNotifications (std::vector<Ptr<SubscriptionObject> > subs,
                                      Ptr<ObjectMetadata> objMeta);

  void HandleSubscriptions (Ptr<SubscriptionObject> subObj);
  std::vector<Ptr<ObjectMetadata> > CheckAllPublications (Ptr<SubscriptionObject> subObj);
  void SendSubscriptionNotifications (Ptr<SubscriptionObject> sub,
                                      std::vector<Ptr<ObjectMetadata> > objsMeta);

  void RemoveMsg (uint64_t puid);

  void NodeMoved (uint16_t newCell, uint16_t oldCell);
  void NodeStabilized (uint16_t newCell, uint16_t oldCell);
  void UpdateLocation ();

  std::set<std::string> GetPublicationsTags ();
  std::set<std::string> GetPassiveReplicasTags ();
  std::set<std::string> GetSubscriptionsTags ();

  void Join (Ipv4Address dst);
  void JoinExpired ();
  void HandoffExpired ();

  virtual void IfDown ();
  virtual void IfUp ();
  virtual bool IsIfUp ();

  Time CalculateRequestTimeout (uint16_t src,
                                std::vector<uint16_t> dsts) const;
};

}
}

#endif /* THYME_H */

