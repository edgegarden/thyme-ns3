/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2015 NOVA LINCS DI-FCT-UNL <http://nova-lincs.di.fct.unl.pt/>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Joao A. Silva <jaa.silva@campus.fct.unl.pt>
 */
#ifndef THYME_COMMON_H
#define THYME_COMMON_H

#include "thyme-object.h"
#include <map>

namespace ns3 {
namespace hyrax {

////////// Default values
#define NO_REP_LOOP_OPT true // true - cache loops in routing (perimeter mode)
#define CHOOSE_HAND_OPT true // true - choose closest hand to dst in routing

////////// Macros
#define PAIR(x,y) (std::make_pair (x,y))
#define PEEK(x) (*(PeekPointer (x)))
#define OBJ_KEY(x,y) (PAIR(PEEK(x),y))

////////// Types definition
typedef std::pair<ObjectIdentifier, Ipv4Address> ObjectKey;
typedef std::pair<Ipv4Address, uint16_t> Location;

typedef std::map<ObjectIdentifier, Ptr<ThymeObject> > MyPublications;
typedef std::map<ObjectKey, Ptr<ThymeObject> > Publications;

typedef std::map<ObjectKey, Ptr<ObjectMetadata> > ObjectsMetadata;
typedef std::map<std::string, ObjectsMetadata> TagIndex;

typedef std::map<Ipv4Address, uint16_t> Locations;
typedef std::map<ObjectKey, Locations> ObjectsLocations;

typedef std::pair<std::string, bool> Tag; // (tag, isNegated)
typedef std::vector<Tag> Conjunction; // list of ANDs
typedef std::vector<Conjunction> Filter; // list of ORs

class Subscription;

typedef std::map<uint32_t, Ptr<Subscription> > MySubscriptions;
typedef std::map<Ipv4Address, MySubscriptions> Subscriptions;

////////// Enums

enum CardinalPos
{ // counterclockwise
  WRONG = -1,

  EAST,
  NORTHEAST,
  NORTH,
  NORTHWEST,
  WEST,
  SOUTHWEST,
  SOUTH,
  SOUTHEAST,
};

enum MessageStatus
{
  OBJECT_FOUND,
  OBJECT_NOT_FOUND,
  USED_KEY,
  MOVING,
  TIMEOUT,
  NOT_OWNER,
  INVALID_ARG,
};

enum MessageType
{
  // 0 - NOT_DEFINED

  PUBLISH = 1,
  PUBLISH_RSP,
  ACTIVE_REP,

  DOWNLOAD,
  DOWNLOAD_RSP,

  UNPUBLISH,
  UNPUBLISH_RSP,
  UNPUBLISH_REP,

  UPDATE_LOC,
  REGISTER_REP,

  SUBSCRIBE,
  SUBSCRIBE_RSP,
  SUBSCRIBE_NOT,

  UNSUBSCRIBE,
  UNSUBSCRIBE_RSP,

  JOIN,
  JOIN_RSP,

  NACK_MSG,
};

enum MessageTarget
{
  // 0 - NOT_DEFINED

  CELL = 1,
  NODE,
  CELL_BCAST,
};

enum FwdMode
{
  GREEDY,
  PERIMETER,
  HOME_CELL,
  DROP,
};

}
}

#endif /* THYME_COMMON_H */

