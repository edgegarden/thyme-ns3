/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2015 NOVA LINCS DI-FCT-UNL <http://nova-lincs.di.fct.unl.pt/>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Joao A. Silva <jaa.silva@campus.fct.unl.pt>
 */
#include "thyme-subscription.h"
#include <ns3/address-utils.h>
#include <ns3/log.h>
#include <ns3/simulator.h>

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("Sub");

namespace hyrax {

Subscription::Subscription () : m_subId (-1), m_subscriber (), m_start (0),
  m_end (0), m_filter ()
{
  NS_LOG_FUNCTION (this);
}

Subscription::Subscription (uint32_t subId, Ipv4Address subscriber, Time start,
  Time end, std::string filter) : m_subId (subId), m_subscriber (subscriber),
  m_start (start), m_end (end), m_filter (ParseFilter (filter))
{
  NS_LOG_FUNCTION (this << subId << subscriber << start << end << filter);
}

uint32_t
Subscription::GetSubscriptionId () const
{
  NS_LOG_FUNCTION (this);
  return m_subId;
}

Ipv4Address
Subscription::GetSubscriber () const
{
  NS_LOG_FUNCTION (this);
  return m_subscriber;
}

Time
Subscription::GetStartTime () const
{
  NS_LOG_FUNCTION (this);
  return m_start;
}

Time
Subscription::GetEndTime () const
{
  NS_LOG_FUNCTION (this);
  return m_end;
}

const Filter
Subscription::GetFilter () const
{
  NS_LOG_FUNCTION (this);
  return m_filter;
}

bool
Subscription::IsMatch (Ptr<ObjectMetadata> meta) const
{
  NS_LOG_FUNCTION (this << meta);
  return IsValidTime (meta->GetPublicationTimestamp ())
    && IsValidFilter (meta->GetTags ());
}

bool
Subscription::IsExpired () const
{
  NS_LOG_FUNCTION (this);
  return m_end != 0 && Simulator::Now () > m_end;
}

bool
Subscription::IsStarted () const
{
  NS_LOG_FUNCTION (this);
  return m_start == 0 || Simulator::Now () >= m_start;
}

bool
Subscription::IsActive () const
{ // has started but not yet expired
  NS_LOG_FUNCTION (this);
  return IsStarted () && !IsExpired ();
}

void
Subscription::Serialize (Buffer::Iterator &start) const
{
  NS_LOG_FUNCTION (this);
  start.WriteHtonU32 (m_subId);

  WriteTo (start, m_subscriber);

  start.WriteHtonU64 (m_start.GetNanoSeconds ());
  start.WriteHtonU64 (m_end.GetNanoSeconds ());
}

uint32_t
Subscription::Deserialize (Buffer::Iterator &start)
{
  NS_LOG_FUNCTION (this);
  m_subId = start.ReadNtohU32 ();

  ReadFrom (start, m_subscriber);

  m_start = NanoSeconds (start.ReadNtohU64 ());
  m_end = NanoSeconds (start.ReadNtohU64 ());

  return GetSerializedSize ();
}

uint32_t
Subscription::GetSerializedSize () const
{
  NS_LOG_FUNCTION (this);
  return 24; // subId, subscriber, start and end
}

void
Subscription::Print (std::ostream &os) const
{
  os << "Sub(" << m_subId << "): Owner: " << m_subscriber
    << " Int: [" << m_start.GetSeconds () << "s,"
    << m_end.GetSeconds () << "s]";
}

std::vector<std::string>
Subscription::Split (std::string str, char delimiter)
{
  NS_LOG_FUNCTION (str << delimiter);
  std::stringstream ss (str);

  std::vector<std::string> internal;
  std::string tok;
  while (getline (ss, tok, delimiter))
   internal.push_back (tok);

  return internal;
}

std::string
Subscription::Trim (std::string str)
{
  NS_LOG_FUNCTION (str);
  const size_t first = str.find_first_not_of (' ');
  const size_t last = str.find_last_not_of (' ');

  return str.substr (first, (last - first + 1));
}

std::string
Subscription::RemoveParenthesis (std::string str)
{
  NS_LOG_FUNCTION (str);
  const size_t first = str.find_first_of ('(');
  const size_t last = str.find_last_of (')');

  return str.substr (first + 1, (last - first - 1));
}

Filter // list of ORs (list of ANDs (pair< string tag, bool isNeg >))
Subscription::ParseFilter (std::string filter)
{ // filter of the form "(A & B) | (C & ~D) | (E)" (DNF)
  NS_LOG_FUNCTION (filter);
  // divide each conjunction
  const std::vector<std::string> conjs = Split (filter, '|');

  Filter res;
  for (std::string conj : conjs) // "(A & B)", "(C & ~D)", "(E)"
   {
    std::string tr = RemoveParenthesis (conj);
    const std::vector<std::string> ands = Split (tr, '&');

    Conjunction sepAND;
    uint32_t negs = 0;
    for (std::string b : ands)
     {
      tr = Trim (b);
      NS_ABORT_MSG_IF (Split (tr, ' ').size () != 1,
        "Spaces not allowed: '" << tr << "'");

      const bool isNeg = tr[0] == '~';
      sepAND.push_back ({isNeg ? tr.substr (1) : tr, isNeg});

      negs += isNeg ? 1 : 0;
     }

    NS_ABORT_MSG_IF (negs == sepAND.size (),
      "One or more positive literals required.");
    res.push_back (sepAND);
   }

  return res;
}

bool
Subscription::IsValidTime (Time pubTs) const
{
  NS_LOG_FUNCTION (this << pubTs);
  return (m_start == 0 || pubTs >= m_start) && (m_end == 0 || pubTs <= m_end);
}

std::ostream&
operator<< (std::ostream &os, Ptr<const Subscription> &sub)
{
  sub->Print (os);
  return os;
}

}
}

