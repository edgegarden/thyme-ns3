/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2015 NOVA LINCS DI-FCT-UNL <http://nova-lincs.di.fct.unl.pt/>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Joao A. Silva <jaa.silva@campus.fct.unl.pt>
 */
#include "thyme-object-metadata.h"
#include "thyme-utils.h"
#include <ns3/address-utils.h>
#include <ns3/log.h>

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("Meta");

namespace hyrax {

ObjectMetadata::ObjectMetadata () : m_objId (0), m_tags (), m_summarySize (0),
  m_summary (0), m_pubTs (0), m_owner ()
{
  NS_LOG_FUNCTION (this);
}

ObjectMetadata::ObjectMetadata (Ptr<ObjectIdentifier> id, Time pubTs) :
  m_objId (id), m_tags (), m_summarySize (0), m_summary (0), m_pubTs (pubTs),
  m_owner ()
{
  NS_LOG_FUNCTION (this << id << pubTs);
}

ObjectMetadata::~ObjectMetadata ()
{
  NS_LOG_FUNCTION (this);
  m_objId = 0;
  m_tags.clear ();
  FreeSummary ();
}

Ptr<ObjectIdentifier>
ObjectMetadata::GetObjectIdentifier () const
{
  NS_LOG_FUNCTION (this);
  return m_objId;
}

const std::set<std::string>
ObjectMetadata::GetTags () const
{
  NS_LOG_FUNCTION (this);
  return m_tags;
}

void
ObjectMetadata::SetTags (std::set<std::string> tags)
{
  NS_ABORT_MSG_IF (tags.empty (), "One or more tags required.");
  NS_LOG_FUNCTION (this << tags.size ());
  m_tags = std::set<std::string> (tags);
}

void
ObjectMetadata::AddTag (std::string tag)
{
  NS_LOG_FUNCTION (this << tag);
  m_tags.insert (tag);
}

uint8_t
ObjectMetadata::GetTagIndex (std::string tag) const
{
  NS_LOG_FUNCTION (this << tag);
  uint8_t idx = 0;
  for (std::string t : m_tags)
   {
    if (t == tag) return idx;
    ++idx;
   }

  NS_ABORT_MSG ("Invalid tag " << tag);
  return -1;
}

std::string
ObjectMetadata::GetTag (uint8_t index) const
{
  NS_LOG_FUNCTION (this << index);
  uint8_t idx = 0;
  for (std::string tag : m_tags)
   {
    if (idx == index) return tag;
    ++idx;
   }

  NS_ABORT_MSG ("Invalid tag index " << (uint16_t) index
    << " (size: " << m_tags.size () << ")");
  return "";
}

const uint8_t*
ObjectMetadata::GetSummary () const
{
  NS_LOG_FUNCTION (this);
  return m_summary;
}

void
ObjectMetadata::SetSummary (const uint8_t* summary, uint16_t summarySize)
{
  NS_LOG_FUNCTION (this << summarySize);
  FreeSummary ();

  m_summarySize = summarySize;
  m_summary = m_summarySize > 0 ?
    ObjectIdentifier::AllocateBuffer (m_summarySize) : 0;
  if (m_summarySize > 0) memcpy (m_summary, summary, m_summarySize);
}

uint16_t
ObjectMetadata::GetSummarySize () const
{
  NS_LOG_FUNCTION (this);
  return m_summarySize;
}

Time
ObjectMetadata::GetPublicationTimestamp () const
{
  NS_LOG_FUNCTION (this);
  return m_pubTs;
}

void
ObjectMetadata::SetPublicationTimestamp (Time pubTs)
{
  NS_LOG_FUNCTION (this << pubTs);
  m_pubTs = pubTs;
}

Ipv4Address
ObjectMetadata::GetOwner () const
{
  NS_LOG_FUNCTION (this);
  return m_owner;
}

void
ObjectMetadata::SetOwner (Ipv4Address owner)
{
  NS_LOG_FUNCTION (this << owner);
  m_owner = owner;
}

bool
ObjectMetadata::IsEqual (Ptr<const ObjectMetadata> meta) const
{
  NS_LOG_FUNCTION (this << meta);
  return m_objId->IsEqual (meta->GetObjectIdentifier ())
    && m_owner == meta->GetOwner ()
    && m_pubTs == meta->GetPublicationTimestamp ();
}

bool
ObjectMetadata::IsLess (Ptr<const ObjectMetadata> meta) const
{ // compares objId, followed by owner and then by pubTs
  NS_LOG_FUNCTION (this << meta);
  if (m_objId->IsEqual (meta->GetObjectIdentifier ())) // ids are equal
   {
    if (m_owner == meta->GetOwner ()) // owners are equal
     return m_pubTs < meta->GetPublicationTimestamp (); // break tie with pubTs

    return m_owner < meta->GetOwner (); // break tie with owner
   }

  return m_objId->IsLess (meta->GetObjectIdentifier ());
}

void
ObjectMetadata::Serialize (Buffer::Iterator &start) const
{
  NS_LOG_FUNCTION (this);
  const Buffer::Iterator i = start;
  m_objId->Serialize (start);

  start.WriteU8 (m_tags.size ());
  for (std::string tag : m_tags)
   Utils::SerializeString (tag, start);

  start.WriteHtonU16 (m_summarySize);
  if (m_summarySize > 0) start.Write (m_summary, m_summarySize);

  start.WriteHtonU64 (m_pubTs.GetNanoSeconds ());
  WriteTo (start, m_owner);

  uint32_t dist = 0;
  uint32_t serSize = 0;
  NS_ASSERT_MSG ((dist = start.GetDistanceFrom (i))
    == (serSize = GetSerializedSize ()), dist << " != " << serSize);
}

uint32_t
ObjectMetadata::Deserialize (Buffer::Iterator &start)
{
  NS_LOG_FUNCTION (this);
  const Buffer::Iterator i = start;
  m_objId = Create<ObjectIdentifier> ();
  m_objId->Deserialize (start);

  const int numTags = start.ReadU8 ();
  for (int i = 0; i < numTags; ++i)
   m_tags.insert (Utils::DeserializeString (start));

  m_summarySize = start.ReadNtohU16 ();
  m_summary = m_summarySize > 0 ?
    ObjectIdentifier::AllocateBuffer (m_summarySize) : 0;
  if (m_summarySize > 0) start.Read (m_summary, m_summarySize);

  m_pubTs = NanoSeconds (start.ReadNtohU64 ());
  ReadFrom (start, m_owner);

  const uint32_t dist = start.GetDistanceFrom (i);
  uint32_t serSize = 0;
  NS_ASSERT_MSG (dist == (serSize = GetSerializedSize ()),
    dist << " != " << serSize);
  return dist;
}

uint32_t
ObjectMetadata::GetSerializedSize () const
{
  NS_LOG_FUNCTION (this);
  uint32_t size = m_objId->GetSerializedSize (); // objId

  size += 1; // tags set size
  for (std::string tag : m_tags)
   size += Utils::GetSerializedStringSize (tag); // tag

  size += 2 + m_summarySize; // summarySize and summary

  size += 12; // pubTs and owner

  return size;
}

void
ObjectMetadata::Print (std::ostream &os) const
{
  os << "Meta: ";
  m_objId->Print (os);
  os << " Tags: [" << TagsToString () << "] SummSize: " << m_summarySize
    << " PubTs: " << m_pubTs.GetSeconds () << "s Owner: " << m_owner;
}

std::string
ObjectMetadata::TagsToString () const
{
  NS_LOG_FUNCTION (this);
  std::string res = "";
  bool first = true;
  for (std::string tag : m_tags)
   {
    if (!first) res += " "; // every time but the first
    else first = false;
    res += tag;
   }

  return res;
}

void
ObjectMetadata::FreeSummary ()
{
  NS_LOG_FUNCTION (this);
  if (m_summary != 0) // if there is previously allocated memory...
   {
    // this requires m_summary to be initialized (to zero) in every c-tor
    // otherwise, this call might use a random address and trigger a SIGSEGV
    free (m_summary);
    m_summary = 0;
    m_summarySize = 0;
   }
}

std::ostream&
operator<< (std::ostream &os, const ObjectMetadata &meta)
{
  meta.Print (os);
  return os;
}

std::ostream&
operator<< (std::ostream &os, Ptr<const ObjectMetadata> &meta)
{
  meta->Print (os);
  return os;
}

bool
operator== (const ObjectMetadata &metaL, const ObjectMetadata &metaR)
{
  Ptr<ObjectMetadata> objMetaL = const_cast<ObjectMetadata *> (&metaL);
  Ptr<ObjectMetadata> objMetaR = const_cast<ObjectMetadata *> (&metaR);
  return objMetaL->IsEqual (objMetaR);
}

bool
operator< (const ObjectMetadata &metaL, const ObjectMetadata &metaR)
{
  Ptr<ObjectMetadata> objMetaL = const_cast<ObjectMetadata *> (&metaL);
  Ptr<ObjectMetadata> objMetaR = const_cast<ObjectMetadata *> (&metaR);
  return objMetaL->IsLess (objMetaR);
}

}
}

