/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2015 NOVA LINCS DI-FCT-UNL <http://nova-lincs.di.fct.unl.pt/>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Joao A. Silva <jaa.silva@campus.fct.unl.pt>
 */
#include "thyme-object.h"
#include <ns3/log.h>
#include <ns3/simulator.h>

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("Obj");

namespace hyrax {

ThymeObject::ThymeObject () : m_objMeta (0), m_objSize (0), m_obj (0)
{
  NS_LOG_FUNCTION (this);
}

ThymeObject::ThymeObject (const uint8_t* key, uint8_t keySize,
  const uint8_t* obj, uint32_t objSize) : m_objMeta (0), m_objSize (0),
  m_obj (0)
{
  NS_LOG_FUNCTION (this << std::string ((char*) key, keySize)
    << (uint16_t) keySize << objSize);
  ConstructObject (Create<ObjectIdentifier> (key, keySize), obj, objSize);
}

ThymeObject::ThymeObject (Ptr<ObjectIdentifier> id, const uint8_t* obj,
  uint32_t objSize) : m_objMeta (0), m_objSize (0), m_obj (0)
{
  NS_LOG_FUNCTION (this << id << objSize);
  ConstructObject (id, obj, objSize);
}

ThymeObject::~ThymeObject ()
{
  NS_LOG_FUNCTION (this);
  m_objMeta = 0;
  FreeObject ();
}

Ptr<ObjectMetadata>
ThymeObject::GetObjectMetadata () const
{
  NS_LOG_FUNCTION (this);
  return m_objMeta;
}

void
ThymeObject::SetObjectMetadata (Ptr<ObjectMetadata> objMeta)
{
  NS_LOG_FUNCTION (this << objMeta);
  m_objMeta = objMeta;
}

const uint8_t*
ThymeObject::GetObject () const
{
  NS_LOG_FUNCTION (this);
  return m_obj;
}

uint32_t
ThymeObject::GetObjectSize () const
{
  NS_LOG_FUNCTION (this);
  return m_objSize;
}

Ptr<ObjectIdentifier>
ThymeObject::GetObjectIdentifier () const
{
  NS_LOG_FUNCTION (this);
  return m_objMeta->GetObjectIdentifier ();
}

const std::set<std::string>
ThymeObject::GetTags () const
{
  NS_LOG_FUNCTION (this);
  return m_objMeta->GetTags ();
}

uint8_t
ThymeObject::GetTagIndex (std::string tag) const
{
  NS_LOG_FUNCTION (this << tag);
  return m_objMeta->GetTagIndex (tag);
}

std::string
ThymeObject::GetTag (uint8_t index) const
{
  NS_LOG_FUNCTION (this << index);
  return m_objMeta->GetTag (index);
}

const uint8_t*
ThymeObject::GetSummary () const
{
  NS_LOG_FUNCTION (this);
  return m_objMeta->GetSummary ();
}

uint16_t
ThymeObject::GetSummarySize () const
{
  NS_LOG_FUNCTION (this);
  return m_objMeta->GetSummarySize ();
}

Time
ThymeObject::GetPublicationTimestamp () const
{
  NS_LOG_FUNCTION (this);
  return m_objMeta->GetPublicationTimestamp ();
}

Ipv4Address
ThymeObject::GetOwner () const
{
  NS_LOG_FUNCTION (this);
  return m_objMeta->GetOwner ();
}

void
ThymeObject::Serialize (Buffer::Iterator &start) const
{
  NS_LOG_FUNCTION (this);
  const Buffer::Iterator i = start;
  m_objMeta->Serialize (start);

  start.WriteHtonU32 (m_objSize);
  start.Write (m_obj, m_objSize);

  uint32_t dist = 0;
  uint32_t serSize = 0;
  NS_ASSERT_MSG ((dist = start.GetDistanceFrom (i))
    == (serSize = GetSerializedSize ()), dist << " != " << serSize);
}

uint32_t
ThymeObject::Deserialize (Buffer::Iterator &start)
{
  NS_LOG_FUNCTION (this);
  const Buffer::Iterator i = start;
  m_objMeta = Create<ObjectMetadata> ();
  m_objMeta->Deserialize (start);

  m_objSize = start.ReadNtohU32 ();
  m_obj = ObjectIdentifier::AllocateBuffer (m_objSize);
  start.Read (m_obj, m_objSize);

  const uint32_t dist = start.GetDistanceFrom (i);
  uint32_t serSize = 0;
  NS_ASSERT_MSG (dist == (serSize = GetSerializedSize ()),
    dist << " != " << serSize);
  return dist;
}

uint32_t
ThymeObject::GetSerializedSize () const
{
  NS_LOG_FUNCTION (this);
  uint32_t size = m_objMeta->GetSerializedSize (); // objMeta

  size += 4 + m_objSize; // objSize and obj

  return size;
}

void
ThymeObject::Print (std::ostream &os) const
{
  os << "Obj(" << m_objSize << ")";
  if (m_objMeta != 0)
   {
    os << ": [";
    m_objMeta->Print (os);
    os << "]";
   }
}

void
ThymeObject::Serialize2 (Buffer::Iterator &start) const
{
  NS_LOG_FUNCTION (this);
  const Buffer::Iterator i = start;
  start.WriteHtonU32 (m_objSize);
  start.Write (m_obj, m_objSize);

  uint32_t dist = 0;
  uint32_t serSize = 0;
  NS_ASSERT_MSG ((dist = start.GetDistanceFrom (i))
    == (serSize = GetSerializedSize2 ()), dist << " != " << serSize);
}

uint32_t
ThymeObject::Deserialize2 (Buffer::Iterator &start)
{
  NS_LOG_FUNCTION (this);
  const Buffer::Iterator i = start;
  m_objSize = start.ReadNtohU32 ();
  m_obj = ObjectIdentifier::AllocateBuffer (m_objSize);
  start.Read (m_obj, m_objSize);

  const uint32_t dist = start.GetDistanceFrom (i);
  uint32_t serSize = 0;
  NS_ASSERT_MSG (dist == (serSize = GetSerializedSize2 ()),
    dist << " != " << serSize);
  return dist;
}

uint32_t
ThymeObject::GetSerializedSize2 () const
{
  NS_LOG_FUNCTION (this);
  return 4 + m_objSize; // objSize and obj
}

void
ThymeObject::ConstructObject (Ptr<ObjectIdentifier> id, const uint8_t* obj,
  uint32_t objSize)
{
  NS_ABORT_MSG_IF (obj == 0 || objSize == 0, "Object cannot be null.");
  NS_LOG_FUNCTION (this << id << objSize);
  m_objMeta = Create<ObjectMetadata> (id, Simulator::Now ());
  m_objSize = objSize;
  m_obj = ObjectIdentifier::AllocateBuffer (m_objSize);
  memcpy (m_obj, obj, m_objSize);
}

void
ThymeObject::FreeObject ()
{
  NS_LOG_FUNCTION (this);
  if (m_obj != 0) // if there is previously allocated memory...
   {
    // this requires m_obj to be initialized (to zero) in every c-tor
    // otherwise, this call might use a random address and trigger a SIGSEGV
    free (m_obj);
    m_obj = 0;
    m_objSize = 0;
   }
}

std::ostream&
operator<< (std::ostream &os, Ptr<const ThymeObject> &obj)
{
  obj->Print (os);
  return os;
}

}
}

