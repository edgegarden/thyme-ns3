/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2015 NOVA LINCS DI-FCT-UNL <http://nova-lincs.di.fct.unl.pt/>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Joao A. Silva <jaa.silva@campus.fct.unl.pt>
 */
#ifndef THYME_OBJECT_METADATA_H
#define THYME_OBJECT_METADATA_H

#include "thyme-identifier.h"
#include <ns3/ipv4-address.h>
#include <ns3/nstime.h>

namespace ns3 {
namespace hyrax {

/**
 * \ingroup hyrax
 * 
 * \brief The metadata of an object contains the following information:
 * i) the object's identifier;
 * ii) a set of tags (i.e., a set of keywords related with the object);
 * iii) a summary of the object's content (e.g., a thumbnail);
 * iv) the object's publication timestamp; and
 * v) the object's owner.
 * The summary has a maximum limit of 64 KB (because we serialize its size in
 * an unsigned short - uint16_t) and it **can** be null.
 * We also define the maximum number of tags to be 255 (because we serialize
 * the set's size in an unsigned byte - uint8_t). Each tag has a maximum length
 * of 255 chars (because we serialize its size in an unsigned byte - uint8_t).
 */
class ObjectMetadata : public SimpleRefCount<ObjectMetadata>
{
private:
  Ptr<ObjectIdentifier> m_objId;
  std::set<std::string> m_tags; // max 255 tags (each with max 255 chars)
  uint16_t m_summarySize;
  uint8_t* m_summary; // max size 2^16 - 1 = 65535 bytes, 64 KB
  Time m_pubTs;
  Ipv4Address m_owner;

public:
  ObjectMetadata (); // for deserialization **only**
  ObjectMetadata (Ptr<ObjectIdentifier> id, Time pubTs);
  virtual ~ObjectMetadata ();

  Ptr<ObjectIdentifier> GetObjectIdentifier () const;

  const std::set<std::string> GetTags () const;
  void SetTags (std::set<std::string> tags);
  void AddTag (std::string tag);
  uint8_t GetTagIndex (std::string tag) const;
  std::string GetTag (uint8_t index) const;

  const uint8_t* GetSummary () const;
  void SetSummary (const uint8_t* summary, uint16_t summarySize);
  uint16_t GetSummarySize () const;

  Time GetPublicationTimestamp () const;
  void SetPublicationTimestamp (Time pubTs);

  Ipv4Address GetOwner () const;
  void SetOwner (Ipv4Address owner);

  bool IsEqual (Ptr<const ObjectMetadata> meta) const;
  bool IsLess (Ptr<const ObjectMetadata> meta) const;

  void Serialize (Buffer::Iterator &start) const;
  uint32_t Deserialize (Buffer::Iterator &start);
  uint32_t GetSerializedSize () const;
  void Print (std::ostream &os) const;

private:
  std::string TagsToString () const;
  void FreeSummary ();
};

std::ostream& operator<< (std::ostream &os, const ObjectMetadata &meta);
std::ostream& operator<< (std::ostream &os, Ptr<const ObjectMetadata> &meta);
bool operator== (const ObjectMetadata &metaL, const ObjectMetadata &metaR);
bool operator< (const ObjectMetadata &metaL, const ObjectMetadata &metaR);

}
}

#endif /* THYME_OBJECT_METADATA_H */

