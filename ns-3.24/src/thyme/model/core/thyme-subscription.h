/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2015 NOVA LINCS DI-FCT-UNL <http://nova-lincs.di.fct.unl.pt/>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Joao A. Silva <jaa.silva@campus.fct.unl.pt>
 */
#ifndef THYME_SUBSCRIPTION_H
#define THYME_SUBSCRIPTION_H

#include "thyme-common.h"

namespace ns3 {
namespace hyrax {

/**
 * \ingroup hyrax
 * 
 * \brief Abstract subscription object. It implements some of the most 
 * general functions concerning the subscription feature.
 */
class Subscription : public SimpleRefCount<Subscription>
{
protected:
  uint32_t m_subId;
  Ipv4Address m_subscriber;
  Time m_start;
  Time m_end;

  Filter m_filter; // **not** (de)serialized by this class

public:
  Subscription (); // for deserialization **only**
  Subscription (uint32_t subId, Ipv4Address subscriber, Time start, Time end,
                std::string filter);

  uint32_t GetSubscriptionId () const;
  Ipv4Address GetSubscriber () const;
  Time GetStartTime () const;
  Time GetEndTime () const;
  const Filter GetFilter () const;

  bool IsMatch (Ptr<ObjectMetadata> meta) const;
  bool IsExpired () const;
  bool IsStarted () const;
  bool IsActive () const;

  virtual void Serialize (Buffer::Iterator &start) const;
  virtual uint32_t Deserialize (Buffer::Iterator &start);
  virtual uint32_t GetSerializedSize () const;
  virtual void Print (std::ostream &os) const;

  static std::vector<std::string> Split (std::string str, char delimiter);
  static std::string Trim (std::string str);
  static std::string RemoveParenthesis (std::string str);
  static Filter ParseFilter (std::string filter);

protected:
  virtual bool IsValidFilter (std::set<std::string> tags) const = 0;
  bool IsValidTime (Time pubTs) const;
};

std::ostream& operator<< (std::ostream &os, Ptr<const Subscription> &sub);

}
}

#endif /* THYME_SUBSCRIPTION_H */

