/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2015 NOVA LINCS DI-FCT-UNL <http://nova-lincs.di.fct.unl.pt/>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Joao A. Silva <jaa.silva@campus.fct.unl.pt>
 */
#include "thyme-api.h"
#include <ns3/boolean.h>
#include <ns3/log.h>
#include <ns3/pointer.h>
#include <ns3/simulator.h>
#include <ns3/string.h>
#include <ns3/uinteger.h>

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("API");

namespace hyrax {

NS_OBJECT_ENSURE_REGISTERED (ThymeApi);

const Time ThymeApi::m_zero = Time (0);

TypeId
ThymeApi::GetTypeId (void)
{
  NS_LOG_FUNCTION_NOARGS ();
  static TypeId tid = TypeId ("ns3::hyrax::ThymeApi")
    .SetParent<Application> ()

    .AddAttribute ("Profiler",
    "Pointer for the system-wide profiler.",
    StringValue ("ns3::hyrax::Profiler"),
    MakePointerAccessor (&ThymeApi::m_profiler),
    MakePointerChecker<Profiler> ())

    .AddAttribute ("ReqMaxRetries",
    "Max number of request retries.",
    UintegerValue (REQ_MAX_RETRIES),
    MakeUintegerAccessor (&ThymeApi::m_reqMaxRetries),
    MakeUintegerChecker<uint32_t> ())

    .AddAttribute ("PubReqTimeout",
    "Publish operation request timeout interval (ms).",
    TimeValue (MilliSeconds (PUB_REQ_TIMEOUT)),
    MakeTimeAccessor (&ThymeApi::m_pubReqTimeout),
    MakeTimeChecker ())

    .AddAttribute ("UnpubReqTimeout",
    "Unpublish operation request timeout interval (ms).",
    TimeValue (MilliSeconds (UNPUB_REQ_TIMEOUT)),
    MakeTimeAccessor (&ThymeApi::m_unpubReqTimeout),
    MakeTimeChecker ())

    .AddAttribute ("DownReqTimeout",
    "Download operation request timeout interval (ms).",
    TimeValue (MilliSeconds (DOWN_REQ_TIMEOUT)),
    MakeTimeAccessor (&ThymeApi::m_downReqTimeout),
    MakeTimeChecker ())

    .AddAttribute ("SubReqTimeout",
    "Subscribe operation request timeout interval (ms).",
    TimeValue (MilliSeconds (SUB_REQ_TIMEOUT)),
    MakeTimeAccessor (&ThymeApi::m_subReqTimeout),
    MakeTimeChecker ())

    .AddAttribute ("UnsubReqTimeout",
    "Unsubscribe operation request timeout interval (ms).",
    TimeValue (MilliSeconds (UNSUB_REQ_TIMEOUT)),
    MakeTimeAccessor (&ThymeApi::m_unsubReqTimeout),
    MakeTimeChecker ())

    .AddAttribute ("PassiveRep",
    "Passive replication flag.",
    BooleanValue (PASSIVE_REP),
    MakeBooleanAccessor (&ThymeApi::m_passiveRep),
    MakeBooleanChecker ())

    .AddAttribute ("JoinMaxRetries",
    "Max number of join retries.",
    UintegerValue (JOIN_MAX_RETRIES),
    MakeUintegerAccessor (&ThymeApi::m_joinMaxRetries),
    MakeUintegerChecker<uint32_t> ())

    .AddAttribute ("JoinTimeout",
    "Join timeout interval (ms).",
    TimeValue (MilliSeconds (JOIN_TIMEOUT)),
    MakeTimeAccessor (&ThymeApi::m_joinTimeout),
    MakeTimeChecker ());

  return tid;
}

void
ThymeApi::Publish (const uint8_t *key, uint8_t keySize, const uint8_t *value,
  uint32_t valueSize, std::set<std::string> tags, const uint8_t* summary,
  uint16_t summarySize)
{
  NS_LOG_FUNCTION (this << (uint16_t) keySize << valueSize << tags.size ()
    << summarySize);
  Ptr<ObjectIdentifier> objId = Create<ObjectIdentifier> (key, keySize);

  auto found = m_myPubs.find (PEEK (objId)); // check for used key
  if (found != m_myPubs.end ()) // key already in use
   {
    m_profiler->OnPublishStart (m_addr, -1, 0);
    objId = 0; // return already published object
    NotifyPublishFailure (found->second, USED_KEY);
    m_profiler->OnPublishFailure (m_addr, -1, USED_KEY);

    return;
   } // else key not in use

  Ptr<ThymeObject> obj = CreateThymeObject (objId, value, valueSize, tags,
    summary, summarySize, m_addr); // create object for tentative publication

  DoPublish (obj); // try to publish
}

void
ThymeApi::Unpublish (Ptr<ObjectIdentifier> objId)
{
  NS_LOG_FUNCTION (this << objId);
  const auto found = m_myPubs.find (PEEK (objId)); // check for existing key
  if (found == m_myPubs.end ()) // object not published
   {
    m_profiler->OnUnpublishStart (m_addr, -1, 0);
    NotifyUnpublishFailure (objId, NOT_OWNER);
    m_profiler->OnUnpublishFailure (m_addr, -1, NOT_OWNER);

    return;
   } // else object published

  DoUnpublish (found->second); // try to unpublish
}

void
ThymeApi::Unpublish (const uint8_t *key, uint8_t keySize)
{
  NS_LOG_FUNCTION (this << (uint16_t) keySize);
  Unpublish (Create<ObjectIdentifier> (key, keySize));
}

void
ThymeApi::Unpublish (Ptr<ObjectMetadata> objMeta)
{
  NS_LOG_FUNCTION (this << objMeta);
  Unpublish (objMeta->GetObjectIdentifier ());
}

void
ThymeApi::Unpublish (Ptr<ThymeObject> obj)
{
  NS_LOG_FUNCTION (this << obj);
  Unpublish (obj->GetObjectIdentifier ());
}

void
ThymeApi::Download (Ptr<ObjectMetadata> objMeta, Locations locs)
{
  NS_LOG_FUNCTION (this << objMeta << locs.size ());
  Ipv4Address owner = objMeta->GetOwner ();
  Ptr<ObjectIdentifier> objId = objMeta->GetObjectIdentifier ();

  if (owner == m_addr) // check my publications
   { // it's one of my publications
    const auto found = m_myPubs.find (PEEK (objId));
    m_profiler->OnDownloadStart (m_addr, -1);

    if (found != m_myPubs.end ())
     {
      NotifyDownloadSuccess (found->second);
      m_profiler->OnDownloadSuccess (m_addr, -1, found->second, 1);
     }
    else
     {
      NotifyDownloadFailure (objId, owner, NOT_OWNER);
      m_profiler->OnDownloadFailure (m_addr, -1, NOT_OWNER);
     }

    return;
   }

  if (m_passiveRep) // if passive replication is on...
   { // check passive replicas (previously downloaded objects)
    const auto found = m_passReps.find (OBJ_KEY (objId, owner));
    if (found != m_passReps.end ()) // objKey exists (but can be stale!)
     {
      m_profiler->OnDownloadStart (m_addr, -1);
      NotifyDownloadSuccess (found->second);
      m_profiler->OnDownloadSuccess (m_addr, -1, found->second, 1);

      return;
     }
   }

  DoDownload (objMeta, locs); // try to download
}

void
ThymeApi::Subscribe (std::string filter, Time startTs, Time endTs)
{
  NS_LOG_FUNCTION (this << filter << startTs << endTs);
  if (startTs != 0 && endTs != 0 && startTs > endTs) // check args
   { // startTs is in front of endTs
    m_profiler->OnSubscribeStart (m_addr, -1, 0);
    NotifySubscribeFailure (0, INVALID_ARG);
    m_profiler->OnSubscribeFailure (m_addr, -1, INVALID_ARG);

    return;
   }

  DoSubscribe (filter, startTs, endTs); // try to subscribe
}

void
ThymeApi::SubscribeFromNowUntil (std::string filter, Time endTs)
{
  NS_LOG_FUNCTION (this << filter << endTs);
  Subscribe (filter, Simulator::Now (), endTs);
}

void
ThymeApi::SubscribeFromNowOn (std::string filter)
{
  NS_LOG_FUNCTION (this << filter);
  Subscribe (filter, Simulator::Now (), m_zero);
}

void
ThymeApi::SubscribeForEternity (std::string filter)
{
  NS_LOG_FUNCTION (this << filter);
  Subscribe (filter, m_zero, m_zero);
}

void
ThymeApi::Unsubscribe (Ptr<Subscription> subObj)
{
  NS_LOG_FUNCTION (this << subObj);
  if (subObj->GetSubscriber () != m_addr) // check subscription ownership
   { // I'm not the owner of this subscription?!?!?
    m_profiler->OnUnsubscribeStart (m_addr, -1, 0);
    NotifyUnsubscribeFailure (subObj, NOT_OWNER);
    m_profiler->OnUnsubscribeFailure (m_addr, -1, NOT_OWNER);

    return;
   }

  const auto found = m_mySubs.find (subObj->GetSubscriptionId ());
  if (found == m_mySubs.end ()) // subObj is not in mySubs ("invalid" object)
   {
    m_profiler->OnUnsubscribeStart (m_addr, -1, 0);
    NotifyUnsubscribeFailure (subObj, OBJECT_NOT_FOUND);
    m_profiler->OnUnsubscribeFailure (m_addr, -1, OBJECT_NOT_FOUND);

    return;
   }

  subObj = found->second; // work with own obj

  if (subObj->IsExpired ()) // check subscription validity
   { // endTs is in the past (it will be lazily purged, eventually...)
    m_profiler->OnUnsubscribeStart (m_addr, -1, subObj);
    m_mySubs.erase (subObj->GetSubscriptionId ());
    NotifyUnsubscribeSuccess (subObj);
    m_profiler->OnUnsubscribeSuccess (m_addr, -1, 1);

    return;
   }

  DoUnsubscribe (subObj); // try to unsubscribe
}

void
ThymeApi::Unsubscribe (uint32_t subId)
{
  NS_LOG_FUNCTION (this << subId);
  const auto found = m_mySubs.find (subId);
  if (found == m_mySubs.end ()) // subId is not in mySubs ("invalid" identifier)
   {
    m_profiler->OnUnsubscribeStart (m_addr, -1, 0);
    NotifyUnsubscribeFailure (0, INVALID_ARG);
    m_profiler->OnUnsubscribeFailure (m_addr, -1, INVALID_ARG);

    return;
   }

  Unsubscribe (found->second);
}

bool
ThymeApi::IsOperationOngoing () const
{
  NS_LOG_FUNCTION (this);
  return !m_ongoingReqs.empty ();
}

void
ThymeApi::SetPublishSuccessCallback (Callback <void, Ptr<ThymeObject> > pubSuccFn)
{
  NS_LOG_FUNCTION (this);
  m_pubSuccFn = pubSuccFn;
}

void
ThymeApi::SetPublishFailureCallback (Callback <void, Ptr<ThymeObject>, MessageStatus> pubFailFn)
{
  NS_LOG_FUNCTION (this);
  m_pubFailFn = pubFailFn;
}

void
ThymeApi::SetUnpublishSuccessCallback (Callback <void, Ptr<ThymeObject> > unpubSuccFn)
{
  NS_LOG_FUNCTION (this);
  m_unpubSuccFn = unpubSuccFn;
}

void
ThymeApi::SetUnpublishFailureCallback (Callback <void, Ptr<ObjectIdentifier>, MessageStatus> unpubFailFn)
{
  NS_LOG_FUNCTION (this);
  m_unpubFailFn = unpubFailFn;
}

void
ThymeApi::SetDownloadSuccessCallback (Callback <void, Ptr<ThymeObject> > downSuccFn)
{
  NS_LOG_FUNCTION (this);
  m_downSuccFn = downSuccFn;
}

void
ThymeApi::SetDownloadFailureCallback (Callback <void, Ptr<ObjectIdentifier>, Ipv4Address, MessageStatus> downFailFn)
{
  NS_LOG_FUNCTION (this);
  m_downFailFn = downFailFn;
}

void
ThymeApi::SetSubscribeSuccessCallback (Callback <void, Ptr<Subscription> > subSuccFn)
{
  NS_LOG_FUNCTION (this);
  m_subSuccFn = subSuccFn;
}

void
ThymeApi::SetSubscribeFailureCallback (Callback <void, Ptr<Subscription>, MessageStatus> subFailFn)
{
  NS_LOG_FUNCTION (this);
  m_subFailFn = subFailFn;
}

void
ThymeApi::SetUnsubscribeSuccessCallback (Callback <void, Ptr<Subscription> > unsubSuccFn)
{
  NS_LOG_FUNCTION (this);
  m_unsubSuccFn = unsubSuccFn;
}

void
ThymeApi::SetUnsubscribeFailureCallback (Callback <void, Ptr<Subscription>, MessageStatus> unsubFailFn)
{
  NS_LOG_FUNCTION (this);
  m_unsubFailFn = unsubFailFn;
}

void
ThymeApi::SetSubscriptionNotificationCallback (Callback <void, Ptr<Subscription>, Ptr<ObjectMetadata>, Locations> subNotFn)
{
  NS_LOG_FUNCTION (this);
  m_subNotFn = subNotFn;
}

void
ThymeApi::BenchmarkPublish (std::string key, std::string value,
  std::string summary, std::set<std::string> tags)
{
  if (!IsRunning () || !IsIfUp ()) return;
  NS_LOG_FUNCTION (this << key << value << summary << tags.size ());
  NS_LOG_DEBUG (m_nodeId << " PUBLISH " << key << " " << tags.size ());
  Publish ((uint8_t*) key.c_str (), key.size (), (uint8_t*) value.c_str (),
    value.size (), tags, (uint8_t*) summary.c_str (), summary.size ());
}

void
ThymeApi::BenchmarkUnpublishRandom ()
{ // unpublish one of my publications, randomly
  if (!IsRunning () || !IsIfUp ()) return;
  NS_LOG_FUNCTION (this << m_myPubs.size ());
  if (m_myPubs.empty ())
   {
    NS_LOG_WARN ("No publications. Unpublish bypassed by node " << m_nodeId);
    return;
   }

  uint32_t rand = m_rand->GetInteger (0, m_myPubs.size () - 1);
  Ptr<ObjectIdentifier> objId = 0;
  for (auto it = m_myPubs.begin (); it != m_myPubs.end (); ++it)
   {
    if (rand == 0)
     {
      objId = it->second->GetObjectIdentifier ();
      break;
     }
    --rand;
   }

  if (objId != 0)
   {
    NS_LOG_DEBUG (m_nodeId << " UNPUBLISH " << objId->ToString ());
    Unpublish (objId);
   }
}

void
ThymeApi::BenchmarkUnpublish (std::string key)
{
  if (!IsRunning () || !IsIfUp ()) return;
  NS_LOG_FUNCTION (this << key);
  NS_LOG_DEBUG (m_nodeId << " UNPUBLISH " << key);
  Unpublish ((uint8_t*) key.c_str (), key.size ());
}

void
ThymeApi::BenchmarkDownload (Ptr<Subscription> sub, Ptr<ObjectMetadata> meta,
  Locations locs)
{
  if (!IsRunning () || !IsIfUp ()) return;
  NS_LOG_FUNCTION (this << sub << meta << locs.size ());
  NS_LOG_DEBUG (m_nodeId << " DOWNLOAD "
    << meta->GetObjectIdentifier ()->ToString () << " " << meta->GetOwner ());
  Download (meta, locs);
}

void
ThymeApi::BenchmarkSubscribeFuture (std::string filter)
{
  if (!IsRunning () || !IsIfUp ()) return;
  NS_LOG_FUNCTION (this << filter);
  NS_LOG_DEBUG (m_nodeId << " SUBSCRIBE_F " << m_subIds << " " << filter);
  SubscribeFromNowOn (filter);
}

void
ThymeApi::BenchmarkSubscribePast (std::string filter)
{
  if (!IsRunning () || !IsIfUp ()) return;
  NS_LOG_FUNCTION (this << filter);
  NS_LOG_DEBUG (m_nodeId << " SUBSCRIBE_P " << m_subIds << " " << filter);
  SubscribeForEternity (filter);
}

void
ThymeApi::BenchmarkUnsubscribeRandom ()
{ // unsubscribe one of my subscriptions, randomly
  if (!IsRunning () || !IsIfUp ()) return;
  NS_LOG_FUNCTION (this << m_mySubs.size ());
  if (m_mySubs.empty ())
   {
    NS_LOG_WARN ("No subscriptions. Unsubscribe bypassed by node " << m_nodeId);
    return;
   }

  uint32_t rand = m_rand->GetInteger (0, m_mySubs.size () - 1);
  Ptr<Subscription> sub = 0;
  for (auto it = m_mySubs.begin (); it != m_mySubs.end (); ++it)
   {
    if (rand == 0)
     {
      sub = it->second;
      break;
     }
    --rand;
   }

  if (sub != 0)
   {
    NS_LOG_DEBUG (m_nodeId << " UNSUBSCRIBE " << sub->GetSubscriptionId ());
    Unsubscribe (sub);
   }
}

void
ThymeApi::BenchmarkUnsubscribe (uint32_t subId)
{
  if (!IsRunning () || !IsIfUp ()) return;
  NS_LOG_FUNCTION (this << subId);
  NS_LOG_DEBUG (m_nodeId << " UNSUBSCRIBE " << subId);
  Unsubscribe (subId);
}

void
ThymeApi::BenchmarkPause ()
{
  if (!IsRunning () || !IsIfUp ()) return;
  NS_LOG_FUNCTION (this);
  NS_LOG_DEBUG (m_nodeId << " PAUSE");
  IfDown ();
}

void
ThymeApi::BenchmarkResume ()
{
  if (!IsRunning () || IsIfUp ()) return;
  NS_LOG_FUNCTION (this);
  NS_LOG_DEBUG (m_nodeId << " RESUME");
  IfUp ();
}

void
ThymeApi::BenchmarkExit ()
{
  if (!IsRunning ()) return;
  NS_LOG_FUNCTION (this);
  NS_LOG_DEBUG (m_nodeId << " EXIT");
  StopApplicationNow ();
}

void
ThymeApi::DoDispose ()
{
  NS_LOG_FUNCTION (this);
  Application::DoDispose ();
}

void
ThymeApi::DoInitialize ()
{
  NS_LOG_FUNCTION (this);
  m_node = GetNode ();
  m_nodeId = m_node->GetId ();
  m_rand = CreateObject<UniformRandomVariable> ();
  Application::DoInitialize ();
}

void
ThymeApi::StopApplication ()
{
  NS_LOG_FUNCTION (this);
  m_node = 0;
  m_rand = 0;
  m_join.Cancel ();
  RemoveAllRequests ();
}

Ptr<ThymeObject>
ThymeApi::CreateThymeObject (Ptr<ObjectIdentifier> id, const uint8_t *value,
  uint32_t valueSize, std::set<std::string> tags, const uint8_t* summary,
  uint16_t summarySize, Ipv4Address owner)
{
  NS_LOG_FUNCTION (this << id << valueSize << tags.size () << summarySize
    << owner);
  Ptr<ThymeObject> obj = Create<ThymeObject> (id, value, valueSize);

  // finish setting up metadata attributes (objId and pubTs set in obj c-tor)
  Ptr<ObjectMetadata> objMeta = obj->GetObjectMetadata ();
  objMeta->SetTags (tags);
  objMeta->SetOwner (owner);
  objMeta->SetSummary (summary, summarySize);

  return obj;
}

uint32_t
ThymeApi::GetNextSubscriptionId ()
{
  NS_LOG_FUNCTION (this);
  return m_subIds++;
}

uint32_t
ThymeApi::GetNextRequestId ()
{
  NS_LOG_FUNCTION (this);
  return m_reqIds++;
}

void
ThymeApi::AddRequest (Ptr<Request> req)
{
  NS_LOG_FUNCTION (this << req);
  m_ongoingReqs.insert (PAIR (req->GetRequestId (), req));
}

Ptr<Request>
ThymeApi::GetRequest (uint32_t reqId)
{
  NS_LOG_FUNCTION (this << reqId);
  const auto found = m_ongoingReqs.find (reqId);
  return found != m_ongoingReqs.end () ? found->second : 0;
}

Ptr<PublishRequest>
ThymeApi::GetPublishRequest (uint32_t reqId)
{
  NS_LOG_FUNCTION (this << reqId);
  Ptr<Request> req = GetRequest (reqId);
  return req == 0 ? 0 : dynamic_cast<PublishRequest*> (PeekPointer (req));
}

Ptr<UnpublishRequest>
ThymeApi::GetUnpublishRequest (uint32_t reqId)
{
  NS_LOG_FUNCTION (this << reqId);
  Ptr<Request> req = GetRequest (reqId);
  return req == 0 ? 0 : dynamic_cast<UnpublishRequest*> (PeekPointer (req));
}

Ptr<DownloadRequest>
ThymeApi::GetDownloadRequest (uint32_t reqId)
{
  NS_LOG_FUNCTION (this << reqId);
  Ptr<Request> req = GetRequest (reqId);
  return req == 0 ? 0 : dynamic_cast<DownloadRequest*> (PeekPointer (req));
}

Ptr<SubscribeRequest>
ThymeApi::GetSubscribeRequest (uint32_t reqId)
{
  NS_LOG_FUNCTION (this << reqId);
  Ptr<Request> req = GetRequest (reqId);
  return req == 0 ? 0 : dynamic_cast<SubscribeRequest*> (PeekPointer (req));
}

Ptr<UnsubscribeRequest>
ThymeApi::GetUnsubscribeRequest (uint32_t reqId)
{
  NS_LOG_FUNCTION (this << reqId);
  Ptr<Request> req = GetRequest (reqId);
  return req == 0 ? 0 : dynamic_cast<UnsubscribeRequest*> (PeekPointer (req));
}

void
ThymeApi::RemoveRequest (uint32_t reqId)
{
  NS_LOG_FUNCTION (this << reqId);
  auto found = m_ongoingReqs.find (reqId);
  if (found != m_ongoingReqs.end ())
   {
    found->second->DoDispose ();
    m_ongoingReqs.erase (found);
   }
}

void
ThymeApi::RemoveAllRequests ()
{
  NS_LOG_FUNCTION (this);
  for (auto it = m_ongoingReqs.begin (); it != m_ongoingReqs.end (); ++it)
   it->second->DoDispose ();

  NS_LOG_INFO ("Removed " << m_ongoingReqs.size () << " ongoing request(s)");
  m_ongoingReqs.clear ();
}

Ptr<PublishRequest>
ThymeApi::CreatePublishRequest (Ptr<ThymeObject> obj, uint16_t cell)
{
  NS_LOG_FUNCTION (this << obj << cell);
  const uint32_t reqId = GetNextRequestId ();
  Ptr<PublishRequest> req =
    Create<PublishRequest> (reqId, m_reqMaxRetries, obj, cell);
  AddRequest (req);

  return req;
}

Ptr<UnpublishRequest>
ThymeApi::CreateUnpublishRequest (Ptr<ThymeObject> obj)
{
  NS_LOG_FUNCTION (this << obj);
  const uint32_t reqId = GetNextRequestId ();
  Ptr<UnpublishRequest> req =
    Create<UnpublishRequest> (reqId, m_reqMaxRetries, obj);
  AddRequest (req);

  return req;
}

Ptr<DownloadRequest>
ThymeApi::CreateDownloadRequest (Ptr<ObjectMetadata> objMeta, Locations locs)
{
  NS_LOG_FUNCTION (this << objMeta << locs.size ());
  const uint32_t reqId = GetNextRequestId ();
  Ptr<DownloadRequest> req =
    Create<DownloadRequest> (reqId, m_reqMaxRetries, objMeta, locs);
  AddRequest (req);

  return req;
}

Ptr<SubscribeRequest>
ThymeApi::CreateSubscribeRequest (Ptr<Subscription> subObj)
{
  NS_LOG_FUNCTION (this << subObj);
  const uint32_t reqId = GetNextRequestId ();
  Ptr<SubscribeRequest> req =
    Create<SubscribeRequest> (reqId, m_reqMaxRetries, subObj);
  AddRequest (req);

  return req;
}

Ptr<UnsubscribeRequest>
ThymeApi::CreateUnsubscribeRequest (Ptr<Subscription> subObj)
{
  NS_LOG_FUNCTION (this << subObj);
  const uint32_t reqId = GetNextRequestId ();
  Ptr<UnsubscribeRequest> req =
    Create<UnsubscribeRequest> (reqId, m_reqMaxRetries, subObj);
  AddRequest (req);

  return req;
}

void
ThymeApi::StartPublishRequestTimeout (Ptr<PublishRequest> req, Time timeout)
{
  NS_LOG_FUNCTION (this << req << timeout);
  const EventId eventId = Simulator::Schedule (timeout,
    &ThymeApi::HandlePublishRequestTimeout, this, req);
  req->SetTimeoutEventId (eventId);
}

void
ThymeApi::StartUnpublishRequestTimeout (Ptr<UnpublishRequest> req, Time timeout)
{
  NS_LOG_FUNCTION (this << req << timeout);
  const EventId eventId = Simulator::Schedule (timeout,
    &ThymeApi::HandleUnpublishRequestTimeout, this, req);
  req->SetTimeoutEventId (eventId);
}

void
ThymeApi::StartDownloadRequestTimeout (Ptr<DownloadRequest> req, Time timeout)
{
  NS_LOG_FUNCTION (this << req << timeout);
  const EventId eventId = Simulator::Schedule (timeout,
    &ThymeApi::HandleDownloadRequestTimeout, this, req);
  req->SetTimeoutEventId (eventId);
}

void
ThymeApi::StartSubscribeRequestTimeout (Ptr<SubscribeRequest> req, Time timeout)
{
  NS_LOG_FUNCTION (this << req << timeout);
  const EventId eventId = Simulator::Schedule (timeout,
    &ThymeApi::HandleSubscribeRequestTimeout, this, req);
  req->SetTimeoutEventId (eventId);
}

void
ThymeApi::StartUnsubscribeRequestTimeout (Ptr<UnsubscribeRequest> req,
  Time timeout)
{
  NS_LOG_FUNCTION (this << req << timeout);
  const EventId eventId = Simulator::Schedule (timeout,
    &ThymeApi::HandleUnsubscribeRequestTimeout, this, req);
  req->SetTimeoutEventId (eventId);
}

void
ThymeApi::NotifyPublishSuccess (Ptr<ThymeObject> obj)
{
  NS_LOG_FUNCTION (this << obj);
  NS_LOG_DEBUG (m_nodeId << " PUBLISH SUCCESS "
    << obj->GetObjectIdentifier ()->ToString ());
  if (!m_pubSuccFn.IsNull ()) m_pubSuccFn (obj);
}

void
ThymeApi::NotifyPublishFailure (Ptr<ThymeObject> obj, MessageStatus st)
{
  NS_LOG_FUNCTION (this << obj << st);
  NS_LOG_DEBUG (m_nodeId << " PUBLISH FAILURE "
    << obj->GetObjectIdentifier ()->ToString () << " " << st);
  if (!m_pubFailFn.IsNull ()) m_pubFailFn (obj, st);
}

void
ThymeApi::NotifyUnpublishSuccess (Ptr<ThymeObject> obj)
{
  NS_LOG_FUNCTION (this << obj);
  NS_LOG_DEBUG (m_nodeId << " UNPUBLISH SUCCESS "
    << obj->GetObjectIdentifier ()->ToString ());
  if (!m_unpubSuccFn.IsNull ()) m_unpubSuccFn (obj);
}

void
ThymeApi::NotifyUnpublishFailure (Ptr<ObjectIdentifier> id, MessageStatus st)
{
  NS_LOG_FUNCTION (this << id << st);
  NS_LOG_DEBUG (m_nodeId << " UNPUBLISH FAILURE " << id->ToString ()
    << " " << st);
  if (!m_unpubFailFn.IsNull ()) m_unpubFailFn (id, st);
}

void
ThymeApi::NotifyDownloadSuccess (Ptr<ThymeObject> obj)
{
  NS_LOG_FUNCTION (this << obj);
  NS_LOG_DEBUG (m_nodeId << " DOWNLOAD SUCCESS "
    << obj->GetObjectIdentifier ()->ToString () << " " << obj->GetOwner ());
  if (!m_downSuccFn.IsNull ()) m_downSuccFn (obj);
}

void
ThymeApi::NotifyDownloadFailure (Ptr<ObjectIdentifier> id, Ipv4Address owner,
  MessageStatus st)
{
  NS_LOG_FUNCTION (this << id << owner << st);
  NS_LOG_DEBUG (m_nodeId << " DOWNLOAD FAILURE " << id->ToString ()
    << " " << owner << " " << st);
  if (!m_downFailFn.IsNull ()) m_downFailFn (id, owner, st);
}

void
ThymeApi::NotifySubscribeSuccess (Ptr<Subscription> sub)
{
  NS_LOG_FUNCTION (this << sub);
  NS_LOG_DEBUG (m_nodeId << " SUBSCRIBE SUCCESS " << sub->GetSubscriptionId ());
  if (!m_subSuccFn.IsNull ()) m_subSuccFn (sub);
}

void
ThymeApi::NotifySubscribeFailure (Ptr<Subscription> sub, MessageStatus st)
{
  NS_LOG_FUNCTION (this << sub << st);
  NS_LOG_DEBUG (m_nodeId << " SUBSCRIBE FAILURE " << sub->GetSubscriptionId ()
    << " " << st);
  if (!m_subFailFn.IsNull ()) m_subFailFn (sub, st);
}

void
ThymeApi::NotifyUnsubscribeSuccess (Ptr<Subscription> sub)
{
  NS_LOG_FUNCTION (this << sub);
  NS_LOG_DEBUG (m_nodeId << " UNSUBSCRIBE SUCCESS "
    << sub->GetSubscriptionId ());
  if (!m_unsubSuccFn.IsNull ()) m_unsubSuccFn (sub);
}

void
ThymeApi::NotifyUnsubscribeFailure (Ptr<Subscription> sub, MessageStatus st)
{
  NS_LOG_FUNCTION (this << sub << st);
  NS_LOG_DEBUG (m_nodeId << " UNSUBSCRIBE FAILURE " << sub->GetSubscriptionId ()
    << " " << st);
  if (!m_unsubFailFn.IsNull ()) m_unsubFailFn (sub, st);
}

void
ThymeApi::NotifySubscriptionNotification (Ptr<Subscription> sub,
  Ptr<ObjectMetadata> meta, Locations locs)
{
  NS_LOG_FUNCTION (this << sub << meta << locs.size ());
  NS_LOG_DEBUG (m_nodeId << " NOTIFICATION " << sub->GetSubscriptionId () << " "
    << meta->GetObjectIdentifier ()->ToString () << " " << meta->GetOwner ());
  if (!m_subNotFn.IsNull ()) m_subNotFn (sub, meta, locs);
}

}
}

