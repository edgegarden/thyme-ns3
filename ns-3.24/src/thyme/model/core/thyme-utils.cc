/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2015 NOVA LINCS DI-FCT-UNL <http://nova-lincs.di.fct.unl.pt/>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Joao A. Silva <jaa.silva@campus.fct.unl.pt>
 */
#include "thyme-utils.h"
#include <ns3/abort.h>

namespace ns3 {
namespace hyrax {

void
Utils::SerializeString (std::string str, Buffer::Iterator &start)
{
  const uint8_t size = str.size ();
  start.WriteU8 (size); // max 255 chars

  start.Write ((uint8_t*) str.c_str (), size);
}

std::string
Utils::DeserializeString (Buffer::Iterator &start)
{
  const uint8_t size = start.ReadU8 (); // max 255 chars

  char str[size];
  start.Read ((uint8_t*) str, size);

  return std::string (str, size);
}

uint32_t
Utils::GetSerializedStringSize (std::string str)
{
  return 1 + str.size (); // string size and string
}

void
Utils::SerializeConjunction (Conjunction conj, Buffer::Iterator &start)
{
  start.WriteU8 (conj.size ()); // max 255 tags

  for (Tag t : conj)
   {
    SerializeString (t.first, start); // string
    start.WriteU8 ((uint8_t) t.second); // bool isNeg
   }
}

Conjunction
Utils::DeserializeConjunction (Buffer::Iterator &start)
{
  Conjunction res;
  const int size = start.ReadU8 (); // max 255 tags
  for (int i = 0; i < size; ++i)
   {
    const std::string str = DeserializeString (start);
    const bool isNeg = (bool) start.ReadU8 ();

    res.push_back (PAIR (str, isNeg));
   }

  return res;
}

uint32_t
Utils::GetSerializedConjunctionSize (Conjunction conj)
{
  uint32_t size = 1; // conjunction size

  for (Tag t : conj)
   size += GetSerializedStringSize (t.first) + 1; // tag and isNeg

  return size;
}

std::string
Utils::GetMsgStatusString (MessageStatus st)
{
  switch (st)
  {
  case OBJECT_FOUND:
    return "OBJ_FOUND";
  case OBJECT_NOT_FOUND:
    return "OBJ_NOT_FOUND";
  case USED_KEY:
    return "USED_KEY";
  case MOVING:
    return "MOVING";
  case TIMEOUT:
    return "TIMEOUT";
  case NOT_OWNER:
    return "NOT_OWNER";
  case INVALID_ARG:
    return "INVALID_ARG";

  default:
    NS_ABORT_MSG ("Unknown status " << st);
  }
}

std::string
Utils::GetMsgTypeString (MessageType type)
{
  switch (type)
  {
  case 0:
    return "NOT_DEFINED";

  case PUBLISH:
    return "PUB";
  case PUBLISH_RSP:
    return "PUB_RSP";
  case ACTIVE_REP:
    return "ACT_REP";

  case DOWNLOAD:
    return "DOWN";
  case DOWNLOAD_RSP:
    return "DOWN_RSP";

  case UNPUBLISH:
    return "UNPUB";
  case UNPUBLISH_RSP:
    return "UNPUB_RSP";
  case UNPUBLISH_REP:
    return "UNPUB_REP";

  case UPDATE_LOC:
    return "UP_LOC";
  case REGISTER_REP:
    return "REG_REP";

  case SUBSCRIBE:
    return "SUB";
  case SUBSCRIBE_RSP:
    return "SUB_RSP";
  case SUBSCRIBE_NOT:
    return "SUB_NOT";

  case UNSUBSCRIBE:
    return "UNSUB";
  case UNSUBSCRIBE_RSP:
    return "UNSUB_RSP";

  case JOIN:
    return "JOIN";
  case JOIN_RSP:
    return "JOIN_RSP";

  case NACK_MSG:
    return "NACK_MSG";

  default:
    NS_ABORT_MSG ("Unknown type " << type);
  }
}

std::string
Utils::GetMsgTargetString (MessageTarget target)
{
  switch (target)
  {
  case 0:
    return "NOT_DEFINED";

  case CELL:
    return "CELL";
  case NODE:
    return "NODE";
  case CELL_BCAST:
    return "CBCAST";

  default:
    NS_ABORT_MSG ("Unknown target " << target);
  }
}

}
}

