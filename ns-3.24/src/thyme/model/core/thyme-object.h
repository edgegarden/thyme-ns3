/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2015 NOVA LINCS DI-FCT-UNL <http://nova-lincs.di.fct.unl.pt/>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Joao A. Silva <jaa.silva@campus.fct.unl.pt>
 */
#ifndef THYME_OBJECT_H
#define THYME_OBJECT_H

#include "thyme-object-metadata.h"

namespace ns3 {
namespace hyrax {

/**
 * \ingroup hyrax
 * 
 * \brief An object contains the object bytes in the form of an array and 
 * its associated metadata object. The object size is limited to 4 GB
 * (because we serialize its size in an unsigned integer - uint32_t) and
 * it **cannot** be null. Trying to create a null object will abort the 
 * program execution (except when using the default c-tor, but this
 * should **only** be used in deserialization).
 */
class ThymeObject : public SimpleRefCount<ThymeObject>
{
private:
  Ptr<ObjectMetadata> m_objMeta;
  uint32_t m_objSize;
  uint8_t* m_obj;

public:
  ThymeObject (); // for deserialization **only**
  ThymeObject (const uint8_t* key, uint8_t keySize, const uint8_t* obj,
               uint32_t objSize);
  ThymeObject (Ptr<ObjectIdentifier> id, const uint8_t* obj, uint32_t objSize);
  virtual ~ThymeObject ();

  Ptr<ObjectMetadata> GetObjectMetadata () const;
  void SetObjectMetadata (Ptr<ObjectMetadata> objMeta);

  const uint8_t* GetObject () const;
  uint32_t GetObjectSize () const;

  // useful functions to access metadata info directly
  Ptr<ObjectIdentifier> GetObjectIdentifier () const;
  const std::set<std::string> GetTags () const;
  uint8_t GetTagIndex (std::string tag) const;
  std::string GetTag (uint8_t index) const;
  const uint8_t* GetSummary () const;
  uint16_t GetSummarySize () const;
  Time GetPublicationTimestamp () const;
  Ipv4Address GetOwner () const;

  void Serialize (Buffer::Iterator &start) const;
  uint32_t Deserialize (Buffer::Iterator &start);
  uint32_t GetSerializedSize () const;
  void Print (std::ostream &os) const;

  // for download operations (de)serialization (i.e., with no metadata)
  void Serialize2 (Buffer::Iterator &start) const;
  uint32_t Deserialize2 (Buffer::Iterator &start);
  uint32_t GetSerializedSize2 () const;

private:
  void ConstructObject (Ptr<ObjectIdentifier> id, const uint8_t* obj,
                        uint32_t objSize);
  void FreeObject ();
};

std::ostream& operator<< (std::ostream &os, Ptr<const ThymeObject> &obj);

}
}

#endif /* THYME_OBJECT_H */

