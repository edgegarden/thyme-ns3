/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2015 NOVA LINCS DI-FCT-UNL <http://nova-lincs.di.fct.unl.pt/>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Joao A. Silva <jaa.silva@campus.fct.unl.pt>
 */
#include "thyme-dpd.h"
#include <ns3/log.h>
#include <ns3/simulator.h>

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("Dpd");

namespace hyrax {

DuplicatePacketDetection::DuplicatePacketDetection () :
  m_lifetime (Seconds (DUP_LIFETIME))
{
  NS_LOG_FUNCTION (this);
}

DuplicatePacketDetection::DuplicatePacketDetection (Time lifetime) :
  m_lifetime (lifetime)
{
  NS_LOG_FUNCTION (this << lifetime);
}

DuplicatePacketDetection::~DuplicatePacketDetection ()
{
  NS_LOG_FUNCTION (this);
  m_pckts.clear ();
}

Time
DuplicatePacketDetection::GetLifetime () const
{
  NS_LOG_FUNCTION (this);
  return m_lifetime;
}

void
DuplicatePacketDetection::SetLifetime (Time lifetime)
{
  NS_LOG_FUNCTION (this << lifetime);
  m_lifetime = lifetime;
}

bool
DuplicatePacketDetection::IsDuplicate (uint64_t pcktId)
{
  NS_LOG_FUNCTION (this << pcktId);
  if (Purge (pcktId)) return true; // packet found

  // packet not found. insert new entry
  m_pckts.push_back (Pckt (pcktId, Simulator::Now () + m_lifetime));
  return false;
}

bool
DuplicatePacketDetection::Purge (uint64_t pcktId)
{ // delete expired entries. at the same time, search for a specific packet id
  NS_LOG_FUNCTION (this << pcktId);
  bool res = false;
  const Time now = Simulator::Now ();

  for (auto p = m_pckts.begin (); p != m_pckts.end ();)
   { // for each packet in the vector...
    if (p->expiration < now) // entry expired. delete it
     {
      NS_LOG_LOGIC ("Packet " << p->puid << " expired");
      p = m_pckts.erase (p);
     }
    else
     {
      if (p->puid == pcktId) res = true; // pcktId found. save result

      ++p; // continue to iterate the vector
     }
   }

  return res;
}

}
}

