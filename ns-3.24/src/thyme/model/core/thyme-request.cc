/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2015 NOVA LINCS DI-FCT-UNL <http://nova-lincs.di.fct.unl.pt/>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Joao A. Silva <jaa.silva@campus.fct.unl.pt>
 */
#include "thyme-request.h"
#include <ns3/log.h>
#include <ns3/simulator.h>

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("Req");

namespace hyrax {

Request::Request (uint32_t reqId, uint32_t maxRetries) : m_reqId (reqId),
  m_timeoutEventId (), m_timeout (0), m_retry (1), m_maxRetries (maxRetries),
  m_start (Simulator::Now ())
{
  NS_LOG_FUNCTION (this << reqId << maxRetries);
}

uint32_t
Request::GetRequestId () const
{
  NS_LOG_FUNCTION (this);
  return m_reqId;
}

void
Request::SetTimeoutEventId (EventId eventId)
{
  NS_LOG_FUNCTION (this << eventId.GetUid ());
  m_timeoutEventId = eventId;
}

void
Request::CancelTimeout ()
{
  NS_LOG_FUNCTION (this);
  m_timeoutEventId.Cancel ();
}

Time
Request::GetDuration () const
{
  NS_LOG_FUNCTION (this);
  return Simulator::Now () - m_start;
}

bool
Request::HasReachedMaxRetries () const
{
  NS_LOG_FUNCTION (this);
  return m_retry == m_maxRetries;
}

void
Request::IncrementRetry ()
{
  NS_LOG_FUNCTION (this);
  ++m_retry;
}

uint32_t
Request::GetRetries () const
{
  NS_LOG_FUNCTION (this);
  return m_retry;
}

Time
Request::GetTimeout () const
{
  NS_LOG_FUNCTION (this);
  return m_timeout;
}

void
Request::SetTimeout (Time timeout)
{
  NS_LOG_FUNCTION (this << timeout);
  m_timeout = timeout;
}

void
Request::DoDispose ()
{
  NS_LOG_FUNCTION (this);
  CancelTimeout ();
}

PublishRequest::PublishRequest (uint32_t reqId, uint32_t maxRetries,
  Ptr<ThymeObject> obj, uint16_t cell) : Request (reqId, maxRetries),
  m_obj (obj), m_tags (obj->GetTags ()), m_cell (cell)
{
  NS_LOG_FUNCTION (this << reqId << maxRetries << obj << cell);
}

Ptr<ThymeObject>
PublishRequest::GetObject () const
{
  NS_LOG_FUNCTION (this);
  return m_obj;
}

uint16_t
PublishRequest::GetCell () const
{
  NS_LOG_FUNCTION (this);
  return m_cell;
}

void
PublishRequest::ReceivedRspForTag (std::string tag)
{
  NS_LOG_FUNCTION (this);
  m_tags.erase (tag);
}

const std::set<std::string>
PublishRequest::GetRemainingTags () const
{
  NS_LOG_FUNCTION (this);
  return m_tags;
}

bool
PublishRequest::IsFinished () const
{
  NS_LOG_FUNCTION (this);
  return m_tags.empty ();
}

std::string
PublishRequest::GetState () const
{
  NS_LOG_FUNCTION (this);
  std::string res = "waiting for ";
  res += std::to_string (m_tags.size ());
  res += "/";
  res += std::to_string (m_obj->GetTags ().size ());

  return res;
}

void
PublishRequest::DoDispose ()
{
  NS_LOG_FUNCTION (this);
  m_obj = 0;
  m_tags.clear ();
  Request::DoDispose ();
}

DownloadRequest::DownloadRequest (uint32_t reqId, uint32_t maxRetries,
  Ptr<ObjectMetadata> objMeta, Locations locs) :
  Request (reqId, maxRetries), m_objMeta (objMeta), m_locs (locs)
{
  NS_LOG_FUNCTION (this << reqId << maxRetries << objMeta << locs.size ());
}

Ptr<ObjectMetadata>
DownloadRequest::GetObjectMetadata () const
{
  NS_LOG_FUNCTION (this);
  return m_objMeta;
}

Ptr<ObjectIdentifier>
DownloadRequest::GetObjectIdentifier () const
{
  NS_LOG_FUNCTION (this);
  return m_objMeta->GetObjectIdentifier ();
}

Ipv4Address
DownloadRequest::GetOwner () const
{
  NS_LOG_FUNCTION (this);
  return m_objMeta->GetOwner ();
}

const Locations
DownloadRequest::GetLocations () const
{
  NS_LOG_FUNCTION (this);
  return m_locs;
}

void
DownloadRequest::RemoveLocation (Ipv4Address node)
{
  NS_LOG_FUNCTION (this << node);
  m_locs.erase (node);
}

bool
DownloadRequest::IsFinished () const
{
  NS_ABORT_MSG ("Not implemented!");
}

std::string
DownloadRequest::GetState () const
{
  NS_LOG_FUNCTION (this);
  std::string res = "waiting for ";
  res += std::to_string (GetDuration ().GetMilliSeconds ());
  res += "ms";

  return res;
}

void
DownloadRequest::DoDispose ()
{
  NS_LOG_FUNCTION (this);
  m_objMeta = 0;
  m_locs.clear ();
  Request::DoDispose ();
}

SubscribeRequest::SubscribeRequest (uint32_t reqId, uint32_t maxRetries,
  Ptr<Subscription> sub) : Request (reqId, maxRetries), m_sub (sub)
{
  NS_LOG_FUNCTION (this << reqId << maxRetries << sub);
}

Ptr<Subscription>
SubscribeRequest::GetSubscription ()
{
  NS_LOG_FUNCTION (this);
  return m_sub;
}

void
SubscribeRequest::SetTags (std::vector<std::string> tags)
{
  NS_LOG_FUNCTION (this << tags.size ());
  m_tags = std::set<std::string> (tags.begin (), tags.end ());
  m_numTags = tags.size ();
}

void
SubscribeRequest::ReceivedRspForTag (std::string tag)
{
  NS_LOG_FUNCTION (this);
  m_tags.erase (tag);
}

const std::set<std::string>
SubscribeRequest::GetRemainingTags () const
{
  NS_LOG_FUNCTION (this);
  return m_tags;
}

bool
SubscribeRequest::IsFinished () const
{
  NS_LOG_FUNCTION (this);
  return m_tags.empty ();
}

std::string
SubscribeRequest::GetState () const
{
  NS_LOG_FUNCTION (this);
  std::string res = "waiting for ";
  res += std::to_string (m_tags.size ());
  res += "/";
  res += std::to_string (m_numTags);

  return res;
}

void
SubscribeRequest::DoDispose ()
{
  NS_LOG_FUNCTION (this);
  m_sub = 0;
  m_tags.clear ();
  Request::DoDispose ();
}

UnpublishRequest::UnpublishRequest (uint32_t reqId, uint32_t maxRetries,
  Ptr<ThymeObject> obj) : PublishRequest (reqId, maxRetries, obj)
{
  NS_LOG_FUNCTION (this << reqId << maxRetries << obj);
}

UnsubscribeRequest::UnsubscribeRequest (uint32_t reqId, uint32_t maxRetries,
  Ptr<Subscription> sub) : SubscribeRequest (reqId, maxRetries, sub)
{
  NS_LOG_FUNCTION (this << reqId << maxRetries << sub);
}

}
}

