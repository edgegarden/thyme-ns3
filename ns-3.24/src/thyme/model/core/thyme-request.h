/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2015 NOVA LINCS DI-FCT-UNL <http://nova-lincs.di.fct.unl.pt/>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Joao A. Silva <jaa.silva@campus.fct.unl.pt>
 */
#ifndef THYME_REQUEST_H
#define THYME_REQUEST_H

#include "thyme-subscription.h"
#include <ns3/event-id.h>

namespace ns3 {
namespace hyrax {

/**
 * \ingroup hyrax
 * 
 * \brief Encapsulates an ongoing request's state. It is specialized into the
 * five types of operations: publish, unpublish, download, subscribe, and
 * unsubscribe.
 */
class Request : public SimpleRefCount<Request>
{
protected:
  uint32_t m_reqId;

  EventId m_timeoutEventId;
  Time m_timeout;

  uint32_t m_retry;
  uint32_t m_maxRetries;

  Time m_start;

public:
  Request (uint32_t reqId, uint32_t maxRetries);

  uint32_t GetRequestId () const;
  void SetTimeoutEventId (EventId eventId);
  void CancelTimeout ();
  Time GetDuration () const;

  bool HasReachedMaxRetries () const;
  void IncrementRetry ();
  uint32_t GetRetries () const;

  Time GetTimeout () const;
  void SetTimeout (Time timeout);

  virtual bool IsFinished () const = 0;
  virtual std::string GetState () const = 0;

  virtual void DoDispose ();
};

class PublishRequest : public Request
{
protected:
  Ptr<ThymeObject> m_obj;
  std::set<std::string> m_tags;
  uint16_t m_cell;

public:
  PublishRequest (uint32_t reqId, uint32_t maxRetries, Ptr<ThymeObject> obj,
                  uint16_t cell = -1);

  Ptr<ThymeObject> GetObject () const;
  uint16_t GetCell () const;

  void ReceivedRspForTag (std::string tag);
  const std::set<std::string> GetRemainingTags () const;

  virtual bool IsFinished () const override;
  virtual std::string GetState () const override;

  virtual void DoDispose () override;
};

class DownloadRequest : public Request
{
private:
  Ptr<ObjectMetadata> m_objMeta;
  Locations m_locs;

public:
  DownloadRequest (uint32_t reqId, uint32_t maxRetries,
                   Ptr<ObjectMetadata> objMeta, Locations locs = Locations ());

  Ptr<ObjectMetadata> GetObjectMetadata () const;
  Ptr<ObjectIdentifier> GetObjectIdentifier () const;
  Ipv4Address GetOwner () const;
  const Locations GetLocations () const;
  void RemoveLocation (Ipv4Address node);

  bool IsFinished () const override;
  std::string GetState () const override;

  void DoDispose () override;
};

class SubscribeRequest : public Request
{
protected:
  Ptr<Subscription> m_sub;
  uint32_t m_numTags;
  std::set<std::string> m_tags;

public:
  SubscribeRequest (uint32_t reqId, uint32_t maxRetries, Ptr<Subscription> sub);

  Ptr<Subscription> GetSubscription ();
  void SetTags (std::vector<std::string> tags); // used by DCS version

  void ReceivedRspForTag (std::string tag);
  const std::set<std::string> GetRemainingTags () const;

  virtual bool IsFinished () const override;
  virtual std::string GetState () const override;

  virtual void DoDispose () override;
};

class UnpublishRequest : public PublishRequest
{
public:
  UnpublishRequest (uint32_t reqId, uint32_t maxRetries, Ptr<ThymeObject> obj);
};

class UnsubscribeRequest : public SubscribeRequest
{
public:
  UnsubscribeRequest (uint32_t reqId, uint32_t maxRetries,
                      Ptr<Subscription> sub);
};

}
}

#endif /* THYME_REQUEST_H */

