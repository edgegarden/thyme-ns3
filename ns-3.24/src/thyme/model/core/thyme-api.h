/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2015 NOVA LINCS DI-FCT-UNL <http://nova-lincs.di.fct.unl.pt/>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Joao A. Silva <jaa.silva@campus.fct.unl.pt>
 */
#ifndef THYME_API_H
#define THYME_API_H

#include "thyme-request.h"
#include "thyme-subscription.h"
#include <ns3/application.h>
#include <ns3/random-variable-stream.h>
#include <ns3/thyme-profiler.h>

namespace ns3 {
namespace hyrax {

#define REQ_MAX_RETRIES 3 // number of max retries for timed out requests
#define PUB_REQ_TIMEOUT 1500 // milliseconds
#define UNPUB_REQ_TIMEOUT 1000 // milliseconds
#define DOWN_REQ_TIMEOUT 1500 // milliseconds
#define SUB_REQ_TIMEOUT 1000 // milliseconds
#define UNSUB_REQ_TIMEOUT 1000 // milliseconds

#define PASSIVE_REP true // true - use passive replication

#define JOIN_MAX_RETRIES 3 // number of max retries when joining the system
#define JOIN_TIMEOUT 2400 // milliseconds

/**
 * \ingroup hyrax
 * 
 * \brief General API that implements only the general behavior
 * of the system. The specific versions (i.e., v1 PL;SG and v2 DCS) implement
 * the rest of the concrete behaviors.
 */
class ThymeApi : public Application
{
protected:
  Ptr<Node> m_node;
  uint32_t m_nodeId;
  Ipv4Address m_addr;
  Ptr<Profiler> m_profiler;
  Ptr<UniformRandomVariable> m_rand;

  uint32_t m_reqIds = 0;
  // < reqId, req > my ongoing requests
  std::map<uint32_t, Ptr<Request> > m_ongoingReqs; // indexed by reqId
  uint32_t m_reqMaxRetries;
  Time m_pubReqTimeout;
  Time m_unpubReqTimeout;
  Time m_downReqTimeout;
  Time m_subReqTimeout;
  Time m_unsubReqTimeout;

  // < objId, obj > my (locally stored) publications
  MyPublications m_myPubs; // indexed by objId
  // < (objId, node), obj > passive replicas/previously downloaded objects
  Publications m_passReps; // indexed by objKey (objId, node)

  uint32_t m_subIds = 0;
  // < subId, subObj > my (locally stored) subscriptions
  MySubscriptions m_mySubs; // indexed by subscriptionId
  // < node, < subId, subObj > > cell subscriptions
  Subscriptions m_subscriptions; // indexed by subscriber

  Callback <void, Ptr<ThymeObject> > m_pubSuccFn;
  Callback <void, Ptr<ThymeObject>, MessageStatus> m_pubFailFn;
  Callback <void, Ptr<ThymeObject> > m_unpubSuccFn;
  Callback <void, Ptr<ObjectIdentifier>, MessageStatus> m_unpubFailFn;
  Callback <void, Ptr<ThymeObject> > m_downSuccFn;
  Callback <void, Ptr<ObjectIdentifier>, Ipv4Address, MessageStatus> m_downFailFn;
  Callback <void, Ptr<Subscription> > m_subSuccFn;
  Callback <void, Ptr<Subscription>, MessageStatus> m_subFailFn;
  Callback <void, Ptr<Subscription>, Ptr<ObjectMetadata>, Locations> m_subNotFn;
  Callback <void, Ptr<Subscription> > m_unsubSuccFn;
  Callback <void, Ptr<Subscription>, MessageStatus> m_unsubFailFn;

  bool m_passiveRep;

  uint32_t m_joinMaxRetries;
  Time m_joinTimeout;
  EventId m_join;
  bool m_joined = false;
  uint32_t m_joinRetries = 0;

  static const Time m_zero;

public:
  static TypeId GetTypeId (void);

  /**
   * Publishes an object in the system, creating the association between
   * the specified value and the specified key. Each user can associate one
   * value with each key, but different users can associate different values
   * with the same key, i.e., the object key is in fact a pair <key, user>.
   * Neither the key nor the value can be null (but the summary is allowed to
   * be null). If there is already a mapping for the pair <key, user>, this
   * publish operation will fail.
   * 
   * @param key Pointer to key array (identifier)
   * @param keySize Key array size (max 2^8 - 1 = 255)
   * @param value Pointer to value array (object)
   * @param valueSize Value array size (max 2^32 - 1)
   * @param tags Set of tags associated with this object (max 2^8 - 1 = 255)
   * @param summary Pointer to summary array (e.g., thumbnail)
   * @param summarySize Summary array size (max 2^16 - 1 = 65535)
   * 
   * 1. Check for used key
   * 2. Create object for tentative publication
   * 3. Try to publish
   * 
   * Failures : USED_KEY
   */
  void Publish (const uint8_t *key, uint8_t keySize, const uint8_t *value,
                uint32_t valueSize, std::set<std::string> tags,
                const uint8_t* summary, uint16_t summarySize);

  /**
   * Unpublishes an object from the system, removing the mapping for the 
   * specified key (and for the requester user), if it is present. Other values
   * associated with the same key by different users will remain unchanged. If
   * a copy of the data associated with this object key was made in the past
   * (by some other user), that copy will remain in the user's private copies,
   * i.e., remote (previously) downloaded copies of (currently) unpublished
   * data will not be revoked. From the unpublish success notification point
   * forward, every subscription into the past will not receive a notification
   * for the unpublished object.
   * 
   * @param objId Pointer to object identifier
   * 
   * 1. Check for existing key
   * 2. Try to unpublish
   * 
   * Failures: NOT_OWNER
   */
  void Unpublish (Ptr<ObjectIdentifier> objId);

  /**
   * See Unpublish (Ptr<ObjectIdentifier> objId).
   * 
   * @param key Pointer to key array (identifier)
   * @param keySize Key array size (max 2^8 - 1 = 255)
   */
  void Unpublish (const uint8_t *key, uint8_t keySize);

  /**
   * See Unpublish (Ptr<ObjectIdentifier> objId).
   * 
   * @param objMeta Pointer to object metadata
   */
  void Unpublish (Ptr<ObjectMetadata> objMeta);

  /**
   * See Unpublish (Ptr<ObjectIdentifier> objId).
   * 
   * @param obj Pointer to object
   */
  void Unpublish (Ptr<ThymeObject> obj);

  /**
   * Downloads an object, retrieving from the system
   * the object associated with the specified identifier and owner in the 
   * metadata (the pair <key, user>), if it exists.
   * 
   * @param objMeta Pointer to object metadata
   * @param locs List of possible download locations
   * 
   * 1. Check my publications
   * 2. Check passive replicas (previously downloaded objects)
   * 3. Try to download
   * 
   * Failures: NOT_OWNER
   */
  void Download (Ptr<ObjectMetadata> objMeta, Locations locs = Locations ());

  /**
   * Subscribes the interest in being notified about publications concerning 
   * the specified filter, that have occurred during the specified
   * time interval.
   * 
   * @param filter Tags to be subscribed and notified about (in DNF form)
   * @param startTs Timestamp defining the lower bound for the subscription
   * @param endTs Timestamp defining the upper bound for the subscription
   * 
   * 1. Check args
   * 2. Try to subscribe
   * 
   * Failures: INVALID_ARG
   */
  void Subscribe (std::string filter, Time startTs, Time endTs);

  /**
   * Subscribe from now until endTs.
   * See Subscribe (std::string filter, Time startTs, Time endTs).
   * 
   * @param filter Tags to be subscribed and notified about (in DNF form)
   * @param endTs Timestamp defining the upper bound for the subscription
   */
  void SubscribeFromNowUntil (std::string filter, Time endTs);

  /**
   * Subscribe from now until the end of time.
   * See Subscribe (std::string filter, Time startTs, Time endTs).
   * 
   * @param filter Tags to be subscribed and notified about (in DNF form)
   */
  void SubscribeFromNowOn (std::string filter);

  /**
   * Subscribe from the beginning of time until the end of time.
   * See Subscribe (std::string filter, Time startTs, Time endTs).
   * 
   * @param filter Tags to be subscribed and notified about (in DNF form)
   */
  void SubscribeForEternity (std::string filter);

  /**
   * Unsubscribes from the subscription corresponding to the specified object
   * (from this point on).
   * 
   * @param subObj Pointer to subscription object
   * 
   * 1. Check subscription ownership
   * 2. Check subscription validity
   * 3. Try to unsubscribe
   * 
   * Failures: NOT_OWNER, OBJECT_NOT_FOUND, INVALID_ARG
   */
  void Unsubscribe (Ptr<Subscription> subObj);

  /**
   * See Unsubscribe (Ptr<Subscription> subObj).
   * 
   * @param subId Subscription id
   */
  void Unsubscribe (uint32_t subId);

  bool IsOperationOngoing () const;

  void SetPublishSuccessCallback (Callback <void, Ptr<ThymeObject> > pubSuccFn);
  void SetPublishFailureCallback (Callback <void, Ptr<ThymeObject>, MessageStatus> pubFailFn);
  void SetUnpublishSuccessCallback (Callback <void, Ptr<ThymeObject> > unpubSuccFn);
  void SetUnpublishFailureCallback (Callback <void, Ptr<ObjectIdentifier>, MessageStatus> unpubFailFn);
  void SetDownloadSuccessCallback (Callback <void, Ptr<ThymeObject> > downSuccFn);
  void SetDownloadFailureCallback (Callback <void, Ptr<ObjectIdentifier>, Ipv4Address, MessageStatus> downFailFn);
  void SetSubscribeSuccessCallback (Callback <void, Ptr<Subscription> > subSuccFn);
  void SetSubscribeFailureCallback (Callback <void, Ptr<Subscription>, MessageStatus> subFailFn);
  void SetUnsubscribeSuccessCallback (Callback <void, Ptr<Subscription> > unsubSuccFn);
  void SetUnsubscribeFailureCallback (Callback <void, Ptr<Subscription>, MessageStatus> unsubFailFn);
  void SetSubscriptionNotificationCallback (Callback <void, Ptr<Subscription>, Ptr<ObjectMetadata>, Locations> subNotFn);

  void BenchmarkPublish (std::string key, std::string value,
                         std::string summary, std::set<std::string> tags);
  void BenchmarkUnpublishRandom ();
  void BenchmarkUnpublish (std::string key);
  void BenchmarkDownload (Ptr<Subscription> sub, Ptr<ObjectMetadata> meta,
                          Locations locs = Locations ());
  void BenchmarkSubscribeFuture (std::string filter);
  void BenchmarkSubscribePast (std::string filter);
  void BenchmarkUnsubscribeRandom ();
  void BenchmarkUnsubscribe (uint32_t subId);
  void BenchmarkPause ();
  void BenchmarkResume ();
  void BenchmarkExit ();

protected:
  virtual void DoPublish (Ptr<ThymeObject> obj) = 0;
  virtual void DoUnpublish (Ptr<ThymeObject> obj) = 0;
  virtual void DoDownload (Ptr<ObjectMetadata> objMeta, Locations locs) = 0;
  virtual void DoSubscribe (std::string filter, Time startTs, Time endTs) = 0;
  virtual void DoUnsubscribe (Ptr<Subscription> subObj) = 0;

  virtual void DoDispose () override;
  virtual void DoInitialize () override;

  virtual void StartApplication () = 0;
  virtual void StopApplication () override;

  Ptr<ThymeObject> CreateThymeObject (Ptr<ObjectIdentifier> id,
                                      const uint8_t *value, uint32_t valueSize,
                                      std::set<std::string> tags,
                                      const uint8_t* summary,
                                      uint16_t summarySize, Ipv4Address owner);

  uint32_t GetNextSubscriptionId ();

  uint32_t GetNextRequestId ();
  void AddRequest (Ptr<Request> req);
  Ptr<Request> GetRequest (uint32_t reqId);
  Ptr<PublishRequest> GetPublishRequest (uint32_t reqId);
  Ptr<UnpublishRequest> GetUnpublishRequest (uint32_t reqId);
  Ptr<DownloadRequest> GetDownloadRequest (uint32_t reqId);
  Ptr<SubscribeRequest> GetSubscribeRequest (uint32_t reqId);
  Ptr<UnsubscribeRequest> GetUnsubscribeRequest (uint32_t reqId);
  void RemoveRequest (uint32_t reqId);
  void RemoveAllRequests ();

  Ptr<PublishRequest> CreatePublishRequest (Ptr<ThymeObject> obj,
                                            uint16_t cell = -1);
  Ptr<UnpublishRequest> CreateUnpublishRequest (Ptr<ThymeObject> obj);
  Ptr<DownloadRequest> CreateDownloadRequest (Ptr<ObjectMetadata> objMeta,
                                              Locations locs = Locations ());
  Ptr<SubscribeRequest> CreateSubscribeRequest (Ptr<Subscription> sub);
  Ptr<UnsubscribeRequest> CreateUnsubscribeRequest (Ptr<Subscription> sub);

  void StartPublishRequestTimeout (Ptr<PublishRequest> req, Time timeout);
  void StartUnpublishRequestTimeout (Ptr<UnpublishRequest> req, Time timeout);
  void StartDownloadRequestTimeout (Ptr<DownloadRequest> req, Time timeout);
  void StartSubscribeRequestTimeout (Ptr<SubscribeRequest> req, Time timeout);
  void StartUnsubscribeRequestTimeout (Ptr<UnsubscribeRequest> req,
                                       Time timeout);

  virtual void HandlePublishRequestTimeout (Ptr<PublishRequest> req) = 0;
  virtual void HandleUnpublishRequestTimeout (Ptr<UnpublishRequest> req) = 0;
  virtual void HandleDownloadRequestTimeout (Ptr<DownloadRequest> req) = 0;
  virtual void HandleSubscribeRequestTimeout (Ptr<SubscribeRequest> req) = 0;
  virtual void HandleUnsubscribeRequestTimeout (Ptr<UnsubscribeRequest> req) = 0;

  void NotifyPublishSuccess (Ptr<ThymeObject> obj);
  void NotifyPublishFailure (Ptr<ThymeObject> obj, MessageStatus st);
  void NotifyUnpublishSuccess (Ptr<ThymeObject> obj);
  void NotifyUnpublishFailure (Ptr<ObjectIdentifier> id, MessageStatus st);
  void NotifyDownloadSuccess (Ptr<ThymeObject> obj);
  void NotifyDownloadFailure (Ptr<ObjectIdentifier> id, Ipv4Address owner,
                              MessageStatus st);
  void NotifySubscribeSuccess (Ptr<Subscription> subObj);
  void NotifySubscribeFailure (Ptr<Subscription> subObj, MessageStatus status);
  void NotifyUnsubscribeSuccess (Ptr<Subscription> sub);
  void NotifyUnsubscribeFailure (Ptr<Subscription> sub, MessageStatus st);
  void NotifySubscriptionNotification (Ptr<Subscription> sub,
                                       Ptr<ObjectMetadata> meta,
                                       Locations locs = Locations ());

  virtual void IfDown () = 0;
  virtual void IfUp () = 0;
  virtual bool IsIfUp () = 0;
};

}
}

#endif /* THYME_API_H */

