/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2015 NOVA LINCS DI-FCT-UNL <http://nova-lincs.di.fct.unl.pt/>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Joao A. Silva <jaa.silva@campus.fct.unl.pt>
 */
#ifndef THYME_DPD_H
#define THYME_DPD_H

#include <vector>
#include <ns3/nstime.h>

namespace ns3 {
namespace hyrax {

#define DUP_LIFETIME 6.0 // seconds

/**
 * \ingroup hyrax
 * 
 * \brief Simple duplicate packet detection using the ns-3 packet unique
 * identifiers.
 */
class DuplicatePacketDetection
{
private:

  struct Pckt
  {
    uint64_t puid;
    Time expiration;

    Pckt (uint64_t id, Time exp) : puid (id), expiration (exp)
    {
    }
  };

  std::vector<Pckt> m_pckts;
  Time m_lifetime;

public:
  DuplicatePacketDetection (); // uses the default lifetime
  DuplicatePacketDetection (Time lifetime);
  virtual ~DuplicatePacketDetection ();

  Time GetLifetime () const;
  void SetLifetime (Time lifetime);

  bool IsDuplicate (uint64_t pcktId);

private:
  bool Purge (uint64_t pcktId);
};

}
}

#endif /* THYME_DPD_H */

