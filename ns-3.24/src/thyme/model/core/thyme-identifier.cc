/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2015 NOVA LINCS DI-FCT-UNL <http://nova-lincs.di.fct.unl.pt/>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Joao A. Silva <jaa.silva@campus.fct.unl.pt>
 */
#include "thyme-identifier.h"
#include <ns3/abort.h>
#include <ns3/log.h>

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("Id");

namespace hyrax {

ObjectIdentifier::ObjectIdentifier () : m_keySize (0), m_key (0)
{
  NS_LOG_FUNCTION (this);
}

ObjectIdentifier::ObjectIdentifier (const uint8_t* key, uint8_t keySize) :
  m_keySize (0), m_key (0)
{
  NS_LOG_FUNCTION (this << std::string ((char*) key, keySize)
    << (uint16_t) keySize);
  SetKey (key, keySize);
}

ObjectIdentifier::ObjectIdentifier (Ptr<const ObjectIdentifier> id) :
  m_keySize (0), m_key (0)
{
  NS_LOG_FUNCTION (this << id);
  CopyKey (id);
}

ObjectIdentifier::ObjectIdentifier (const ObjectIdentifier &id) :
  m_keySize (0), m_key (0)
{
  NS_LOG_FUNCTION (this << id);
  CopyKey (const_cast<ObjectIdentifier*> (&id));
}

ObjectIdentifier::~ObjectIdentifier ()
{
  NS_LOG_FUNCTION (this);
  FreeKey ();
}

const uint8_t*
ObjectIdentifier::GetKey () const
{
  NS_LOG_FUNCTION (this);
  return m_key;
}

uint8_t
ObjectIdentifier::GetKeySize () const
{
  NS_LOG_FUNCTION (this);
  return m_keySize;
}

std::string
ObjectIdentifier::ToString () const
{
  NS_LOG_FUNCTION (this);
  return std::string ((char*) m_key, m_keySize);
}

bool
ObjectIdentifier::IsEqual (Ptr<const ObjectIdentifier> id) const
{ // fast path to fail: different key sizes (otherwise, compare byte by byte)
  NS_LOG_FUNCTION (this << id);
  return id->GetKeySize () != m_keySize ? false :
    memcmp (m_key, id->GetKey (), m_keySize) == 0;
}

bool
ObjectIdentifier::IsLess (Ptr<const ObjectIdentifier> id) const
{
  NS_LOG_FUNCTION (this << id);
  const uint8_t* key = id->GetKey ();
  const uint8_t keySize = id->GetKeySize ();

  for (int i = m_keySize - 1, j = keySize - 1; i >= 0 && j >= 0; --i, --j)
   { // compare the two keys, byte by byte. start in least significant bytes
    if (m_key[i] < key[j]) return true;
    else if (m_key[i] > key[j]) return false;
   }

  return m_keySize < keySize;
}

void
ObjectIdentifier::Serialize (Buffer::Iterator &start) const
{
  NS_LOG_FUNCTION (this);
  const Buffer::Iterator i = start;
  start.WriteU8 (m_keySize);

  start.Write (m_key, m_keySize);

  uint32_t dist = 0;
  uint32_t serSize = 0;
  NS_ASSERT_MSG ((dist = start.GetDistanceFrom (i))
    == (serSize = GetSerializedSize ()), dist << " != " << serSize);
}

uint32_t
ObjectIdentifier::Deserialize (Buffer::Iterator &start)
{
  NS_LOG_FUNCTION (this);
  const Buffer::Iterator i = start;
  m_keySize = start.ReadU8 ();

  m_key = AllocateBuffer (m_keySize);
  start.Read (m_key, m_keySize);

  const uint32_t dist = start.GetDistanceFrom (i);
  uint32_t serSize = 0;
  NS_ASSERT_MSG (dist == (serSize = GetSerializedSize ()),
    dist << " != " << serSize);
  return dist;
}

uint32_t
ObjectIdentifier::GetSerializedSize () const
{
  NS_LOG_FUNCTION (this);
  return 1 + m_keySize; // keySize and key
}

void
ObjectIdentifier::Print (std::ostream &os) const
{
  os << "Id(" << (uint16_t) m_keySize << "): " << ToString ();
}

uint8_t*
ObjectIdentifier::AllocateBuffer (uint32_t bytes)
{
  NS_ABORT_MSG_IF (bytes == 0, "Cannot allocate zero bytes.");
  NS_LOG_FUNCTION (bytes);

  uint8_t* buf = (uint8_t*) malloc (bytes);
  NS_ABORT_MSG_IF (buf == 0, "Malloc failed.");

  return buf;
}

void
ObjectIdentifier::SetKey (const uint8_t* key, uint8_t keySize)
{
  NS_ABORT_MSG_IF (key == 0 || keySize == 0, "ObjId cannot be null.");
  NS_LOG_FUNCTION (this << std::string ((char*) key, keySize)
    << (uint16_t) keySize);
  FreeKey ();

  m_keySize = keySize;
  m_key = AllocateBuffer (m_keySize);
  memcpy (m_key, key, m_keySize);
}

void
ObjectIdentifier::CopyKey (Ptr<const ObjectIdentifier> id)
{
  NS_LOG_FUNCTION (this << id);
  SetKey (id->GetKey (), id->GetKeySize ());
}

void
ObjectIdentifier::FreeKey ()
{
  NS_LOG_FUNCTION (this);
  if (m_key != 0) // if there is previously allocated memory...
   {
    // this requires m_key to be initialized (to zero) in every c-tor
    // otherwise, this call might use a random address and trigger a SIGSEGV
    free (m_key);
    m_key = 0;
    m_keySize = 0;
   }
}

std::ostream&
operator<< (std::ostream &os, const ObjectIdentifier &id)
{
  id.Print (os);
  return os;
}

std::ostream&
operator<< (std::ostream &os, Ptr<const ObjectIdentifier> &id)
{
  id->Print (os);
  return os;
}

bool
operator== (const ObjectIdentifier &idL, const ObjectIdentifier &idR)
{
  Ptr<ObjectIdentifier> objIdL = const_cast<ObjectIdentifier *> (&idL);
  Ptr<ObjectIdentifier> objIdR = const_cast<ObjectIdentifier *> (&idR);
  return objIdL->IsEqual (objIdR);
}

bool
operator< (const ObjectIdentifier &idL, const ObjectIdentifier &idR)
{
  Ptr<ObjectIdentifier> objIdL = const_cast<ObjectIdentifier *> (&idL);
  Ptr<ObjectIdentifier> objIdR = const_cast<ObjectIdentifier *> (&idR);
  return objIdL->IsLess (objIdR);
}

}
}

