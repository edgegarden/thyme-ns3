/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2015 NOVA LINCS DI-FCT-UNL <http://nova-lincs.di.fct.unl.pt/>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Joao A. Silva <jaa.silva@campus.fct.unl.pt>
 */
#ifndef THYME_UTILS_H
#define THYME_UTILS_H

#include "thyme-common.h"

namespace ns3 {
namespace hyrax {

/**
 * \ingroup hyrax
 * 
 * \brief Some useful general functions.
 */
class Utils
{
public:
  static void SerializeString (std::string str, Buffer::Iterator &start);
  static std::string DeserializeString (Buffer::Iterator &start);
  static uint32_t GetSerializedStringSize (std::string str);

  static void SerializeConjunction (Conjunction conj, Buffer::Iterator &start);
  static Conjunction DeserializeConjunction (Buffer::Iterator &start);
  static uint32_t GetSerializedConjunctionSize (Conjunction conj);

  static std::string GetMsgStatusString (MessageStatus st);
  static std::string GetMsgTypeString (MessageType type);
  static std::string GetMsgTargetString (MessageTarget target);
};

}
}

#endif /* THYME_UTILS_H */

