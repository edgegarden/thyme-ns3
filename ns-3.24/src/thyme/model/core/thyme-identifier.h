/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2015 NOVA LINCS DI-FCT-UNL <http://nova-lincs.di.fct.unl.pt/>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Joao A. Silva <jaa.silva@campus.fct.unl.pt>
 */
#ifndef THYME_IDENTIFIER_H
#define THYME_IDENTIFIER_H

#include <ns3/buffer.h>
#include <ns3/ptr.h>
#include <ns3/simple-ref-count.h>

namespace ns3 {
namespace hyrax {

/**
 * \ingroup hyrax
 * 
 * \brief Any array of bytes can be used as an object identifier,
 * but we expect it to be something human-readable, e.g., a string.
 * It has a maximum limit of 255 bytes (because we serialize its size in an
 * unsigned byte - uint8_t) and **cannot** be null. Trying to create a null
 * object identifier will abort the program execution (except when using the
 * default c-tor, but this should **only** be used in deserialization).
 */
class ObjectIdentifier : public SimpleRefCount<ObjectIdentifier>
{
private:
  uint8_t m_keySize;
  uint8_t* m_key; // max size 2^8 - 1 = 255 bytes

public:
  ObjectIdentifier (); // for deserialization **only**
  ObjectIdentifier (const uint8_t* key, uint8_t keySize);
  ObjectIdentifier (Ptr<const ObjectIdentifier> id);
  ObjectIdentifier (const ObjectIdentifier &id);
  virtual ~ObjectIdentifier ();

  const uint8_t* GetKey () const;
  uint8_t GetKeySize () const;
  std::string ToString () const;

  bool IsEqual (Ptr<const ObjectIdentifier> id) const;
  bool IsLess (Ptr<const ObjectIdentifier> id) const;

  void Serialize (Buffer::Iterator &start) const;
  uint32_t Deserialize (Buffer::Iterator &start);
  uint32_t GetSerializedSize () const;
  void Print (std::ostream &os) const;

  static uint8_t* AllocateBuffer (uint32_t bytes);

private:
  void SetKey (const uint8_t* key, uint8_t keySize);
  void CopyKey (Ptr<const ObjectIdentifier> id);
  void FreeKey ();
};

std::ostream& operator<< (std::ostream &os, const ObjectIdentifier &id);
std::ostream& operator<< (std::ostream &os, Ptr<const ObjectIdentifier> &id);
bool operator== (const ObjectIdentifier &idL, const ObjectIdentifier &idR);
bool operator< (const ObjectIdentifier &idL, const ObjectIdentifier &idR);

}
}

#endif /* THYME_IDENTIFIER_H */

