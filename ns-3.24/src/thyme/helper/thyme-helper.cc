/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2015 NOVA LINCS DI-FCT-UNL <http://nova-lincs.di.fct.unl.pt/>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Joao A. Silva <jaa.silva@campus.fct.unl.pt>
 */
#include "thyme-helper.h"
#include <ns3/names.h>
#include <ns3/pointer.h>
#include <ns3/thyme.h>

namespace ns3 {

ThymeHelper::ThymeHelper ()
{
  m_factory.SetTypeId (hyrax::Thyme::GetTypeId ());
}

ThymeHelper::ThymeHelper (Ptr<hyrax::Profiler> profiler)
{
  m_factory.SetTypeId (hyrax::Thyme::GetTypeId ());
  SetAttribute ("Profiler", PointerValue (profiler));
}

void
ThymeHelper::SetAttribute (std::string name, const AttributeValue &value)
{
  m_factory.Set (name, value);
}

ApplicationContainer
ThymeHelper::Install (NodeContainer c) const
{
  ApplicationContainer apps;
  for (auto i = c.Begin (); i != c.End (); ++i)
   apps.Add (InstallPriv (*i));

  return apps;
}

ApplicationContainer
ThymeHelper::Install (Ptr<Node> node) const
{
  return ApplicationContainer (InstallPriv (node));
}

ApplicationContainer
ThymeHelper::Install (std::string nodeName) const
{
  Ptr<Node> node = Names::Find<Node> (nodeName);
  return ApplicationContainer (InstallPriv (node));
}

Ptr<Application>
ThymeHelper::InstallPriv (Ptr<Node> node) const
{
  Ptr<Application> app = m_factory.Create<hyrax::Thyme> ();
  node->AddApplication (app);
  return app;
}

}

