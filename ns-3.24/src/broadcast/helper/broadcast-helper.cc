/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2015 NOVA LINCS DI-FCT-UNL <http://nova-lincs.di.fct.unl.pt/>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Joao A. Silva <jaa.silva@campus.fct.unl.pt>
 */
#include "broadcast-helper.h"
#include <ns3/broadcast.h>
#include <ns3/names.h>
#include <ns3/pointer.h>

namespace ns3 {

BroadcastHelper::BroadcastHelper ()
{
  m_factory.SetTypeId (hyrax::Broadcast::GetTypeId ());
}

BroadcastHelper::BroadcastHelper (Ptr<hyrax::Profiler> profiler)
{
  m_factory.SetTypeId (hyrax::Broadcast::GetTypeId ());
  SetAttribute ("Profiler", PointerValue (profiler));
}

void
BroadcastHelper::SetAttribute (std::string name, const AttributeValue &value)
{
  m_factory.Set (name, value);
}

ApplicationContainer
BroadcastHelper::Install (NodeContainer c) const
{
  ApplicationContainer apps;
  for (auto i = c.Begin (); i != c.End (); ++i)
   apps.Add (InstallPriv (*i));

  return apps;
}

ApplicationContainer
BroadcastHelper::Install (Ptr<Node> node) const
{
  return ApplicationContainer (InstallPriv (node));
}

ApplicationContainer
BroadcastHelper::Install (std::string nodeName) const
{
  Ptr<Node> node = Names::Find<Node> (nodeName);
  return ApplicationContainer (InstallPriv (node));
}

Ptr<Application>
BroadcastHelper::InstallPriv (Ptr<Node> node) const
{
  Ptr<Application> app = m_factory.Create<hyrax::Broadcast> ();
  node->AddApplication (app);
  return app;
}

}

