/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2015 NOVA LINCS DI-FCT-UNL <http://nova-lincs.di.fct.unl.pt/>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Joao A. Silva <jaa.silva@campus.fct.unl.pt>
 */
#include <ns3/test.h>

using namespace ns3;

class SubscriptionObjectTestCase : public TestCase
{
public:

  SubscriptionObjectTestCase () : TestCase ("Test BSubscriptionObject class")
  {
  }

  virtual
  ~SubscriptionObjectTestCase ()
  {
  }

private:

  virtual void
  DoRun (void)
  {
    // @TODO implement
  }
};

class MessageTestCase : public TestCase
{
public:

  MessageTestCase () : TestCase ("Test BMessage class")
  {
  }

  virtual
  ~MessageTestCase ()
  {
  }

private:

  virtual void
  DoRun (void)
  {
    // @TODO implement
  }
};

class BroadcastTestCase : public TestCase
{
public:

  BroadcastTestCase () : TestCase ("Test Broadcast class")
  {
  }

  virtual
  ~BroadcastTestCase ()
  {
  }

private:

  virtual void
  DoRun (void)
  {
    // @TODO implement
  }
};

class BroadcastTestSuite : public TestSuite
{
public:

  BroadcastTestSuite () : TestSuite ("broadcast", UNIT)
  { // TestDuration for TestCase can be QUICK, EXTENSIVE or TAKES_FOREVER
    AddTestCase (new SubscriptionObjectTestCase, TestCase::QUICK);
    AddTestCase (new MessageTestCase, TestCase::QUICK);
    AddTestCase (new BroadcastTestCase, TestCase::QUICK);
  } // TestSuite type can be ALL, BVT, UNIT, SYSTEM, EXAMPLE or PERFORMANCE
};

static BroadcastTestSuite broadcastTestSuite;

