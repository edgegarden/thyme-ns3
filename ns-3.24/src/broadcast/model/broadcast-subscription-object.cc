/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2015 NOVA LINCS DI-FCT-UNL <http://nova-lincs.di.fct.unl.pt/>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Joao A. Silva <jaa.silva@campus.fct.unl.pt>
 */
#include "broadcast-subscription-object.h"
#include <ns3/log.h>
#include <ns3/thyme-utils.h>

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("BSubObj");

namespace hyrax {

BSubscriptionObject::BSubscriptionObject () : Subscription ()
{
  NS_LOG_FUNCTION (this);
}

BSubscriptionObject::BSubscriptionObject (uint32_t subId,
  Ipv4Address subscriber, Time start, Time end, std::string filter) :
  Subscription (subId, subscriber, start, end, filter)
{
  NS_LOG_FUNCTION (this << subId << subscriber << start << end << filter);
}

void
BSubscriptionObject::Serialize (Buffer::Iterator &start) const
{
  NS_LOG_FUNCTION (this);
  const Buffer::Iterator i = start;
  Subscription::Serialize (start);

  // serialize filter (super class doesn't do it!)
  start.WriteHtonU32 (m_filter.size ());
  for (Conjunction conj : m_filter)
   Utils::SerializeConjunction (conj, start);

  uint32_t dist = 0;
  uint32_t serSize = 0;
  NS_ASSERT_MSG ((dist = start.GetDistanceFrom (i))
    == (serSize = GetSerializedSize ()), dist << " != " << serSize);
}

uint32_t
BSubscriptionObject::Deserialize (Buffer::Iterator &start)
{
  NS_LOG_FUNCTION (this);
  const Buffer::Iterator i = start;
  Subscription::Deserialize (start);

  // deserialize filter (super class doesn't do it!)
  const int size = start.ReadNtohU32 ();
  for (int i = 0; i < size; ++i)
   m_filter.push_back (Utils::DeserializeConjunction (start));

  const uint32_t dist = start.GetDistanceFrom (i);
  uint32_t serSize = 0;
  NS_ASSERT_MSG (dist == (serSize = GetSerializedSize ()),
    dist << " != " << serSize);
  return dist;
}

uint32_t
BSubscriptionObject::GetSerializedSize () const
{
  NS_LOG_FUNCTION (this);
  uint32_t size = Subscription::GetSerializedSize (); // super class

  size += 4; // filter size
  for (Conjunction conj : m_filter)
   size += Utils::GetSerializedConjunctionSize (conj); // conjunction

  return size;
}

void
BSubscriptionObject::Print (std::ostream &os) const
{
  Subscription::Print (os);
  os << " #Conjs: " << m_filter.size ();
}

bool
BSubscriptionObject::IsValidFilter (std::set<std::string> tags) const
{
  NS_LOG_FUNCTION (this << tags.size ());
  for (Conjunction conj : m_filter) // for each conjunction...
   {
    bool res = true;

    for (Tag t : conj) // for each literal...
     {
      bool found = tags.find (t.first) != tags.end ();
      bool isNeg = t.second;

      if ((found && isNeg) || (!found && !isNeg))
       { // (found tag and tag is neg) OR (didn't find tag and tag is pos)
        res = false;
        break; // skip this conjunction
       }
     }

    if (res) // if res is true, we can return immediately
     return true; // this conjunction is true (and so is the whole formula)
   }

  return false;
}

}
}

