/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2015 NOVA LINCS DI-FCT-UNL <http://nova-lincs.di.fct.unl.pt/>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Joao A. Silva <jaa.silva@campus.fct.unl.pt>
 */
#ifndef BROADCAST_MESSAGE_H
#define BROADCAST_MESSAGE_H

#include "broadcast-subscription-object.h"
#include <ns3/header.h>

namespace ns3 {
namespace hyrax {

class BMessage : public Header
{
private:
  Ipv4Address m_src;
  MessageType m_type;

public:
  static TypeId GetTypeId (void);
  TypeId GetInstanceTypeId (void) const override;
  BMessage (); // for deserialization **only**
  BMessage (Ipv4Address src);

  Ipv4Address GetMsgSource () const;
  MessageType GetMsgType () const;

  void Serialize (Buffer::Iterator start) const override;
  uint32_t Deserialize (Buffer::Iterator start) override;
  uint32_t GetSerializedSize () const override;
  void Print (std::ostream &os) const override;

  struct BDownload
  {
    uint32_t reqId;
    Ptr<ObjectIdentifier> objId;

    void Serialize (Buffer::Iterator &start) const;
    uint32_t Deserialize (Buffer::Iterator &start);
    uint32_t GetSerializedSize () const;
    void Print (std::ostream &os) const;
  };

  BDownload GetDownload () const;
  void SetDownload (uint32_t rId, Ptr<ObjectIdentifier> oId);

  struct BDownloadRsp
  {
    uint32_t reqId;
    MessageStatus st;
    Ptr<ThymeObject> obj;

    void Serialize (Buffer::Iterator &start) const;
    uint32_t Deserialize (Buffer::Iterator &start);
    uint32_t GetSerializedSize () const;
    void Print (std::ostream &os) const;
  };

  BDownloadRsp GetDownloadRsp () const;
  void SetDownloadRsp (uint32_t rId, MessageStatus s, Ptr<ThymeObject> o);

  struct BSubscribe
  {
    Ptr<BSubscriptionObject> subObj;

    void Serialize (Buffer::Iterator &start) const;
    uint32_t Deserialize (Buffer::Iterator &start);
    uint32_t GetSerializedSize () const;
    void Print (std::ostream &os) const;
  };

  BSubscribe GetSubscribe () const;
  void SetSubscribe (Ptr<BSubscriptionObject> sObj);

  struct BSubscribeNot
  {
    uint32_t subId;
    std::vector<Ptr<ObjectMetadata> > objMeta;

    void Serialize (Buffer::Iterator &start) const;
    uint32_t Deserialize (Buffer::Iterator &start);
    uint32_t GetSerializedSize () const;
    void Print (std::ostream &os) const;
  };

  BSubscribeNot GetSubscribeNot () const;
  void SetSubscribeNot (uint32_t sId, std::vector<Ptr<ObjectMetadata> > m);

  struct BUnsubscribe
  {
    uint32_t subId;

    void Serialize (Buffer::Iterator &start) const;
    uint32_t Deserialize (Buffer::Iterator &start);
    uint32_t GetSerializedSize () const;
    void Print (std::ostream &os) const;
  };

  BUnsubscribe GetUnsubscribe () const;
  void SetUnsubscribe (uint32_t sId);

  void SetJoin ();

  struct BJoinRsp
  {
    Subscriptions subs;

    void Serialize (Buffer::Iterator &start) const;
    uint32_t Deserialize (Buffer::Iterator &start);
    uint32_t GetSerializedSize () const;
    void Print (std::ostream &os) const;
  };

  BJoinRsp GetJoinRsp () const;
  void SetJoinRsp (Subscriptions s);

private:

  struct
  {
    BDownload down;
    BDownloadRsp downRsp;
    BSubscribe sub;
    BSubscribeNot subNot;
    BUnsubscribe unsub;
    BJoinRsp joinRsp;
  } m_message;
};

std::ostream& operator<< (std::ostream &os, const BMessage::BDownload &x);
std::ostream& operator<< (std::ostream &os, const BMessage::BDownloadRsp &x);
std::ostream& operator<< (std::ostream &os, const BMessage::BSubscribe &x);
std::ostream& operator<< (std::ostream &os, const BMessage::BSubscribeNot &x);
std::ostream& operator<< (std::ostream &os, const BMessage::BUnsubscribe &x);
std::ostream& operator<< (std::ostream &os, const BMessage::BJoinRsp &x);

}
}

#endif /* BROADCAST_MESSAGE_H */

