/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2015 NOVA LINCS DI-FCT-UNL <http://nova-lincs.di.fct.unl.pt/>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Joao A. Silva <jaa.silva@campus.fct.unl.pt>
 */
#ifndef BROADCAST_SUBSCRIPTION_OBJECT_H
#define BROADCAST_SUBSCRIPTION_OBJECT_H

#include <ns3/thyme-subscription.h>

namespace ns3 {
namespace hyrax {

class BSubscriptionObject : public Subscription
{
public:
  BSubscriptionObject (); // for deserialization **only**
  BSubscriptionObject (uint32_t subId, Ipv4Address subscriber, Time start,
                       Time end, std::string filter);

  void Serialize (Buffer::Iterator &start) const override;
  uint32_t Deserialize (Buffer::Iterator &start) override;
  uint32_t GetSerializedSize () const override;
  void Print (std::ostream &os) const override;

private:
  bool IsValidFilter (std::set<std::string> tags) const override;
};

}
}

#endif /* BROADCAST_SUBSCRIPTION_OBJECT_H */

