/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2015 NOVA LINCS DI-FCT-UNL <http://nova-lincs.di.fct.unl.pt/>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Joao A. Silva <jaa.silva@campus.fct.unl.pt>
 */
#ifndef BROADCAST_H
#define BROADCAST_H

#include "broadcast-message.h"
#include <ns3/thyme-api.h>
#include <ns3/thyme-dpd.h>

namespace ns3 {
namespace hyrax {

#define PORT 0xff

class Broadcast : public ThymeApi
{
private:
  Ptr<Ipv4> m_ipv4;
  Ptr<NetDevice> m_iface;
  Ptr<Socket> m_socket;
  uint32_t m_port;

  DuplicatePacketDetection m_dpd;

  // < tag, < objId, obj > > my publications indexed by tag
  std::map<std::string, MyPublications> m_pubsByTag;

public:
  static TypeId GetTypeId (void);
  Broadcast ();
  virtual ~Broadcast ();

protected:
  /**
   * 1. Save publication locally
   * 2. Check subscriptions (send notifications, if need be)
   * 3. Notify application
   * 
   * (Never fails!)
   */
  void DoPublish (Ptr<ThymeObject> obj) override;

  /**
   * 1. Delete publication locally
   * 2. Notify application
   * 
   * (Never fails!)
   */
  void DoUnpublish (Ptr<ThymeObject> obj) override;

  /**
   * 1. Send DOWNLOAD to owner
   * 2. Recv RSP (DOWNLOAD_RSP)
   * 3. Save object in passive replicas
   * 4. Notify application
   * 
   * Failures: OBJECT_NOT_FOUND, TIMEOUT
   */
  void DoDownload (Ptr<ObjectMetadata> objMeta, Locations locs) override;

  /**
   * 1. Save subscription locally
   * 2. Broadcast BSUBSCRIBE
   * 3. Notify application
   * 
   * (Never fails!)
   */
  void DoSubscribe (std::string filter, Time startTs, Time endTs) override;

  /**
   * 1. Delete subscription locally
   * 2. Broadcast BUNSUBSCRIBE
   * 3. Notify application
   * 
   * (Never fails!)
   */
  void DoUnsubscribe (Ptr<Subscription> subObj) override;

  void DoDispose () override;
  void DoInitialize () override;

private:
  void StartApplication () override;
  void StopApplication () override;
  void SetupRoutingLayer ();

  void InsertPubByTags (Ptr<ThymeObject> obj);
  void RemovePubByTags (Ptr<ThymeObject> obj);

  void ProcessPacket (Ptr<Socket> socket);

  void ProcessDownload (BMessage::BDownload msg, Ipv4Address src);
  void ProcessDownloadRsp (BMessage::BDownloadRsp msg);

  void MakePassiveReplica (Ptr<ThymeObject> obj);

  void ProcessSubscribe (BMessage msg, Ipv4Address src, Ptr<Packet> packet);
  void ProcessSubscribeNot (BMessage::BSubscribeNot msg, Ipv4Address src);
  void ProcessUnsubscribe (BMessage msg, Ipv4Address src, Ptr<Packet> packet);

  void InsertSubscription (Ptr<BSubscriptionObject> subObj);
  void RemoveSubscription (Ipv4Address subscriber, uint32_t subId);

  void ProcessJoin (Ipv4Address src);
  void ProcessJoinRsp (BMessage::BJoinRsp msg);

  void Bcast (BMessage msg, Ptr<Packet> packet);
  void Unicast (BMessage msg, Ipv4Address dst);

  void HandleSubscriptions (Ptr<ObjectMetadata> objMeta); // in the present
  std::vector<Ptr<Subscription> > CheckAllSubscriptions (Ptr<ObjectMetadata> objMeta);
  void SendNotifications (std::vector<Ptr<Subscription> > subs,
                          Ptr<ObjectMetadata> objMeta);

  void HandleSubscriptions (Ptr<BSubscriptionObject> subObj); // in the past
  std::vector<Ptr<ObjectMetadata> > CheckAllPublications (Ptr<BSubscriptionObject> subObj);
  void SendNotifications (Ptr<BSubscriptionObject> sub,
                          std::vector<Ptr<ObjectMetadata> > objsMeta);

  void HandlePublishRequestTimeout (Ptr<PublishRequest> req) override;
  void HandleUnpublishRequestTimeout (Ptr<UnpublishRequest> req) override;
  void HandleDownloadRequestTimeout (Ptr<DownloadRequest> req) override;
  void HandleSubscribeRequestTimeout (Ptr<SubscribeRequest> req) override;
  void HandleUnsubscribeRequestTimeout (Ptr<UnsubscribeRequest> req) override;

  void Join ();

  void IfDown () override;
  void IfUp () override;
  bool IsIfUp () override;
};

}
}

#endif /* BROADCAST_H */

