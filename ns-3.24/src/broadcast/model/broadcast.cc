/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2015 NOVA LINCS DI-FCT-UNL <http://nova-lincs.di.fct.unl.pt/>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Joao A. Silva <jaa.silva@campus.fct.unl.pt>
 */
#include "broadcast.h"
#include <ns3/log.h>
#include <ns3/thyme-utils.h>
#include <ns3/udp-socket-factory.h>
#include <ns3/uinteger.h>

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("Bcast");

namespace hyrax {

NS_OBJECT_ENSURE_REGISTERED (Broadcast);

TypeId
Broadcast::GetTypeId (void)
{
  NS_LOG_FUNCTION_NOARGS ();
  static TypeId tid = TypeId ("ns3::hyrax::Broadcast")
    .SetParent<ThymeApi> ()
    .AddConstructor<Broadcast> ()

    .AddAttribute ("Port",
    "Messages port number.",
    UintegerValue (PORT),
    MakeUintegerAccessor (&Broadcast::m_port),
    MakeUintegerChecker<uint32_t> ());

  return tid;
}

Broadcast::Broadcast () : ThymeApi ()
{
  NS_LOG_FUNCTION (this);
}

Broadcast::~Broadcast ()
{
  NS_LOG_FUNCTION (this);
}

void
Broadcast::DoPublish (Ptr<ThymeObject> obj)
{
  NS_LOG_FUNCTION (this << obj);
  m_profiler->OnPublishStart (m_addr, -1, obj);
  m_profiler->OnPublishReceived (m_addr);

  // save publication locally
  m_myPubs.insert (OBJ_KEY (obj->GetObjectIdentifier (), obj)); // index by id
  InsertPubByTags (obj); // index by tags

  HandleSubscriptions (obj->GetObjectMetadata ()); // check subscriptions

  NotifyPublishSuccess (obj); // notify application
  m_profiler->OnPublishSuccess (m_addr, -1, 1);
}

void
Broadcast::DoUnpublish (Ptr<ThymeObject> obj)
{
  NS_LOG_FUNCTION (this << obj);
  m_profiler->OnUnpublishStart (m_addr, -1, obj);
  m_profiler->OnUnpublishReceived (m_addr);

  // delete publication locally
  m_myPubs.erase (PEEK (obj->GetObjectIdentifier ())); // index by id
  RemovePubByTags (obj); // index by tags

  NotifyUnpublishSuccess (obj); // notify application
  m_profiler->OnUnpublishSuccess (m_addr, -1, 1);
}

void
Broadcast::DoDownload (Ptr<ObjectMetadata> objMeta, Locations locs)
{
  NS_LOG_FUNCTION (this << objMeta);
  Ptr<DownloadRequest> req = CreateDownloadRequest (objMeta);
  m_profiler->OnDownloadStart (m_addr, req->GetRequestId ());

  BMessage msg (m_addr);
  msg.SetDownload (req->GetRequestId (), objMeta->GetObjectIdentifier ());
  Unicast (msg, objMeta->GetOwner ()); // send DOWNLOAD to owner

  StartDownloadRequestTimeout (req, m_downReqTimeout);
}

void
Broadcast::DoSubscribe (std::string filter, Time startTs, Time endTs)
{
  NS_LOG_FUNCTION (this << filter << startTs << endTs);
  uint32_t subId = GetNextSubscriptionId ();
  Ptr<BSubscriptionObject> subObj =
    Create<BSubscriptionObject> (subId, m_addr, startTs, endTs, filter);
  m_profiler->OnSubscribeStart (m_addr, subId, subObj);

  // save subscription locally
  m_mySubs.insert (PAIR (subId, subObj)); // index by id
  InsertSubscription (subObj); // index by subscriber

  BMessage msg (m_addr);
  msg.SetSubscribe (subObj);
  Ptr<Packet> p = Create<Packet> ();
  m_dpd.IsDuplicate (p->GetUid ()); // register my own message as duplicate
  Bcast (msg, p); // broadcast SUBSCRIBE

  NotifySubscribeSuccess (subObj); // notify application
}

void
Broadcast::DoUnsubscribe (Ptr<Subscription> subObj)
{
  NS_LOG_FUNCTION (this << subObj);
  uint32_t subId = subObj->GetSubscriptionId ();
  m_profiler->OnUnsubscribeStart (m_addr, subId, subObj);

  // delete subscription locally
  m_mySubs.erase (subId); // index by id
  RemoveSubscription (m_addr, subId); // index by subscriber

  BMessage msg (m_addr);
  msg.SetUnsubscribe (subId);
  Ptr<Packet> p = Create<Packet> ();
  m_dpd.IsDuplicate (p->GetUid ()); // register my own message as duplicate
  Bcast (msg, p); // broadcast BUNSUBSCRIBE

  NotifyUnsubscribeSuccess (subObj); // notify application
}

void
Broadcast::DoDispose ()
{
  NS_LOG_FUNCTION (this);
  ThymeApi::DoDispose ();
}

void
Broadcast::DoInitialize ()
{
  NS_LOG_FUNCTION (this);
  ThymeApi::DoInitialize ();
}

void
Broadcast::StartApplication ()
{
  NS_LOG_FUNCTION (this);
  m_ipv4 = m_node->GetObject<Ipv4> ();
  m_dpd = DuplicatePacketDetection ();

  SetupRoutingLayer ();
  m_profiler->RegisterNode (m_addr);
  Join ();
}

void
Broadcast::StopApplication ()
{
  NS_LOG_FUNCTION (this);
  m_socket->Close ();
  m_ipv4 = 0;
  m_iface = 0;
  m_socket = 0;
  ThymeApi::StopApplication ();
}

void
Broadcast::SetupRoutingLayer ()
{
  NS_LOG_FUNCTION (this);
  const uint32_t devs = m_node->GetNDevices (); // get interfaces
  NS_LOG_INFO (devs << " device(s) found");

  for (int i = 0; i < devs; ++i)
   {
    Ptr<NetDevice> dev = m_node->GetDevice (i);
    NS_LOG_INFO ("Dev " << i << " " << dev->GetAddress () << " "
      << dev->GetInstanceTypeId ().GetName () << " "
      << m_ipv4->GetAddress (m_ipv4->GetInterfaceForDevice (dev), 0).GetLocal ()
      << " " << m_ipv4->GetInterfaceForDevice (dev));

    if (dev->GetInstanceTypeId ().GetName ()
        .compare ("ns3::LoopbackNetDevice") != 0) // if not the loopback iface
     {
      if (m_iface == 0) // first iface is considered the primary iface
       {
        m_iface = dev;
        m_addr = m_ipv4->GetAddress (m_ipv4->GetInterfaceForDevice (dev), 0)
          .GetLocal ();
        NS_LOG_INFO ("Assigned iface: " << m_iface->GetAddress ());
        break;
       }
     }
   }

  NS_ABORT_MSG_IF (m_iface == 0 || !IsIfUp (), "Iface not assigned or not up");

  m_socket = Socket::CreateSocket (m_node, UdpSocketFactory::GetTypeId ());
  NS_ABORT_MSG_IF (m_socket == 0, "Socket not created");
  m_socket->SetAllowBroadcast (true); // allow broadcast
  m_socket->SetRecvCallback (MakeCallback (&Broadcast::ProcessPacket, this));
  const Ipv4Address bindAddr = Ipv4Address::GetAny ();
  const int bind = m_socket->Bind (InetSocketAddress (bindAddr, m_port));
  NS_ABORT_MSG_IF (bind == -1, "Socket not bound to addr");
  m_socket->BindToNetDevice (m_iface);
  NS_LOG_INFO ("Bind socket to " << bindAddr << ":" << m_port);
}

void
Broadcast::InsertPubByTags (Ptr<ThymeObject> obj)
{
  NS_LOG_FUNCTION (this << obj);
  ObjectIdentifier id = PEEK (obj->GetObjectIdentifier ());

  for (std::string tag : obj->GetTags ()) // index by tag
   { // for each tag in the object metadata
    auto found = m_pubsByTag.find (tag);
    if (found != m_pubsByTag.end ()) // tag exists
     {
      found->second.insert (PAIR (id, obj));
     }
    else // tag doesn't exist
     {
      MyPublications aux;
      aux.insert (PAIR (id, obj)); // insert new entry

      m_pubsByTag.insert (PAIR (tag, aux));
     }
   }
}

void
Broadcast::RemovePubByTags (Ptr<ThymeObject> obj)
{
  NS_LOG_FUNCTION (this << obj);
  Ptr<ObjectIdentifier> objId = obj->GetObjectIdentifier ();
  ObjectIdentifier id = PEEK (objId);

  for (std::string tag : obj->GetTags ()) // index by tag
   { // for each tag in the object metadata
    auto found = m_pubsByTag.find (tag);
    if (found != m_pubsByTag.end ()) // tag exists
     {
      found->second.erase (id);

      if (found->second.empty ())
       m_pubsByTag.erase (found); // erase empty tag
     }
   }
}

void
Broadcast::ProcessPacket (Ptr<Socket> socket)
{
  NS_LOG_FUNCTION (this << socket);
  Address sender;
  Ptr<Packet> packet = socket->RecvFrom (sender);
  const Ipv4Address src = InetSocketAddress::ConvertFrom (sender).GetIpv4 ();
  const uint32_t pcktSize = packet->GetSize ();

  BMessage msg;
  packet->RemoveHeader (msg);
  const MessageType type = msg.GetMsgType ();
  NS_LOG_DEBUG ("Recv msg " << Utils::GetMsgTypeString (type) << " "
    << packet->GetUid () << " from " << src);
  m_profiler->OnAppRx (m_nodeId, type, pcktSize);

  switch (type)
  {
  case DOWNLOAD:
    m_profiler->RegisterDownloadPacket (packet->GetUid ());
    ProcessDownload (msg.GetDownload (), src);
    break;
  case DOWNLOAD_RSP:
    m_profiler->RegisterDownloadRspPacket (packet->GetUid ());
    ProcessDownloadRsp (msg.GetDownloadRsp ());
    break;
  case SUBSCRIBE:
    ProcessSubscribe (msg, src, packet);
    break;
  case SUBSCRIBE_NOT:
    ProcessSubscribeNot (msg.GetSubscribeNot (), src);
    break;
  case UNSUBSCRIBE:
    ProcessUnsubscribe (msg, src, packet);
    break;
  case JOIN:
    ProcessJoin (src);
    break;
  case JOIN_RSP:
    ProcessJoinRsp (msg.GetJoinRsp ());
    break;

  default:
    NS_ABORT_MSG ("Unknown  type " << type);
  }
}

void
Broadcast::ProcessDownload (BMessage::BDownload msg, Ipv4Address src)
{
  NS_LOG_FUNCTION (this << msg << src);
  const auto found = m_myPubs.find (PEEK (msg.objId)); // check for existing key
  Ptr<ThymeObject> obj = found != m_myPubs.end () ? found->second : 0;
  MessageStatus st = obj == 0 ? OBJECT_NOT_FOUND : OBJECT_FOUND;

  BMessage rsp (m_addr);
  rsp.SetDownloadRsp (msg.reqId, st, obj);
  Unicast (rsp, src); // send DOWNLOAD_RSP to requester
}

void
Broadcast::ProcessDownloadRsp (BMessage::BDownloadRsp msg)
{
  NS_LOG_FUNCTION (this << msg);
  Ptr<DownloadRequest> req = GetDownloadRequest (msg.reqId);
  if (req != 0)
   {
    req->CancelTimeout ();

    if (msg.st == OBJECT_FOUND) // object found
     {
      if (m_passiveRep) MakePassiveReplica (msg.obj);

      NotifyDownloadSuccess (msg.obj);
      m_profiler->OnDownloadSuccess (m_addr, req->GetRequestId (), msg.obj,
        req->GetRetries ());
     }
    else // object not found
     {
      NotifyDownloadFailure (req->GetObjectIdentifier (), req->GetOwner (),
        msg.st);
      m_profiler->OnDownloadFailure (m_addr, req->GetRequestId (), msg.st);
     }

    RemoveRequest (req->GetRequestId ());
   }
  else // delayed rsp received
   m_profiler->OnDownloadRspReceived (m_addr, msg.reqId);
}

void
Broadcast::MakePassiveReplica (Ptr<ThymeObject> obj)
{
  NS_LOG_FUNCTION (this << obj);
  ObjectKey objKey = OBJ_KEY (obj->GetObjectIdentifier (), obj->GetOwner ());

  auto found = m_passReps.find (objKey);
  if (found != m_passReps.end ()) // delete possible duplicate
   found = m_passReps.erase (found); // should not happen!

  m_passReps.insert (found, PAIR (objKey, obj));
}

void
Broadcast::ProcessSubscribe (BMessage msg, Ipv4Address src, Ptr<Packet> packet)
{
  NS_LOG_FUNCTION (this << msg << src << packet);
  if (m_dpd.IsDuplicate (packet->GetUid ())) // register message as duplicate
   {
    NS_LOG_INFO ("Duplicate packet " << packet->GetUid () << " from "
      << msg.GetMsgSource () << " received from " << src << ". Drop it.");
    return;
   }

  Ptr<BSubscriptionObject> subObj = msg.GetSubscribe ().subObj;
  m_profiler->OnSubscribeReceived (m_addr, subObj->GetSubscriber (),
    subObj->GetSubscriptionId ());
  InsertSubscription (subObj);

  HandleSubscriptions (subObj); // check subscriptions

  Bcast (msg, packet);
}

void
Broadcast::ProcessSubscribeNot (BMessage::BSubscribeNot msg, Ipv4Address src)
{
  NS_LOG_FUNCTION (this << msg << src);
  const auto found = m_mySubs.find (msg.subId);
  if (found != m_mySubs.end ())
   {
    Ptr<Subscription> subObj = found->second;

    for (Ptr<ObjectMetadata> meta : msg.objMeta)
     {
      NotifySubscriptionNotification (subObj, meta);
      m_profiler->OnNotificationReceived (src, m_addr, subObj, meta,
        msg.objMeta.size ());
     }
   }
  else // subscription does not exist
   m_profiler->OnNoIdNotification (msg.objMeta.size ());
}

void
Broadcast::ProcessUnsubscribe (BMessage msg, Ipv4Address src,
  Ptr<Packet> packet)
{
  NS_LOG_FUNCTION (this << msg << src << packet);
  if (m_dpd.IsDuplicate (packet->GetUid ())) // register message as duplicate
   {
    NS_LOG_DEBUG ("Duplicate packet " << packet->GetUid () << " from "
      << msg.GetMsgSource () << " received from " << src << ". Drop it.");
    return;
   }

  const uint32_t subId = msg.GetUnsubscribe ().subId;
  m_profiler->OnUnsubscribeReceived (m_addr, msg.GetMsgSource (), subId);
  RemoveSubscription (msg.GetMsgSource (), subId);

  Bcast (msg, packet);
}

void
Broadcast::InsertSubscription (Ptr<BSubscriptionObject> subObj)
{
  NS_LOG_FUNCTION (this << subObj);
  const Ipv4Address subscriber = subObj->GetSubscriber ();
  const uint32_t subId = subObj->GetSubscriptionId ();

  const auto found = m_subscriptions.find (subscriber);
  if (found != m_subscriptions.end ()) // subscriber exists
   {
    found->second.insert (PAIR (subId, subObj));
   }
  else // subscriber doesn't exist
   {
    MySubscriptions aux;
    aux.insert (PAIR (subId, subObj)); // insert new entry

    m_subscriptions.insert (PAIR (subscriber, aux));
   }
}

void
Broadcast::RemoveSubscription (Ipv4Address subscriber, uint32_t subId)
{
  NS_LOG_FUNCTION (this << subscriber << subId);
  auto found = m_subscriptions.find (subscriber);
  if (found != m_subscriptions.end ()) // subscriber exists
   {
    found->second.erase (subId);

    if (found->second.empty ())
     m_subscriptions.erase (found); // erase empty subscriber
   }
}

void
Broadcast::ProcessJoin (Ipv4Address src)
{
  NS_LOG_FUNCTION (this << src << m_subscriptions.size ());
  BMessage msg (m_addr);
  msg.SetJoinRsp (m_subscriptions);
  Unicast (msg, src);
}

void
Broadcast::ProcessJoinRsp (BMessage::BJoinRsp msg)
{
  NS_LOG_FUNCTION (this << msg << msg.subs.size ());
  if (!m_joined)
   {
    m_join.Cancel ();
    m_joined = true;
   }

  std::vector<Ptr<Subscription> > diff; // get subscriptions diff
  for (auto it : msg.subs) // for each subscriber
   {
    NS_LOG_LOGIC ("Msg subscriber " << it.first << " with "
      << it.second.size () << " subs");
    auto found = m_subscriptions.find (it.first);
    if (found != m_subscriptions.end ())
     { // subscriber exists. check subscriptions
      NS_LOG_LOGIC ("Existent subscriber with " << found->second.size ()
        << " subscriptions");
      for (auto sub : it.second)
       { // for each subscription of msg subscriber
        auto found2 = found->second.find (sub.first);
        if (found2 == found->second.end ()) // new subscription
         {
          diff.push_back (sub.second);
          found->second.insert (PAIR (sub.first, sub.second));
         }
       }
     }
    else // all subscriptions from this subscriber are new
     {
      NS_LOG_LOGIC ("New subscriber " << it.first << " with "
        << it.second.size () << " subscriptions");
      m_subscriptions.insert (PAIR (it.first, it.second));

      for (auto it2 : it.second)
       diff.push_back (it2.second);
     }
   }
  NS_LOG_DEBUG (diff.size () << " new subscriptions");

  for (Ptr<Subscription> subObj : diff) // send delayed notifications
   {
    Ptr<BSubscriptionObject> so =
      dynamic_cast<BSubscriptionObject*> (PeekPointer (subObj));
    std::vector<Ptr<ObjectMetadata> > metas = CheckAllPublications (so);

    if (!metas.empty ())
     {
      NS_LOG_DEBUG ("Send " << metas.size () << " metas to "
        << so->GetSubscriber ());
      SendNotifications (so, metas);
     }
   }
}

void
Broadcast::Bcast (BMessage msg, Ptr<Packet> packet)
{ // broadcast
  NS_LOG_FUNCTION (this << &msg << packet);
  packet->RemoveAllPacketTags (); // to prevent errors
  packet->RemoveAllByteTags ();
  packet->RemoveAtStart (packet->GetSize ());

  packet->AddHeader (msg);
  const int sent = m_socket->SendTo (packet, 0,
    InetSocketAddress (Ipv4Address::GetBroadcast (), m_port));
  if (sent == -1)
   {
    NS_LOG_WARN ("Error " << m_socket->GetErrno () << " while bcast "
      << Utils::GetMsgTypeString (msg.GetMsgType ()) << " "
      << packet->GetUid () << " " << packet->GetSize ());
    return;
   }

  NS_LOG_DEBUG ("Bcast " << Utils::GetMsgTypeString (msg.GetMsgType ())
    << " " << packet->GetUid () << " " << packet->GetSize ());
  m_profiler->OnAppBcast (m_nodeId, msg.GetMsgType (), packet->GetSize ());
}

void
Broadcast::Unicast (BMessage msg, Ipv4Address dst)
{ // direct send
  NS_LOG_FUNCTION (this << &msg << dst);
  Ptr<Packet> packet = Create<Packet> ();

  packet->AddHeader (msg);
  const int sent = m_socket->SendTo (packet, 0, InetSocketAddress (dst, m_port));
  if (sent == -1)
   {
    NS_LOG_WARN ("Error " << m_socket->GetErrno () << " while send "
      << Utils::GetMsgTypeString (msg.GetMsgType ()) << " "
      << packet->GetUid () << " " << packet->GetSize () << " to " << dst);
    return;
   }

  NS_LOG_DEBUG ("Unicast " << Utils::GetMsgTypeString (msg.GetMsgType ())
    << " " << packet->GetUid () << " " << packet->GetSize () << " to " << dst);
  m_profiler->OnAppTx (m_nodeId, msg.GetMsgType (), packet->GetSize ());
}

void
Broadcast::HandleSubscriptions (Ptr<ObjectMetadata> objMeta)
{
  NS_LOG_FUNCTION (this << objMeta);
  std::vector<Ptr<Subscription> > subs = CheckAllSubscriptions (objMeta);
  if (!subs.empty ()) SendNotifications (subs, objMeta);
}

std::vector<Ptr<Subscription> > // send one metadata to multiple subscribers
Broadcast::CheckAllSubscriptions (Ptr<ObjectMetadata> objMeta)
{ // published object. check subscriptions that match publication
  NS_LOG_FUNCTION (this << objMeta);
  std::vector<Ptr<Subscription> > res;

  // pass through every subscription and check tags and time
  for (auto subs = m_subscriptions.begin (); subs != m_subscriptions.end ();)
   { // iterate over subscribers
    if (subs->first == m_addr) // ignore my own subscriptions
     {
      ++subs; // do not notify me about my own publications
      continue; // I know!
     }

    for (auto nSubs = subs->second.begin (); nSubs != subs->second.end ();)
     { // iterate over subscriptions of each subscriber
      if (nSubs->second->IsExpired ()) // if expired, erase subscription
       {
        nSubs = subs->second.erase (nSubs);
       }
      else // if subscription is valid, check match with publication
       {
        if (nSubs->second->IsMatch (objMeta)) res.push_back (nSubs->second);

        ++nSubs;
       }
     }

    // erase empty subscriber
    subs = subs->second.empty () ? m_subscriptions.erase (subs) : ++subs;
   }

  return res;
}

void
Broadcast::SendNotifications (std::vector<Ptr<Subscription> > subs,
  Ptr<ObjectMetadata> objMeta)
{
  NS_LOG_FUNCTION (this << subs.size () << objMeta);
  std::vector<Ptr<ObjectMetadata> > aux;
  aux.push_back (objMeta);

  for (Ptr<Subscription> sub : subs) // for each subscription...
   {
    m_profiler->OnNotificationSent (m_addr, sub->GetSubscriber (),
      sub->GetSubscriptionId (), objMeta);

    BMessage msg (m_addr);
    msg.SetSubscribeNot (sub->GetSubscriptionId (), aux);
    Unicast (msg, sub->GetSubscriber ());
   }
}

void
Broadcast::HandleSubscriptions (Ptr<BSubscriptionObject> subObj)
{
  NS_LOG_FUNCTION (this << subObj);
  std::vector<Ptr<ObjectMetadata> > metas = CheckAllPublications (subObj);
  if (!metas.empty ()) SendNotifications (subObj, metas);
}

std::vector<Ptr<ObjectMetadata> > // send multiple metadata to one subscriber
Broadcast::CheckAllPublications (Ptr<BSubscriptionObject> subObj)
{ // new subscription. check past publications that match subscription
  NS_LOG_FUNCTION (this << subObj);
  std::vector<Ptr<ObjectMetadata> > objsMeta;

  if (!subObj->IsStarted ()) return objsMeta; // if not yet active...

  std::set<ObjectKey> keys;
  for (Conjunction conj : subObj->GetFilter ()) // for each conjunction...
   {
    bool posTag = false;

    for (Tag t : conj) // for each literal...
     {
      if (!t.second) // if tag is positive...
       {
        posTag = true;
        const auto pubs = m_pubsByTag.find (t.first);

        if (pubs != m_pubsByTag.end ()) // if it is in the tags list...
         {
          for (auto pub : pubs->second)
           { // for each object published under that tag...
            Ptr<ObjectMetadata> meta = pub.second->GetObjectMetadata ();

            if (subObj->IsMatch (meta)) // check if it matches the subscription
             {
              if (keys.insert (OBJ_KEY (meta->GetObjectIdentifier (),
                  meta->GetOwner ())).second) // to avoid duplicates
               objsMeta.push_back (meta);
             }
           }
         }
       }

      if (posTag) break; // go to next conjunction
     }
   }

  return objsMeta;
}

void
Broadcast::SendNotifications (Ptr<BSubscriptionObject> sub,
  std::vector<Ptr<ObjectMetadata> > objsMeta)
{
  NS_LOG_FUNCTION (this << sub << objsMeta.size ());
  for (Ptr<ObjectMetadata> meta : objsMeta)
   m_profiler->OnNotificationSent (m_addr, sub->GetSubscriber (),
    sub->GetSubscriptionId (), meta);

  BMessage msg (m_addr);
  msg.SetSubscribeNot (sub->GetSubscriptionId (), objsMeta);
  Unicast (msg, sub->GetSubscriber ());
}

void
Broadcast::HandlePublishRequestTimeout (Ptr<PublishRequest> req)
{
  NS_ABORT_MSG ("Not implemented!");
}

void
Broadcast::HandleUnpublishRequestTimeout (Ptr<UnpublishRequest> req)
{
  NS_ABORT_MSG ("Not implemented!");
}

void
Broadcast::HandleDownloadRequestTimeout (Ptr<DownloadRequest> req)
{
  NS_LOG_FUNCTION (this << req->GetRequestId ()
    << (uint16_t) req->GetRetries ());
  if (!req->HasReachedMaxRetries ()) // if I have more retries to try
   {
    req->IncrementRetry ();

    BMessage msg (m_addr);
    msg.SetDownload (req->GetRequestId (), req->GetObjectIdentifier ());
    Unicast (msg, req->GetOwner ()); // send DOWNLOAD to owner

    StartDownloadRequestTimeout (req, m_downReqTimeout);
   }
  else // notify failure
   {
    Ptr<ObjectIdentifier> objId = req->GetObjectIdentifier ();
    const Ipv4Address owner = req->GetOwner ();
    const uint32_t reqId = req->GetRequestId ();
    RemoveRequest (req->GetRequestId ());

    NotifyDownloadFailure (objId, owner, TIMEOUT);
    m_profiler->OnDownloadFailure (m_addr, reqId, TIMEOUT);
   }
}

void
Broadcast::HandleSubscribeRequestTimeout (Ptr<SubscribeRequest> req)
{
  NS_ABORT_MSG ("Not implemented!");
}

void
Broadcast::HandleUnsubscribeRequestTimeout (Ptr<UnsubscribeRequest> req)
{
  NS_ABORT_MSG ("Not implemented!");
}

void
Broadcast::Join ()
{
  NS_LOG_FUNCTION (this);
  if (m_joinRetries < m_joinMaxRetries)
   {
    NS_LOG_DEBUG ("Joining the network. retry: " << m_joinRetries);
    BMessage msg (m_addr);
    msg.SetJoin ();
    Bcast (msg, Create<Packet> ());

    m_join = Simulator::Schedule (m_joinTimeout, &Broadcast::Join, this);
    ++m_joinRetries;
   }
  else if (!m_joined) // assume I'm alone...
   {
    NS_LOG_DEBUG ("Unable to join the network through other node...");
    m_joined = true;
   }
}

void
Broadcast::IfDown ()
{
  NS_LOG_FUNCTION (this);
  m_ipv4->SetDown (m_ipv4->GetInterfaceForDevice (m_iface));
  m_joined = false;
  m_joinRetries = 0;
  m_join.Cancel ();
  RemoveAllRequests ();
}

void
Broadcast::IfUp ()
{
  NS_LOG_FUNCTION (this);
  m_ipv4->SetUp (m_ipv4->GetInterfaceForDevice (m_iface));
  if (!m_joined) Join ();
}

bool
Broadcast::IsIfUp ()
{
  NS_LOG_FUNCTION (this);
  return m_ipv4->IsUp (m_ipv4->GetInterfaceForDevice (m_iface));
}

}
}

