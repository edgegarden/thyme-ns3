/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2015 NOVA LINCS DI-FCT-UNL <http://nova-lincs.di.fct.unl.pt/>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Joao A. Silva <jaa.silva@campus.fct.unl.pt>
 */
#include "broadcast-message.h"
#include <ns3/abort.h>
#include <ns3/address-utils.h>
#include <ns3/thyme-utils.h>

namespace ns3 {
namespace hyrax {

NS_OBJECT_ENSURE_REGISTERED (BMessage);

TypeId
BMessage::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::hyrax::BMessage")
    .SetParent<Header> ()
    .AddConstructor<BMessage> ();

  return tid;
}

TypeId
BMessage::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}

BMessage::BMessage () : m_src (), m_type ((MessageType) 0)
{
}

BMessage::BMessage (Ipv4Address src) : m_src (src), m_type ((MessageType) 0)
{
}

Ipv4Address
BMessage::GetMsgSource () const
{
  return m_src;
}

MessageType
BMessage::GetMsgType () const
{
  return m_type;
}

void
BMessage::Serialize (Buffer::Iterator start) const
{
  const Buffer::Iterator i = start;
  WriteTo (start, m_src);

  start.WriteU8 ((uint8_t) m_type);

  switch (m_type)
  {
  case DOWNLOAD:
    m_message.down.Serialize (start);
    break;
  case DOWNLOAD_RSP:
    m_message.downRsp.Serialize (start);
    break;
  case SUBSCRIBE:
    m_message.sub.Serialize (start);
    break;
  case SUBSCRIBE_NOT:
    m_message.subNot.Serialize (start);
    break;
  case UNSUBSCRIBE:
    m_message.unsub.Serialize (start);
    break;
  case JOIN:
    break;
  case JOIN_RSP:
    m_message.joinRsp.Serialize (start);
    break;

  default:
    NS_ABORT_MSG ("Unknown type " << m_type);
  }

  uint32_t dist = 0;
  uint32_t serSize = 0;
  NS_ASSERT_MSG ((dist = start.GetDistanceFrom (i))
    == (serSize = GetSerializedSize ()), dist << " != " << serSize);
}

uint32_t
BMessage::Deserialize (Buffer::Iterator start)
{
  const Buffer::Iterator i = start;
  ReadFrom (start, m_src);

  m_type = (MessageType) start.ReadU8 ();

  switch (m_type)
  {
  case DOWNLOAD:
    m_message.down.Deserialize (start);
    break;
  case DOWNLOAD_RSP:
    m_message.downRsp.Deserialize (start);
    break;
  case SUBSCRIBE:
    m_message.sub.Deserialize (start);
    break;
  case SUBSCRIBE_NOT:
    m_message.subNot.Deserialize (start);
    break;
  case UNSUBSCRIBE:
    m_message.unsub.Deserialize (start);
    break;
  case JOIN:
    break;
  case JOIN_RSP:
    m_message.joinRsp.Deserialize (start);
    break;

  default:
    NS_ABORT_MSG ("Unknown type " << m_type);
  }

  const uint32_t dist = start.GetDistanceFrom (i);
  uint32_t serSize = 0;
  NS_ASSERT_MSG (dist == (serSize = GetSerializedSize ()),
    dist << " != " << serSize);
  return dist;
}

uint32_t
BMessage::GetSerializedSize (void) const
{
  uint32_t size = 5; // src ip and msgType

  switch (m_type)
  {
  case DOWNLOAD:
    size += m_message.down.GetSerializedSize ();
    break;
  case DOWNLOAD_RSP:
    size += m_message.downRsp.GetSerializedSize ();
    break;
  case SUBSCRIBE:
    size += m_message.sub.GetSerializedSize ();
    break;
  case SUBSCRIBE_NOT:
    size += m_message.subNot.GetSerializedSize ();
    break;
  case UNSUBSCRIBE:
    size += m_message.unsub.GetSerializedSize ();
    break;
  case JOIN:
    break;
  case JOIN_RSP:
    size += m_message.joinRsp.GetSerializedSize ();
    break;

  default:
    NS_ABORT_MSG ("Unknown type " << m_type);
  }

  return size;
}

void
BMessage::Print (std::ostream &os) const
{
  os << "\n:::Header:::\nMsgType: " << Utils::GetMsgTypeString (m_type)
    << " (" << m_type << ")\nSrc: " << m_src << "\n:::Payload:::\n";
  switch (m_type)
  {
  case DOWNLOAD:
    m_message.down.Print (os);
    break;
  case DOWNLOAD_RSP:
    m_message.downRsp.Print (os);
    break;
  case SUBSCRIBE:
    m_message.sub.Print (os);
    break;
  case SUBSCRIBE_NOT:
    m_message.subNot.Print (os);
    break;
  case UNSUBSCRIBE:
    m_message.unsub.Print (os);
    break;
  case JOIN:
    os << "::JoinReq::";
    break;
  case JOIN_RSP:
    m_message.joinRsp.Print (os);
    break;

  default:
    NS_ABORT_MSG ("Unknown type " << m_type);
  }
}

// Download -------------------------------------------------------------------

void
BMessage::BDownload::Serialize (Buffer::Iterator &start) const
{
  start.WriteHtonU32 (reqId);

  objId->Serialize (start);
}

uint32_t
BMessage::BDownload::Deserialize (Buffer::Iterator &start)
{
  reqId = start.ReadNtohU32 ();

  objId = Create<ObjectIdentifier> ();
  objId->Deserialize (start);

  return GetSerializedSize ();
}

uint32_t
BMessage::BDownload::GetSerializedSize () const
{
  return 4 + objId->GetSerializedSize (); // reqId and objId
}

void
BMessage::BDownload::Print (std::ostream &os) const
{
  os << "::Down:: ReqId: " << reqId << " ";
  objId->Print (os);
}

BMessage::BDownload
BMessage::GetDownload () const
{
  return m_message.down;
}

void
BMessage::SetDownload (uint32_t rId, Ptr<ObjectIdentifier> oId)
{
  if (m_type == 0) m_type = DOWNLOAD;
  else NS_ABORT_MSG_IF (m_type != DOWNLOAD, m_type);

  m_message.down.reqId = rId;
  m_message.down.objId = oId;
}

std::ostream&
operator<< (std::ostream &os, const BMessage::BDownload &x)
{
  x.Print (os);
  return os;
}

// DownloadRsp ----------------------------------------------------------------

void
BMessage::BDownloadRsp::Serialize (Buffer::Iterator &start) const
{
  start.WriteHtonU32 (reqId);

  start.WriteU8 ((uint8_t) st);
  if (st == OBJECT_FOUND) obj->Serialize (start);
}

uint32_t
BMessage::BDownloadRsp::Deserialize (Buffer::Iterator &start)
{
  reqId = start.ReadNtohU32 ();

  st = (MessageStatus) start.ReadU8 ();
  if (st == OBJECT_FOUND)
   {
    obj = Create<ThymeObject> ();
    obj->Deserialize (start);
   }

  return GetSerializedSize ();
}

uint32_t
BMessage::BDownloadRsp::GetSerializedSize () const
{
  // reqId, status and obj
  return 5 + (st == OBJECT_FOUND ? obj->GetSerializedSize () : 0);
}

void
BMessage::BDownloadRsp::Print (std::ostream &os) const
{
  os << "::DownRsp:: ReqId: " << reqId
    << " St: " << Utils::GetMsgStatusString (st);
  if (st == OBJECT_FOUND)
   {
    os << " ";
    obj->Print (os);
   }
}

BMessage::BDownloadRsp
BMessage::GetDownloadRsp () const
{
  return m_message.downRsp;
}

void
BMessage::SetDownloadRsp (uint32_t rId, MessageStatus s, Ptr<ThymeObject> o)
{
  if (m_type == 0) m_type = DOWNLOAD_RSP;
  else NS_ABORT_MSG_IF (m_type != DOWNLOAD_RSP, m_type);

  m_message.downRsp.reqId = rId;
  m_message.downRsp.st = s;
  m_message.downRsp.obj = o;
}

std::ostream&
operator<< (std::ostream &os, const BMessage::BDownloadRsp &x)
{
  x.Print (os);
  return os;
}

// Subscribe ------------------------------------------------------------------

void
BMessage::BSubscribe::Serialize (Buffer::Iterator &start) const
{
  subObj->Serialize (start);
}

uint32_t
BMessage::BSubscribe::Deserialize (Buffer::Iterator &start)
{
  subObj = Create<BSubscriptionObject> ();
  subObj->Deserialize (start);

  return GetSerializedSize ();
}

uint32_t
BMessage::BSubscribe::GetSerializedSize () const
{
  return subObj->GetSerializedSize (); // subObj
}

void
BMessage::BSubscribe::Print (std::ostream &os) const
{
  os << "::Sub:: ";
  subObj->Print (os);
}

BMessage::BSubscribe
BMessage::GetSubscribe () const
{
  return m_message.sub;
}

void
BMessage::SetSubscribe (Ptr<BSubscriptionObject> sObj)
{
  if (m_type == 0) m_type = SUBSCRIBE;
  else NS_ABORT_MSG_IF (m_type != SUBSCRIBE, m_type);

  m_message.sub.subObj = sObj;
}

std::ostream&
operator<< (std::ostream &os, const BMessage::BSubscribe &x)
{
  x.Print (os);
  return os;
}

// SubscribeNot ---------------------------------------------------------------

void
BMessage::BSubscribeNot::Serialize (Buffer::Iterator &start) const
{
  start.WriteHtonU32 (subId);

  start.WriteHtonU32 (objMeta.size ());
  for (Ptr<ObjectMetadata> meta : objMeta)
   meta->Serialize (start);
}

uint32_t
BMessage::BSubscribeNot::Deserialize (Buffer::Iterator &start)
{
  subId = start.ReadNtohU32 ();

  const int size = start.ReadNtohU32 ();
  for (int i = 0; i < size; ++i)
   {
    Ptr<ObjectMetadata> meta = Create<ObjectMetadata> ();
    meta->Deserialize (start);

    objMeta.push_back (meta);
   }

  return GetSerializedSize ();
}

uint32_t
BMessage::BSubscribeNot::GetSerializedSize () const
{
  uint32_t size = 8; // subId and list size

  for (Ptr<ObjectMetadata> meta : objMeta)
   size += meta->GetSerializedSize (); // meta

  return size;
}

void
BMessage::BSubscribeNot::Print (std::ostream &os) const
{
  os << "::SubNot:: SubId: " << subId << " #Metas: " << objMeta.size ();
}

BMessage::BSubscribeNot
BMessage::GetSubscribeNot () const
{
  return m_message.subNot;
}

void
BMessage::SetSubscribeNot (uint32_t sId, std::vector<Ptr<ObjectMetadata> > m)
{
  if (m_type == 0) m_type = SUBSCRIBE_NOT;
  else NS_ABORT_MSG_IF (m_type != SUBSCRIBE_NOT, m_type);

  m_message.subNot.subId = sId;
  m_message.subNot.objMeta = m;
}

std::ostream&
operator<< (std::ostream &os, const BMessage::BSubscribeNot &x)
{
  x.Print (os);
  return os;
}

// Unsubscribe ----------------------------------------------------------------

void
BMessage::BUnsubscribe::Serialize (Buffer::Iterator &start) const
{
  start.WriteHtonU32 (subId);
}

uint32_t
BMessage::BUnsubscribe::Deserialize (Buffer::Iterator &start)
{
  subId = start.ReadNtohU32 ();

  return GetSerializedSize ();
}

uint32_t
BMessage::BUnsubscribe::GetSerializedSize () const
{
  return 4; // subId
}

void
BMessage::BUnsubscribe::Print (std::ostream &os) const
{
  os << "::Unsub:: SubId: " << subId;
}

BMessage::BUnsubscribe
BMessage::GetUnsubscribe () const
{
  return m_message.unsub;
}

void
BMessage::SetUnsubscribe (uint32_t sId)
{
  if (m_type == 0) m_type = UNSUBSCRIBE;
  else NS_ABORT_MSG_IF (m_type != UNSUBSCRIBE, m_type);

  m_message.unsub.subId = sId;
}

std::ostream&
operator<< (std::ostream &os, const BMessage::BUnsubscribe &x)
{
  x.Print (os);
  return os;
}

// Join -----------------------------------------------------------------------

void
BMessage::SetJoin ()
{
  if (m_type == 0) m_type = JOIN;
  else NS_ABORT_MSG_IF (m_type != JOIN, m_type);
}

// JoinRsp --------------------------------------------------------------------

void
BMessage::BJoinRsp::Serialize (Buffer::Iterator &start) const
{
  start.WriteHtonU32 (subs.size ()); // list size
  for (auto it : subs)
   {
    start.WriteHtonU32 (it.first.Get ()); // subscriber

    start.WriteHtonU32 (it.second.size ()); // list size
    for (auto it2 : it.second)
     {
      start.WriteHtonU32 (it2.first); // subId
      it2.second->Serialize (start); // subObj
     }
   }
}

uint32_t
BMessage::BJoinRsp::Deserialize (Buffer::Iterator &start)
{
  const int size = start.ReadNtohU32 (); // list size
  for (int i = 0; i < size; ++i)
   {
    const uint32_t ip = start.ReadNtohU32 (); // subscriber
    const Ipv4Address addr = Ipv4Address (ip);

    const int size2 = start.ReadNtohU32 (); // list size
    MySubscriptions aux;
    for (int j = 0; j < size2; ++j)
     {
      const uint32_t id = start.ReadNtohU32 (); // subId

      Ptr<BSubscriptionObject> obj = Create<BSubscriptionObject> ();
      obj->Deserialize (start); // subObj

      aux.insert (PAIR (id, obj));
     }

    subs.insert (PAIR (addr, aux));
   }

  return GetSerializedSize ();
}

uint32_t
BMessage::BJoinRsp::GetSerializedSize () const
{
  uint32_t size = 4; // list size
  for (auto it : subs)
   {
    size += 8; // ip and list size

    for (auto it2 : it.second)
     size += 4 + it2.second->GetSerializedSize (); // subId and subObj
   }

  return size;
}

void
BMessage::BJoinRsp::Print (std::ostream &os) const
{
  os << "::JoinRsp:: #Subs: " << subs.size ();
}

BMessage::BJoinRsp
BMessage::GetJoinRsp () const
{
  return m_message.joinRsp;
}

void
BMessage::SetJoinRsp (Subscriptions s)
{
  if (m_type == 0) m_type = JOIN_RSP;
  else NS_ABORT_MSG_IF (m_type != JOIN_RSP, m_type);

  m_message.joinRsp.subs = s;
}

std::ostream&
operator<< (std::ostream &os, const BMessage::BJoinRsp &x)
{
  x.Print (os);
  return os;
}

}
}

