/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2015 NOVA LINCS DI-FCT-UNL <http://nova-lincs.di.fct.unl.pt/>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Joao A. Silva <jaa.silva@campus.fct.unl.pt>
 */
#include <ns3/thyme-experiment.h>

using namespace ns3;

int
main (int argc, char *argv[])
{
  ThymeExperiment::EnableLogs (false, true, false);

  uint32_t numNodes = NUM_NODES;
  double simTime = SIM_TIME;
  double stopAppsTime = STOP_APPS;
  std::string traceFile = TRACE_FILE;
  uint32_t rp = ROUTING_PROTOCOL;

  CommandLine cmd;
  cmd.AddValue ("numNodes", "Number of nodes.", numNodes);
  cmd.AddValue ("simTime", "Total simulation time (s).", simTime);
  cmd.AddValue ("stopTime", "Time for stopping apps (s).", stopAppsTime);
  cmd.AddValue ("file", "File with traces to run.", traceFile);
  cmd.AddValue ("rp", "Routing protocol (0-AODV, 1-OLSR, 2-DSDV, 3-DSR, 4-BATMAN).", rp);
  cmd.Parse (argc, argv);

  std::cout << ">>> Creating " << numNodes << " nodes...\n";
  NodeContainer nodes = ThymeExperiment::CreateNodes (numNodes);
  ThymeExperiment::SetupFileMobilityModel (traceFile);
  NetDeviceContainer devices = ThymeExperiment::CreateDevices (nodes);

  switch (rp)
  {
  case 0:
    std::cout << ">>> Installing AODV...\n";
    ThymeExperiment::InstallAodvRouting (nodes, devices);
    break;
  case 1:
    std::cout << ">>> Installing OLSR...\n";
    ThymeExperiment::InstallOlsrRouting (nodes, devices);
    break;
  case 2:
    std::cout << ">>> Installing DSDV...\n";
    ThymeExperiment::InstallDsdvRouting (nodes, devices);
    break;
  case 3:
    std::cout << ">>> Installing DSR...\n";
    ThymeExperiment::InstallDsrRouting (nodes, devices);
    break;
  case 4:
    std::cout << ">>> Installing BATMAN...\n";
    ThymeExperiment::InstallBatmanRouting (nodes, devices);
    break;
  default:
    std::cout << ">>> ERROR: Unknown routing protocol " << rp << "\n";
    return -1;
  }
  Ptr<hyrax::Profiler> profiler = ThymeExperiment::ScheduleTasks (traceFile,
    nodes, stopAppsTime, true);

  std::cout << ">>> Starting simulation for " << simTime << "s...\n";
  Simulator::Stop (Seconds (simTime));
  Simulator::Run ();

  profiler->PrintReport ();
  Simulator::Destroy ();

  return 0;
}

